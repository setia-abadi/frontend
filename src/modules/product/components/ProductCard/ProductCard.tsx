import { Box, Stack, SxProps, Theme, styled } from '@mui/material'
import Button from 'components/Button'
import Chip from 'components/Chip'
import FormattedCurrency from 'components/FormattedCurrency'
import Image from 'components/Image'
import Paper from 'components/Paper'
import Typography from 'components/Typography'
import { FC } from 'react'
import TypeUtil from 'utils/typeUtil'

type Props = {
  imageSrc: string
  name: string
  price: number
  addedCartCount: number
  stock: number
  onClick: () => void
}

const Badge = styled('div')(({ theme }) => ({
  position: 'absolute',
  top: theme.spacing(1),
  right: theme.spacing(1),
  borderRadius: '50px',
  backgroundColor: theme.palette.primary.main,
  color: theme.palette.primary.contrastText,
  width: 30,
  height: 30,
  fontWeight: 500,
  fontSize: theme.typography.pxToRem(15),
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}))

const ProductCard: FC<Props> = ({ imageSrc, name, price, addedCartCount, stock, onClick }) => {
  const hasBadge = !TypeUtil.isEmpty(addedCartCount)
  const hasStock = addedCartCount < stock

  const paperSx: SxProps<Theme> = (theme) => ({
    height: '100%',
    border: hasBadge ? `2px solid ${theme.palette.primary.main}` : 'none',
    position: 'relative',
  })

  const imageSx: SxProps<Theme> = {
    width: '100%',
    border: 0,
    borderEndEndRadius: 0,
    borderEndStartRadius: 0,
  }

  return (
    <Paper sx={paperSx}>
      <Image src={imageSrc} aspectRatio={1} sx={imageSx} />

      <Box p={2}>
        <Stack spacing={1}>
          <Typography variant="body1" fontWeight={600} maxLine={2} title={name}>
            {name}
          </Typography>
          <Typography variant="body1">
            <FormattedCurrency value={price} />
          </Typography>
          <Box>
            <Chip size="small" color="primary" variant="outlined" label={`Stok: ${stock - addedCartCount}`} />
          </Box>
        </Stack>

        <Box mt={2}>
          <Button fullWidth variant="contained" disabled={!hasStock} onClick={onClick}>
            {hasStock ? 'Tambahkan' : 'Stock Habis'}
          </Button>
        </Box>
      </Box>

      {hasBadge && <Badge>{addedCartCount}</Badge>}
    </Paper>
  )
}

export default ProductCard
