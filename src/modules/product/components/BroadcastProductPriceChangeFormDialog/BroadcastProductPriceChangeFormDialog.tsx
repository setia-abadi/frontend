import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { SelectOption } from 'components/Select'
import { DisabledFields, FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'
import NumberInputField from 'components/NumberInputField'
import CustomerTypeForWhatsappProductPriceChangeBroadcastFormSelect from 'modules/customer-type/components/CustomerTypeForWhatsappProductPriceChangeBroadcastFormSelect'
import NumberInput from 'components/NumberInput'
import TextInput from 'components/TextInput'
import { CURRENCY } from 'constants/currency'

type BroadcastProductPriceChangeFormDialogShape = FormShape<{
  customer_type_id: SelectOption
  old_price: number
}>

type Props = {
  open: boolean
  dialogTitle: string
  newPrice: number
  productName: string
  disabledFields?: Partial<DisabledFields<BroadcastProductPriceChangeFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: BroadcastProductPriceChangeFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: BroadcastProductPriceChangeFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const BroadcastProductPriceChangeFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  newPrice,
  productName,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: BroadcastProductPriceChangeFormDialogShape,
    helpers: FormikHelpers<BroadcastProductPriceChangeFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<BroadcastProductPriceChangeFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel required>Produk</FormLabel>
                  <TextInput disabled fullWidth value={productName} />
                </Grid>

                <Grid item xs={12}>
                  <PermissionControl
                    renderError
                    match={[PermissionEnum.CUSTOMER_TYPE_OPTION_FOR_WHATSAPP_PRODUCT_PRICE_CHANGE_BROADCAST_FORM]}
                  >
                    <FormLabel required>Jenis Pelanggan</FormLabel>
                    <CustomerTypeForWhatsappProductPriceChangeBroadcastFormSelect
                      name="customer_type_id"
                      disabled={disabledFields?.customer_type_id}
                    />
                    <FormErrorText message={formik.errors.customer_type_id} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Harga Lama</FormLabel>
                  <NumberInputField
                    fullWidth
                    name="old_price"
                    disabled={disabledFields?.old_price}
                    InputProps={{ startAdornment: CURRENCY }}
                  />
                  <FormErrorText message={formik.errors?.old_price} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Harga Baru</FormLabel>
                  <NumberInput
                    disabled
                    fullWidth
                    value={newPrice}
                    InputProps={{ startAdornment: CURRENCY }}
                    onChange={() => {}}
                  />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { BroadcastProductPriceChangeFormDialogShape }

export default BroadcastProductPriceChangeFormDialog
