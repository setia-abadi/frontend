import Image, { ImageProps } from 'components/Image'
import { FC } from 'react'

type Props = ImageProps

const ProductImage: FC<Props> = (props) => {
  return (
    <Image {...props} aspectRatio={1/1} />
  )
}

export default ProductImage
