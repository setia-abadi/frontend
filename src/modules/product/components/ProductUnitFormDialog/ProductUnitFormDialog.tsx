import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape, ID } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import { SelectOption } from 'components/Select'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import UnitDestinationSelectFieldForProductUnitForm from 'modules/unit/components/UnitDestinationSelectFieldForProductUnitForm'
import UnitBaseSelectFieldForProductUnitForm from 'modules/unit/components/UnitBaseSelectFieldForProductUnitForm'

type ProductUnitFormDialogShape = FormShape<{
  base_scale: number
  scale: number
  unit_id: SelectOption
  to_unit_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  productId: ID
  disabledFields?: Partial<DisabledFields<ProductUnitFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ProductUnitFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductUnitFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductUnitFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  productId,
  dialogSubTitle,
  disabledFields,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductUnitFormDialogShape,
    helpers: FormikHelpers<ProductUnitFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductUnitFormDialogShape> initialValues={{ base_scale: 1, ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel>Skala</FormLabel>
                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <NumberInputField name="base_scale" disabled />
                    </Grid>
                    <Grid item xs={8}>
                      <UnitBaseSelectFieldForProductUnitForm
                        productId={productId}
                        name="unit_id"
                        disabled={disabledFields?.unit_id}
                      />
                    </Grid>
                  </Grid>
                  <FormErrorText message={formik.errors.unit_id} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Sama Dengan</FormLabel>
                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <NumberInputField name="scale" value={1} disabled={disabledFields?.scale} />
                    </Grid>
                    <Grid item xs={8}>
                      <UnitDestinationSelectFieldForProductUnitForm
                        productId={productId}
                        name="to_unit_id"
                        disabled={disabledFields?.to_unit_id}
                      />
                    </Grid>
                  </Grid>
                  <FormErrorText message={formik.errors.scale || formik.errors.to_unit_id} />
                </Grid>
              </Grid>

              <Box mt={3}></Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductUnitFormDialogShape }

export default ProductUnitFormDialog
