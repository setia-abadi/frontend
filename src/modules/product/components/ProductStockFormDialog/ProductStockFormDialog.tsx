import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import NumberInputField from 'components/NumberInputField'
import FormLabel from 'components/FormLabel'
import { CURRENCY } from 'constants/currency'

type ProductStockFormDialogShape = FormShape<{
  cost_price: number
  qty: number
}>

type Props = {
  open: boolean
  dialogTitle: string
  dialogSubTitle?: string
  initialValues?: ProductStockFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductStockFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductStockFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductStockFormDialogShape,
    helpers: FormikHelpers<ProductStockFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductStockFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Box mt={3}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <FormLabel required>Kuantitas</FormLabel>
                    <NumberInputField name="qty" fullWidth />
                    <FormErrorText message={formik.errors.qty} />
                  </Grid>

                  <Grid item xs={12}>
                    <FormLabel>Harga Dasar</FormLabel>
                    <NumberInputField name="cost_price" InputProps={{ startAdornment: CURRENCY }} fullWidth />
                    <FormErrorText message={formik.errors.cost_price} />
                  </Grid>
                </Grid>
              </Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductStockFormDialogShape }

export default ProductStockFormDialog
