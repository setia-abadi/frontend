import ProductsAPI from 'apis/productsAPI'
import AsyncSelectField, { AsyncSelectFieldProps } from 'components/AsyncSelectField'
import { SelectOption } from 'components/Select'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { ID } from 'models/base'

type Props<Multiple extends boolean> = Omit<AsyncSelectFieldProps<Multiple>, 'fetchOptions'> & {
  productReceiveId: ID
}

const ProductForProductReceiveItemFormSelect = <Multiple extends boolean = false>({
  productReceiveId,
  ...props
}: Props<Multiple>) => {
  const fetchOptions: AsyncSelectFieldProps<Multiple>['fetchOptions'] = async ({ page, phrase, signal }) => {
    const response = await ProductsAPI.optionsForProductReceiveItemForm(
      {
        page,
        phrase,
        limit: LIMIT_PER_PAGE,
        product_receive_id: productReceiveId,
      },
      { signal }
    )

    if (response.data.data) {
      return {
        ...response.data.data,
        nodes: response.data.data.nodes.map<SelectOption>((node) => ({
          label: node.name,
          value: node.id,
        })),
      }
    }

    return undefined
  }

  return <AsyncSelectField {...props} fetchOptions={fetchOptions} />
}

export default ProductForProductReceiveItemFormSelect
