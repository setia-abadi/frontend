import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { ProductModel } from 'models/product'
import FormattedCurrency from 'components/FormattedCurrency'
import StatusChip from 'components/StatusChip'
import { Box, IconButton, Stack, Tooltip } from '@mui/material'
import Image from 'components/Image'
import FormattedNumber from 'components/FormattedNumber'
import { IconAlertCircle } from '@tabler/icons'

type Model = ProductModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama',
    minWidth: 200,
    width: 300,
    enableSort: true,
    render: (node) => (
      <Stack direction="row" alignItems="center" spacing={2}>
        {node.image_file && <Image width={50} height={50} src={node.image_file.link} />}
        <span>{node.name}</span>
      </Stack>
    ),
  },
  {
    key: 'price',
    title: 'Harga Jual',
    width: 125,
    render: (node) => (
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <FormattedCurrency value={node.price} />

        {node.is_loss && (
          <Tooltip title="Jual Rugi">
            <IconButton disableRipple>
              <IconAlertCircle size={30} color="white" fill="red" />
            </IconButton>
          </Tooltip>
        )}
      </Box>
    ),
  },
  {
    key: 'base_cost_price',
    title: 'Harga Dasar Pembelian',
    width: 125,
    render: (node) => <FormattedCurrency value={node.stock.base_cost_price} />,
  },
  {
    key: 'stock',
    title: 'Stok',
    width: 100,
    render: (node) => <FormattedNumber value={node.stock.qty} />,
  },
  {
    key: 'status',
    title: 'Status',
    width: 100,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const ProductTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductTable
