import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import ProductsAPI from 'apis/productsAPI'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import Paper from 'components/Paper'
import TextInputField from 'components/TextInputField'
import UploadImageField from 'components/UploadImageField'
import { CURRENCY } from 'constants/currency'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type UpdateProductFormCardShape = FormShape<{
  name: string
  description: string
  image_file_path: string
  price: number
}>

type Props = {
  cardTitle: string
  initialValues?: UpdateProductFormCardShape
  initialProductImageSrc?: string
  onSubmit: (values: UpdateProductFormCardShape) => Promise<void>
  onCancel: () => void
}

const UpdateProductFormCard: FC<Props> = ({ cardTitle, initialValues, initialProductImageSrc, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (values: UpdateProductFormCardShape, helpers: FormikHelpers<UpdateProductFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<UpdateProductFormCardShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Gambar</FormLabel>
                  <UploadImageField
                    name="image_file_path"
                    aspectRatio={1 / 1}
                    initialImageSrc={initialProductImageSrc}
                    uploadFn={(file) => ProductsAPI.upload({ file })}
                    select={(data) => data.data.data.path}
                  />
                  <FormErrorText message={formik.errors.image_file_path} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Price</FormLabel>
                  <NumberInputField name="price" InputProps={{ startAdornment: CURRENCY }} fullWidth />
                  <FormErrorText message={formik.errors.price} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Deskripsi</FormLabel>
                  <TextInputField name="description" multiline rows={4} fullWidth />
                  <FormErrorText message={formik.errors.description} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { UpdateProductFormCardShape }
export default UpdateProductFormCard
