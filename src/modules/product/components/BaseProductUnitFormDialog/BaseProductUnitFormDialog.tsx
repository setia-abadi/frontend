import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape, ID } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import { SelectOption } from 'components/Select'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import UnitBaseSelectFieldForProductUnitForm from 'modules/unit/components/UnitBaseSelectFieldForProductUnitForm'

type BaseProductUnitFormDialogShape = FormShape<{
  scale: number
  unit_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  productId: ID
  dialogSubTitle?: string
  initialValues?: BaseProductUnitFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: BaseProductUnitFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const BaseProductUnitFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  productId,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: BaseProductUnitFormDialogShape,
    helpers: FormikHelpers<BaseProductUnitFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<BaseProductUnitFormDialogShape> initialValues={{ ...initialValues, }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel>Skala Dasar</FormLabel>
                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <NumberInputField name="scale" />
                    </Grid>
                    <Grid item xs={8}>
                      <UnitBaseSelectFieldForProductUnitForm productId={productId} name="unit_id" />
                    </Grid>
                  </Grid>
                  <FormErrorText message={formik.errors.unit_id} />
                </Grid>
              </Grid>

              <Box mt={3}></Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { BaseProductUnitFormDialogShape }

export default BaseProductUnitFormDialog
