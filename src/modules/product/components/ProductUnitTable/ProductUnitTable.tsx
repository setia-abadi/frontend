import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { ProductUnitModel } from 'models/productUnit'
import FormattedNumber from 'components/FormattedNumber'
import { Box, Stack } from '@mui/material'
import { IconArrowRight } from '@tabler/icons'

type Model = ProductUnitModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'unit',
    title: 'Satuan',
    minWidth: 200,
    width: 300,
    render: (node) => 
      node.to_unit && node.unit ? (
        <Stack direction="row" alignItems="center" spacing={1}>
          <Box>{node.unit.name}</Box>
          <IconArrowRight size="1rem" color="grey" />
          <Box>{node.to_unit.name}</Box>
        </Stack>
      ) : (
        <ContentWithEmptyPlaceholder>{node.unit?.name}</ContentWithEmptyPlaceholder>
      ),
  },
  {
    key: 'scale',
    title: 'Skala',
    width: 150,
    render: (node) => <FormattedNumber value={node.scale} />,
  },
]

const ProductUnitTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductUnitTable
