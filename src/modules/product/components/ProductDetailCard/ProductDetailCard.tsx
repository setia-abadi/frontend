import { Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { FC, ReactNode } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import ProductImage from '../ProductImage'
import StatusChip from 'components/StatusChip'

type Props = {
  name: string
  price: number
  imageSrc: string
  isActive: boolean
  description: string | null
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const ProductDetailCard: FC<Props> = (props) => {
  return (
    <Paper>
      <CardHeader title={props.cardTitle} action={props.cardAction} />
      <Divider />
      <CardContent>
        <Grid container spacing={5}>
          <Grid item xs={12} sm={4} md={3}>
            <ProductImage src={props.imageSrc} width="200px" />
          </Grid>

          <Grid item xs={12} sm={8} md={9}>
            <Grid container rowSpacing={3}>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} sm={4} lg={3}>
                    <Typography variant="body1" fontWeight={700}>
                      Nama
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={8} lg={9}>
                    <Typography variant="body1">{props.name}</Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} sm={4} lg={3}>
                    <Typography variant="body1" fontWeight={700}>
                      Price
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={8} lg={9}>
                    <Typography variant="body1">
                      <FormattedCurrency value={props.price} />
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} sm={4} lg={3}>
                    <Typography variant="body1" fontWeight={700}>
                      Deskripsi
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={8} lg={9}>
                    <Typography variant="body1">
                      <ContentWithEmptyPlaceholder>{props.description}</ContentWithEmptyPlaceholder>
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} sm={4} lg={3}>
                    <Typography variant="body1" fontWeight={700}>
                      Status
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={8} lg={9}>
                    <Typography variant="body1">
                      <StatusChip isActive={props.isActive} />
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              {props.createdAt && (
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12} sm={4} lg={3}>
                      <Typography variant="body1" fontWeight={700}>
                        Dibuat Pada
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={8} lg={9}>
                      <Typography variant="body1">
                        <FormattedDate date={props.createdAt} />
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              )}

              {props.updatedAt && (
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12} sm={4} lg={3}>
                      <Typography variant="body1" fontWeight={700}>
                        Diubah Pada
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={8} lg={9}>
                      <Typography variant="body1">
                        <FormattedDate date={props.updatedAt} />
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default ProductDetailCard
