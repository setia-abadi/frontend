import AlertTitle from '@mui/material/AlertTitle'
import CardActions from '@mui/material/CardActions'
import Alert from '@mui/material/Alert'
import Divider from '@mui/material/Divider'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import Paper from 'components/Paper'
import { FC } from 'react'

type Props = {
  onBtnClick: () => void
}

const ProductDeactivateCard: FC<Props> = ({ onBtnClick }) => {
  return (
    <Paper>
      <CardHeader title="Nonaktifkan Produk" />
      <Divider />
      <CardContent>
        <Alert severity="warning" variant="outlined">
          <AlertTitle>Anda Menonaktifkan Produk</AlertTitle>
          Anda akan menonaktifkan produk. Harap dicatat bahwa menonaktifkan produk ini akan membuatnya tidak dapat
          digunakan pada transaksi.
        </Alert>
      </CardContent>
      <Divider />
      <CardActions sx={{ p: 2, justifyContent: 'flex-end' }}>
        <Button variant="contained" color="error" onClick={onBtnClick}>
          Nonaktifkan
        </Button>
      </CardActions>
    </Paper>
  )
}

export default ProductDeactivateCard
