import AlertTitle from '@mui/material/AlertTitle'
import CardActions from '@mui/material/CardActions'
import Alert from '@mui/material/Alert'
import Divider from '@mui/material/Divider'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import Paper from 'components/Paper'
import { FC } from 'react'

type Props = {
  onBtnClick: () => void
}

const ProductActivateCard: FC<Props> = ({ onBtnClick }) => {
  return (
    <Paper>
      <CardHeader title="Aktifasi Produk" />
      <Divider />
      <CardContent>
        <Alert severity="info" variant="outlined">
          <AlertTitle>Anda Mengaktifkan Produk</AlertTitle>
          Untuk mengaktifkan produk, pastikan setidaknya satu unit telah dibuat. Aktivasi memerlukan minimal satu unit untuk dapat dilanjutkan.
        </Alert>
      </CardContent>
      <Divider />
      <CardActions sx={{ p: 2, justifyContent: 'flex-end' }}>
        <Button variant="contained" color="success" onClick={onBtnClick}>
          Aktifkan
        </Button>
      </CardActions>
    </Paper>
  )
}

export default ProductActivateCard
