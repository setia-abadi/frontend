import { useContext } from "react";
import ConfirmationContext from "../contexts/ConfirmationContext";

const useConfirmation = () => useContext(ConfirmationContext)

export default useConfirmation