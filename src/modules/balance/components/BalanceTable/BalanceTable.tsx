import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { BalanceModel } from 'models/balance'

type Model = BalanceModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama',
    minWidth: 200,
    enableSort: true,
    render: (node) => node.name,
  },
  {
    key: 'bank_name',
    title: 'Nama Bank',
    width: 250,
    enableSort: true,
    render: (node) => node.bank_name,
  },
  {
    key: 'account_name',
    title: 'Nama Akun',
    width: 250,
    enableSort: true,
    render: (node) => node.account_name,
  },
  {
    key: 'account_number',
    title: 'Nomor Akun',
    width: 250,
    enableSort: true,
    render: (node) => node.account_number,
  },
]

const BalanceTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }

    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default BalanceTable
