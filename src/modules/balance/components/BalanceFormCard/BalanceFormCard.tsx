import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import TextInputField from 'components/TextInputField'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type BalanceFormCardShape = FormShape<{
  name: string
  bank_name: string
  account_name: string
  account_number: string
}>

type Props = {
  cardTitle: string
  initialValues?: BalanceFormCardShape
  onSubmit: (values: BalanceFormCardShape) => Promise<void>
  onCancel: () => void
}

const BalanceFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (values: BalanceFormCardShape, helpers: FormikHelpers<BalanceFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<BalanceFormCardShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Nama Bank</FormLabel>
                  <TextInputField name="bank_name" fullWidth />
                  <FormErrorText message={formik.errors.bank_name} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Nama Akun</FormLabel>
                  <TextInputField name="account_name" fullWidth />
                  <FormErrorText message={formik.errors.account_name} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Nomor Akun</FormLabel>
                  <TextInputField name="account_number" fullWidth />
                  <FormErrorText message={formik.errors.account_number} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { BalanceFormCardShape }
export default BalanceFormCard
