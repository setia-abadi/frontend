import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import { Link } from 'react-router-dom'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PagePermission } from 'constants/pagePermission'
import { SupplierDebtSummarizeModel } from 'models/supplierDebtSummarize'

type Model = SupplierDebtSummarizeModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama Supplier',
    minWidth: 200,
    render: (node) => (
      <PermissionControl match={PagePermission.DEBT_LIST} fallback={node.supplier_name}>
        <Link to={`/debts`}>{node.supplier_name}</Link>
      </PermissionControl>
    )
  },
  {
    key: 'total_debt',
    title: 'Total',
    width: 175,
    render: (node) => <FormattedCurrency value={node.total_debt} />,
  },
]

const SummarizeSupplierDebtTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default SummarizeSupplierDebtTable