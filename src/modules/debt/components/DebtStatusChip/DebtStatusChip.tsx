import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { DebtStatusEnum } from 'enums/debtStatusEnum'
import { FC } from 'react'

type Props = {
  status: DebtStatusEnum
}

const DebtStatusChip: FC<Props> = ({ status }) => {
  const text: Record<DebtStatusEnum, string> = {
    [DebtStatusEnum.CANCELED]: 'Dibatalkan',
    [DebtStatusEnum.PAID]: 'Lunas',
    [DebtStatusEnum.UNPAID]: 'Belum Dibayar',
    [DebtStatusEnum.HALF_PAID]: 'Dibayar Sebagian',
    [DebtStatusEnum.RETURNED]: 'Dikembalikan',
  }

  const color: Record<DebtStatusEnum, ChipOwnProps['color']> = {
    [DebtStatusEnum.CANCELED]: 'default',
    [DebtStatusEnum.PAID]: 'success',
    [DebtStatusEnum.UNPAID]: 'error',
    [DebtStatusEnum.HALF_PAID]: 'warning',
    [DebtStatusEnum.RETURNED]: 'default',
  }

  return <Chip label={text[status]} size="small" color={color[status]} />
}

export default DebtStatusChip
