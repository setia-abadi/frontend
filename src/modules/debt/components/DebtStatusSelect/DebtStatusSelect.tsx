import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { DebtStatusEnum } from 'enums/debtStatusEnum'
import { FC } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const DebtStatusSelect: FC<Props> = (props) => {
  const text: Record<DebtStatusEnum, string> = {
    [DebtStatusEnum.CANCELED]: 'Dibatalkan',
    [DebtStatusEnum.PAID]: 'Lunas',
    [DebtStatusEnum.UNPAID]: 'Belum Dibayar',
    [DebtStatusEnum.HALF_PAID]: 'Dibayar Sebagian',
    [DebtStatusEnum.RETURNED]: 'Dikembalikan',
  }

  const options: SelectOption[] = Object.values(DebtStatusEnum).map((status) => ({
    label: text[status],
    value: status,
  }))

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default DebtStatusSelect
