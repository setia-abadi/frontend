import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import FormattedDate from 'components/FormattedDate'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import { DebtModel } from 'models/debt'
import DebtStatusChip from '../DebtStatusChip'
import { Link } from 'react-router-dom'

type Model = DebtModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'supplier',
    title: 'Supplier',
    minWidth: 200,
    render: (node) =>
      node.supplier ? (
        <Link to={`/suppliers/${node.supplier.id}`}>{node.supplier!.name}</Link>
      ) : (
        <EmptyTextPlaceholder />
      ),
  },
  {
    key: 'total_amount',
    title: 'Jumlah Total',
    width: 150,
    render: (node) => <FormattedCurrency value={node.amount} />,
  },
  {
    key: 'remaining_amount',
    title: 'Jumlah Tersisa',
    width: 150,
    render: (node) => <FormattedCurrency value={node.remaining_amount} />,
  },
  {
    key: 'due_date',
    title: 'Tenggat Waktu',
    width: 175,
    render: (node) => (node.due_date ? <FormattedDate date={node.due_date} /> : <EmptyTextPlaceholder />),
  },
  {
    key: 'status',
    title: 'Status',
    width: 200,
    render: (node) => <DebtStatusChip status={node.status} />,
  },
]

const DebtTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default DebtTable
