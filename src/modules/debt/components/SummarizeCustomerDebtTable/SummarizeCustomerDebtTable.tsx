import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import { Link } from 'react-router-dom'
import { CustomerDebtSummarizeModel } from 'models/customerDebtSummarize'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PagePermission } from 'constants/pagePermission'

type Model = CustomerDebtSummarizeModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama Pelanggan',
    minWidth: 200,
    render: (node) => (
      <PermissionControl match={PagePermission.CUSTOMER_DEBT_LIST} fallback={node.customer_name}>
        <Link to={`/receivables`}>{node.customer_name}</Link>
      </PermissionControl>
    )
  },
  {
    key: 'total_debt',
    title: 'Total',
    width: 175,
    render: (node) => <FormattedCurrency value={node.total_debt} />,
  },
]

const SummarizeCustomerDebtTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default SummarizeCustomerDebtTable