import { Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { FC, ReactNode } from 'react'
import FormattedNumber from 'components/FormattedNumber'
import TypeUtil from 'utils/typeUtil'
import FormattedCurrency from 'components/FormattedCurrency'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'

type Props = {
  productName: string
  minimumQty: number
  discountAmount: number | null
  discountPercentage: number | null
  isActive: boolean
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const ProductDiscountDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Nama Produk
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <ContentWithEmptyPlaceholder>{props.productName}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Kuantias Minimum
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <FormattedNumber value={props.minimumQty} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Diskon
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  {TypeUtil.isDefined(props.discountAmount) ? (
                    <FormattedCurrency value={props.discountAmount} />
                  ) : TypeUtil.isDefined(props.discountPercentage) ? (
                    `${props.discountPercentage} %`
                  ) : (
                    <EmptyTextPlaceholder />
                  )}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          {props.createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {props.updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default ProductDiscountDetailCard
