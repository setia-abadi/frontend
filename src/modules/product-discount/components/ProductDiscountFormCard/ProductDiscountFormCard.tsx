import { Box, Divider, Grid, InputAdornment, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import ProductForProductDiscountFormSelect from 'modules/product/components/ProductForProductDiscountFormSelect'
import RadioGroup from 'components/RadioGroup'
import NumberInputField from 'components/NumberInputField'
import { CURRENCY } from 'constants/currency'
import RadioGroupField from 'components/RadioGroupField'
import { SelectOption } from 'components/Select'

type DiscountType = 'discount_percentage' | 'discount_amount'

type ProductDiscountFormCardShape = FormShape<{
  discount_amount: number
  discount_percentage: number
  is_active: boolean
  minimum_qty: number
  product_id: SelectOption
}>

type Props = {
  cardTitle: string
  initialValues?: ProductDiscountFormCardShape
  disabledFields?: Partial<DisabledFields<ProductDiscountFormCardShape>>
  onSubmit: (values: ProductDiscountFormCardShape) => Promise<void>
  onCancel: () => void
}

const ProductDiscountFormCard: FC<Props> = ({ cardTitle, initialValues, disabledFields, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: ProductDiscountFormCardShape = {
    minimum_qty: 1,
    is_active: true,
  }

  const [discountType, setDiscountType] = useState<DiscountType>(
    TypeUtil.isDefined(initialValues)
      ? TypeUtil.isDefined(initialValues.discount_amount)
        ? 'discount_amount'
        : 'discount_percentage'
      : 'discount_amount'
  )

  const handleSubmit = async (
    values: ProductDiscountFormCardShape,
    helpers: FormikHelpers<ProductDiscountFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<ProductDiscountFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Produk</FormLabel>
                  <ProductForProductDiscountFormSelect
                    name="product_id"
                    disabled={disabledFields?.product_id}
                    fullWidth
                  />
                  <FormErrorText message={formik.errors.product_id} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Kuantitas Minimum</FormLabel>
                  <NumberInputField name="minimum_qty" disabled={disabledFields?.minimum_qty} sx={{ width: '60%' }} />
                  <FormErrorText message={formik.errors.minimum_qty} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Diskon</FormLabel>
                  <RadioGroup
                    row
                    value={discountType}
                    disabled={disabledFields?.discount_amount || disabledFields?.discount_percentage}
                    onChange={(value) => {
                      setDiscountType(value as DiscountType)
                      formik.setFieldValue('discount_amount', null)
                      formik.setFieldValue('discount_percentage', null)
                    }}
                    options={[
                      { label: 'Jumlah Diskon', value: 'discount_amount' },
                      { label: 'Persentase Diskon', value: 'discount_percentage' },
                    ]}
                  />

                  <Box mt={1}>
                    {discountType === 'discount_amount' ? (
                      <NumberInputField
                        name="discount_amount"
                        disabled={disabledFields?.discount_amount}
                        InputProps={{ startAdornment: <InputAdornment position="start">{CURRENCY}</InputAdornment> }}
                        sx={{ width: '60%' }}
                      />
                    ) : (
                      <NumberInputField
                        name="discount_percentage"
                        disabled={disabledFields?.discount_percentage}
                        InputProps={{ endAdornment: <InputAdornment position="end">%</InputAdornment> }}
                        sx={{ width: '60%' }}
                      />
                    )}
                  </Box>
                  <FormErrorText message={formik.errors.discount_amount || formik.errors.discount_percentage} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Status</FormLabel>
                  <RadioGroupField
                    row={true}
                    name="is_active"
                    options={[
                      { label: 'Aktif', value: true },
                      { label: 'Tidak Aktif', value: false },
                    ]}
                  />
                  <FormErrorText message={formik.errors.is_active} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { ProductDiscountFormCardShape }

export default ProductDiscountFormCard
