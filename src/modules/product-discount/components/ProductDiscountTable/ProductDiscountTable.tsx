import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import Stack from '@mui/material/Stack'
import Image from 'components/Image'
import Box from '@mui/material/Box'
import { ProductDiscountModel } from 'models/productDiscount'
import StatusChip from 'components/StatusChip'
import FormattedNumber from 'components/FormattedNumber'
import FormattedCurrency from 'components/FormattedCurrency'
import TypeUtil from 'utils/typeUtil'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'

type Model = ProductDiscountModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product',
    title: 'Produk',
    width: 250,
    render: (node) => (
      <Stack direction="row" spacing={1.5} alignItems="center">
        {node.product && <Image width={50} aspectRatio={1} src={node.product.image_file.link} />}
        <Box>
          <ContentWithEmptyPlaceholder>{node.product?.name}</ContentWithEmptyPlaceholder>
        </Box>
      </Stack>
    ),
  },
  {
    key: 'minimum_qty',
    title: 'Jumlah Minimal',
    width: 175,
    render: (node) => <FormattedNumber value={node.minimum_qty} />,
  },
  {
    key: 'discount',
    title: 'Diskon',
    width: 200,
    render: (node) => {
      if (TypeUtil.isDefined(node.discount_amount)) {
        return <FormattedCurrency value={node.discount_amount} />
      }

      if (TypeUtil.isDefined(node.discount_percentage)) {
        return `${node.discount_percentage} %`
      }

      return <EmptyTextPlaceholder />
    },
  },
  {
    key: 'is_active',
    title: 'Status',
    width: 175,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const ProductDiscountTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductDiscountTable
