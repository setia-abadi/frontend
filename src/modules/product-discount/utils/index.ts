import dayjs from "dayjs";
import { ProductDiscountModel } from "models/productDiscount";
import NumberUtil from "utils/numberUtil";
import StringUtil from "utils/stringUtil";

const ProductDiscountUtil = {
  randomRecord: (): ProductDiscountModel => {
    const discountType = NumberUtil.random(1, 3) === 2 ? 'amount' : 'percentage'

    return {
      created_at: dayjs().toISOString(),
      discount_amount: discountType === 'amount' ? NumberUtil.random(5000, 20000) : null,
      discount_percentage: discountType === 'percentage' ? NumberUtil.random(1, 80) : null,
      id: StringUtil.uuid(),
      is_active: true,
      minimum_qty:  NumberUtil.random(1, 10),
      product: null,
      product_id: StringUtil.uuid(),
      updated_at: dayjs().toISOString(),
    }
  }
}

export default ProductDiscountUtil
