import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import FormattedCurrency from 'components/FormattedCurrency'
import Image from 'components/Image'
import { FC } from 'react'
import Typography from 'components/Typography'
import InputDialer from 'components/InputDialer'
import { IconButton } from '@mui/material'
import { IconTrash } from '@tabler/icons'

type Props = {
  imageSrc: string
  name: string
  price: number
  qty: number
  stock: number
  onQtyChange: (qty: number) => void
  onDeleteBtnClick: () => void
}

const CartItem: FC<Props> = ({ imageSrc, name, qty, stock, price, onDeleteBtnClick, onQtyChange }) => {
  return (
    <Box display="flex" p={2}>
      <Box mr={2}>
        <Image src={imageSrc} width={60} aspectRatio={1} />
      </Box>

      <Box flexGrow={1}>
        <Stack spacing={1}>
          <Typography variant="body1" fontWeight={600} maxLine={2} title={name}>
            {name}
          </Typography>

          <Box display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="body1">
              <FormattedCurrency value={price / qty} />
            </Typography>

            <Stack direction="row" spacing={1}>
              <IconButton color="error" onClick={onDeleteBtnClick}>
                <IconTrash size="1.2rem" />
              </IconButton>
              <InputDialer min={1} max={stock} value={qty} onChange={onQtyChange} />
            </Stack>
          </Box>
        </Stack>
      </Box>
    </Box>
  )
}

export default CartItem
