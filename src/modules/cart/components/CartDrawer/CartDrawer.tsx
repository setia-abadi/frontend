import { Theme, useMediaQuery, useTheme } from '@mui/material'
import Drawer from '@mui/material/Drawer'
import { FC, PropsWithChildren } from 'react'

type Props = {
  open: boolean
  onClose: () => void
}

const drawerWidth = 380

const CartDrawer: FC<PropsWithChildren<Props>> = ({ open, children, onClose }) => {
  const lgUp = useMediaQuery<Theme>((theme) => theme.breakpoints.up('lg'))
  const theme = useTheme()

  return (
    <Drawer
      open={open}
      anchor="right"
      variant={lgUp ? 'permanent' : 'temporary'}
      sx={{ width: drawerWidth }}
      PaperProps={{
        sx: {
          width: drawerWidth,
          position: lgUp ? 'relative' : 'fixed',
          boxSizing: 'border-box',
          borderLeft: `1px solid ${theme.palette.grey[300]}`,
        },
      }}
      onClose={onClose}
    >
      {children}
    </Drawer>
  )
}

export default CartDrawer
