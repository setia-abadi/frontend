import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'
import NumberInputField from 'components/NumberInputField'
import UploadImageField from 'components/UploadImageField'
import CustomerDebtsAPI from 'apis/customerDebtsAPI'
import { CURRENCY } from 'constants/currency'
import TextInputField from 'components/TextInputField'

type CustomerDebtPaymentFormDialogShape = FormShape<{
  amount: number
  description: string | null
  image_file_path: string
}>

type Props = {
  open: boolean
  dialogTitle: string
  disabledFields?: Partial<DisabledFields<CustomerDebtPaymentFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: CustomerDebtPaymentFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: CustomerDebtPaymentFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const CustomerDebtPaymentFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: CustomerDebtPaymentFormDialogShape,
    helpers: FormikHelpers<CustomerDebtPaymentFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<CustomerDebtPaymentFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <PermissionControl renderError match={[PermissionEnum.CUSTOMER_DEBT_UPLOAD_IMAGE]}>
                    <FormLabel required>Bukti Pembayaran</FormLabel>
                    <UploadImageField
                      name="image_file_path"
                      aspectRatio={1}
                      uploadFn={(file) => CustomerDebtsAPI.uploadImage({ file })}
                      select={(response) => response.data.data.path}
                    />
                    <FormErrorText message={formik.errors.image_file_path} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12}>
                    <FormLabel required>Jumlah Bayar</FormLabel>
                    <NumberInputField name="amount" InputProps={{ startAdornment: CURRENCY }} fullWidth/>
                    <FormErrorText message={formik.errors.amount} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Deskripsi</FormLabel>
                  <TextInputField name="description" multiline rows={5} fullWidth />
                  <FormErrorText message={formik.errors.description} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { CustomerDebtPaymentFormDialogShape }

export default CustomerDebtPaymentFormDialog
