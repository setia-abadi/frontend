import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { CustomerDebtModel } from 'models/customerDebt'
import FormattedCurrency from 'components/FormattedCurrency'
import CustomerDebtStatusChip from '../CustomerDebtStatusChip'
import FormattedDate from 'components/FormattedDate'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'

type Model = CustomerDebtModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'customer',
    title: 'Pelanggan',
    minWidth: 200,
    render: (node) => node.customer!.name,
  },
  {
    key: 'total_amount',
    title: 'Jumlah Total',
    width: 150,
    render: (node) => <FormattedCurrency value={node.amount} />,
  },
  {
    key: 'remaining_amount',
    title: 'Jumlah Tersisa',
    width: 150,
    render: (node) => <FormattedCurrency value={node.remaining_amount} />,
  },
  {
    key: 'due_date',
    title: 'Tenggat Waktu',
    width: 175,
    render: (node) => (node.due_date ? <FormattedDate date={node.due_date} /> : <EmptyTextPlaceholder />),
  },
  {
    key: 'status',
    title: 'Status',
    width: 175,
    render: (node) => <CustomerDebtStatusChip status={node.status} />,
  },
]

const CustomerDebtTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default CustomerDebtTable
