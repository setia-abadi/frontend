import { Link, Stack } from '@mui/material'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import FormattedCurrency from 'components/FormattedCurrency'
import FormattedDate from 'components/FormattedDate'
import Image from 'components/Image'
import Table, { ColumnProps, TableProps } from 'components/Table'
import { CustomerPaymentModel } from 'models/customerPayment'
import { FC, ReactNode, useMemo } from 'react'

type Model = CustomerPaymentModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'image',
    title: 'Gambar',
    minWidth: 300,
    width: 350,
    render: (node) => (
      <Stack direction="row" alignItems="center" spacing={1.5}>
        <Image src={node.image_file!.link} width={50} aspectRatio={1} fit="contain" />
        <Link href={node.image_file!.link} target="_blank">{node.image_file?.name}</Link>
      </Stack>
    ),
  },
  {
    key: 'amount',
    title: 'Jumlah Pembayaran',
    width: 200,
    render: (node) => <FormattedCurrency value={node.amount} />,
  },
  {
    key: 'description',
    title: 'Deskripsi',
    minWidth: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.description}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'created_at',
    title: 'Dibuat Pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const CustomerDebtPaymentTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 100,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default CustomerDebtPaymentTable
