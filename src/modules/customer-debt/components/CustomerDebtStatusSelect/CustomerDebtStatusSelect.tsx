import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import { FC } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const CustomerDebtStatusSelect: FC<Props> = (props) => {
  const text: Record<CustomerDebtStatusEnum, string> = {
    [CustomerDebtStatusEnum.CANCELED]: 'Dibatalkan',
    [CustomerDebtStatusEnum.PAID]: 'Lunas',
    [CustomerDebtStatusEnum.UNPAID]: 'Belum Dibayar',
    [CustomerDebtStatusEnum.HALF_PAID]: 'Dibayar Sebagian',
  }

  const options: SelectOption[] = Object.values(CustomerDebtStatusEnum).map((status) => ({
    label: text[status],
    value: status,
  }))

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default CustomerDebtStatusSelect
