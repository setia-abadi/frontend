import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'
import CustomerForWhatsappCustomerDebtBroadcastFormSelect from 'modules/customer/components/CustomerForWhatsappCustomerDebtBroadcastFormSelect'
import { SelectOption } from 'components/Select'

type CustomerDebtBroadcastWhatsappFormDialogShape = FormShape<{
  customer_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  disabledFields?: Partial<DisabledFields<CustomerDebtBroadcastWhatsappFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: CustomerDebtBroadcastWhatsappFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: CustomerDebtBroadcastWhatsappFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const CustomerDebtBroadcastWhatsappFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: CustomerDebtBroadcastWhatsappFormDialogShape,
    helpers: FormikHelpers<CustomerDebtBroadcastWhatsappFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<CustomerDebtBroadcastWhatsappFormDialogShape>
        initialValues={{ ...initialValues }}
        onSubmit={handleSubmit}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel required>Pelanggan</FormLabel>
                  <PermissionControl
                    renderError
                    match={[PermissionEnum.CUSTOMER_OPTION_FOR_WHATSAPP_CUSTOMER_DEBT_BROADCAST_FORM]}
                  >
                    <CustomerForWhatsappCustomerDebtBroadcastFormSelect name="customer_id" />
                  </PermissionControl>
                  <FormErrorText message={formik.errors.customer_id} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Siarkan
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { CustomerDebtBroadcastWhatsappFormDialogShape }

export default CustomerDebtBroadcastWhatsappFormDialog
