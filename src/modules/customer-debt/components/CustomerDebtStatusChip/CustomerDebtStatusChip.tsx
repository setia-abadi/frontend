import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import { FC } from 'react'

type Props = {
  status: CustomerDebtStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status }) => {
  const text: Record<CustomerDebtStatusEnum, string> = {
    [CustomerDebtStatusEnum.CANCELED]: 'Dibatalkan',
    [CustomerDebtStatusEnum.PAID]: 'Lunas',
    [CustomerDebtStatusEnum.UNPAID]: 'Belum Dibayar',
    [CustomerDebtStatusEnum.HALF_PAID]: 'Dibayar Sebagian',
  }

  const color: Record<CustomerDebtStatusEnum, ChipOwnProps['color']> = {
    [CustomerDebtStatusEnum.CANCELED]: 'default',
    [CustomerDebtStatusEnum.PAID]: 'success',
    [CustomerDebtStatusEnum.UNPAID]: 'error',
    [CustomerDebtStatusEnum.HALF_PAID]: 'warning',
  }

  return <Chip label={text[status]} size="small" color={color[status]} />
}

export default CustomerDebtStatusChip
