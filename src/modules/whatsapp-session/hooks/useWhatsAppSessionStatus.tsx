import { useQuery } from '@tanstack/react-query'
import WhatsappAPI from 'apis/whatsappAPI'
import { QueryKeyEnum } from 'enums/queryKeyEnum'

const useWhatsAppSessionStatus = () => {
  const {
    data: isLoggedIn,
    isLoading,
  } = useQuery({
    queryKey: [QueryKeyEnum.WHATSAPP_SESSION_GET_CURRENT],
    queryFn: () => WhatsappAPI.isLoggedIn(),
    select: (response) => response.data.data.is_logged_in,
  })

  return {
    isLoggedIn,
    isLoading,
  }
}

export default useWhatsAppSessionStatus
