import { Box, Stack, Typography } from '@mui/material'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import Paper from 'components/Paper'
import { FC } from 'react'

type Props = {
  onEndSessionClick: () => void
}

const WhatsappSessionLoginQrCard: FC<Props> = ({ onEndSessionClick }) => {
  return (
    <Paper>
      <CardContent>
        <Stack spacing={1} mb={3}>
          <Typography variant="h4">Sesi Aktif</Typography>
          <Typography variant="subtitle2">Sesi Whatsapp telah aktif, anda dapat melakukan broadcast.</Typography>
        </Stack>

        <Box>
          <Button type="submit" variant="contained" size="large" fullWidth onClick={onEndSessionClick}>
            Akhiri Sesi
          </Button>
        </Box>
      </CardContent>
    </Paper>
  )
}

export default WhatsappSessionLoginQrCard
