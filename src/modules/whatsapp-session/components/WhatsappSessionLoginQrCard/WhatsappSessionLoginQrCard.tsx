import { Box, Stack, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import Paper from 'components/Paper'
import useSSE from 'hooks/useSSE'
import { Response } from 'models/apiBase'
import { FC, useState } from 'react'

type Props = {
  onLoggedIn: () => void
}

type ResponseSSRWhatsappLogin = Response<{ base64_qr: string; is_connected: boolean }>

const WhatsappSessionLoginQrCard: FC<Props> = ({ onLoggedIn }) => {
  const [base64qrImage, setBase64qrImage] = useState<string>()

  const { closeConnection } = useSSE<ResponseSSRWhatsappLogin>(
    `${process.env.REACT_APP_API_URL}ssr/whatsapp/login`,
    (value) => {
      if (value.data.is_connected) {
        onLoggedIn()
        closeConnection()
      } else {
        setBase64qrImage(value.data.base64_qr)
      }
    }
  )

  return (
    <Paper>
      <CardContent>
        <Stack spacing={1} mb={3}>
          <Typography variant="h4">Sesi Whatsapp</Typography>
          <Typography variant="subtitle2">
            Silahkan scan QR dibawah dengan handphone anda untuk memulai sesi WhatsApp.
          </Typography>
        </Stack>

        <Box width="100%" height="100%">
          {base64qrImage && <img width="100%" height="100%" src={`data:image/png;base64,${base64qrImage}`} alt="" />}
        </Box>
      </CardContent>
    </Paper>
  )
}

export default WhatsappSessionLoginQrCard
