import { TransactionPaymentTypeEnum } from "enums/transactionPaymentTypeEnum";

const TRANSACTION_PAYMENT_TYPE_TEXT: Record<TransactionPaymentTypeEnum, string> = {
  [TransactionPaymentTypeEnum.BCA_TRANSFER]: 'Transfer BCA',
  [TransactionPaymentTypeEnum.CASH]: 'Tunai',
}

export { TRANSACTION_PAYMENT_TYPE_TEXT}