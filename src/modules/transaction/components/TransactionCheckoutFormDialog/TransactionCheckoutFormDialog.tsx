import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Stack } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import { TransactionPaymentTypeEnum } from 'enums/transactionPaymentTypeEnum'
import NumberInputField from 'components/NumberInputField'
import FormLabel from 'components/FormLabel'
import TransactionPaymentTypeRadioButtonField from '../TransactionPaymentTypeRadioButtonField'
import { CURRENCY } from 'constants/currency'
import TextInputField from 'components/TextInputField'
import TextInput from 'components/TextInput'
import Typography from 'components/Typography'
import FormattedCurrency from 'components/FormattedCurrency'

type TransactionCheckoutFormDialogShape = FormShape<{
  cash_paid: number
  payment_type: TransactionPaymentTypeEnum
  reference_number: string
}>

type Props = {
  open: boolean
  dialogTitle: string
  totalAmount: number
  dialogSubTitle?: string
  initialValues?: TransactionCheckoutFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: TransactionCheckoutFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const TransactionCheckoutFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  totalAmount,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const defaultValues: TransactionCheckoutFormDialogShape = {
    payment_type: TransactionPaymentTypeEnum.CASH,
  }

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: TransactionCheckoutFormDialogShape,
    helpers: FormikHelpers<TransactionCheckoutFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<TransactionCheckoutFormDialogShape>
        initialValues={initialValues ?? defaultValues}
        onSubmit={handleSubmit}
      >
        {(formik) => {
          const diffPaidCash = TypeUtil.isDefined(formik.values.cash_paid) ? formik.values.cash_paid - totalAmount : 0
          const returnAmount = diffPaidCash > 0 ? diffPaidCash : 0

          return (
            <Form onSubmit={formik.handleSubmit}>
              <DialogTitle>{dialogTitle}</DialogTitle>

              <DialogContent>
                {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

                {error && (
                  <Box mt={3}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Box>
                )}

                <Stack gap={3} mt={3}>
                  <Box>
                    <FormLabel>Total Belanja</FormLabel>
                    <Box mt={1}>
                      <Typography variant="h5">
                        <FormattedCurrency value={totalAmount} />
                      </Typography>
                    </Box>
                  </Box>

                  <Box>
                    <FormLabel>Jenis Pembayaran</FormLabel>
                    <TransactionPaymentTypeRadioButtonField name="payment_type" />
                    <FormErrorText message={formik.errors.payment_type} />
                  </Box>

                  {formik.values.payment_type === TransactionPaymentTypeEnum.CASH ? (
                    <>
                      <Box>
                        <FormLabel required>Tunai</FormLabel>
                        <NumberInputField name="cash_paid" InputProps={{ startAdornment: CURRENCY }} fullWidth />
                        <FormErrorText message={formik.errors.cash_paid} />
                      </Box>

                      <Box>
                        <FormLabel>Kembalian</FormLabel>
                        <TextInput InputProps={{ startAdornment: CURRENCY }} disabled value={returnAmount} />
                      </Box>
                    </>
                  ) : (
                    <Box>
                      <FormLabel required>Nomor Referensi</FormLabel>
                      <TextInputField name="reference_number" fullWidth />
                      <FormErrorText message={formik.errors.reference_number} />
                    </Box>
                  )}
                </Stack>
              </DialogContent>

              <DialogActions>
                <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                  Batalkan
                </Button>

                <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                  Bayar
                </Button>
              </DialogActions>
            </Form>
          )
        }}
      </Formik>
    </Dialog>
  )
}

export type { TransactionCheckoutFormDialogShape }

export default TransactionCheckoutFormDialog
