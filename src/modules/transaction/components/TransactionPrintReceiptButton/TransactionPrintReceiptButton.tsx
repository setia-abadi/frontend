import { Button } from '@mui/material'
import { useMutation } from '@tanstack/react-query'
import PrintersAPI from 'apis/printersAPI'
import TransactionsAPI from 'apis/transactionsAPI'
import { ButtonProps } from 'components/Button'
import { ID } from 'models/base'
import { useSnackbar } from 'notistack'
import React from 'react'
import APIUtil from 'utils/APIUtil'

type Props = Omit<ButtonProps, 'onClick'> & {
  transactionId: ID
}

const TransactionPrintReceiptButton: React.FC<Props> = ({ transactionId, ...buttonProps }) => {
  const snackbar = useSnackbar()

  const { mutate, isPending } = useMutation({
    mutationFn: () => TransactionsAPI.reprint(transactionId),
    onSuccess: async (response) => {
      await PrintersAPI.print({ printer_data: response.data.data.printer_data })
      snackbar.enqueueSnackbar('Kuitansi telah berhasil di cetak', { variant: 'success' })
    },
    onError: (err) => {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    },
  })

  return <Button disabled={isPending} {...buttonProps} onClick={() => mutate()} />
}

export default TransactionPrintReceiptButton
