import { Stack } from '@mui/material'
import Button from 'components/Button'
import { TransactionPaymentTypeEnum } from 'enums/transactionPaymentTypeEnum'
import { useField } from 'formik'
import { TRANSACTION_PAYMENT_TYPE_TEXT } from 'modules/transaction/constants'
import { FC } from 'react'

type Props = {
  name: string
  onChange?: (value: TransactionPaymentTypeEnum) => void
}

const TransactionPaymentTypeRadioButtonField: FC<Props> = ({ name, onChange }) => {
  const [field,, helpers] = useField(name)

  const handleClick = (value: TransactionPaymentTypeEnum) => {
    helpers.setValue(value)
  }

  return (
    <Stack direction="row" spacing={1}>
      {Object.values(TransactionPaymentTypeEnum).map((value) => (
        <Button variant={field.value === value ? 'contained' : 'outlined'} onClick={() => handleClick(value)}>
          {TRANSACTION_PAYMENT_TYPE_TEXT[value]}
        </Button>
      ))}
    </Stack>
  )
}

export default TransactionPaymentTypeRadioButtonField
