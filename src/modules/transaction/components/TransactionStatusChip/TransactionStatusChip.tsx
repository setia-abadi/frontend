import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { TransactionStatusEnum } from 'enums/transactionStatusEnum'
import { FC } from 'react'

type Props = {
  status: TransactionStatusEnum
}

const TransactionStatusChip: FC<Props> = ({ status }) => {
  const text: Record<TransactionStatusEnum, string> = {
    [TransactionStatusEnum.PAID]: 'Lunas',
    [TransactionStatusEnum.UNPAID]: 'Belum Dibayar',
  }

  const color: Record<TransactionStatusEnum, ChipOwnProps['color']> = {
    [TransactionStatusEnum.PAID]: 'success',
    [TransactionStatusEnum.UNPAID]: 'warning',
  }

  return <Chip label={text[status]} size="small" color={color[status]} />
}

export default TransactionStatusChip
