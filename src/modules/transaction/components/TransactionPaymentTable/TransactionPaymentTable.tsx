import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import FormattedCurrency from 'components/FormattedCurrency'
import FormattedDate from 'components/FormattedDate'
import Table, { ColumnProps, TableProps } from 'components/Table'
import { TranscationPaymentModel } from 'models/transactionPayment'
import { TRANSACTION_PAYMENT_TYPE_TEXT } from 'modules/transaction/constants'
import { FC, ReactNode, useMemo } from 'react'

type Model = TranscationPaymentModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'amount',
    title: 'Jumlah Pembayaran',
    width: 200,
    render: (node) => <FormattedCurrency value={node.total} />,
  },
  {
    key: 'type',
    title: 'Tipe',
    width: 150,
    render: (node) => TRANSACTION_PAYMENT_TYPE_TEXT[node.payment_type],
  },
  {
    key: 'reference_number',
    title: 'Nomor Referensi',
    width: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.reference_number}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'created_at',
    title: 'Dibayar Pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const TransactionPaymentTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 100,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default TransactionPaymentTable
