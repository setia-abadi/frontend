import { useContext } from "react";
import { SplashScreenContext } from "../contexts/SpashScreenContext";

const useSplashScreen = () => useContext(SplashScreenContext)

export { useSplashScreen }