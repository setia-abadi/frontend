import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { SupplierModel } from 'models/supplier'
import StatusChip from 'components/StatusChip'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'

type Model = SupplierModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'code',
    title: 'Code',
    minWidth: 150,
    width: 150,
    render: (node) => node.code,
  },
  {
    key: 'name',
    title: 'Nama',
    minWidth: 250,
    enableSort: true,
    render: (node) => node.name,
  },
  {
    key: 'phone',
    title: 'Nomor Telepon',
    width: 200,
    render: (node) => node.phone,
  },
  {
    key: 'supplier_type_id',
    title: 'Jenis',
    width: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.supplier_type?.name}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'status',
    title: 'Status',
    width: 150,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const SuppliersTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 100,
        minWidth: 100,
        render: (node) => action(node),
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default SuppliersTable
