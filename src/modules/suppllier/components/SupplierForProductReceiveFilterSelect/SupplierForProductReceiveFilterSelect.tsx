import SuppliersAPI from 'apis/suppliersAPI'
import AsyncSelectField, { AsyncSelectFieldProps } from 'components/AsyncSelectField'
import { SelectOption } from 'components/Select'
import { SELECT_OPTION_ALL } from 'constants/option'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import APIUtil from 'utils/APIUtil'

type Props<Multiple extends boolean> = Omit<AsyncSelectFieldProps<Multiple>, 'fetchOptions'>

const SupplierForProductReceiveFilterSelect = <Multiple extends boolean = false>(props: Props<Multiple>) => {
  const fetchOptions: AsyncSelectFieldProps<Multiple>['fetchOptions'] = async ({ page, phrase, signal }) => {
    const response = await APIUtil.withConfig(
      () =>
        SuppliersAPI.optionsForProductReceiveFilter({
          limit: LIMIT_PER_PAGE,
          page,
          phrase,
        }),
      { signal }
    )

    if (response.data.data) {
      return {
        ...response.data.data,
        nodes: response.data.data.nodes.map<SelectOption>((node) => ({
          label: node.name,
          value: node.id,
        })),
      }
    }

    return undefined
  }

  return <AsyncSelectField {...props} defaultOptions={[SELECT_OPTION_ALL]} fetchOptions={fetchOptions} />
}

export default SupplierForProductReceiveFilterSelect
