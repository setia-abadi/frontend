import { Divider, Grid, InputAdornment, Stack } from '@mui/material'
import { IconMail, IconPhone } from '@tabler/icons'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import RadioGroupField from 'components/RadioGroupField'
import { SelectOption } from 'components/Select'
import TextInputField from 'components/TextInputField'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import SupplierTypeSelectFieldForSupperForm from 'modules/supplier-type/components/SupplierTypeSelectFieldForSupperForm'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type SupplierFormCardShape = FormShape<{
  address: string
  code: string
  description: string
  email: string
  is_active: boolean
  name: string
  phone: string
  supplier_type_id: SelectOption
}>

type Props = {
  cardTitle: string
  initialValues?: SupplierFormCardShape
  onSubmit: (values: SupplierFormCardShape) => Promise<void>
  onCancel: () => void
}

const SupplierFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: SupplierFormCardShape = {
    is_active: true,
  }

  const handleSubmit = async (values: SupplierFormCardShape, helpers: FormikHelpers<SupplierFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<SupplierFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12} md={6}>
                  <FormLabel required>Code</FormLabel>
                  <TextInputField name="code" fullWidth />
                  <FormErrorText message={formik.errors.code} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Jenis</FormLabel>
                  <SupplierTypeSelectFieldForSupperForm name="supplier_type_id" fullWidth />
                  <FormErrorText message={formik.errors.supplier_type_id} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Nomor Telepon</FormLabel>
                  <TextInputField
                    name="phone"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <IconPhone size="1.3rem" />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <FormErrorText message={formik.errors.phone} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel>Alamat Email</FormLabel>
                  <TextInputField
                    name="email"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <IconMail size="1.3rem" />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <FormErrorText message={formik.errors.email} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Alamat</FormLabel>
                  <TextInputField name="address" multiline rows={4} fullWidth />
                  <FormErrorText message={formik.errors.address} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Deskripsi</FormLabel>
                  <TextInputField name="description" multiline rows={4} fullWidth />
                  <FormErrorText message={formik.errors.description} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Status</FormLabel>
                  <RadioGroupField
                    row={true}
                    name="is_active"
                    options={[
                      { label: 'Aktif', value: true },
                      { label: 'Tidak Aktif', value: false },
                    ]}
                  />
                  <FormErrorText message={formik.errors.is_active} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { SupplierFormCardShape }
export default SupplierFormCard
