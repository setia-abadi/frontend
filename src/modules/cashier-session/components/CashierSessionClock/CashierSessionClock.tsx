import Box from '@mui/material/Box'
import Stack from '@mui/material/Stack'
import { styled } from '@mui/material/styles'
import { useTimeCounter } from 'hooks/useTimeCounter'
import { FC } from 'react'

type Props = {
  startDateTime: string
}

const BoxStyled = styled(Box)(({ theme }) => ({
  width: 75,
  height: 75,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: theme.shape.borderRadius,
  border: `1px solid ${theme.palette.grey[300]}`,
  ...theme.typography.h1,
}))

const Separator = styled('div')(({ theme }) => ({
  ...theme.typography.h1,
}))

const CashierSessionClock: FC<Props> = ({ startDateTime }) => {
  const { hours, minutes, seconds} = useTimeCounter({ startAt: startDateTime })

  return (
    <Stack direction="row" alignItems="center"spacing={1}>
      <BoxStyled>{hours}</BoxStyled>
      <Separator>:</Separator>
      <BoxStyled>{minutes}</BoxStyled>
      <Separator>:</Separator>
      <BoxStyled>{seconds}</BoxStyled>
    </Stack>
  )
}

export default CashierSessionClock
