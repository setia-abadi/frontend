import { Alert, Box, Stack } from '@mui/material'
import Button from 'components/Button'
import DateTimePickerField from 'components/DateTimePickerField'
import FilterPopoverWithButton from 'components/FilterPopoverWithButton/FilterPopoverWithButton'
import FormLabel from 'components/FormLabel'
import { SelectOption } from 'components/Select'
import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape, HiddenFields } from 'models/base'
import UserForCashierSessionFilterSelect from 'modules/user/components/UserForCashierSessionFilterSelect'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

import CashierSessionStatusSelect from '../CashierSessionStatusSelect'

import type { Dayjs } from 'dayjs'
type CashierSessionFilterFormPopoverShape = FormShape<{
  ended_date: Dayjs
  user_id: SelectOption
  status: CashierSessionStatusEnum
  started_date: Dayjs
}>

type Props = {
  initialValues?: CashierSessionFilterFormPopoverShape
  onApply: (values: CashierSessionFilterFormPopoverShape) => void
  onReset: () => void
  hiddenFields?: Partial<HiddenFields<CashierSessionFilterFormPopoverShape>>
}

const CashierSessionFilterFormPopover: FC<Props> = ({ initialValues, onApply, onReset, hiddenFields }) => {
  const [error, setError] = useState<string>()
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const handleButtonClick = () => {
    setIsOpen((prev) => !prev)
  }

  const handlePopoverClose = () => {
    setIsOpen(false)
    setError(undefined)
  }

  const handleReset = () => {
    setIsOpen(false)
    onReset()
  }

  const handleSubmit = async (
    values: CashierSessionFilterFormPopoverShape,
    helpers: FormikHelpers<CashierSessionFilterFormPopoverShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      onApply(values)
      setIsOpen(false)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <FilterPopoverWithButton isShow={isOpen} onButtonClick={handleButtonClick} onClose={handlePopoverClose}>
      <Formik<CashierSessionFilterFormPopoverShape>
        initialValues={{ ...initialValues }}
        onSubmit={handleSubmit}
        onReset={handleReset}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            {error && (
              <Box mb={3}>
                <Alert severity="error" variant="outlined">
                  {error}
                </Alert>
              </Box>
            )}

            <Stack gap={3} mb={3}>
              {!hiddenFields?.started_date && (
                <Box>
                  <FormLabel>Tanggal Mulai</FormLabel>
                  <DateTimePickerField name="started_date" fullWidth />
                </Box>
              )}

              {!hiddenFields?.ended_date && (
                <Box>
                  <FormLabel>Tanggal Berakhir</FormLabel>
                  <DateTimePickerField minDate={formik.values.started_date ?? undefined} name="ended_date" fullWidth />
                </Box>
              )}

              {!hiddenFields?.user_id && (
                <Box>
                  <FormLabel>User</FormLabel>
                  <UserForCashierSessionFilterSelect name="user_id" fullWidth />
                </Box>
              )}

              {!hiddenFields?.status && (
                <Box>
                  <FormLabel>Status</FormLabel>
                  <CashierSessionStatusSelect name="status" fullWidth />
                </Box>
              )}
            </Stack>

            <Stack gap={1} direction="row" justifyContent="flex-end" alignItems="flex-end">
              <Button color="error" variant="outlined" type="reset" isLoading={formik.isSubmitting}>
                Reset
              </Button>

              <Button color="primary" variant="contained" type="submit" isLoading={formik.isSubmitting}>
                Terapkan
              </Button>
            </Stack>
          </Form>
        )}
      </Formik>
    </FilterPopoverWithButton>
  )
}

export type { CashierSessionFilterFormPopoverShape }
export default CashierSessionFilterFormPopover
