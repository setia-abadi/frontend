import { Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import CashierSessionStatusChip from '../CashierSessionStatusChip'
import TypeUtil from 'utils/typeUtil'

type Props = {
  user: string | null
  startedAt: string
  endedAt: string | null
  startingCash: number
  endingCash: number | null
  status: CashierSessionStatusEnum
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const CashierSessionDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Kasir
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{props.user ? props.user : <EmptyTextPlaceholder />}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Total Tunai Awal
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <FormattedCurrency value={props.startingCash} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Total Tunai Akhir
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  {TypeUtil.isDefined(props.endingCash) ? (
                    <FormattedCurrency value={props.endingCash} />
                  ) : (
                    <EmptyTextPlaceholder />
                  )}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Dimulai Pada
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <FormattedDate date={props.startedAt} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Selesai Pada
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  {props.endedAt ? <FormattedDate date={props.endedAt} /> : <EmptyTextPlaceholder />}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Status
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <CashierSessionStatusChip status={props.status} />
              </Grid>
            </Grid>
          </Grid>

          {props.createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {props.updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default CashierSessionDetailCard
