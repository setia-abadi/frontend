import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import { FC } from 'react'

type Props = {
  status: CashierSessionStatusEnum
}

const CashierSessionStatusChip: FC<Props> = ({ status }) => {
  const text: Record<CashierSessionStatusEnum, string> = {
    [CashierSessionStatusEnum.ACTIVE]: 'Aktif',
    [CashierSessionStatusEnum.COMPLETED]: 'Selesai',
  }

  const color: Record<CashierSessionStatusEnum, ChipOwnProps['color']> = {
    [CashierSessionStatusEnum.ACTIVE]: 'warning',
    [CashierSessionStatusEnum.COMPLETED]: 'success',
  }

  return <Chip label={text[status]} size="small" color={color[status]} />
}

export default CashierSessionStatusChip
