import { Box, Stack, Typography } from '@mui/material'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import Paper from 'components/Paper'
import { Form, Formik, FormikHelpers } from 'formik'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'

import CashierSessionClock from '../CashierSessionClock'

type Props = {
  startDateTime: string
  onSubmit: () => Promise<void>
}

const CashierSessionEndFromCard: FC<Props> = ({ startDateTime, onSubmit }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (values: {}, helpers: FormikHelpers<{}>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit()
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        setError(err.response.data.message)
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardContent>
        <Stack spacing={1} mb={3}>
          <Typography variant="h4">Akhiri Sesi</Typography>
          <Typography variant="subtitle2">
            Setelah menyelesaikan shift Anda, Anda perlu mengakhiri sesi agar Admin dapat memproses shift Anda untuk
            hari ini.
          </Typography>
        </Stack>

        <Box>
          <Formik initialValues={{}} onSubmit={handleSubmit}>
            {(formik) => (
              <Form onSubmit={formik.handleSubmit}>
                <Box mt={4}>
                  <CashierSessionClock startDateTime={startDateTime} />
                </Box>

                {error && (
                  <Box mt={4}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Box>
                )}

                <Box mt={5}>
                  <Button type="submit" variant="contained" size="large" fullWidth>
                    Akhiri Sesi
                  </Button>
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
      </CardContent>
    </Paper>
  )
}

export default CashierSessionEndFromCard
