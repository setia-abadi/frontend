import { CircularProgress, IconButton, IconButtonProps } from '@mui/material'
import { IconFileDownload } from '@tabler/icons'
import { useMutation } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import CashierSessionsAPI from 'apis/cashierSessionsAPI'
import { ResponseError } from 'models/apiBase'
import { ID } from 'models/base'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import BlobUtil from 'utils/blobUtil'
import FileUtil from 'utils/fileUtil'
import StringUtil from 'utils/stringUtil'

type Props = Omit<IconButtonProps, 'onClick'> & {
  cashierSessionId: ID
}

const DownloadCashierSessionReportButton: FC<Props> = ({ cashierSessionId, disabled, ...props }) => {
  const snackbar = useSnackbar()
  const [loading, setLoading] = useState<boolean>(false)

  const { mutate } = useMutation({
    mutationFn: () => CashierSessionsAPI.downloadReport(cashierSessionId),
    onMutate: () => {
      setLoading(true)
    },
    onSettled: () => {
      setLoading(false)
    },
    onSuccess: (response) => {
      const contentDisposition = response.headers['content-disposition']
      const filename = contentDisposition
        ? FileUtil.getFilenameFromContentDisposition(contentDisposition)
        : StringUtil.uniqueId()

      FileUtil.download(response.data, filename!)
      snackbar.enqueueSnackbar('Laporan sesi kasir berhasil diunduh', { variant: 'success' })
    },
    onError: async (err) => {
      if (APIUtil.isAPIError(err)) {
        const blob = err.response?.data as unknown as Blob
        const errJson = await BlobUtil.toJson<ResponseError>(blob)
        if (errJson && errJson.message) {
          snackbar.enqueueSnackbar(errJson.message, { variant: 'error' })
        } else {
          snackbar.enqueueSnackbar(err.message, { variant: 'error' })
        }
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    },
  })

  return (
    <IconButton onClick={() => mutate()} disabled={loading || disabled} {...props}>
      {loading ? <CircularProgress color="inherit" size="1.2rem" /> : <IconFileDownload size="1.2rem" />}
    </IconButton>
  )
}

export default DownloadCashierSessionReportButton
