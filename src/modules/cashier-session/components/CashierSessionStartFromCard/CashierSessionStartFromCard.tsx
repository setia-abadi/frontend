import { Box, Stack, Typography } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import Paper from 'components/Paper'
import { CURRENCY } from 'constants/currency'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type CashierSessionStartFormCardShape = FormShape<{
  starting_cash: number
}>

type Props = {
  initialValues?: CashierSessionStartFormCardShape
  onSubmit: (values: CashierSessionStartFormCardShape) => Promise<void>
}

const CashierSessionStartFromCard: FC<Props> = ({ initialValues, onSubmit }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (
    values: CashierSessionStartFormCardShape,
    helpers: FormikHelpers<CashierSessionStartFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardContent>
        <Stack spacing={1} mb={3}>
          <Typography variant="h4">Sesi Kasir</Typography>
          <Typography variant="subtitle2">Masukkan uang tunai awal untuk memulai sesi kasir.</Typography>
        </Stack>

        <Box>
          <Formik<CashierSessionStartFormCardShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
            {(formik) => (
              <Form onSubmit={formik.handleSubmit}>
                {error && (
                  <Box mt={3}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Box>
                )}

                <Box mt={4}>
                  <FormLabel>Uang Tunai Awal</FormLabel>
                  <NumberInputField name="starting_cash" InputProps={{ startAdornment: CURRENCY }} fullWidth />
                  <FormErrorText message={formik.errors.starting_cash} />
                </Box>

                <Box mt={5}>
                  <Button type="submit" variant="contained" size="large" fullWidth>
                    Mulai Sesi
                  </Button>
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
      </CardContent>
    </Paper>
  )
}

export type { CashierSessionStartFormCardShape }

export default CashierSessionStartFromCard
