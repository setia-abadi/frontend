import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { CashierSessionModel } from 'models/cashierSession'
import CashierSessionStatusChip from '../CashierSessionStatusChip'
import { Stack } from '@mui/material'
import FormattedDate from 'components/FormattedDate'
import FormattedCurrency from 'components/FormattedCurrency'
import TypeUtil from 'utils/typeUtil'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'

type Model = CashierSessionModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'user',
    title: 'Pengguna',
    minWidth: 200,
    width: 250,
    enableSort: true,
    render: (node) => node.user!.name,
  },
  {
    key: 'starting_cash',
    title: 'Uang Tunai Awal',
    width: 175,
    render: (node) => <FormattedCurrency value={node.starting_cash} />,
  },
  {
    key: 'ending_cash',
    title: 'Uang Tunai Akhir',
    width: 175,
    render: (node) =>
      TypeUtil.isDefined(node.ending_cash) ? <FormattedCurrency value={node.ending_cash} /> : <EmptyTextPlaceholder />,
  },
  {
    key: 'shift',
    title: 'Shift ',
    width: 325,
    render: (node) => (
      <Stack direction="row" spacing={1}>
        <span>
          <FormattedDate date={node.started_at} />
        </span>
        <span>-</span>
        <span>{node.ended_at ? <FormattedDate date={node.ended_at} /> : <EmptyTextPlaceholder />}</span>
      </Stack>
    ),
  },
  {
    key: 'status',
    title: 'Status',
    width: 100,
    render: (node) => <CashierSessionStatusChip status={node.status} />,
  },
]

const CashierSessionTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default CashierSessionTable
