import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedDate from 'components/FormattedDate'
import FormattedCurrency from 'components/FormattedCurrency'
import { TransactionModel } from 'models/transaction'
import TransactionStatusChip from 'modules/transaction/components/TransactionStatusChip'

type Model = TransactionModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'total',
    title: 'Total',
    minWidth: 200,
    render: (node) => <FormattedCurrency value={node.total} />,
  },
  {
    key: 'status',
    title: 'Status',
    width: 150,
    render: (node) => <TransactionStatusChip status={node.status} />,
  },
  {
    key: 'payment_at',
    title: 'Dibayar Pada',
    width: 200,
    enableSort: true,
    render: (node) => <FormattedDate date={node.payment_at} />,
  },
  {
    key: 'created_at',
    title: 'Dibuat Pada',
    width: 200,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const CashierSessionTransactionTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default CashierSessionTransactionTable
