import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import { FC } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const CashierSessionStatusSelect: FC<Props> = (props) => {
  const text: Record<CashierSessionStatusEnum, string> = {
    [CashierSessionStatusEnum.ACTIVE]: 'Aktif',
    [CashierSessionStatusEnum.COMPLETED]: 'Selesai',
  }

  const options: SelectOption[] = Object.values(CashierSessionStatusEnum).map((status) => ({
    label: text[status],
    value: status,
  }))

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]}  />
}

export default CashierSessionStatusSelect
