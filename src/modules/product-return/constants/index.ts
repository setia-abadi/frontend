import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'

const PRODUCT_RECEIVE_STATUS_TEXT: Record<ProductReceiveStatusEnum, string> = {
  [ProductReceiveStatusEnum.CANCELED]: 'Dibatalkan',
  [ProductReceiveStatusEnum.COMPLETED]: 'Selesai',
  [ProductReceiveStatusEnum.PENDING]: 'Pending',
  [ProductReceiveStatusEnum.RETURNED]: 'Diretur',
}

export { PRODUCT_RECEIVE_STATUS_TEXT }
