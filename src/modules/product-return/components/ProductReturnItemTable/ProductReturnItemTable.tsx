import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { ProductReturnItemModel } from 'models/productReturnItem'
import FormattedNumber from 'components/FormattedNumber'
import { Box, Stack } from '@mui/material'
import Image from 'components/Image'

type Model = ProductReturnItemModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product_name',
    title: 'Nama Produk',
    minWidth: 250,
    render: (node) => (
      <Stack direction="row" spacing={1.5} alignItems="center">
        {node.product_unit?.product.image_file && (
          <Image width={50} aspectRatio={1} src={node.product_unit.product.image_file.link} />
        )}
        <Box>
          <ContentWithEmptyPlaceholder>{node.product_unit?.product.name}</ContentWithEmptyPlaceholder>
        </Box>
      </Stack>
    ),
  },
  {
    key: 'price',
    title: 'Harga per Satuan',
    width: 175,
    render: (node) => <FormattedCurrency value={node.price_per_unit} />,
  },
  {
    key: 'unit',
    title: 'Satuan',
    width: 150,
    render: (node) => <ContentWithEmptyPlaceholder>{node.product_unit?.unit?.name}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'qty',
    title: 'Kuantitas',
    width: 150,
    render: (node) => <FormattedNumber value={node.qty} />,
  },
]

const ProductReturnItemTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductReturnItemTable
