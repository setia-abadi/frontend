import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { PRODUCT_RECEIVE_STATUS_TEXT } from 'modules/product-return/constants'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const ProductReturnStatusSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(ProductReturnStatusEnum).map<SelectOption>((status) => ({
      label: PRODUCT_RECEIVE_STATUS_TEXT[status],
      value: status,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default ProductReturnStatusSelect
