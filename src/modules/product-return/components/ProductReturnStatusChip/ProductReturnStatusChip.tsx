import { ChipOwnProps, ChipProps } from '@mui/material'
import Chip from 'components/Chip'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { PRODUCT_RECEIVE_STATUS_TEXT } from 'modules/product-return/constants'
import { FC } from 'react'

type Props = Omit<ChipProps, 'color' | 'label'> & {
  status: ProductReturnStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status, ...props }) => {
  const color: Record<ProductReturnStatusEnum, ChipOwnProps['color']> = {
    [ProductReturnStatusEnum.COMPLETED]: 'success',
    [ProductReturnStatusEnum.PENDING]: 'warning',
  }

  return <Chip label={PRODUCT_RECEIVE_STATUS_TEXT[status]} size="small" color={color[status]} {...props} />
}

export default CustomerDebtStatusChip
