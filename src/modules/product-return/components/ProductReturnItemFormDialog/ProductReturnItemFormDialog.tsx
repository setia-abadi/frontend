import { DialogContentText, Grid } from '@mui/material'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import Alert from 'components/Alert'
import Button from 'components/Button'
import DialogActions from 'components/DialogActions'
import DialogContent from 'components/DialogContent'
import DialogTitle from 'components/DialogTitle'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import { SelectOption } from 'components/Select'
import { PermissionEnum } from 'enums/permissionEnum'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape, ID } from 'models/base'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductForProductReceiveItemFormSelect from 'modules/product/components/ProductForProductReceiveItemFormSelect'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type ProductReturnItemFormDialogShape = FormShape<{
  product_id: SelectOption
  qty: number
}>

type Props = {
  open: boolean
  dialogTitle: string
  productReturnId: ID
  disabledFields?: Partial<DisabledFields<ProductReturnItemFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ProductReturnItemFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductReturnItemFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductReturnItemFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  productReturnId,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductReturnItemFormDialogShape,
    helpers: FormikHelpers<ProductReturnItemFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductReturnItemFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <PermissionControl renderError match={[PermissionEnum.PRODUCT_OPTION_FOR_PRODUCT_RECEIVE_ITEM_FORM]}>
                    <FormLabel required>Product</FormLabel>
                    <ProductForProductReceiveItemFormSelect
                      productReceiveId={productReturnId}
                      name="product_id"
                      disabled={disabledFields?.product_id}
                    />
                    <FormErrorText message={formik.errors.product_id} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Kuantitas</FormLabel>
                  <NumberInputField name="qty" disabled={disabledFields?.qty} fullWidth />
                  <FormErrorText message={formik.errors.qty} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductReturnItemFormDialogShape }

export default ProductReturnItemFormDialog
