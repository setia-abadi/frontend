import ProductUnitsAPI from 'apis/productUnitsAPI'
import AsyncSelectField, { AsyncSelectFieldProps } from 'components/AsyncSelectField'
import { SelectOption } from 'components/Select'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { ID } from 'models/base'
import { useId } from 'react'
import APIUtil from 'utils/APIUtil'

type Props<Multiple extends boolean> = Omit<AsyncSelectFieldProps<Multiple>, 'fetchOptions'> & {
  productId: ID
}

const ProductUnitForProductReceiveItemSelect = <Multiple extends boolean = false>({
  productId,
  ...props
}: Props<Multiple>) => {
  const id = useId()
  const fetchOptions: AsyncSelectFieldProps<Multiple>['fetchOptions'] = async ({ page, phrase, signal }) => {
    const response = await APIUtil.withConfig(
      () =>
        ProductUnitsAPI.getOptionsForProductReceiveItemForm({
          page,
          phrase,
          limit: LIMIT_PER_PAGE,
          product_id: productId,
        }),
      { signal }
    )

    if (response.data.data) {
      return {
        ...response.data.data,
        nodes: response.data.data.nodes.map<SelectOption>((node) => ({
          label: node.unit?.name ?? '-- unloaded --',
          value: node.unit_id,
        })),
      }
    }

    return undefined
  }

  return <AsyncSelectField key={`${id}-${productId}`} {...props} fetchOptions={fetchOptions} />
}

export default ProductUnitForProductReceiveItemSelect
