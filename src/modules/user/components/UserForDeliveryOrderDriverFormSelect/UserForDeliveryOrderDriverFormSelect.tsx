import UsersAPI from 'apis/usersAPI'
import AsyncSelectField, { AsyncSelectFieldProps } from 'components/AsyncSelectField'
import { SelectOption } from 'components/Select'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { ID } from 'models/base'

type Props<Multiple extends boolean> = Omit<AsyncSelectFieldProps<Multiple>, 'fetchOptions'> & {
  deliveryOrderId: ID
}

const UserForDeliveryOrderDriverFormSelect = <Multiple extends boolean = false>({
  deliveryOrderId,
  ...props
}: Props<Multiple>) => {
  const fetchOptions: AsyncSelectFieldProps<Multiple>['fetchOptions'] = async ({ page, phrase, signal }) => {
    const response = await UsersAPI.optionsForDeliveryOrderDriverForm(
      {
        delivery_order_id: deliveryOrderId,
        limit: LIMIT_PER_PAGE,
        page,
        phrase,
      },
      { signal }
    )

    if (response.data.data) {
      return {
        ...response.data.data,
        nodes: response.data.data.nodes.map<SelectOption>((node) => ({
          label: node.name,
          value: node.id,
        })),
      }
    }

    return undefined
  }

  return <AsyncSelectField {...props} fetchOptions={fetchOptions} />
}

export default UserForDeliveryOrderDriverFormSelect
