import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import RoleSelectFieldForUserForm from 'modules/role/components/RoleSelectFieldForUserForm'
import { Form, Formik, FormikHelpers } from 'formik'
import { SelectOption } from 'components/Select'
import { FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'

type UserRoleFormDialogShape = FormShape<{
  role_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  dialogSubTitle?: string
  initialValues?: UserRoleFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: UserRoleFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const UserRoleFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (values: UserRoleFormDialogShape, helpers: FormikHelpers<UserRoleFormDialogShape>) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<UserRoleFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">{error}</Alert>
                </Box>
              )}

              <Box mt={3}>
                <RoleSelectFieldForUserForm name="role_id" />
                <FormErrorText message={formik.errors.role_id} />
              </Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { UserRoleFormDialogShape }

export default UserRoleFormDialog
