import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import RadioGroupField from 'components/RadioGroupField'
import TextInputField from 'components/TextInputField'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type UserFormCardShape = FormShape<{
  name: string
  username: string
  password: string
  is_active: boolean
}>

type Props = {
  cardTitle: string
  initialValues?: UserFormCardShape
  onSubmit: (values: UserFormCardShape) => Promise<void>
  onCancel: () => void
}

const UserFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: UserFormCardShape = {
    is_active: true,
  }

  const handleSubmit = async (values: UserFormCardShape, helpers: FormikHelpers<UserFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<UserFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Username</FormLabel>
                  <TextInputField name="username" fullWidth />
                  <FormErrorText message={formik.errors.username} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Password</FormLabel>
                  <TextInputField name="password" type="password" fullWidth />
                  <FormErrorText message={formik.errors.password} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Status</FormLabel>
                  <RadioGroupField
                    row={true}
                    name="is_active"
                    options={[
                      { label: 'Aktif', value: true },
                      { label: 'Tidak Aktif', value: false },
                    ]}
                  />
                  <FormErrorText message={formik.errors.is_active} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { UserFormCardShape }
export default UserFormCard
