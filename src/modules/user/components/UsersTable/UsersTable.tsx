import StatusChip from 'components/StatusChip'
import Table, { ColumnProps, TableProps } from 'components/Table'
import { UserModel } from 'models/user'
import { FC, ReactNode, useMemo } from 'react'

type Model = UserModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'username',
    title: 'Username',
    width: 250,
    enableSort: true,
    render: (node) => node.username,
  },
  {
    key: 'name',
    title: 'Nama',
    minWidth: 200,
    render: (node) => node.name,
  },
  {
    key: 'active',
    title: 'Status',
    width: 150,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const UsersTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 100,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default UsersTable
