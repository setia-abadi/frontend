import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import { SelectOption } from 'components/Select'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import AsyncSelect from 'components/AsyncSelect'
import TextInputField from 'components/TextInputField'

type DeliveryOrderFormCardShape = FormShape<{
  delivery_order_id: SelectOption
  reason: string
}>

type Props = {
  cardTitle: string
  initialValues?: DeliveryOrderFormCardShape
  disabledFields?: Partial<DisabledFields<DeliveryOrderFormCardShape>>
  onSubmit: (values: DeliveryOrderFormCardShape) => Promise<void>
  onCancel: () => void
}

const DeliveryOrderFormCard: FC<Props> = ({ cardTitle, initialValues, disabledFields, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (
    values: DeliveryOrderFormCardShape,
    helpers: FormikHelpers<DeliveryOrderFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<DeliveryOrderFormCardShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Pesanan</FormLabel>
                  <AsyncSelect
                    name="delivery_order_id"
                    fetchOptions={async () => undefined}
                    disabled={disabledFields?.delivery_order_id}
                    fullWidth
                  />
                  <FormErrorText message={formik.errors.delivery_order_id} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Alasan Pengembalian</FormLabel>
                  <TextInputField name="reason" multiline rows={5} disabled={disabledFields?.reason} fullWidth />
                  <FormErrorText message={formik.errors.reason} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { DeliveryOrderFormCardShape }
export default DeliveryOrderFormCard
