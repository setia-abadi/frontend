import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { ReturnOrderStatusEnum } from 'enums/returnOrderStatusEnum'
import { RETURN_ORDER_STATUS_TEXT } from 'modules/return-order/constants'
import { FC } from 'react'

type Props = {
  status: ReturnOrderStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status }) => {
  const color: Record<ReturnOrderStatusEnum, ChipOwnProps['color']> = {
    [ReturnOrderStatusEnum.PENDING]: 'warning',
    [ReturnOrderStatusEnum.RETURNED]: 'success',
  }

  return <Chip label={RETURN_ORDER_STATUS_TEXT[status]} size="small" color={color[status]} />
}

export default CustomerDebtStatusChip
