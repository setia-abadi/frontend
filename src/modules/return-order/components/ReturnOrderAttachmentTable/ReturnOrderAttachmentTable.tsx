import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { Box, Link, Stack, styled } from '@mui/material'
import FormattedDate from 'components/FormattedDate'
import { FileModel } from 'models/file'
import { IconFile } from '@tabler/icons'

const ItemIcon = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.grey[100],
  border: `1px solid ${theme.palette.grey[300]}`,
  width: 40,
  height: 40,
}))

const Icon = styled(IconFile)(({ theme }) => ({
  color: theme.palette.grey[400],
  width: '50%',
}))

type Model = FileModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'image',
    title: 'Gambar',
    minWidth: 300,
    width: 350,
    render: (node) => (
      <Stack direction="row" alignItems="center" spacing={1.5}>
        <ItemIcon>
          <Icon />
        </ItemIcon>
        <Link href={node.link} target="_blank">{node.name}</Link>
      </Stack>
    ),
  },
  {
    key: 'created_at',
    title: 'Dibuat Pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const ReturnOrderAttachmentTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ReturnOrderAttachmentTable
