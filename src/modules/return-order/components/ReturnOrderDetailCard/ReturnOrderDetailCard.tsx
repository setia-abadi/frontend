import { Divider, Grid, Link, Stack, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { NavLink } from 'react-router-dom'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import { IconFileInvoice } from '@tabler/icons'
import ReturnOrderStatusChip from '../ReturnOrderStatusChip'
import { ReturnOrderStatusEnum } from 'enums/returnOrderStatusEnum'

type Props = {
  deliveryOrderId: string
  invoiceNumber: string
  reason: string
  date: string | null
  status: ReturnOrderStatusEnum
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const DeliveryOrderDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Pesanan
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <NavLink to={`/delivery-orders/${props.deliveryOrderId}`}>
                    <Link>
                      <Stack direction="row" alignItems="center" gap={0.5}>
                        <IconFileInvoice size="1.1rem" />
                        <span>{props.invoiceNumber}</span>
                      </Stack>
                    </Link>
                  </NavLink>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Alasan
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  <ContentWithEmptyPlaceholder>{props.reason}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Tanggal Pengembalian
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  {props.date ? <FormattedDate date={props.date} /> : <EmptyTextPlaceholder />}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Status
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <ReturnOrderStatusChip status={props.status} />
              </Grid>
            </Grid>
          </Grid>

          {props.createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {props.updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={props.updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default DeliveryOrderDetailCard
