import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedDate from 'components/FormattedDate'
import ReturnOrderStatusChip from '../ReturnOrderStatusChip'
import { ReturnOrderModel } from 'models/returnOrder'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import { NavLink } from 'react-router-dom'
import { Link, Stack } from '@mui/material'
import { IconFileInvoice } from '@tabler/icons'

type Model = ReturnOrderModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'invoice_number',
    title: 'Pesanan',
    minWidth: 200,
    render: (node) =>
      node.delivery_order ? (
        <NavLink to={`/delivery-orders/${node.delivery_order_id}`}>
          <Link>
            <Stack direction="row" alignItems="center" gap={0.5}>
              <IconFileInvoice size="1.1rem" />
              <span>{node.delivery_order.invoice_number}</span>
            </Stack>
          </Link>
        </NavLink>
      ) : (
        <EmptyTextPlaceholder />
      ),
  },
  {
    key: 'status',
    title: 'Status',
    width: 175,
    render: (node) => <ReturnOrderStatusChip status={node.status} />,
  },
  {
    key: 'date',
    title: 'Dikembalikan Pada',
    width: 175,
    render: (node) => (node.date ? <FormattedDate date={node.date} /> : <EmptyTextPlaceholder />),
  },
  {
    key: 'created_at',
    title: 'Dibuat pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const DeliveryOrderTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default DeliveryOrderTable
