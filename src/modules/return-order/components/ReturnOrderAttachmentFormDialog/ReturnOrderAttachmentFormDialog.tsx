import { DialogContentText } from '@mui/material'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import Alert from 'components/Alert'
import Button from 'components/Button'
import DialogActions from 'components/DialogActions'
import DialogContent from 'components/DialogContent'
import DialogTitle from 'components/DialogTitle'
import DropzoneField from 'components/DropzoneField'
import { SelectOption } from 'components/Select'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type ReturnOrderAttachmentFormDialogShape = FormShape<{
  base_scale: number
  scale: number
  unit_id: SelectOption
  to_unit_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  disabledFields?: Partial<DisabledFields<ReturnOrderAttachmentFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ReturnOrderAttachmentFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ReturnOrderAttachmentFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ReturnOrderAttachmentFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  disabledFields,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ReturnOrderAttachmentFormDialogShape,
    helpers: FormikHelpers<ReturnOrderAttachmentFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ReturnOrderAttachmentFormDialogShape>
        initialValues={{ base_scale: 1, ...initialValues }}
        onSubmit={handleSubmit}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Box mt={3}>
                <DropzoneField select={() => ''} uploadFn={async () => null} name="_" />
              </Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ReturnOrderAttachmentFormDialogShape }

export default ReturnOrderAttachmentFormDialog
