import { ReturnOrderStatusEnum } from "enums/returnOrderStatusEnum";

const RETURN_ORDER_STATUS_TEXT: Record<ReturnOrderStatusEnum, string> = {
  [ReturnOrderStatusEnum.PENDING]: 'Pending',
  [ReturnOrderStatusEnum.RETURNED]: 'Telah Dikembalikan',
}

export { RETURN_ORDER_STATUS_TEXT }