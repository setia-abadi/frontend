import dayjs from "dayjs";
import ArrayUtil from "utils/arrayUtil";
import StringUtil from "utils/stringUtil";
import { faker } from '@faker-js/faker';
import { FileModel } from "models/file";
import { ReturnOrderModel } from "models/returnOrder";
import { ReturnOrderStatusEnum } from "enums/returnOrderStatusEnum";
import { DeliveryOrderModel } from "models/deliveryOrder";

const ReturnOrderUtil = {
  randomAttachmentRecord: (): FileModel => {
    return {
      created_at: dayjs().toISOString(),
      updated_at: dayjs().toISOString(),
      id: StringUtil.uuid(),
      link: faker.image.url(),
      name: faker.word.words(3) + '.png',
      path: faker.image.url(),
    }
  },

  randomRecord: (): ReturnOrderModel => {
    const status = ArrayUtil.pickRandom(Object.values(ReturnOrderStatusEnum))

    return {
      created_at: dayjs().toISOString(),
      updated_at: dayjs().toISOString(),
      id: StringUtil.uuid(),
      attachments: Array.from({ length: 5 }, () => ReturnOrderUtil.randomAttachmentRecord()),
      date: status === ReturnOrderStatusEnum.RETURNED ? dayjs().toISOString() : null,
      delivery_order: { invoice_number: StringUtil.random(15).toUpperCase() } as DeliveryOrderModel,
      delivery_order_id: StringUtil.uuid(),
      reason: faker.word.words(10),
      status,
    }
  }
}

export default ReturnOrderUtil