import { DeliveryOrderReviewTypeEnum } from "enums/deliveryOrderReviewTypeEnum";
import { DeliveryOrderStatusEnum } from "enums/deliveryOrderStatusEnum";

const DELIVERY_ORDER_STATUS_TEXT: Record<DeliveryOrderStatusEnum, string> = {
  [DeliveryOrderStatusEnum.CANCELED]: 'Dibatalkan',
  [DeliveryOrderStatusEnum.COMPLETED]: 'Selesai',
  [DeliveryOrderStatusEnum.DELIVERING]: 'Dalam Pengiriman',
  [DeliveryOrderStatusEnum.ONGOING]: 'Sedang Berlangsung',
  [DeliveryOrderStatusEnum.PENDING]: 'Pending',
  [DeliveryOrderStatusEnum.RETURNED]: 'Diretur',
}

const DELIVERY_ORDER_REVIEW_TYPE_TEXT: Record<DeliveryOrderReviewTypeEnum, string> = {
  [DeliveryOrderReviewTypeEnum.DELIVERY]: 'Pengiriman',
  [DeliveryOrderReviewTypeEnum.PRODUCT]: 'Kualitas Produk',
}

export { DELIVERY_ORDER_STATUS_TEXT, DELIVERY_ORDER_REVIEW_TYPE_TEXT }