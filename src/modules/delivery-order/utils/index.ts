import dayjs from "dayjs";
import StringUtil from "utils/stringUtil";
import { faker } from '@faker-js/faker';
import { DeliveryOrderReviewModel } from "models/deliveryOrderReview";
import { DeliveryOrderModel } from "models/deliveryOrder";
import ArrayUtil from "utils/arrayUtil";
import { DeliveryOrderReviewTypeEnum } from "enums/deliveryOrderReviewTypeEnum";

const DeliveryOrderUtil = {
  randomDeliveryOrderReviewRecord: (): DeliveryOrderReviewModel => {
    return {
      created_at: dayjs().toISOString(),
      id: StringUtil.uuid(),
      updated_at: dayjs().toISOString(),
      delivery_order: { invoice_number: StringUtil.random(15).toUpperCase() } as DeliveryOrderModel,
      delivery_order_id: StringUtil.uuid(),
      description: faker.word.words(20),
      type: ArrayUtil.pickRandom(Object.values(DeliveryOrderReviewTypeEnum))
    }
  }
}

export default DeliveryOrderUtil