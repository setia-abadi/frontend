import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { DeliveryOrderReviewModel } from 'models/deliveryOrderReview'
import { Link, Stack } from '@mui/material'
import FormattedDate from 'components/FormattedDate'
import { NavLink } from 'react-router-dom'
import { IconFileInvoice } from '@tabler/icons'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import DeliveryOrderReviewTypeStatusChip from '../DeliveryOrderReviewTypeStatusChip'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'

type Model = DeliveryOrderReviewModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'invoice_number',
    title: 'Pesanan',
    width: 200,
    render: (node) =>
      node.delivery_order ? (
        <PermissionControl fallback={node.delivery_order.invoice_number} match={[PermissionEnum.DELIVERY_ORDER_GET]}>
          <NavLink to={`/delivery-orders/${node.delivery_order_id}`}>
            <Link>
              <Stack direction="row" alignItems="center" gap={0.5}>
                <IconFileInvoice size="1.1rem" />
                <span>{node.delivery_order.invoice_number}</span>
              </Stack>
            </Link>
          </NavLink>
        </PermissionControl>
      ) : (
        <EmptyTextPlaceholder />
      ),
  },
  {
    key: 'type',
    title: 'Jenis Laporan',
    width: 150,
    render: (node) => <DeliveryOrderReviewTypeStatusChip status={node.type} />,
  },
  {
    key: 'description',
    title: 'Deskripsi',
    minWidth: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.description}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'created_at',
    title: 'Dibuat Pada',
    width: 175,
    enableSort: true,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const DeliveryOrderReportTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default DeliveryOrderReportTable
