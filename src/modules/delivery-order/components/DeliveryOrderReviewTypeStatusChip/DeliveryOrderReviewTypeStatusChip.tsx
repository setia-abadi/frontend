import Chip from 'components/Chip'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'
import { DELIVERY_ORDER_REVIEW_TYPE_TEXT } from 'modules/delivery-order/constants'
import { FC } from 'react'

type Props = {
  status: DeliveryOrderReviewTypeEnum
}

const DeliveryOrderReviewTypeStatusChip: FC<Props> = ({ status }) => {
  return <Chip label={DELIVERY_ORDER_REVIEW_TYPE_TEXT[status]} size="small" color="default" />
}

export default DeliveryOrderReviewTypeStatusChip
