import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import FormattedNumber from 'components/FormattedNumber'
import { Box, Stack } from '@mui/material'
import Image from 'components/Image'
import { DeliveryOrderItemModel } from 'models/deliveryOrderitem'
import Typography from 'components/Typography'

type Model = DeliveryOrderItemModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product_name',
    title: 'Nama Produk',
    minWidth: 250,
    render: (node) => (
      <Stack direction="row" spacing={1.5} alignItems="center">
        {node.product_unit?.product.image_file && (
          <Image width={50} aspectRatio={1} src={node.product_unit.product.image_file.link} />
        )}
        <Box>
          <ContentWithEmptyPlaceholder>{node.product_unit?.product.name}</ContentWithEmptyPlaceholder>
        </Box>
      </Stack>
    ),
  },
  {
    key: 'qty',
    title: 'Kuantitas',
    width: 150,
    render: (node) => (
      <Stack direction="row" spacing={1}>
        <div>
          <FormattedNumber value={node.qty} />
        </div>
        <div>{node.product_unit?.unit ? node.product_unit.unit.name : null}</div>
      </Stack>
    ),
  },
  {
    key: 'price_per_unit',
    title: 'Harga Per Satuan',
    width: 175,
    render: (node) => <FormattedCurrency value={node.price_per_unit} />,
  },
  {
    key: 'total_price',
    title: 'Harga Total',
    width: 175,
    render: (node) => {
      const subTotal = node.price_per_unit * node.qty

      if (subTotal !== node.price_total) {
        return (
          <Stack spacing={0.5}>
            <Typography sx={{ textDecoration: 'line-through' }}>
              <FormattedCurrency value={subTotal} />
            </Typography>
            <FormattedCurrency value={node.price_total} />
          </Stack>
        )
      }

      return <FormattedCurrency value={node.price_total} />
    },
  },
]

const DeliveryOrderItemTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default DeliveryOrderItemTable
