import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import { DELIVERY_ORDER_STATUS_TEXT } from 'modules/delivery-order/constants'
import { FC } from 'react'

type Props = {
  status: DeliveryOrderStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status }) => {
  const color: Record<DeliveryOrderStatusEnum, ChipOwnProps['color']> = {
    [DeliveryOrderStatusEnum.CANCELED]: 'default',
    [DeliveryOrderStatusEnum.COMPLETED]: 'success',
    [DeliveryOrderStatusEnum.ONGOING]: 'info',
    [DeliveryOrderStatusEnum.PENDING]: 'warning',
    [DeliveryOrderStatusEnum.DELIVERING]: 'primary',
    [DeliveryOrderStatusEnum.RETURNED]: 'error',
  }

  return <Chip label={DELIVERY_ORDER_STATUS_TEXT[status]} size="small" color={color[status]} />
}

export default CustomerDebtStatusChip
