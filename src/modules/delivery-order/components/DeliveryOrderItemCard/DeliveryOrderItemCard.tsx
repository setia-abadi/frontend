import { Box, Stack, styled } from '@mui/material'
import FormattedCurrency from 'components/FormattedCurrency'
import Image from 'components/Image'
import { FC } from 'react'

type Props = {
  imageSrc: string
  name: string
  priceTotal: number
  quantity: number
  unit: string
  discount?: number
}

const Container = styled(Box)(({ theme }) => ({
  borderRadius: theme.shape.borderRadius,
  border: `1px solid ${theme.palette.grey[300]}`,
  padding: theme.spacing(1),
  display: 'flex',
  alignItems: 'center',
  gap: theme.spacing(1.5),
}))

const Title = styled('div')(({ theme }) => ({
  fontSize: theme.typography.pxToRem(14),
  fontWeight: theme.typography.fontWeightMedium,
  fontFamily: theme.typography.fontFamily,
}))

const Quantity = styled('div')(({ theme }) => ({
  fontSize: theme.typography.pxToRem(13),
  fontWeight: theme.typography.fontWeightRegular,
  fontFamily: theme.typography.fontFamily,
}))

const Price = styled('div')(({ theme }) => ({
  fontSize: theme.typography.pxToRem(13),
  fontWeight: theme.typography.fontWeightRegular,
  fontFamily: theme.typography.fontFamily,
}))

const DeliveryOrderItemCard: FC<Props> = ({ imageSrc, name, quantity, unit, priceTotal, discount }) => {
  return (
    <Container>
      <Image width={50} aspectRatio={1} src={imageSrc} alt={name} />

      <Stack spacing={0.3} flexGrow={1}>
        <Title>{name}</Title>

        <Box display="flex" justifyContent="space-between">
          <Quantity>
            {quantity} {unit}
          </Quantity>

          <Price>
            <FormattedCurrency value={priceTotal} />
          </Price>
        </Box>
      </Stack>
    </Container>
  )
}

export default DeliveryOrderItemCard
