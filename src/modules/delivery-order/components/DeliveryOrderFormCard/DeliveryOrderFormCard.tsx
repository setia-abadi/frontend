import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import DatePickerField from 'components/DatePickerField'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import { SelectOption } from 'components/Select'
import dayjs, { Dayjs } from 'dayjs'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import CustomerForDeliveryOrderFormSelect from 'modules/customer/components/CustomerForDeliveryOrderFormSelect'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type DeliveryOrderFormCardShape = FormShape<{
  date: Dayjs
  customer_id: SelectOption
}>

type Props = {
  cardTitle: string
  initialValues?: DeliveryOrderFormCardShape
  disabledFields?: Partial<DisabledFields<DeliveryOrderFormCardShape>>
  onSubmit: (values: DeliveryOrderFormCardShape) => Promise<void>
  onCancel: () => void
}

const DeliveryOrderFormCard: FC<Props> = ({ cardTitle, initialValues, disabledFields, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: DeliveryOrderFormCardShape = {
    date: dayjs()
  }

  const handleSubmit = async (
    values: DeliveryOrderFormCardShape,
    helpers: FormikHelpers<DeliveryOrderFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<DeliveryOrderFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Tanggal Pengiriman</FormLabel>
                  <DatePickerField name="date" disabled={disabledFields?.date} />
                  <FormErrorText message={formik.errors.date} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Customer</FormLabel>
                  <CustomerForDeliveryOrderFormSelect
                    fullWidth
                    name="customer_id"
                    disabled={disabledFields?.customer_id}
                  />
                  <FormErrorText message={formik.errors.customer_id} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { DeliveryOrderFormCardShape }
export default DeliveryOrderFormCard
