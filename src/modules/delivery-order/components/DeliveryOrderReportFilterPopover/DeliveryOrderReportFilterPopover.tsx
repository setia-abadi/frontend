import { Alert, Box, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Button from 'components/Button'
import FilterPopoverWithButton from 'components/FilterPopoverWithButton/FilterPopoverWithButton'
import FormLabel from 'components/FormLabel'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import FormErrorText from 'components/FormErrorText'
import RadioGroupField from 'components/RadioGroupField'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'
import { DELIVERY_ORDER_REVIEW_TYPE_TEXT } from 'modules/delivery-order/constants'
import { RadioOption } from 'components/RadioGroup'

type DeliveryOrderReportFilterFormPopoverShape = FormShape<{
  type: DeliveryOrderReviewTypeEnum
}>

type Props = {
  initialValues?: DeliveryOrderReportFilterFormPopoverShape
  onApply: (values: DeliveryOrderReportFilterFormPopoverShape) => void
  onReset: () => void
}

const typeOptions: RadioOption[] = [
  {
    label: 'Semua',
    value: null,
  },
  ...Object.values(DeliveryOrderReviewTypeEnum).map<RadioOption>((type) => ({
    label: DELIVERY_ORDER_REVIEW_TYPE_TEXT[type],
    value: type,
  })),
]

const DeliveryOrderReportFilterFormPopover: FC<Props> = ({ initialValues, onApply, onReset }) => {
  const [error, setError] = useState<string>()
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const handleButtonClick = () => {
    setIsOpen((prev) => !prev)
  }

  const handlePopoverClose = () => {
    setIsOpen(false)
    setError(undefined)
  }

  const handleReset = () => {
    setIsOpen(false)
    onReset()
  }

  const handleSubmit = async (
    values: DeliveryOrderReportFilterFormPopoverShape,
    helpers: FormikHelpers<DeliveryOrderReportFilterFormPopoverShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      onApply(values)
      setIsOpen(false)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <FilterPopoverWithButton isShow={isOpen} onButtonClick={handleButtonClick} onClose={handlePopoverClose}>
      <Formik<DeliveryOrderReportFilterFormPopoverShape>
        initialValues={{ ...initialValues }}
        onSubmit={handleSubmit}
        onReset={handleReset}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            {error && (
              <Box mb={3}>
                <Alert severity="error" variant="outlined">
                  {error}
                </Alert>
              </Box>
            )}

            <Stack gap={3} mb={3}>
              <Box>
                <FormLabel>Jenis Laporan</FormLabel>
                <RadioGroupField name="type" options={typeOptions} />
                <FormErrorText message={formik.errors.type} />
              </Box>
            </Stack>

            <Stack gap={1} direction="row" justifyContent="flex-end" alignItems="flex-end">
              <Button color="error" variant="outlined" type="reset" isLoading={formik.isSubmitting}>
                Reset
              </Button>

              <Button color="primary" variant="contained" type="submit" isLoading={formik.isSubmitting}>
                Terapkan
              </Button>
            </Stack>
          </Form>
        )}
      </Formik>
    </FilterPopoverWithButton>
  )
}

export type { DeliveryOrderReportFilterFormPopoverShape }
export default DeliveryOrderReportFilterFormPopover
