import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { DeliveryOrderModel } from 'models/deliveryOrder'
import DeliveryOrderStatusChip from '../DeliveryOrderStatusChip'
import FormattedCurrency from 'components/FormattedCurrency'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import FormattedDate from 'components/FormattedDate'

type Model = DeliveryOrderModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'date',
    title: 'Tanggal Pengiriman',
    width: 200,
    enableSort: true,
    render: (node) => <FormattedDate date={node.date} withTime={false} />,
  },
  {
    key: 'invoice_number',
    title: 'Nomor Faktur',
    minWidth: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.invoice_number}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'customer',
    title: 'Pelanggan',
    width: 200,
    render: (node) => node.customer!.name,
  },
  {
    key: 'total_price',
    title: 'Harga Total',
    width: 175,
    render: (node) => <FormattedCurrency value={node.total_price} />,
  },
  {
    key: 'status',
    title: 'Status',
    width: 175,
    render: (node) => <DeliveryOrderStatusChip status={node.status} />,
  },
]

const DeliveryOrderTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default DeliveryOrderTable
