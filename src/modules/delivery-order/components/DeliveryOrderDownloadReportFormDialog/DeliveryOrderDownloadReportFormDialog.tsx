import { DialogContentText, Grid } from '@mui/material'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import Alert from 'components/Alert'
import Button from 'components/Button'
import DatePickerField from 'components/DatePickerField'
import DialogActions from 'components/DialogActions'
import DialogContent from 'components/DialogContent'
import DialogTitle from 'components/DialogTitle'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import { Dayjs } from 'dayjs'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type DeliveryOrderDownloadReportFormDialogShape = FormShape<{
  start_date: Dayjs
  end_date: Dayjs
}>

type Props = {
  open: boolean
  dialogTitle: string
  disabledFields?: Partial<DisabledFields<DeliveryOrderDownloadReportFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: DeliveryOrderDownloadReportFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: DeliveryOrderDownloadReportFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const DeliveryOrderDownloadReportFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: DeliveryOrderDownloadReportFormDialogShape,
    helpers: FormikHelpers<DeliveryOrderDownloadReportFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<DeliveryOrderDownloadReportFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12} md={6}>
                  <FormLabel required>Tanggal Mulai</FormLabel>
                  <DatePickerField name="start_date" disabled={disabledFields?.start_date} />
                  <FormErrorText message={formik.errors.start_date} />
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormLabel required>Tanggal Berakhir</FormLabel>
                  <DatePickerField
                    name="end_date"
                    disabled={disabledFields?.end_date}
                    minDate={formik.values.start_date ?? undefined}
                  />
                  <FormErrorText message={formik.errors.end_date} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Unduh
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { DeliveryOrderDownloadReportFormDialogShape }

export default DeliveryOrderDownloadReportFormDialog
