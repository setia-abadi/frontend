import { Box } from '@mui/material'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import FormErrorText from 'components/FormErrorText'
import Paper from 'components/Paper'
import RadioGroupField from 'components/RadioGroupField'
import TextInputField from 'components/TextInputField'
import Typography from 'components/Typography'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { DELIVERY_ORDER_REVIEW_TYPE_TEXT } from 'modules/delivery-order/constants'
import { FC, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type DeliveryOrderReportFormCardShape = FormShape<{
  type: DeliveryOrderReviewTypeEnum
  description: string
}>

type Props = {
  initialValues?: DeliveryOrderReportFormCardShape
  onSubmit: (values: DeliveryOrderReportFormCardShape) => Promise<void>
}

const defaultValues: DeliveryOrderReportFormCardShape = {
  type: DeliveryOrderReviewTypeEnum.PRODUCT,
}

const DeliveryOrderReportFormCard: FC<Props> = ({ initialValues, onSubmit }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (
    values: DeliveryOrderReportFormCardShape,
    helpers: FormikHelpers<DeliveryOrderReportFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Formik<DeliveryOrderReportFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
      {(formik) => (
        <Form onSubmit={formik.handleSubmit}>
          <Paper>
            <CardContent>
              <Typography variant="h3">Laporkan Masalah</Typography>

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Box mt={3}>
                <Typography fontWeight={600}>Apa jenis masalah yang Anda laporkan?</Typography>
                <RadioGroupField
                  name="type"
                  options={Object.values(DeliveryOrderReviewTypeEnum).map((type) => ({
                    label: DELIVERY_ORDER_REVIEW_TYPE_TEXT[type],
                    value: type,
                  }))}
                />
                <FormErrorText message={formik.errors.type} />
              </Box>

              <Box mt={3}>
                <Typography fontWeight={600}>Ceritakan masalah yang anda alami</Typography>
                <TextInputField name="description" multiline rows={5} fullWidth />
                <FormErrorText message={formik.errors.description} />
              </Box>

              <Box mt={3}>
                <Button type="submit" variant="contained" fullWidth>
                  Kirim
                </Button>
              </Box>
            </CardContent>
          </Paper>
        </Form>
      )}
    </Formik>
  )
}

export type { DeliveryOrderReportFormCardShape }

export default DeliveryOrderReportFormCard
