import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import { DELIVERY_ORDER_STATUS_TEXT } from 'modules/delivery-order/constants'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const DeliveryOrderStatusSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(DeliveryOrderStatusEnum).map<SelectOption>((status) => ({
      label: DELIVERY_ORDER_STATUS_TEXT[status],
      value: status,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default DeliveryOrderStatusSelect
