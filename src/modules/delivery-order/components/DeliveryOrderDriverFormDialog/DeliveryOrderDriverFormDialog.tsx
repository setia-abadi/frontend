import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { SelectOption } from 'components/Select'
import { DisabledFields, FormShape, ID } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'
import FormLabel from 'components/FormLabel'
import UserForDeliveryOrderDriverFormSelect from 'modules/user/components/UserForDeliveryOrderDriverFormSelect'
import FormErrorText from 'components/FormErrorText'

type DeliveryOrderDriverFormDialogShape = FormShape<{
  driver_user_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  deliveryOrderId: ID
  disabledFields?: Partial<DisabledFields<DeliveryOrderDriverFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: DeliveryOrderDriverFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: DeliveryOrderDriverFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const DeliveryOrderDriverFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  deliveryOrderId,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: DeliveryOrderDriverFormDialogShape,
    helpers: FormikHelpers<DeliveryOrderDriverFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<DeliveryOrderDriverFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <PermissionControl match={[PermissionEnum.USER_OPTION_FOR_DELIVERY_ORDER_DRIVER_FORM]}>
                    <FormLabel required>Driver</FormLabel>
                    <UserForDeliveryOrderDriverFormSelect
                      deliveryOrderId={deliveryOrderId}
                      name="driver_user_id"
                      disabled={disabledFields?.driver_user_id}
                    />
                    <FormErrorText message={formik.errors.driver_user_id} />
                  </PermissionControl>
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { DeliveryOrderDriverFormDialogShape }

export default DeliveryOrderDriverFormDialog
