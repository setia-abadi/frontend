import { ID } from 'models/base'
import { FC, PropsWithChildren, useEffect, useRef } from 'react'
import L from 'leaflet'
import 'leaflet-rotatedmarker'
import 'leaflet-routing-machine'
import 'leaflet/dist/leaflet.css'
import { Response } from 'models/apiBase'
import truckIcon from 'assets/images/map/truck-marker.png'
import pinpointIcon from 'assets/images/map/pinpoint.png'
import { Box, styled } from '@mui/material'
import { DeliveryOrderPositionModel } from 'models/deliveryOrderDriverPosition'

type Props = {
  deliveryOrderId: ID
  latitute: number
  longitude: number
}

const driverIcon = L.icon({
  iconUrl: truckIcon,
  iconSize: [20, 60], // size of the icon
  iconAnchor: [10, 30], // point of the icon which will correspond to marker's location
})

const destinationIcon = L.icon({
  iconUrl: pinpointIcon,
  iconSize: [45, 45], // size of the icon
  iconAnchor: [20, 33], // point of the icon which will correspond to marker's location
})

const copyright = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
})

const Map = styled(Box)(() => ({
  position: 'relative',
  '& .leaflet-control-container .leaflet-routing-container-hide': {
    display: 'none',
  },
}))

const TrackingMap: FC<PropsWithChildren<Props>> = ({ deliveryOrderId, latitute, longitude, children }) => {
  const containerRef = useRef<HTMLDivElement>(null)
  const mapRef = useRef<L.Map>()
  const controlRef = useRef<L.Routing.Control>()

  const handleNextMessage = (event: MessageEvent) => {
    const destinationWaypoint = L.Routing.waypoint(L.latLng([latitute, longitude]), 'destination')
    const response = JSON.parse(event.data) as Response<{ delivery_order_position: DeliveryOrderPositionModel | null }>
    const data = response.data.delivery_order_position
    if (data && controlRef.current) {
      const bearingString = data.bearing.toString()
      const driverWaypoint = L.Routing.waypoint(L.latLng(data.latitude, data.longitude), `driver-${bearingString}`)
      controlRef.current.setWaypoints([driverWaypoint, destinationWaypoint])
    }
  }

  useEffect(() => {
    if (mapRef.current || !containerRef.current) return

    const map = new L.Map(containerRef.current, {
      center: L.latLng(3.5760921155682777, 98.68760567958049),
      zoomControl: false,
      zoom: 20,
    })

    const control = L.Routing.control({
      addWaypoints: false,
      routeWhileDragging: false,
      fitSelectedRoutes: true,
      lineOptions: {
        extendToWaypoints: false,
        missingRouteTolerance: 0,
        styles: [{ color: 'green', opacity: 1, weight: 6 }],
      },
      plan: L.Routing.plan([], {
        createMarker: (_, waypoint: L.Routing.Waypoint) => {
          if (waypoint.name?.startsWith('driver')) {
            const bearing = parseInt(waypoint.name.split('-')[1])
            return L.marker(waypoint.latLng, { icon: driverIcon, rotationAngle: bearing })
          }
          if (waypoint.name === 'destination') {
            return L.marker(waypoint.latLng, { icon: destinationIcon })
          }
          return false
        },
      }),
    })

    copyright.addTo(map)
    control.addTo(map)
    control.hide()

    mapRef.current = map
    controlRef.current = control
  }, [])

  useEffect(() => {
    const eventSource = new EventSource(process.env.REACT_APP_API_URL + `ssr/maps/${deliveryOrderId}`)
    eventSource.onmessage = handleNextMessage
    return () => eventSource.close()
  }, [])

  return (
    <Map width="100vw" height="100vh" ref={containerRef}>
      {children}
    </Map>
  )
}

export default TrackingMap
