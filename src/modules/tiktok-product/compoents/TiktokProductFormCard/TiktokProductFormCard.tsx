import { Box, Divider, Grid, InputAdornment, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import TextInputField from 'components/TextInputField'
import UploadImageField from 'components/UploadImageField'
import { FieldArray, Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import TiktokProductsAPI from 'apis/tiktokProductsAPI'
import TiktokProductCategoryCascader from '../TiktokProductCategoryCascader'
import TiktokProductRecommendCategoryInput from '../TiktokProductRecommendCategoryInput'
import RichTextEditorField from 'components/RichTextEditorField'
import TiktokProductBrandSelect from '../TiktokProductBrandSelect'
import TiktokProductAttributesInput from '../TiktokProductAttributesInput'
import { SelectOption } from 'components/Select'
import NumberInputField from 'components/NumberInputField'

type TiktokProductFormCardShape = FormShape<{
  title: string
  image_uri: { value?: string; placeholder?: string }[]
  description: string
  category_id: string[]
  brand_id: SelectOption
  attributes: { id: string; values: SelectOption[] }[]
  dimension_height: number
  dimension_length: number
  dimension_width: number
  size_chart_uri: string
  weight: number
}>

type Props = {
  cardTitle: string
  initialValues?: TiktokProductFormCardShape
  onSubmit: (values: TiktokProductFormCardShape) => Promise<void>
  onCancel: () => void
}

const TiktokProductFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: TiktokProductFormCardShape = {
    image_uri: [{ value: '' }],
  }

  const handleSubmit = async (
    values: TiktokProductFormCardShape,
    helpers: FormikHelpers<TiktokProductFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<TiktokProductFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel>Gambar</FormLabel>
                  <FieldArray name="image_uri">
                    {() => (
                      <>
                        {formik.values.image_uri && (
                          <Box display="flex" gap={1}>
                            {formik.values.image_uri.map((image, index) => (
                              <UploadImageField
                                key={index}
                                name={`image_uri.${index}.value`}
                                aspectRatio={1 / 1}
                                containerWidth={125}
                                imageWidth={300}
                                imageHeight={300}
                                initialImageSrc={image.placeholder}
                                uploadFn={(file) => TiktokProductsAPI.uploadImage({ file })}
                                select={(data) => data.data.data.uri}
                              />
                            ))}
                          </Box>
                        )}
                      </>
                    )}
                  </FieldArray>
                  <FormErrorText message={formik.errors.image_uri} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Nama Produk</FormLabel>
                  <TextInputField name="title" fullWidth />
                  <FormErrorText message={formik.errors.title} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Kategori</FormLabel>
                  {formik.values.title && (
                    <Box my={2}>
                      <TiktokProductRecommendCategoryInput
                        name="category_id"
                        productTitle={formik.values.title}
                        onClick={() => {
                          formik.setFieldValue('brand_id', null)
                          formik.setFieldValue('attributes', [])
                        }}
                      />
                    </Box>
                  )}
                  <TiktokProductCategoryCascader fullWidth name="category_id" />
                  <FormErrorText message={formik.errors.category_id} />
                </Grid>

                {formik.values.category_id && (
                  <>
                    <Grid item xs={12}>
                      <FormLabel required>Merek</FormLabel>
                      <TiktokProductBrandSelect categoryId={formik.values.category_id.at(-1)} name="brand_id" />
                      <FormErrorText message={formik.errors.brand_id} />
                    </Grid>

                    <Grid item xs={12}>
                      <FormLabel required>Atribut</FormLabel>
                      <TiktokProductAttributesInput categoryId={formik.values.category_id.at(-1)} name="attributes" />
                      <FormErrorText message={formik.errors.attributes} />
                    </Grid>
                  </>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Deskripsi</FormLabel>
                  <RichTextEditorField name="description" />
                  <FormErrorText message={formik.errors.description} />
                </Grid>

                <Grid item xs={12} md={4}>
                  <FormLabel required>Berat dengan Paket</FormLabel>
                  <NumberInputField
                    fullWidth
                    name="weight"
                    placeholder="Masukkan berat produk"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">kg</InputAdornment>,
                    }}
                  />
                  <FormErrorText message={formik.errors.weight} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Product Dimension</FormLabel>

                  <Grid container spacing={3}>
                    <Grid item xs={12} md={4}>
                      <NumberInputField
                        fullWidth
                        name="dimension_height"
                        placeholder="Tinggi"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                        }}
                      />
                      <FormErrorText message={formik.errors.dimension_height} />
                    </Grid>

                    <Grid item xs={12} md={4}>
                      <NumberInputField
                        fullWidth
                        name="dimension_width"
                        placeholder="Lebar"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                        }}
                      />
                      <FormErrorText message={formik.errors.dimension_width} />
                    </Grid>

                    <Grid item xs={12} md={4}>
                      <NumberInputField
                        fullWidth
                        name="dimension_length"
                        placeholder="Panjang"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                        }}
                      />
                      <FormErrorText message={formik.errors.dimension_length} />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained" disabled={formik.isSubmitting}>
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" disabled={formik.isSubmitting} onClick={onCancel} >
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { TiktokProductFormCardShape }
export default TiktokProductFormCard
