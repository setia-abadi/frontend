import TiktokProductsAPI from 'apis/tiktokProductsAPI'
import AsyncSelectField, { AsyncSelectFieldProps } from 'components/AsyncSelectField'
import { SelectOption } from 'components/Select'
import { ID } from 'models/base'
import { useRef } from 'react'

type Props<Multiple extends boolean> = Omit<AsyncSelectFieldProps<Multiple>, 'fetchOptions'> & {
  categoryId: ID | undefined
}

const TiktokProductBrandSelect = <Multiple extends boolean = false>({ categoryId, ...props }: Props<Multiple>) => {
  const nextPageToken = useRef<string | null>(null)
  const page = useRef<number>(1)

  const fetchOptions: AsyncSelectFieldProps<Multiple>['fetchOptions'] = async ({ page: nextPage, phrase, signal }) => {
    if (!categoryId) {
      return
    }

    if (nextPage === 1) {
      nextPageToken.current = null
      page.current = 1
    }

    const response = await TiktokProductsAPI.brands({
      category_id: categoryId,
      next_page_token: nextPageToken.current,
      phrase: phrase,
    })

    if (response.data.data) {
      const cachedPage = page.current
      nextPageToken.current = response.data.data.next_page_token
      page.current = page.current + 1

      return {
        page:  cachedPage,
        limit: nextPageToken.current ? 10 : response.data.data.total_count,
        total: response.data.data.total_count,
        nodes: response.data.data.brands.map<SelectOption>((node) => ({
          label: node.name,
          value: node.id,
        })),
      }
    }

    return undefined
  }

  return <AsyncSelectField {...props} fetchOptions={fetchOptions} />
}

export default TiktokProductBrandSelect
