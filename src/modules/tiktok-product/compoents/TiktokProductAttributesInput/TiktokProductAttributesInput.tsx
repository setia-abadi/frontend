import { Box, CircularProgress, Grid, styled } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import TiktokProductsAPI, { PayloadTiktokProductFetchAttributes } from 'apis/tiktokProductsAPI'
import FormLabel from 'components/FormLabel'
import Typography from 'components/Typography'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { useFormikContext } from 'formik'
import { FC, useMemo } from 'react'
import AsyncSelect from 'components/AsyncSelect'
import { SelectOption } from 'components/Select'
import TypeUtil from 'utils/typeUtil'
import { TiktokProductFormCardShape } from '../TiktokProductFormCard'
import { TiktokPlatfromAttributeModel } from 'models/tiktokPlatformAttribute'

type Props = {
  categoryId: string | undefined
  name: string
}

const Container = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.grey[100],
  padding: theme.spacing(2),
  borderRadius: theme.shape.borderRadius,
}))

const Label = styled(FormLabel)(({ theme }) => ({
  color: theme.palette.grey[800],
  fontWeight: theme.typography.fontWeightMedium,
}))

const TiktokProductAttributesInput: FC<Props> = ({ categoryId, name }) => {
  const { values, setFieldValue } = useFormikContext<TiktokProductFormCardShape>()

  const handleChange = (value: SelectOption | SelectOption[] | null, attributeId: string, fieldIndex: number)=> {
    setFieldValue(`${name}.${fieldIndex}.id`, attributeId)
    if (TypeUtil.isArray(value)) {
      setFieldValue(`${name}.${fieldIndex}.values`, value)
    } else {
      if (value) {
        setFieldValue(`${name}.${fieldIndex}.values`, [value])
      } else {
        setFieldValue(`${name}.${fieldIndex}.values`, [])
      }
    }
  }

  const payload: PayloadTiktokProductFetchAttributes = {
    category_id: categoryId!,
  }

  const { data, isLoading } = useQuery({
    enabled: TypeUtil.isDefined(categoryId),
    queryKey: [QueryKeyEnum.PRODUCT_TIKTOK_CATEGORY_FETCH, payload],
    queryFn: () => TiktokProductsAPI.attributes(categoryId!, payload),
    select: (response) => response.data.data.attributes,
  })

  const attributes = useMemo<TiktokPlatfromAttributeModel[] | undefined>(()=> {
    const tempAttributes: TiktokPlatfromAttributeModel[] = []
    if (data && values.attributes) {
      data.forEach((attribute) => {
        const valueIndex = values.attributes!.findIndex((attr) => attr.id === attribute.id)
        if (valueIndex !== -1) {
          tempAttributes[valueIndex] = attribute
        } else {
          tempAttributes.push(attribute)
        }
      })
      return tempAttributes
    }
    return data
  }, [data])

  const getValue = (attribute: TiktokPlatfromAttributeModel): SelectOption | SelectOption[] | undefined => {
    const value = values.attributes?.find((attr) => attr.id === attribute.id)?.values
    if (!attribute.is_multiple_selection) {
      return value?.at(-1)
    } 
    return value
  }

  return (
    <Container>
      {isLoading ? (
        <CircularProgress size={20} />
      ) : attributes ? (
        <Grid container spacing={3}>
          {attributes.map((attribute, index) => (
            <Grid key={attribute.id} item xs={12} md={4} lg={3}>
              <Label>{attribute.name}</Label>
              <AsyncSelect 
                name={`${name}.${index}.values`}
                multiple={attribute.is_multiple_selection}
                defaultValue={getValue(attribute)}
                onChange={(_, value) => handleChange(value, attribute.id, index)}
                fetchOptions={async () => ({
                  limit: attribute.values.length,
                  page: 1,
                  total: attribute.values.length,
                  nodes: attribute.values.map((attributeValue) => ({
                    label: attributeValue.name,
                    value: attributeValue.id,
                  }))
                })}
              />
            </Grid>
          ))}
        </Grid>
      ) : (
        <Typography color="grey">Tidak ada atribut</Typography>
      )}
    </Container>
  )
}

export default TiktokProductAttributesInput
