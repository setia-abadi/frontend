import { AlertTitle, Chip, CircularProgress, Stack, styled } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import TiktokProductsAPI, { PayloadTiktokProductFetchRecommendedCategories } from 'apis/tiktokProductsAPI'
import Alert from 'components/Alert'
import Typography from 'components/Typography'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { useField } from 'formik'
import useDebounceValue from 'hooks/useDebounceValue'
import { TiktokCategoryModel } from 'models/tiktokCategory'
import { FC } from 'react'
import TypeUtil from 'utils/typeUtil'

type Props = {
  name: string
  productTitle: string
  productDescription?: string | null
  onClick?: () => void
}

const ChipStyled = styled(Chip)(({ theme }) => ({
  backgroundColor: theme.palette.primary.light,
}))

const AlertStyled = styled(Alert)(({ theme }) => ({
  border: `1px solid ${theme.palette.info.main}`,
}))

const getCategoryLabel = (category: TiktokCategoryModel): string => {
  const text: string[] = [category.name]
  if (category.children_categories) {
    const childCategories = category.children_categories.map((childCategory) => getCategoryLabel(childCategory))
    text.push(...childCategories)
  }
  return text.join(' > ')
}

const getCategoryValue = (category: TiktokCategoryModel): string[] => {
  const value: string[] = [category.id]
  if (category.children_categories) {
    category.children_categories.forEach((childCategory) => {
      value.push(...getCategoryValue(childCategory))
    })
  }
  return value
}

const TiktokProductRecommendCategoryInput: FC<Props> = ({ name, productTitle, productDescription, onClick }) => {
  const [, , helpers] = useField(name)
  const debouncedTitle = useDebounceValue(productTitle, 1000)
  const debouncedDescription = useDebounceValue(productDescription, 1000)

  const payload: PayloadTiktokProductFetchRecommendedCategories = {
    product_title: debouncedTitle,
    image_uri: null,
    description: debouncedDescription ?? null,
  }

  const { data, isLoading } = useQuery({
    enabled: TypeUtil.isDefined(debouncedTitle) && !TypeUtil.isEmpty(debouncedTitle) && debouncedTitle.length >= 25,
    queryKey: [QueryKeyEnum.PRODUCT_TIKTOK_RECOMMENDED_CATEGORY_FETCH, payload],
    queryFn: () => TiktokProductsAPI.recommendedCategory(payload),
    select: (response) => response.data.data.category,
  })

  return (
    <AlertStyled variant="outlined" severity="info">
      <AlertTitle>Rekomendasi Kategori</AlertTitle>

      {isLoading ? (
        <CircularProgress size={20} />
      ) : data ? (
        <Stack gap={1} mt={1}>
          <ChipStyled
            label={getCategoryLabel(data)}
            onClick={() => {
              helpers.setValue(getCategoryValue(data))
              onClick?.()
            }}
          />
        </Stack>
      ) : (
        <Typography color="grey">Tidak ada rekomendasi kategori</Typography>
      )}
    </AlertStyled>
  )
}

export default TiktokProductRecommendCategoryInput
