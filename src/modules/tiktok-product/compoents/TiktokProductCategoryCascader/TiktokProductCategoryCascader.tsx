import { useQuery } from '@tanstack/react-query'
import TiktokProductsAPI from 'apis/tiktokProductsAPI'
import { CascaderOption } from 'components/Cascader'
import CascaderField, { CascaderFieldProps } from 'components/Cascader/CascaderField'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { TiktokCategoryModel } from 'models/tiktokCategory'
import { FC } from 'react'

type Props = Omit<CascaderFieldProps, 'options'>

const transformCategoriesToCascaderOption = (categories: TiktokCategoryModel[]): CascaderOption[] => {
  return categories.map((category) => {
    const option: CascaderOption = {
      label: category.name,
      value: category.id,
    }
    if (category.children_categories) {
      option.children = transformCategoriesToCascaderOption(category.children_categories)
    }
    return option
  })
}

const TiktokProductCategoryCascader: FC<Props> = ({ ...props }) => {
  const { data, isLoading } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_TIKTOK_CATEGORY_FETCH],
    queryFn: () => TiktokProductsAPI.categories(),
    select: (response): CascaderOption[] => {
      if (response.data.data.categories) {
        return transformCategoriesToCascaderOption(response.data.data.categories)
      }
      return []
    },
  })

  return (
    <CascaderField
      {...props}
      options={data ?? []}
      disabled={isLoading}
    />
  )
}

export default TiktokProductCategoryCascader
