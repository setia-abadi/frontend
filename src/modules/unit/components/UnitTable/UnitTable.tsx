import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { UnitModel } from 'models/unit'

type Model = UnitModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama',
    minWidth: 200,
    width: 300,
    enableSort: true,
    render: (node) => node.name,
  },
  {
    key: 'description',
    title: 'Deskripsi',
    minWidth: 250,
    render: (node) => <ContentWithEmptyPlaceholder>{node.description}</ContentWithEmptyPlaceholder>,
  },
]

const UnitTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default UnitTable
