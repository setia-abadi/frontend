import { ShopOrderPlatformTypeEnum } from "enums/shopOrderPlatformTypeEnum";
import { ShopOrderTrackingStatusEnum } from "enums/shopOrderTrackingStatusEnum";

const SHOP_ORDER_PLATFORM_TYPE_TEXT: Record<ShopOrderPlatformTypeEnum, string> = {
  [ShopOrderPlatformTypeEnum.TIKTOK_SHOP]: "TikTok",
}

const SHOP_ORDER_TRACKING_STATUS_TEXT: Record<ShopOrderTrackingStatusEnum, string> = {
  [ShopOrderTrackingStatusEnum.AWAITING_COLLECTION]: "Menunggu Pengambilan",
  [ShopOrderTrackingStatusEnum.AWAITING_SHIPMENT]: "Menunggu Pengiriman",
  [ShopOrderTrackingStatusEnum.CANCEL]: "Dibatalkan",
  [ShopOrderTrackingStatusEnum.COMPLETED]: "Selesai",
  [ShopOrderTrackingStatusEnum.DELIVERED]: "Telah Sampai",
  [ShopOrderTrackingStatusEnum.PARTIALLY_SHIPPING]: "Dikirim Sebagian",
  [ShopOrderTrackingStatusEnum.RETURNED]: "Dikembalikan",
  [ShopOrderTrackingStatusEnum.SHIPPING]: "Sedang Dikirim",
  [ShopOrderTrackingStatusEnum.UNPAID]: "Belum Dibayar",
  [ShopOrderTrackingStatusEnum.WILL_RETURN]: "Akan Dikembalikan",
}

export { SHOP_ORDER_PLATFORM_TYPE_TEXT, SHOP_ORDER_TRACKING_STATUS_TEXT }