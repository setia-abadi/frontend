import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import FormattedNumber from 'components/FormattedNumber'
import { Box, Stack, TableCell, TableRow } from '@mui/material'
import Image from 'components/Image'
import { ShopOrderItemModel } from 'models/shopOrderItem'
import Typography from 'components/Typography'

type Model = ShopOrderItemModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  subTotal: number
  shippingFee: number
  serviceFee: number
  tax: number
  total: number
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product_name',
    title: 'Nama Produk',
    minWidth: 250,
    render: (node) => (
      <Stack direction="row" spacing={1.5} alignItems="center">
        {node.image_link && <Image width={50} aspectRatio={1} src={node.image_link} />}
        <Box>
          <ContentWithEmptyPlaceholder>{node.product_unit?.product.name}</ContentWithEmptyPlaceholder>
        </Box>
      </Stack>
    ),
  },
  {
    key: 'qty',
    title: 'Kuantitas',
    width: 150,
    render: (node) => (
      <Stack direction="row" spacing={1}>
        <div>
          <FormattedNumber value={node.quantity} />
        </div>
        <div>{node.product_unit?.unit ? node.product_unit.unit.name : null}</div>
      </Stack>
    ),
  },
  {
    key: 'original_price',
    title: 'Harga Modal',
    width: 175,
    render: (node) => <FormattedCurrency value={node.original_price} />,
  },
  {
    key: 'sale_price',
    title: 'Harga Jual',
    width: 175,
    render: (node) => <FormattedCurrency value={node.sale_price} />,
  },
]

const ShopOrderItemTable: FC<Props> = ({ subTotal, tax, total, shippingFee, serviceFee, ...props }) => {
  const renderFooterItem = (label: ReactNode, value: ReactNode) => {
    return (
      <TableRow>
        <TableCell colSpan={2}></TableCell>
        <TableCell>
          <Typography variant="body1" fontWeight={500}>
            {label}
          </Typography>
        </TableCell>
        <TableCell>
          <Typography variant="body1">{value}</Typography>
        </TableCell>
      </TableRow>
    )
  }

  return (
    <Table<Model>
      columns={baseColumns}
      footer={
        props.data.length > 0 ? (
          <>
            {renderFooterItem('Subtotal', <FormattedCurrency value={subTotal} />)}
            {renderFooterItem('Biaya Pengiriman', <FormattedCurrency value={shippingFee} />)}
            {renderFooterItem('Biaya Servis', <FormattedCurrency value={serviceFee} />)}
            {renderFooterItem('Pajak', <FormattedCurrency value={tax} />)}
            {renderFooterItem('Total', <FormattedCurrency value={total} />)}
          </>
        ) : null
      }
      {...props}
    />
  )
}

export default ShopOrderItemTable
