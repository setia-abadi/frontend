import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { ShopOrderPlatformTypeEnum } from 'enums/shopOrderPlatformTypeEnum'
import { SHOP_ORDER_PLATFORM_TYPE_TEXT } from 'modules/shop-order/const'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const ShopOrderPlatformTypeSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(ShopOrderPlatformTypeEnum).map<SelectOption>((platform) => ({
      label: SHOP_ORDER_PLATFORM_TYPE_TEXT[platform],
      value: platform,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default ShopOrderPlatformTypeSelect
