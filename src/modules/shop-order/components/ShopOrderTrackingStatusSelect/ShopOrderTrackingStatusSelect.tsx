import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { ShopOrderTrackingStatusEnum } from 'enums/shopOrderTrackingStatusEnum'
import { SHOP_ORDER_TRACKING_STATUS_TEXT } from 'modules/shop-order/const'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const ShopOrderTrackingStatusSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(ShopOrderTrackingStatusEnum).map<SelectOption>((status) => ({
      label: SHOP_ORDER_TRACKING_STATUS_TEXT[status],
      value: status,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default ShopOrderTrackingStatusSelect
