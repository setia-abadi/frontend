import { ChipOwnProps } from '@mui/material'
import Chip from 'components/Chip'
import { ShopOrderTrackingStatusEnum } from 'enums/shopOrderTrackingStatusEnum'
import { SHOP_ORDER_TRACKING_STATUS_TEXT } from 'modules/shop-order/const'
import { FC } from 'react'

type Props = {
  status: ShopOrderTrackingStatusEnum
}

const ShopOrderTrackingStatusChip: FC<Props> = ({ status }) => {
  const color: Record<ShopOrderTrackingStatusEnum, ChipOwnProps['color']> = {
    [ShopOrderTrackingStatusEnum.AWAITING_COLLECTION]: 'warning',
    [ShopOrderTrackingStatusEnum.AWAITING_SHIPMENT]: 'warning',
    [ShopOrderTrackingStatusEnum.CANCEL]: 'default',
    [ShopOrderTrackingStatusEnum.COMPLETED]: 'success',
    [ShopOrderTrackingStatusEnum.DELIVERED]: 'success',
    [ShopOrderTrackingStatusEnum.PARTIALLY_SHIPPING]: 'warning',
    [ShopOrderTrackingStatusEnum.RETURNED]: 'error',
    [ShopOrderTrackingStatusEnum.SHIPPING]: 'info',
    [ShopOrderTrackingStatusEnum.UNPAID]: 'error',
    [ShopOrderTrackingStatusEnum.WILL_RETURN]: 'warning',
  }

  return <Chip label={SHOP_ORDER_TRACKING_STATUS_TEXT[status]} size="small" color={color[status]} />
}

export default ShopOrderTrackingStatusChip
