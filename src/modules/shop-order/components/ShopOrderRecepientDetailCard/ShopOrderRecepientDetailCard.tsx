import { Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'

type Props = {
  name: string
  phoneNumber: string
  fullAddress: string
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const ShopOrderRecepientDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Nama Lengkap
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <ContentWithEmptyPlaceholder>{props.name}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Nomor Telepon
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <ContentWithEmptyPlaceholder>{props.phoneNumber}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Alamat
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1" sx={{ wordBreak: 'break-word' }}>
                  <ContentWithEmptyPlaceholder>{props.fullAddress}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          {props.createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <Typography variant="body1">
                    <FormattedDate date={props.createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {props.updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <Typography variant="body1">
                    <FormattedDate date={props.updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default ShopOrderRecepientDetailCard
