import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { ShopOrderModel } from 'models/shopOrder'
import { SHOP_ORDER_PLATFORM_TYPE_TEXT } from 'modules/shop-order/const'
import ShopOrderTrackingStatusChip from '../ShopOrderTrackingStatusChip'
import FormattedDate from 'components/FormattedDate'

type Model = ShopOrderModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'order_id',
    title: 'ID Pesanan',
    minWidth: 200,
    render: (node) => node.platform_identifier,
  },
  {
    key: 'platform_type',
    title: 'Platform',
    width: 125,
    render: (node) => SHOP_ORDER_PLATFORM_TYPE_TEXT[node.platform_type],
  },
  {
    key: 'tracking_number',
    title: 'Nomor Pengiriman',
    width: 200,
    render: (node) => node.tracking_number,
  },
  {
    key: 'tracking_status',
    title: 'Status Pengiriman',
    width: 200,
    render: (node) => <ShopOrderTrackingStatusChip status={node.tracking_status} />,
  },
  {
    key: 'created_at',
    title: 'Dibuat Pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const ShopOrderTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ShopOrderTable
