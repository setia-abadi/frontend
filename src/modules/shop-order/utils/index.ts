import dayjs from "dayjs";
import { ShopOrderPlatformTypeEnum } from "enums/shopOrderPlatformTypeEnum";
import { ShopOrderModel } from "models/shopOrder";
import { ShopOrderItemModel } from "models/shopOrderItem";
import ArrayUtil from "utils/arrayUtil";
import StringUtil from "utils/stringUtil";
import { faker } from '@faker-js/faker';
import NumberUtil from "utils/numberUtil";
import { ShopOrderTrackingStatusEnum } from "enums/shopOrderTrackingStatusEnum";

const ShopOrderUtil = {
  randomItemRecord: (): ShopOrderItemModel => {
    const price = NumberUtil.random(10000, 150000)
    const salePrice = NumberUtil.random(price, price + 20000)
    return {
      created_at: dayjs().toISOString(),
      updated_at: dayjs().toISOString(),
      id: StringUtil.uuid(),
      image_link: faker.image.url(),
      original_price: price,
      platform_product_id: StringUtil.uuid(),
      product_unit: null,
      product_unit_id: StringUtil.uuid(),
      quantity: NumberUtil.random(1, 10),
      sale_price: salePrice,
    }
  },

  randomRecord: (): ShopOrderModel => {
    const subTotal = NumberUtil.random(500000, 300000)
    return {
      service_fee: 0,
      created_at: dayjs().toISOString(),
      id: StringUtil.uuid(),
      items: Array.from({ length: 5 }, () => ShopOrderUtil.randomItemRecord()),
      platform_identifier: StringUtil.random(16).toUpperCase(),
      platform_type: ArrayUtil.pickRandom(Object.values(ShopOrderPlatformTypeEnum)),
      recipient_full_address: faker.location.streetAddress(),
      recipient_name: faker.person.fullName(),
      recipient_phone_number: faker.phone.number(),
      shipping_fee: NumberUtil.random(10000, 150000),
      subtotal: subTotal,
      tax: 0,
      total_amount: subTotal,
      total_original_product_price: subTotal,
      tracking_number: `JPX${StringUtil.random(12).toUpperCase()}`,
      tracking_status: ArrayUtil.pickRandom(Object.values(ShopOrderTrackingStatusEnum)),
      updated_at: dayjs().toISOString(),
    }
  }
}

export default ShopOrderUtil