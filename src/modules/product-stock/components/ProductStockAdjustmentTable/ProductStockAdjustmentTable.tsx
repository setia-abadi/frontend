import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { ProductStockAdjustmentModel } from 'models/productStockAdjustment'
import FormattedNumber from 'components/FormattedNumber'
import FormattedDate from 'components/FormattedDate'

type Model = ProductStockAdjustmentModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'previous_qty',
    title: 'Stok Awal',
    width: 175,
    render: (node) => <FormattedNumber value={node.previous_qty} />,
  },
  {
    key: 'updated_qty',
    title: 'Perubahan Stok',
    width: 175,
    render: (node) => <FormattedNumber value={node.updated_qty} />,
  },
  {
    key: 'updated_by',
    title: 'Diubah Oleh',
    width: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.user?.name}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'created_at',
    title: 'Diubah Pada',
    width: 175,
    enableSort: true,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const ProductStockAdjustmentTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductStockAdjustmentTable
