import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import DialogContent from 'components/DialogContent'
import DialogTitle from 'components/DialogTitle'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { SortDomain } from 'models/apiBase'
import DialogContentText from '@mui/material/DialogContentText'
import ProductStockAdjustmentsAPI, { PayloadFilterProductStockAdjustment } from 'apis/productStockAdjustmentsAPI'
import { ID } from 'models/base'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { keepPreviousData, useQuery, useQueryClient } from '@tanstack/react-query'
import { Box, DialogActions } from '@mui/material'
import Button from 'components/Button'
import ProductStockAdjustmentFilterFormPopover, {
  ProductStockAdjustmentFilterFormPopoverShape,
} from '../ProductStockAdjustmentFilterPopover/ProductStockAdjustmentFilterPopover'
import { SelectOption } from 'components/Select'
import FormUtil from 'utils/formUtil'
import ProductStockHistoryTable from '../ProductStockAdjustmentTable'

type Props = {
  open: boolean
  dialogTitle: string
  dialogSubTitle?: string
  productStockId: ID
  onClose: () => void
  onClosed?: () => void
}

const ProductStockAdjustmentHistoryDialog: FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  productStockId,
  onClose,
  onClosed,
}) => {
  const queryClient = useQueryClient()
  const [limit, setLimit] = useState<number>(LIMIT_PER_PAGE)
  const [page, setPage] = useState<number>(1)
  const [sorts, setSorts] = useState<SortDomain[]>()
  const [user, setUser] = useState<SelectOption>()

  const payload: PayloadFilterProductStockAdjustment = {
    page,
    limit,
    sorts,
    product_stock_id: productStockId,
    user_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(user), null),
  }

  const { data } = useQuery({
    enabled: open,
    queryKey: [QueryKeyEnum.PRODUCT_STOCK_ADJUSTMENT_FILTER, payload],
    queryFn: () => ProductStockAdjustmentsAPI.filter(payload),
    placeholderData: keepPreviousData,
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleTransitionExited = () => {
    queryClient.resetQueries({ queryKey: [QueryKeyEnum.PRODUCT_STOCK_ADJUSTMENT_FILTER] })

    setPage(1)
    setLimit(LIMIT_PER_PAGE)
    setSorts([])
    setUser(undefined)

    onClosed?.()
  }

  const handleResetFilter = () => {
    setUser(undefined)
  }

  const handleApplyFilter = (values: ProductStockAdjustmentFilterFormPopoverShape) => {
    setUser(values.user_id ?? undefined)
  }

  return (
    <Dialog fullWidth maxWidth="md" open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <DialogTitle>{dialogTitle}</DialogTitle>
      <DialogContent>
        {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

        <Box mt={2}>
          {data ? (
            <>
              <ProductStockAdjustmentFilterFormPopover
                initialValues={{
                  user_id: user,
                }}
                onApply={handleApplyFilter}
                onReset={handleResetFilter}
              />

              <Box mt={1}>
                <ProductStockHistoryTable
                  data={data.nodes}
                  pagination={{
                    page,
                    rowsPerPage: limit,
                    total: data.total,
                    onPaginationChange: handlePaginationChange,
                  }}
                  sorts={sorts}
                  onSortChange={setSorts}
                />
              </Box>
            </>
          ) : (
            'Loading...'
          )}
        </Box>

        <DialogActions>
          <Button variant="outlined" onClick={onClose}>
            Tutup
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  )
}

export default ProductStockAdjustmentHistoryDialog
