import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { CustomerTypeDiscountModel } from 'models/customerTypeDiscount'
import StatusChip from 'components/StatusChip'
import TypeUtil from 'utils/typeUtil'
import FormattedCurrency from 'components/FormattedCurrency'
import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'

type Model = CustomerTypeDiscountModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product',
    title: 'Produk',
    minWidth: 200,
    render: (node) => <ContentWithEmptyPlaceholder>{node.product?.name}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'discount',
    title: 'Diskon',
    width: 175,
    render: (node) =>
      TypeUtil.isDefined(node.discount_amount) ? (
        <FormattedCurrency value={node.discount_amount} />
      ) : TypeUtil.isDefined(node.discount_percentage) ? (
        `${node.discount_percentage}%`
      ) : <EmptyTextPlaceholder />,
  },
  {
    key: 'status',
    title: 'Status',
    width: 150,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const CustomerTypeDiscountTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [action])

  return <Table<Model> columns={columns} {...props} />
}

export default CustomerTypeDiscountTable
