import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import TextInputField from 'components/TextInputField'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type CustomerTypeFormCardShape = FormShape<{
  name: string
  description: string
}>

type Props = {
  cardTitle: string
  initialValues?: CustomerTypeFormCardShape
  onSubmit: (values: CustomerTypeFormCardShape) => Promise<void>
  onCancel: () => void
}

const CustomerTypeFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const handleSubmit = async (values: CustomerTypeFormCardShape, helpers: FormikHelpers<CustomerTypeFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<CustomerTypeFormCardShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Deskripsi</FormLabel>
                  <TextInputField name="description" multiline rows={4} fullWidth />
                  <FormErrorText message={formik.errors.description} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { CustomerTypeFormCardShape }
export default CustomerTypeFormCard
