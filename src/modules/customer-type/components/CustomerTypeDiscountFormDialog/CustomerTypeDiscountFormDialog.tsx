import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { SelectOption } from 'components/Select'
import { DisabledFields, FormShape, ID } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid, InputAdornment } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import ProductForCustomerTypeDiscountFormSelect from '../../../product/components/ProductForCustomerTypeDiscountFormSelect'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import { CURRENCY } from 'constants/currency'
import RadioGroup from 'components/RadioGroup'
import RadioGroupField from 'components/RadioGroupField'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'

type DiscountType = 'discount_percentage' | 'discount_amount'

type CustomerTypeDiscountFormDialogShape = FormShape<{
  product_id: SelectOption
  discount_amount: number
  discount_percentage: number
  is_active: boolean
}>

type Props = {
  open: boolean
  dialogTitle: string
  customerTypeId: ID
  disabledFields?: Partial<DisabledFields<CustomerTypeDiscountFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: CustomerTypeDiscountFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: CustomerTypeDiscountFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const CustomerTypeDiscountFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  customerTypeId,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()
  const [discountType, setDiscountType] = useState<DiscountType>(
    TypeUtil.isDefined(initialValues)
      ? TypeUtil.isDefined(initialValues.discount_amount)
        ? 'discount_amount'
        : 'discount_percentage'
      : 'discount_amount'
  )

  const defaultValues: CustomerTypeDiscountFormDialogShape = {
    is_active: true,
  }

  const handleTransitionExited = () => {
    setError(undefined)
    setDiscountType('discount_amount')
    onClosed?.()
  }

  const handleSubmit = async (
    values: CustomerTypeDiscountFormDialogShape,
    helpers: FormikHelpers<CustomerTypeDiscountFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<CustomerTypeDiscountFormDialogShape>
        initialValues={initialValues ?? defaultValues}
        onSubmit={handleSubmit}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <PermissionControl match={[PermissionEnum.PRODUCT_OPTION_FOR_CUSTOMER_TYPE_DISCOUNT_FORM]}>
                    <FormLabel required>Product</FormLabel>
                    <ProductForCustomerTypeDiscountFormSelect
                      customerTypeId={customerTypeId}
                      name="product_id"
                      disabled={disabledFields?.product_id}
                    />
                    <FormErrorText message={formik.errors.product_id} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Diskon</FormLabel>
                  <RadioGroup
                    row
                    value={discountType}
                    disabled={disabledFields?.discount_amount || disabledFields?.discount_percentage}
                    onChange={(value) => {
                      setDiscountType(value as DiscountType)
                      formik.setFieldValue('discount_amount', null)
                      formik.setFieldValue('discount_percentage', null)
                    }}
                    options={[
                      { label: 'Jumlah Diskon', value: 'discount_amount' },
                      { label: 'Persentase Diskon', value: 'discount_percentage' },
                    ]}
                  />
                  <Box mt={1}>
                    {discountType === 'discount_amount' ? (
                      <NumberInputField
                        name="discount_amount"
                        InputProps={{ startAdornment: <InputAdornment position="start">{CURRENCY}</InputAdornment> }}
                        sx={{ width: '60%' }}
                        disabled={disabledFields?.discount_amount}
                      />
                    ) : (
                      <NumberInputField
                        name="discount_percentage"
                        InputProps={{ endAdornment: <InputAdornment position="end">%</InputAdornment> }}
                        sx={{ width: '60%' }}
                        disabled={disabledFields?.discount_percentage}
                      />
                    )}
                  </Box>
                  <FormErrorText message={formik.errors.discount_amount || formik.errors.discount_percentage} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Status</FormLabel>
                  <RadioGroupField
                    row={true}
                    name="is_active"
                    disabled={disabledFields?.is_active}
                    options={[
                      { label: 'Aktif', value: true },
                      { label: 'Tidak Aktif', value: false },
                    ]}
                  />
                  <FormErrorText message={formik.errors.is_active} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { CustomerTypeDiscountFormDialogShape }

export default CustomerTypeDiscountFormDialog
