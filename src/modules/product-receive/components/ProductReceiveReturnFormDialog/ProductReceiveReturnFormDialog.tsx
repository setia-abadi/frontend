import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, FormControlLabel, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import TextInputField from 'components/TextInputField'
import DropzoneField from 'components/DropzoneField'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import Checkbox from 'components/Checkbox'

type ProductReceiveReturnFormDialogShape = FormShape<{
  description: string
  file_paths: string[]
}>

type Props = {
  open: boolean
  dialogTitle: string
  disabledFields?: Partial<DisabledFields<ProductReceiveReturnFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ProductReceiveReturnFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductReceiveReturnFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductReceiveReturnFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()
  const [isChecked, setIsChecked] = useState<boolean>(false)

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductReceiveReturnFormDialogShape,
    helpers: FormikHelpers<ProductReceiveReturnFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductReceiveReturnFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel required>Alasan</FormLabel>
                  <TextInputField
                    fullWidth
                    multiline
                    rows={4}
                    name="description"
                    disabled={disabledFields?.description}
                  />
                  <FormErrorText message={formik.errors.description} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Lampiran</FormLabel>
                  <DropzoneField
                    multiple
                    uploadFn={(file, config) => DeliveryOrdersAPI.uploadImage({ file }, config)}
                    name="file_paths"
                    select={(response) => response.data.data.path}
                  />
                  <FormErrorText message={formik.errors.file_paths} />
                </Grid>
              </Grid>

              <Box mt={1}>
                <FormControlLabel
                  value={isChecked}
                  control={<Checkbox onChange={(e) => setIsChecked(e.target.checked)} />}
                  label="Dengan ini saya menyatakan bahwa data yang saya kirimkan sudah benar."
                />
              </Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" disabled={!isChecked} isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductReceiveReturnFormDialogShape }

export default ProductReceiveReturnFormDialog
