import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { Link, Stack } from '@mui/material'
import Image from 'components/Image'
import { ProductReceiveImageModel } from 'models/productReceiveImage'
import FormattedDate from 'components/FormattedDate'

type Model = ProductReceiveImageModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'image',
    title: 'Gambar',
    minWidth: 300,
    width: 350,
    render: (node) => (
      <Stack direction="row" alignItems="center" spacing={1.5}>
        <Image src={node.file!.link} width={50} aspectRatio={1} fit="contain" />
        <Link href={node.file!.link} target="_blank">{node.file?.name}</Link>
      </Stack>
    ),
  },
  {
    key: 'description',
    title: 'Deskripsi',
    width: 400,
    minWidth: 300,
    render: (node) => <ContentWithEmptyPlaceholder>{node.description}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'created_at',
    title: 'Ditambahkan Pada',
    width: 175,
    render: (node) => <FormattedDate date={node.created_at} />,
  },
]

const ProductReceiveItemTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductReceiveItemTable
