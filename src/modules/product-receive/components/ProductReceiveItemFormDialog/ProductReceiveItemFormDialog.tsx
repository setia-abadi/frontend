import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { SelectOption } from 'components/Select'
import { DisabledFields, FormShape, ID } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { PermissionEnum } from 'enums/permissionEnum'
import ProductForProductReceiveItemFormSelect from 'modules/product/components/ProductForProductReceiveItemFormSelect'
import NumberInputField from 'components/NumberInputField'
import { CURRENCY } from 'constants/currency'
import ProductUnitForProductReceiveItemSelect from 'modules/product-unit/components/ProductUnitForProductReceiveItemSelect'

type ProductReceiveItemFormDialogShape = FormShape<{
  price_per_unit: number
  product_id: SelectOption
  qty: number
  unit_id: SelectOption
}>

type Props = {
  open: boolean
  dialogTitle: string
  productReceiveId: ID
  disabledFields?: Partial<DisabledFields<ProductReceiveItemFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ProductReceiveItemFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductReceiveItemFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductReceiveItemFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  productReceiveId,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductReceiveItemFormDialogShape,
    helpers: FormikHelpers<ProductReceiveItemFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductReceiveItemFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <PermissionControl renderError match={[PermissionEnum.PRODUCT_OPTION_FOR_PRODUCT_RECEIVE_ITEM_FORM]}>
                    <FormLabel required>Product</FormLabel>
                    <ProductForProductReceiveItemFormSelect
                      productReceiveId={productReceiveId}
                      name="product_id"
                      disabled={disabledFields?.product_id}
                    />
                    <FormErrorText message={formik.errors.product_id} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12} md={7}>
                  <FormLabel required>Harga per Satuan</FormLabel>
                  <NumberInputField
                    name="price_per_unit"
                    disabled={disabledFields?.price_per_unit}
                    InputProps={{ startAdornment: CURRENCY }}
                    fullWidth
                  />
                  <FormErrorText message={formik.errors.price_per_unit} />
                </Grid>

                <Grid item xs={12} md={5}>
                  <PermissionControl renderError match={[PermissionEnum.PRODUCT_UNIT_OPTION_FOR_PRODUCT_RECEIVE_ITEM_FORM]}>
                    <FormLabel required>Satuan</FormLabel>
                    <ProductUnitForProductReceiveItemSelect
                      productId={formik.values.product_id?.value?.toString() ?? ''}
                      name="unit_id"
                      disabled={!TypeUtil.isDefined(formik.values.product_id) || disabledFields?.unit_id}
                      fullWidth
                    />
                    <FormErrorText message={formik.errors.unit_id} />
                  </PermissionControl>
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Kuantitas</FormLabel>
                  <NumberInputField name="qty" disabled={disabledFields?.qty} fullWidth />
                  <FormErrorText message={formik.errors.qty} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductReceiveItemFormDialogShape }

export default ProductReceiveItemFormDialog
