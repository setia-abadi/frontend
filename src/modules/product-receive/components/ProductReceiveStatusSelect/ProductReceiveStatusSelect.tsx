import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { PRODUCT_RECEIVE_STATUS_TEXT } from 'modules/product-receive/constants'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const ProductReceiveStatusSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(ProductReceiveStatusEnum).map<SelectOption>((status) => ({
      label: PRODUCT_RECEIVE_STATUS_TEXT[status],
      value: status,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default ProductReceiveStatusSelect
