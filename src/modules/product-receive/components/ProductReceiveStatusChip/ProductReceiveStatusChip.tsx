import { ChipOwnProps, ChipProps } from '@mui/material'
import Chip from 'components/Chip'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { PRODUCT_RECEIVE_STATUS_TEXT } from 'modules/product-receive/constants'
import { FC } from 'react'

type Props = Omit<ChipProps, 'color' | 'label'> & {
  status: ProductReceiveStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status, ...props }) => {
  const color: Record<ProductReceiveStatusEnum, ChipOwnProps['color']> = {
    [ProductReceiveStatusEnum.CANCELED]: 'default',
    [ProductReceiveStatusEnum.COMPLETED]: 'success',
    [ProductReceiveStatusEnum.PENDING]: 'warning',
    [ProductReceiveStatusEnum.RETURNED]: 'error',
  }

  return <Chip label={PRODUCT_RECEIVE_STATUS_TEXT[status]} size="small" color={color[status]} {...props} />
}

export default CustomerDebtStatusChip
