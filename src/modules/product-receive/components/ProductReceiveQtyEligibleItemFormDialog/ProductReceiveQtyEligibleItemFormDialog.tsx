import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import NumberInput from 'components/NumberInput'

type ProductReceiveQtyEligibleItemFormDialogShape = FormShape<{
  qty_eligible: number
}>

type Props = {
  open: boolean
  dialogTitle: string
  maxQty: number
  disabledFields?: Partial<DisabledFields<ProductReceiveQtyEligibleItemFormDialogShape>>
  dialogSubTitle?: string
  initialValues?: ProductReceiveQtyEligibleItemFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductReceiveQtyEligibleItemFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductReceiveQtyEligibleItemFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  maxQty,
  disabledFields,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductReceiveQtyEligibleItemFormDialogShape,
    helpers: FormikHelpers<ProductReceiveQtyEligibleItemFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductReceiveQtyEligibleItemFormDialogShape>
        initialValues={{ ...initialValues }}
        onSubmit={handleSubmit}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel required>Kuantias</FormLabel>
                  <Box display="flex" alignItems="center" gap={2}>
                    <NumberInputField fullWidth name="qty_eligible" disabled={disabledFields?.qty_eligible} />
                    <span>/</span>
                    <NumberInput fullWidth disabled value={maxQty} onChange={() => {}} />
                  </Box>
                  <FormErrorText message={formik.errors.qty_eligible} />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Kirim
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductReceiveQtyEligibleItemFormDialogShape }

export default ProductReceiveQtyEligibleItemFormDialog
