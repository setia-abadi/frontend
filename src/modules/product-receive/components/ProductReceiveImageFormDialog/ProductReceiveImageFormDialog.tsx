import Dialog from '@mui/material/Dialog'
import { FC, useState } from 'react'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'
import FormUtil from 'utils/formUtil'
import Box from '@mui/material/Box'
import { DialogContentText, Grid } from '@mui/material'
import Button from 'components/Button'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogTitle from 'components/DialogTitle'
import Alert from 'components/Alert'
import ProductReceivesAPI from 'apis/productReceivesAPI'
import FormLabel from 'components/FormLabel'
import FormErrorText from 'components/FormErrorText'
import TextInputField from 'components/TextInputField'
import UploadImageField from 'components/UploadImageField'

type ProductReceiveImageFormDialogShape = FormShape<{
  file_path: string
  description: string
}>

type Props = {
  open: boolean
  dialogTitle: string
  dialogSubTitle?: string
  initialValues?: ProductReceiveImageFormDialogShape
  onClose: () => void
  onCancel: () => void
  onSubmit: (values: ProductReceiveImageFormDialogShape) => Promise<void>
  onClosed?: () => void
}

const ProductReceiveImageFormDialog: FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  initialValues,
  onSubmit,
  onCancel,
  onClose,
  onClosed,
}) => {
  const [error, setError] = useState<string>()

  const handleTransitionExited = () => {
    setError(undefined)
    onClosed?.()
  }

  const handleSubmit = async (
    values: ProductReceiveImageFormDialogShape,
    helpers: FormikHelpers<ProductReceiveImageFormDialogShape>
  ) => {
    try {
      setError(undefined)
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={handleTransitionExited}>
      <Formik<ProductReceiveImageFormDialogShape> initialValues={{ ...initialValues }} onSubmit={handleSubmit}>
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <DialogTitle>{dialogTitle}</DialogTitle>

            <DialogContent>
              {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

              {error && (
                <Box mt={3}>
                  <Alert variant="outlined" severity="error">
                    {error}
                  </Alert>
                </Box>
              )}

              <Grid container spacing={3} mt={1}>
                <Grid item xs={12}>
                  <FormLabel required>Gambar</FormLabel>
                  <UploadImageField
                    name="file_path"
                    aspectRatio={1}
                    uploadFn={(file) => ProductReceivesAPI.uploadImage({ file })}
                    select={(response) => response.data.data.path}
                  />
                  <FormErrorText message={formik.errors.file_path} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Deskripsi</FormLabel>
                  <TextInputField multiline name="description" rows={4} fullWidth />
                  <FormErrorText message={formik.errors.description} />
                </Grid>
              </Grid>

              <Box mt={3}></Box>
            </DialogContent>

            <DialogActions>
              <Button variant="outlined" color="error" disabled={formik.isSubmitting} onClick={onCancel}>
                Batalkan
              </Button>

              <Button type="submit" variant="contained" isLoading={formik.isSubmitting}>
                Simpan
              </Button>
            </DialogActions>
          </Form>
        )}
      </Formik>
    </Dialog>
  )
}

export type { ProductReceiveImageFormDialogShape }

export default ProductReceiveImageFormDialog
