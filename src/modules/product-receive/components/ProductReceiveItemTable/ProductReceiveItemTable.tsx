import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import FormattedCurrency from 'components/FormattedCurrency'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { ProductReceiveItemModel } from 'models/productReceiveItem'
import FormattedNumber from 'components/FormattedNumber'
import { Box, IconButton, Stack } from '@mui/material'
import Image from 'components/Image'
import { IconEdit } from '@tabler/icons'

type Model = ProductReceiveItemModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  onEditQtyBtnClick?: (item: Model) => void
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'product_name',
    title: 'Nama Produk',
    minWidth: 250,
    render: (node) => (
      <Stack direction="row" spacing={1.5} alignItems="center">
        {node.product_unit?.product.image_file && (
          <Image width={50} aspectRatio={1} src={node.product_unit.product.image_file.link} />
        )}
        <Box>
          <ContentWithEmptyPlaceholder>{node.product_unit?.product.name}</ContentWithEmptyPlaceholder>
        </Box>
      </Stack>
    ),
  },
  {
    key: 'price',
    title: 'Harga per Satuan',
    width: 175,
    render: (node) => <FormattedCurrency value={node.price_per_unit} />,
  },
  {
    key: 'unit',
    title: 'Satuan',
    width: 150,
    render: (node) => <ContentWithEmptyPlaceholder>{node.product_unit?.unit?.name}</ContentWithEmptyPlaceholder>,
  },
]

const ProductReceiveItemTable: FC<Props> = ({ action, onEditQtyBtnClick, ...props }) => {
  const columns = useMemo(() => {
    const cols = baseColumns.concat({
      key: 'qty',
      title: 'Kuantitas',
      width: 150,
      render: (node) => (
        <Stack alignItems="center" direction="row" gap={1}>
          <Stack direction="row" gap={1}>
            <FormattedNumber value={node.qty_eligible} />
            <span>/</span>
            <FormattedNumber value={node.qty} />
          </Stack>

          {onEditQtyBtnClick && (
            <IconButton onClick={() => onEditQtyBtnClick(node)}>
              <IconEdit size="1.2rem" />
            </IconButton>
          )}
        </Stack>
      ),
    })

    if (action) {
      return cols.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return cols
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default ProductReceiveItemTable
