import { Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import FormattedCurrency from 'components/FormattedCurrency'
import ProductReceiveStatusChip from '../ProductReceiveStatusChip'

type Props = {
  date: string
  invoiceNumber: string
  supplier: string
  totalPrice: number
  status: ProductReceiveStatusEnum
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const ProductReceiveDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Tanggal Terima
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <FormattedDate date={props.date} withTime={false} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Nomor Faktur
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">{props.invoiceNumber}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Supplier
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">{props.supplier}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Total Harga
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <FormattedCurrency value={props.totalPrice} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Status
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <ProductReceiveStatusChip status={props.status} />
              </Grid>
            </Grid>
          </Grid>

          {props.createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <Typography variant="body1">
                    <FormattedDate date={props.createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {props.updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <Typography variant="body1">
                    <FormattedDate date={props.updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default ProductReceiveDetailCard
