import { Divider, Typography, styled } from '@mui/material'
import { FC, PropsWithChildren } from 'react'

type Props = {}

const LoginSectionDivider: FC<PropsWithChildren<Props>> = ({ children }) => {
  const TypographyStyled = styled(Typography)(({ theme }) => ({
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    fontWeight: 400,
  }))

  return (
    <Divider>
      <TypographyStyled variant="h6">{children}</TypographyStyled>
    </Divider>
  )
}

export default LoginSectionDivider
