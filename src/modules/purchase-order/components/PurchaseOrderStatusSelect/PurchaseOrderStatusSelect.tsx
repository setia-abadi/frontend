import { SelectOption } from 'components/Select'
import SelectField, { SelectFieldProps } from 'components/SelectField'
import { SELECT_OPTION_ALL } from 'constants/option'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { PURCHASE_ORDER_STATUS_TEXT } from 'modules/purchase-order/constants'
import { FC, useMemo } from 'react'

type Props = Omit<SelectFieldProps, 'options'>

const PurchaseOrderStatusSelect: FC<Props> = (props) => {
  const options = useMemo(() => {
    return Object.values(PurchaseOrderStatusEnum).map<SelectOption>((status) => ({
      label: PURCHASE_ORDER_STATUS_TEXT[status],
      value: status,
    }))
  }, [])

  return <SelectField displayEmpty {...props} options={[SELECT_OPTION_ALL, ...options]} />
}

export default PurchaseOrderStatusSelect
