import { Divider, Grid, Link, Stack, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'
import { FileModel } from 'models/file'
import Image from 'components/Image'
import { NavLink } from 'react-router-dom'
import { ID } from 'models/base'
import { IconUserCircle } from '@tabler/icons'

type Props = {
  returnedBy: string
  returnedById: ID
  description: string
  images: FileModel[]
  createdAt: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const PurchaseOrderReturnDetailCard: FC<Props> = ({ cardTitle, cardAction, ...props }) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>

          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Diretur Oleh
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <NavLink to={`/users/${props.returnedById}`}>
                    <Link>
                      <Stack direction="row" alignItems="center" gap={0.5}>
                        <IconUserCircle size="1.1rem" />
                        <span>{props.returnedBy}</span>
                      </Stack>
                    </Link>
                  </NavLink>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Diretur Pada
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <FormattedDate date={props.createdAt} />
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={4}>
                <Typography variant="body1" fontWeight={700}>
                  Alasan
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography variant="body1">
                  <ContentWithEmptyPlaceholder>{props.description}</ContentWithEmptyPlaceholder>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Typography variant="body1" fontWeight={700}>
                  Lampiran
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Stack direction="row" gap={1} flexWrap="wrap">
                  {props.images.map((image) => (
                    <a key={image.id} href={image.link} target="_blank" rel="noreferrer">
                      <Image width={100} aspectRatio={1} src={image.link} alt={image.name} />
                    </a>
                  ))}
                </Stack>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Paper>
  )
}

export default PurchaseOrderReturnDetailCard
