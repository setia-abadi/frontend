import { Divider, Grid, Stack } from '@mui/material'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import DatePickerField from 'components/DatePickerField'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import Paper from 'components/Paper'
import { SelectOption } from 'components/Select'
import TextInputField from 'components/TextInputField'
import dayjs, { Dayjs } from 'dayjs'
import { Form, Formik, FormikHelpers } from 'formik'
import { DisabledFields, FormShape } from 'models/base'
import SupplierForProductReceiveFormSelect from 'modules/suppllier/components/SupplierForProductReceiveFormSelect'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type PurchaseOrderFormCardShape = FormShape<{
  date: Dayjs
  supplier_id: SelectOption
  invoice_number: string
}>

type Props = {
  cardTitle: string
  initialValues?: PurchaseOrderFormCardShape
  disabledFields?: Partial<DisabledFields<PurchaseOrderFormCardShape>>
  onSubmit: (values: PurchaseOrderFormCardShape) => Promise<void>
  onCancel: () => void
}

const PurchaseOrderFormCard: FC<Props> = ({ cardTitle, initialValues, disabledFields, onSubmit, onCancel }) => {
  const [error, setError] = useState<string>()

  const defaultValues: PurchaseOrderFormCardShape = {
    date: dayjs(),
  }

  const handleSubmit = async (
    values: PurchaseOrderFormCardShape,
    helpers: FormikHelpers<PurchaseOrderFormCardShape>
  ) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<PurchaseOrderFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12} sm={5} md={4}>
                  <FormLabel required>Tanggal Terima</FormLabel>
                  <DatePickerField name="date" disabled={disabledFields?.date} fullWidth />
                  <FormErrorText message={formik.errors.date} />
                </Grid>

                <Grid item xs={12} sm={7} md={8}>
                  <FormLabel required>Invoice Number</FormLabel>
                  <TextInputField name="invoice_number" disabled={disabledFields?.invoice_number} fullWidth />
                  <FormErrorText message={formik.errors.invoice_number} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Supplier</FormLabel>
                  <SupplierForProductReceiveFormSelect
                    name="supplier_id"
                    disabled={disabledFields?.supplier_id}
                    fullWidth
                  />
                  <FormErrorText message={formik.errors.supplier_id} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { PurchaseOrderFormCardShape }
export default PurchaseOrderFormCard
