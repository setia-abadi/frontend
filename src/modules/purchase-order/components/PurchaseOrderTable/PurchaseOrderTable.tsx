import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import { PurchaseOrderModel } from 'models/purchaseOrder'
import FormattedCurrency from 'components/FormattedCurrency'
import PurchaseOrderStatusChip from '../PurchaseOrderStatusChip'
import FormattedDate from 'components/FormattedDate'
import ContentWithEmptyPlaceholder from 'components/ContentWithEmptyPlaceholder'

type Model = PurchaseOrderModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}
const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'date',
    title: 'Tanggal Terima',
    width: 175,
    enableSort: true,
    render: (node) => <FormattedDate date={node.date} withTime={false} />,
  },
  {
    key: 'invoice_number',
    title: 'Nomor Faktur',
    minWidth: 250,
    render: (node) => <ContentWithEmptyPlaceholder>{node.invoice_number}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'supplier',
    title: 'Supplier',
    width: 250,
    render: (node) => <ContentWithEmptyPlaceholder>{node.supplier?.name}</ContentWithEmptyPlaceholder>,
  },
  {
    key: 'total_estimated_price',
    title: 'Total Harga Perkiraan',
    width: 175,
    render: (node) => <FormattedCurrency value={node.total_estimated_price} />,
  },
  {
    key: 'status',
    title: 'Status',
    width: 125,
    render: (node) => <PurchaseOrderStatusChip status={node.status} />,
  },
]

const PurchaseOrderTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 75,
        render: action,
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default PurchaseOrderTable
