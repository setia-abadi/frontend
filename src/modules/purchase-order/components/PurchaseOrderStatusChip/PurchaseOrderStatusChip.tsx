import { ChipOwnProps, ChipProps } from '@mui/material'
import Chip from 'components/Chip'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { PURCHASE_ORDER_STATUS_TEXT } from 'modules/purchase-order/constants'
import { FC } from 'react'

type Props = Omit<ChipProps, 'color' | 'label'> & {
  status: PurchaseOrderStatusEnum
}

const CustomerDebtStatusChip: FC<Props> = ({ status, ...props }) => {
  const color: Record<PurchaseOrderStatusEnum, ChipOwnProps['color']> = {
    [PurchaseOrderStatusEnum.COMPLETED]: 'success',
    [PurchaseOrderStatusEnum.PENDING]: 'warning',
    [PurchaseOrderStatusEnum.ONGOING]: 'info',
    [PurchaseOrderStatusEnum.CANCELED]: 'error',
  }

  return <Chip label={PURCHASE_ORDER_STATUS_TEXT[status]} size="small" color={color[status]} {...props} />
}

export default CustomerDebtStatusChip
