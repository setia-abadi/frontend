import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'

const PURCHASE_ORDER_STATUS_TEXT: Record<PurchaseOrderStatusEnum, string> = {
  [PurchaseOrderStatusEnum.COMPLETED]: 'Selesai',
  [PurchaseOrderStatusEnum.PENDING]: 'Pending',
  [PurchaseOrderStatusEnum.ONGOING]: 'Diproses',
  [PurchaseOrderStatusEnum.CANCELED]: 'Dibatalkan',
}

export { PURCHASE_ORDER_STATUS_TEXT }
