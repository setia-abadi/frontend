import Table, { ColumnProps, TableProps } from 'components/Table'
import { FC, ReactNode, useMemo } from 'react'
import StatusChip from 'components/StatusChip'
import { CustomerModel } from 'models/customer'

type Model = CustomerModel
type Props = Omit<TableProps<Model>, 'columns'> & {
  action?: (item: Model) => ReactNode
}

const baseColumns: ColumnProps<Model>[] = [
  {
    key: 'name',
    title: 'Nama',
    minWidth: 250,
    enableSort: true,
    render: (node) => node.name,
  },
  {
    key: 'email',
    title: 'Alamat Email',
    width: 250,
    enableSort: true,
    render: (node) => node.email,
  },
  {
    key: 'phone',
    title: 'Nomor Telepon',
    width: 225,
    render: (node) => node.phone,
  },
  {
    key: 'status',
    title: 'Status',
    width: 150,
    render: (node) => <StatusChip isActive={node.is_active} />,
  },
]

const CustomerTable: FC<Props> = ({ action, ...props }) => {
  const columns = useMemo(() => {
    if (action) {
      return baseColumns.concat({
        key: 'action',
        title: 'Aksi',
        width: 100,
        minWidth: 100,
        render: (node) => action(node),
      })
    }
    return baseColumns
  }, [])

  return <Table<Model> columns={columns} {...props} />
}

export default CustomerTable
