import { Box, Divider, Grid, InputAdornment, Stack } from '@mui/material'
import { IconMail, IconPhone } from '@tabler/icons'
import APIUtil from 'utils/APIUtil'
import Alert from 'components/Alert'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormErrorText from 'components/FormErrorText'
import FormLabel from 'components/FormLabel'
import NumberInputField from 'components/NumberInputField'
import Paper from 'components/Paper'
import RadioGroupField from 'components/RadioGroupField'
import { SelectOption } from 'components/Select'
import TextInputField from 'components/TextInputField'
import { Form, Formik, FormikHelpers } from 'formik'
import { FormShape } from 'models/base'
import CustomerTypeForCustomerFormSelect from 'modules/customer-type/components/CustomerTypeForCustomerFormSelect'
import { FC, useState } from 'react'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import PinpointMapDialog from 'components/PinpointMapDialog'
import Map from 'components/Map'
import Pinpoint from 'components/PinpointMapDialog/Pinpoint'

type CustomerFormCardShape = FormShape<{
  address: string
  email: string
  is_active: boolean
  name: string
  phone: string
  latitude: number
  longitude: number
  customer_type_id: SelectOption
}>

type Props = {
  cardTitle: string
  initialValues?: CustomerFormCardShape
  onSubmit: (values: CustomerFormCardShape) => Promise<void>
  onCancel: () => void
}

const CustomerFormCard: FC<Props> = ({ cardTitle, initialValues, onSubmit, onCancel }) => {
  const [openPinpointDialog, setOpenPinpointDialog] = useState<boolean>(false)
  const [error, setError] = useState<string>()

  const defaultValues: CustomerFormCardShape = {
    is_active: true,
  }

  const handleSubmit = async (values: CustomerFormCardShape, helpers: FormikHelpers<CustomerFormCardShape>) => {
    try {
      helpers.setSubmitting(true)
      await onSubmit(values)
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response) {
        if (!TypeUtil.isEmpty(err.response.data.errors)) {
          helpers.setErrors(FormUtil.apiErrorsToFormikErrors(err.response.data.errors))
        } else {
          setError(err.response.data.message)
        }
      } else {
        setError(String(err))
      }
    } finally {
      helpers.setSubmitting(false)
    }
  }

  return (
    <Paper>
      <CardHeader title={cardTitle} />
      <Divider />
      <CardContent>
        <Formik<CustomerFormCardShape> initialValues={initialValues ?? defaultValues} onSubmit={handleSubmit}>
          {(formik) => (
            <Form onSubmit={formik.handleSubmit}>
              <Grid container spacing={3}>
                {error && (
                  <Grid item xs={12}>
                    <Alert variant="outlined" severity="error">
                      {error}
                    </Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <FormLabel required>Jenis Pelanggan</FormLabel>
                  <CustomerTypeForCustomerFormSelect name="customer_type_id" fullWidth />
                  <FormErrorText message={formik.errors.customer_type_id} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Nama</FormLabel>
                  <TextInputField name="name" fullWidth />
                  <FormErrorText message={formik.errors.name} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Alamat Email</FormLabel>
                  <TextInputField
                    name="email"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <IconMail size="1.3rem" />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <FormErrorText message={formik.errors.email} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Nomor Telepon</FormLabel>
                  <TextInputField
                    name="phone"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <IconPhone size="1.3rem" />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <FormErrorText message={formik.errors.phone} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Latitude</FormLabel>
                  <NumberInputField name="latitude" fullWidth />
                  <FormErrorText message={formik.errors.latitude} />
                </Grid>

                <Grid item xs={12} md={6}>
                  <FormLabel required>Longitue</FormLabel>
                  <NumberInputField name="longitude" fullWidth />
                  <FormErrorText message={formik.errors.longitude} />
                </Grid>

                <Grid item xs={12}>
                  <Map
                    width="100%"
                    height="220px"
                    latitude={formik.values.latitude ?? undefined}
                    longitude={formik.values.longitude ?? undefined}
                    options={{
                      zoomControl: false,
                      dragging: false,
                      scrollWheelZoom: false,
                    }}
                  >
                    {TypeUtil.isDefined(formik.values.latitude) && TypeUtil.isDefined(formik.values.longitude) && (
                      <Pinpoint />
                    )}
                    <Box position="absolute" zIndex={4} right={0} top={0} p={2}>
                      <Button
                        variant="contained"
                        onClick={() => setOpenPinpointDialog(true)}
                      >
                        Pilih Lokasi di Peta
                      </Button>
                    </Box>
                  </Map>

                  <PinpointMapDialog
                    dialogTitle="Pilih Lokasi"
                    open={openPinpointDialog}
                    latitude={formik.values.latitude ?? undefined}
                    longitude={formik.values.longitude ?? undefined}
                    onSelected={(lat, long) => {
                      formik.setFieldValue('latitude', lat)
                      formik.setFieldValue('longitude', long)
                      setOpenPinpointDialog(false)
                    }}
                    onClose={() => {
                      setOpenPinpointDialog(false)
                    }}
                  />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel required>Alamat</FormLabel>
                  <TextInputField name="address" multiline rows={4} fullWidth />
                  <FormErrorText message={formik.errors.address} />
                </Grid>

                <Grid item xs={12}>
                  <FormLabel>Status</FormLabel>
                  <RadioGroupField
                    row={true}
                    name="is_active"
                    options={[
                      { label: 'Aktif', value: true },
                      { label: 'Tidak Aktif', value: false },
                    ]}
                  />
                  <FormErrorText message={formik.errors.is_active} />
                </Grid>

                <Grid item xs={12}>
                  <Stack direction="row" gap={1}>
                    <Button type="submit" variant="contained">
                      Kirim
                    </Button>

                    <Button variant="contained" color="error" onClick={onCancel}>
                      Kembali
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </CardContent>
    </Paper>
  )
}

export type { CustomerFormCardShape }
export default CustomerFormCard
