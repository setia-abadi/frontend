import { Box, Divider, Grid, Typography } from '@mui/material'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import FormattedDate from 'components/FormattedDate'
import Paper from 'components/Paper'
import { FC, ReactNode } from 'react'
import StatusChip from 'components/StatusChip'
import Map from 'components/Map'
import Pinpoint from 'components/PinpointMapDialog/Pinpoint'

type Props = {
  address: string
  email: string
  isActive: boolean
  name: string
  phone: string
  latitude: number
  longitude: number
  customerType: string
  createdAt?: string
  updatedAt?: string
  cardAction?: ReactNode
  cardTitle?: ReactNode
}

const CustomerDetailCard: FC<Props> = ({
  address,
  email,
  isActive,
  name,
  phone,
  latitude,
  longitude,
  customerType,
  createdAt,
  updatedAt,
  cardTitle,
  cardAction,
}) => {
  return (
    <Paper>
      <CardHeader title={cardTitle} action={cardAction} />
      <Divider />
      <CardContent>
        <Grid container rowSpacing={3}>
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Nama
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{name}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Jenis Pelanggan
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{customerType}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Nomor Telepon
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{phone}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Alamat Email
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{email}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Kordinat
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">
                  {latitude}, {longitude}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Alamat
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <Typography variant="body1">{address}</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={12} sm={4} lg={3}>
                <Typography variant="body1" fontWeight={700}>
                  Status
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8} lg={9}>
                <StatusChip isActive={isActive} />
              </Grid>
            </Grid>
          </Grid>

          {createdAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Dibuat Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={createdAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}

          {updatedAt && (
            <Grid item xs={12}>
              <Grid container>
                <Grid item xs={12} sm={4} lg={3}>
                  <Typography variant="body1" fontWeight={700}>
                    Diubah Pada
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={8} lg={9}>
                  <Typography variant="body1">
                    <FormattedDate date={updatedAt} />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>

        <Box mt={3}>
          <Map
            height={250}
            width="100%"
            latitude={latitude}
            longitude={longitude}
            options={{
              zoomControl: false,
              dragging: false,
              scrollWheelZoom: false,
            }}
          >
            <Pinpoint />
          </Map>
        </Box>
      </CardContent>
    </Paper>
  )
}

export default CustomerDetailCard
