enum DeliveryOrderReviewTypeEnum {
  PRODUCT = 'PRODUCT',
  DELIVERY = 'DELIVERY',
}

export { DeliveryOrderReviewTypeEnum }

