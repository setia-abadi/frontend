enum ReturnOrderStatusEnum {
  PENDING = "PENDING",
  RETURNED = "RETURNED",
}

export { ReturnOrderStatusEnum }