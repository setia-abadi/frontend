enum TransactionPaymentTypeEnum {
  CASH = 'CASH',
  BCA_TRANSFER = 'BCA_TRANSFER',
}

export { TransactionPaymentTypeEnum }
