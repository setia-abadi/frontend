enum DeliveryOrderStatusEnum {
  PENDING = 'PENDING',
  ONGOING = 'ONGOING',
  DELIVERING = 'DELIVERING',
  CANCELED = 'CANCELED',
  COMPLETED = 'COMPLETED',
  RETURNED = 'RETURNED',
}

export { DeliveryOrderStatusEnum }
