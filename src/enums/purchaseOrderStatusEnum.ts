enum PurchaseOrderStatusEnum {
  PENDING = 'PENDING',
  ONGOING = 'ONGOING',
  CANCELED = 'CANCELED',
  COMPLETED = 'COMPLETED',
}

export { PurchaseOrderStatusEnum }
