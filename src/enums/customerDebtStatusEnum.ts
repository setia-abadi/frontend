enum CustomerDebtStatusEnum {
  UNPAID = "UNPAID",
  CANCELED = "CANCELED",
  PAID = "PAID",
  HALF_PAID = "HALF_PAID"
}

export { CustomerDebtStatusEnum }