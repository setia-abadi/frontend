enum CashierSessionStatusEnum {
  ACTIVE = 'ACTIVE',
  COMPLETED = 'COMPLETED',
}

export { CashierSessionStatusEnum }
