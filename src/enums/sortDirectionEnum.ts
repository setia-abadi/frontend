enum SortDirectionEnum {
  ASC = 'asc',
  DESC = 'desc',
}

export { SortDirectionEnum }