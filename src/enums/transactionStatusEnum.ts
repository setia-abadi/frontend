enum TransactionStatusEnum {
  UNPAID = 'UNPAID',
  PAID = 'PAID',
}

export { TransactionStatusEnum }
