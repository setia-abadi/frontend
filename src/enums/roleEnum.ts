enum RoleEnum {
  SUPER_ADMIN = 'Super Admin',
  INVENTORY = 'Inventory',
  CASHIER = 'Cashier',
  DRIVER = 'Driver',
}

export { RoleEnum }
