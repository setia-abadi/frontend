enum ProductReceiveStatusEnum {
  PENDING = 'PENDING',
  CANCELED = 'CANCELED',
  COMPLETED = 'COMPLETED',
  RETURNED = 'RETURNED',
}

export { ProductReceiveStatusEnum }
