enum ShopOrderTrackingStatusEnum {
  UNPAID = "UNPAID",
  AWAITING_SHIPMENT = "AWAITING_SHIPMENT",
  AWAITING_COLLECTION = "AWAITING_COLLECTION",
  PARTIALLY_SHIPPING = "PARTIALLY_SHIPPING",
  SHIPPING = "SHIPPING",
  CANCEL = "CANCEL",
  DELIVERED = "DELIVERED",
  COMPLETED = "COMPLETED",
  WILL_RETURN = "WILL_RETURN",
  RETURNED = "RETURNED"
} 

export { ShopOrderTrackingStatusEnum }
