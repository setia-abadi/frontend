enum ProductReturnStatusEnum {
  PENDING = 'PENDING',
  COMPLETED = 'COMPLETED',
}

export { ProductReturnStatusEnum }
