enum DebtStatusEnum {
  UNPAID = 'UNPAID',
  CANCELED = 'CANCELED',
  RETURNED = 'RETURNED',
  HALF_PAID = 'HALF_PAID',
  PAID = 'PAID',
}

export { DebtStatusEnum }
