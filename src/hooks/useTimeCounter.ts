import dayjs from 'dayjs'
import { useEffect, useMemo, useState } from 'react'

type UseTimeCounter = {
  hours: number
  minutes: number
  seconds: number
}

type UseTimeCounterProps = {
  startAt?: string
}

const useTimeCounter = ({ startAt }: UseTimeCounterProps): UseTimeCounter => {
  const [counter, setCounter] = useState<number>(0)

  useEffect(() => {
    if (startAt) {
      const diffSeconds = dayjs().diff(startAt, 'seconds')

      setCounter(diffSeconds < 0 ? 0 : diffSeconds)
      const intervalId = setInterval(() => {
        setCounter((prev) => prev + 1)
      }, 1000)

      return () => {
        clearInterval(intervalId)
      }
    }
  }, [startAt])

  const time = useMemo(() => {
    let hours = Math.floor(counter / 3600)
    let minutes = Math.floor((counter - hours * 3600) / 60)
    let seconds = counter - hours * 3600 - minutes * 60

    return { hours, minutes, seconds }
  }, [counter])

  return time
}

export { useTimeCounter }
