import { useEffect, useRef } from 'react'

const useSSE = <T,>(url: string, onMessage: (value: T) => void, config?: EventSourceInit) => {
  const eventSource = useRef<EventSource>()

  useEffect(() => {
    eventSource.current = new EventSource(url, config)

    eventSource.current.onmessage = (ev) => {
      onMessage(JSON.parse(ev.data))
    }

    return () => {
      eventSource.current?.close()
    }
  }, [])

  const closeConnection = () => {
    if (eventSource.current && eventSource.current.readyState !== 2) {
      eventSource.current.close()
    }
  }

  return { closeConnection }
}

export default useSSE
