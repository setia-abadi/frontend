import CircularProgress from '@mui/material/CircularProgress'
import Box from '@mui/material/Box'
import { FC } from 'react'
import { Navigate, useParams } from 'react-router'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import APIUtil from 'utils/APIUtil'
import DeliveryOrderReviewsAPI from 'apis/deliveryOrderReviewsAPI'
import FormUtil from 'utils/formUtil'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'
import Typography from 'components/Typography'
import DeliveryOrderReportFormCard, { DeliveryOrderReportFormCardShape } from 'modules/delivery-order/components/DeliveryOrderReportFormCard'

type Props = {}

const DeliveryOrderReportPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const { deliveryOrderId } = useParams()

  const {
    data: isExist,
    isLoading: isLoadingGetExist,
    error: errorGetExist,
  } = useQuery({
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_REVIEW_GET_EXIST, { deliveryOrderId }],
    queryFn: () => DeliveryOrderReviewsAPI.getIsExist(deliveryOrderId!),
    select: (response) => response.data.data.review_exist,
  })

  const { data: deliveryOrder, isLoading: isLoadingGetDeliveryOrder, error: errorGetDeliveryOrder } = useQuery({
    enabled: !isLoadingGetExist && !isExist,
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }],
    queryFn: () => DeliveryOrdersAPI.getForGuest(deliveryOrderId!),
    select: (response) => response.data.data.delivery_order,
  })

  const handleSubmit = async (values: DeliveryOrderReportFormCardShape) => {
    await DeliveryOrderReviewsAPI.createForGuest({
      delivery_order_id: deliveryOrderId!,
      description: FormUtil.withDefault(values.description, ''),
      type: FormUtil.withDefault(values.type, DeliveryOrderReviewTypeEnum.PRODUCT),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_REVIEW_GET_EXIST, { deliveryOrderId }] })
  }

  const error = errorGetExist || errorGetDeliveryOrder
  if (error && APIUtil.isAPIError(error)) {
    if (error.status === 404) {
      return <Navigate to="/error/404" />
    } else {
      return <Navigate to="/error/500" />
    }
  }

  if (isLoadingGetDeliveryOrder || isLoadingGetExist) {
    return (
      <Box display="flex" alignItems="center" justifyContent="center" height="100vh">
        <CircularProgress />
      </Box>
    )
  }

  if (isExist) {
    return (
      <Box display="flex" alignItems="center" justifyContent="center" height="100vh">
        <Box width="100%" maxWidth={600} textAlign="center">
          <Typography variant="h1" mb={4}>Terima Kasih atas Laporan Anda!</Typography>
          <Typography variant="h6" textAlign="center">Kami sangat menghargai partisipasi Anda dalam membantu kami meningkatkan layanan dan kualitas kami. Setiap laporan yang Anda kirimkan memberikan kontribusi berharga bagi kami dalam mengidentifikasi dan memperbaiki masalah, serta memastikan bahwa pengalaman Anda dengan kami tetap optimal.</Typography>
        </Box>
      </Box>
    )
  }

  if (deliveryOrder && deliveryOrder.status !== DeliveryOrderStatusEnum.COMPLETED) {
    return <Navigate to="/error/404" />
  }

  return (
    <Box display="flex" alignItems="center" justifyContent="center" height="100vh">
      <Box width="100%" maxWidth={400}>
        <DeliveryOrderReportFormCard onSubmit={handleSubmit} />
      </Box>
    </Box>
  )
}

export default DeliveryOrderReportPage
