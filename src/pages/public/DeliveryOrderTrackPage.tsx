import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import Stack from '@mui/material/Stack'
import SwipeableDrawer from '@mui/material/SwipeableDrawer'
import { Theme, styled } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'
import { FC, useState } from 'react'
import Paper from 'components/Paper'
import { Navigate, useParams } from 'react-router'
import { useQuery } from '@tanstack/react-query'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import TypeUtil from 'utils/typeUtil'
import APIUtil from 'utils/APIUtil'
import Typography from 'components/Typography'
import TrackingItem from 'modules/delivery-order/components/DeliveryOrderItemCard'
import Image from 'components/Image'
import driverAvatar from 'assets/images/map/driver.jpg'
import TrackingMap from 'modules/track-order/components/TrackingMap'
import CircularProgress from '@mui/material/CircularProgress'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import { Link } from 'react-router-dom'

type Props = {}

const drawerBleeding = 125

const Container = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  maxWidth: 450,
  overflowY: 'auto',
  background: 'transparent',
  zIndex: 10000,
  padding: theme.spacing(1.5),
}))

const SectionTitle = styled(Box)(({ theme }) => ({
  ...theme.typography.body1,
  fontSize: theme.typography.pxToRem(15),
  fontWeight: theme.typography.fontWeightBold,
  marginBottom: theme.spacing(1),
}))

const PaperStyled = styled(Paper)(({ theme }) => ({
  border: `1px solid ${theme.palette.grey[300]}`,
  boxShadow: 'none',
  [theme.breakpoints.up('lg')]: {
    border: 'none',
    boxShadow: theme.shadows[3],
  },
}))

const Puller = styled('div')(({ theme }) => ({
  width: 30,
  height: 6,
  backgroundColor: theme.palette.mode === 'light' ? theme.palette.grey[300] : theme.palette.grey[900],
  borderRadius: 3,
  position: 'absolute',
  top: 8,
  left: 'calc(50% - 15px)',
}))

const SwipeableArea = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: -drawerBleeding,
  borderTopLeftRadius: 8,
  borderTopRightRadius: 8,
  visibility: 'visible',
  background: theme.palette.background.paper,
  padding: theme.spacing(2),
  paddingTop: theme.spacing(3.5),
  right: 0,
  left: 0,
}))

const DeliveryOrderTrackPage: FC<Props> = () => {
  const { deliveryOrderId } = useParams()
  const [open, setOpen] = useState(false)
  const matches = useMediaQuery<Theme>((theme) => theme.breakpoints.up('lg'))

  const { data, error } = useQuery({
    enabled: TypeUtil.isDefined(deliveryOrderId),
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }],
    queryFn: () => DeliveryOrdersAPI.getForGuest(deliveryOrderId!),
    select: (response) => response.data.data.delivery_order,
  })

  if (error && APIUtil.isAPIError(error)) {
    if (error.status === 404) {
      return <Navigate to="/error/404" />
    }
  }

  if (!data) {
    return (
      <Box display="flex" alignItems="center" justifyContent="center" height="100vh">
        <CircularProgress />
      </Box>
    )
  }

  if (data && data.status === DeliveryOrderStatusEnum.COMPLETED) {
    return (
      <Box display="flex" alignItems="center" justifyContent="center" height="100vh">
        <Box width="100%" maxWidth={600} textAlign="center">
          <Typography variant="h1" mb={4}>
            Barang Anda Telah Sampai!
          </Typography>
          <Typography variant="h6" textAlign="center">
            Terima kasih telah berbelanja di toko setia abadi. Jika ada masalah pada barang atau pengantaran silahkan laporkan
            melalui <Link to={`/p/delivery-orders/report/${deliveryOrderId}`}>link</Link> berikut.
          </Typography>
        </Box>
      </Box>
    )
  }

  if (data && data.status !== DeliveryOrderStatusEnum.DELIVERING) {
    return <Navigate to="/error/404" />
  }

  const ItemSection = () => {
    return (
      <PaperStyled>
        <Box px={2} py={2}>
          <SectionTitle>Barang</SectionTitle>
          {data.items ? (
            <Stack gap={1} mt={2}>
              {data.items.map((item) => (
                <TrackingItem
                  key={item.id}
                  name={item.product_unit?.product.name ?? '-'}
                  imageSrc={item.product_unit?.product.image_file.link ?? ''}
                  priceTotal={item.price_total}
                  quantity={item.qty}
                  unit={item.product_unit?.unit?.name ?? '-'}
                />
              ))}
            </Stack>
          ) : (
            <Box px={1}>
              <Typography variant="body1" color="grey" textAlign="center">
                Tidak ada barang
              </Typography>
            </Box>
          )}
        </Box>
      </PaperStyled>
    )
  }

  const DriverSection = () => {
    return (
      <PaperStyled>
        <Box px={2} py={2}>
          <SectionTitle>Driver</SectionTitle>
          {data.drivers ? (
            <Stack direction="row" flexWrap="wrap" gap={1}>
              {data.drivers.map((driver) => (
                <Chip
                  sx={(theme) => ({ borderColor: theme.palette.grey[300] })}
                  key={driver.id}
                  avatar={<Avatar src={driverAvatar} alt={driver.name} />}
                  label={driver.name}
                  variant="outlined"
                />
              ))}
            </Stack>
          ) : (
            <></>
          )}
        </Box>
      </PaperStyled>
    )
  }

  const InvoiceSection = () => {
    return (
      <PaperStyled>
        <Box px={2} py={2}>
          <SectionTitle>Nomor Faktur</SectionTitle>
          <Typography variant="h6">{data.invoice_number}</Typography>
        </Box>
      </PaperStyled>
    )
  }

  const ProofSection = () => {
    return (
      <PaperStyled>
        <Box px={2} py={2}>
          <SectionTitle>Bukti Pengiriman</SectionTitle>
          {data.images ? (
            <Stack flexWrap="wrap" direction="row" gap={1} mt={2}>
              {data.images.map((image) => (
                <a key={image.id} href={image.file?.link!} target="_blank" rel="noreferrer">
                  <Image width={100} aspectRatio={1} src={image.file?.link!} />
                </a>
              ))}
            </Stack>
          ) : (
            <Box px={1}>
              <Typography variant="body1" color="grey" textAlign="center">
                Bukti pengiriman belum tersedia
              </Typography>
            </Box>
          )}
        </Box>
      </PaperStyled>
    )
  }

  return (
    <Box>
      <TrackingMap
        deliveryOrderId={deliveryOrderId!}
        latitute={data.customer!.latitude}
        longitude={data.customer!.longitude}
      />

      {matches ? (
        <Container>
          <Stack gap={1.5}>
            <InvoiceSection />
            <DriverSection />
            <ItemSection />
            <ProofSection />
          </Stack>
        </Container>
      ) : (
        <SwipeableDrawer
          container={document.body}
          anchor="bottom"
          open={open}
          onClose={() => setOpen(false)}
          onOpen={() => setOpen(true)}
          swipeAreaWidth={drawerBleeding}
          disableSwipeToOpen={false}
          ModalProps={{
            keepMounted: true,
          }}
          PaperProps={{
            sx: (theme) => ({
              overflow: 'visible',
              padding: theme.spacing(2),
              paddingTop: theme.spacing(1),
            }),
          }}
        >
          <SwipeableArea>
            <Puller />
            <InvoiceSection />
          </SwipeableArea>

          <Stack gap={1.5}>
            <DriverSection />
            <ItemSection />
            <ProofSection />
          </Stack>
        </SwipeableDrawer>
      )}
    </Box>
  )
}

export default DeliveryOrderTrackPage
