import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import PurchaseOrdersAPI from 'apis/purchaseOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import dayjs from 'dayjs'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PurchaseOrderFormCard, {
  PurchaseOrderFormCardShape,
} from 'modules/purchase-order/components/PurchaseOrderFormCard'
import { FC } from 'react'
import { Navigate, useNavigate, useParams } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemesanan Produk',
    href: '/purchase-orders',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const PurchaseOrderUpdatePage: FC<Props> = () => {
  const { purchaseOrderId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }],
    queryFn: () => PurchaseOrdersAPI.get(purchaseOrderId!),
    select: (response) => response.data.data.purchase_order,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: PurchaseOrderFormCardShape) => {
    const response = await PurchaseOrdersAPI.update(purchaseOrderId!, {
      invoice_number: FormUtil.withDefault(values.invoice_number, ''),
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
    })

    if (response.data.data.purchase_order) {
      return navigate(`/purchase-orders/${response.data.data.purchase_order.id}`)
    }

    return goBack()
  }

  if (data && data.status !== PurchaseOrderStatusEnum.PENDING) {
    return <Navigate to={`/purchase-orders/${purchaseOrderId}`} replace />
  }

  return (
    <PageContainer metaTitle="Ubah Pemesanan Produk">
      <PageHeader title="Ubah Pemesanan Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <PurchaseOrderFormCard
            cardTitle="Ubah"
            initialValues={{
              date: dayjs(data.date),
              invoice_number: data.invoice_number,
              supplier_id: data.supplier
                ? {
                    label: data.supplier.name,
                    value: data.supplier.id,
                  }
                : undefined,
            }}
            disabledFields={{
              supplier_id: true,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default PurchaseOrderUpdatePage
