import { Box } from '@mui/material'
import PurchaseOrdersAPI from 'apis/purchaseOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import PurchaseOrderFormCard, {
  PurchaseOrderFormCardShape,
} from 'modules/purchase-order/components/PurchaseOrderFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemesanan Produk',
    href: '/purchase-orders',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const PurchaseOrderCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: PurchaseOrderFormCardShape) => {
    const response = await PurchaseOrdersAPI.create({
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
      supplier_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.supplier_id), ''),
      invoice_number: FormUtil.withDefault(values.invoice_number, ''),
    })

    if (response.data.data.purchase_order) {
      return navigate(`/purchase-orders/${response.data.data.purchase_order.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Pemesanan Produk">
      <PageHeader title="Tambah Pemesanan Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <PurchaseOrderFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default PurchaseOrderCreatePage
