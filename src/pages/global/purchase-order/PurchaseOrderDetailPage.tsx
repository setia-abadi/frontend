import { Divider, Grid, IconButton, Menu, MenuItem, Stack, Tooltip } from '@mui/material'
import { IconCloudUpload, IconEdit, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import PurchaseOrdersAPI from 'apis/purchaseOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { PurchaseOrderImageModel } from 'models/purchaseOrderImage'
import { PurchaseOrderItemModel } from 'models/purchaseOrderItem'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import PurchaseOrderDetailCard from 'modules/purchase-order/components/PurchaseOrderDetailCard'
import PurchaseOrderImageFormDialog, {
  PurchaseOrderImageFormDialogShape,
} from 'modules/purchase-order/components/PurchaseOrderImageFormDialog'
import PurchaseOrderImageTable from 'modules/purchase-order/components/PurchaseOrderImageTable'
import PurchaseOrderItemFormDialog, {
  PurchaseOrderItemFormDialogShape,
} from 'modules/purchase-order/components/PurchaseOrderItemFormDialog'
import PurchaseOrderItemTable from 'modules/purchase-order/components/PurchaseOrderItemTable'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { Navigate, useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemesanan Produk',
    href: '/purchase-orders',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const PurchaseOrderDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { purchaseOrderId } = useParams()
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null)
  const [openAddItemDialog, setOpenAddItemDialog] = useState<boolean>(false)
  const [openAddImageDialog, setOpenAddImageDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(purchaseOrderId),
    queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }],
    queryFn: () => PurchaseOrdersAPI.get(purchaseOrderId!),
    select: (response) => response.data.data.purchase_order,
  })

  const complete = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin menyelesaikan pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await PurchaseOrdersAPI.completed(purchaseOrderId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di selesaikan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const ongoing = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin memproses pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await PurchaseOrdersAPI.ongoing(purchaseOrderId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di proses', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const cancel = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin membatalkan pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await PurchaseOrdersAPI.cancel(purchaseOrderId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di batalkan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteItem = async (item: PurchaseOrderItemModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Barang',
        description: `Apakah kamu yakin ingin menghapus barang "${item.product_unit?.product.name}"?`,
      })
      if (!isConfirmed) return

      await PurchaseOrdersAPI.deleteItem(purchaseOrderId!, item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
      snackbar.enqueueSnackbar('Barang telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteImage = async (image: PurchaseOrderImageModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Gambar',
        description: `Apakah kamu yakin ingin menghapus gambar "${image.file!.name}"?`,
      })
      if (!isConfirmed) return

      await PurchaseOrdersAPI.deleteImage(purchaseOrderId!, image.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
      snackbar.enqueueSnackbar('Gambar telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleAddItem = async (values: PurchaseOrderItemFormDialogShape) => {
    await PurchaseOrdersAPI.addItem(purchaseOrderId!, {
      price_per_unit: FormUtil.withDefault(values.price_per_unit, 0),
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.product_id), ''),
      unit_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.unit_id), ''),
      qty: FormUtil.withDefault(values.qty, 0),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
    snackbar.enqueueSnackbar('Barang telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddItemDialog(false)
  }

  const handleAddImage = async (values: PurchaseOrderImageFormDialogShape) => {
    await PurchaseOrdersAPI.addImage(purchaseOrderId!, {
      file_path: FormUtil.withDefault(values.file_path, ''),
      description: FormUtil.withDefault(values.description, null),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_GET, { purchaseOrderId }] })
    snackbar.enqueueSnackbar('Gamber telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddImageDialog(false)
  }

  if (!purchaseOrderId) {
    return <Navigate to="/error/404" />
  }

  const isPending = data?.status === PurchaseOrderStatusEnum.PENDING
  const isCompleted = data?.status === PurchaseOrderStatusEnum.COMPLETED
  const isOngoing = data?.status === PurchaseOrderStatusEnum.ONGOING
  const isCanceled = data?.status === PurchaseOrderStatusEnum.CANCELED

  return (
    <PageContainer metaTitle="Detail Pemesanan Produk">
      <PageHeader title="Detail Pemesanan Produk" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12} md={12}>
          {data ? (
            <PurchaseOrderDetailCard
              cardTitle="Detail"
              date={data.date}
              invoiceNumber={data.invoice_number}
              status={data.status}
              supplier={data.supplier!.name}
              totalPrice={data.total_estimated_price}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" alignItems="center" spacing={1}>
                  {!isCompleted && !isCanceled && (
                    <Button variant="outlined" onClick={(e) => setAnchorMenu(e.currentTarget)}>
                      Ubah Status
                    </Button>
                  )}

                  {isPending && (
                    <PermissionControl match={PagePermission.PURCHASE_ORDER_UPDATE}>
                      <NavLink to={`/purchase-orders/${data.id}/update`}>
                        <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                          Ubah
                        </Button>
                      </NavLink>
                    </PermissionControl>
                  )}

                  <Menu
                    anchorEl={anchorMenu}
                    open={TypeUtil.isDefined(anchorMenu)}
                    onClose={() => setAnchorMenu(null)}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    slotProps={{
                      paper: {
                        sx: (theme) => ({
                          marginTop: 1,
                          border: `1px solid ${theme.palette.grey[300]}`,
                          minWidth: 200,
                        }),
                      },
                    }}
                  >
                    {isPending && (
                      <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_ONGOING]}>
                        <MenuItem onClick={ongoing}>Diproses</MenuItem>
                      </PermissionControl>
                    )}
                    {isOngoing && (
                      <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_MARK_COMPLETE]}>
                        <MenuItem onClick={complete}>Selesai</MenuItem>
                      </PermissionControl>
                    )}
                    <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_CANCEL]}>
                      <MenuItem onClick={cancel}>Batalkan</MenuItem>
                    </PermissionControl>
                  </Menu>
                </Stack>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={7} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Barang"
              action={
                isPending && (
                  <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_ADD_ITEM]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddItemDialog(true)}
                    >
                      Tambah barang
                    </Button>
                  </PermissionControl>
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <PurchaseOrderItemTable
                  data={data.items ?? []}
                  action={
                    isPending
                      ? (item) => (
                          <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_DELETE_ITEM]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteItem(item)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[undefined, 175, 150, 150, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Gambar"
              action={
                <Button
                  variant="contained"
                  startIcon={<IconCloudUpload size="1.2rem" />}
                  onClick={() => setOpenAddImageDialog(true)}
                >
                  Unggah Gambar
                </Button>
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <PurchaseOrderImageTable
                  data={data.images ?? []}
                  action={
                    isPending
                      ? (image) => (
                          <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_DELETE_IMAGE]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteImage(image)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[300, undefined, 175, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <PurchaseOrderItemFormDialog
        open={openAddItemDialog}
        dialogTitle="Tambah Barang"
        purchaseOrderId={purchaseOrderId!}
        onCancel={() => setOpenAddItemDialog(false)}
        onClose={() => setOpenAddItemDialog(false)}
        onSubmit={handleAddItem}
      />

      <PurchaseOrderImageFormDialog
        open={openAddImageDialog}
        dialogTitle="Unggah Gambar"
        onCancel={() => setOpenAddImageDialog(false)}
        onClose={() => setOpenAddImageDialog(false)}
        onSubmit={handleAddImage}
      />
    </PageContainer>
  )
}

export default PurchaseOrderDetailPage
