import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import PurchaseOrdersAPI, { PayloadPurchaseOrdersFilter } from 'apis/purchaseOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SelectOptionParam, SortsParam } from 'libs/useQueryParam'
import { PurchaseOrderModel } from 'models/purchaseOrder'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import PurchaseOrderFilterFormPopover, {
  PurchaseOrderFilterFormPopoverShape,
} from 'modules/purchase-order/components/PurchaseOrderFilterFormPopover'
import PurchaseOrderTable from 'modules/purchase-order/components/PurchaseOrderTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'
import { SELECT_OPTION_ALL } from 'constants/option'
import FormUtil from 'utils/formUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemesanan Produk',
    href: '/purchase-orders',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)
const SupplierParam = withDefault(SelectOptionParam, SELECT_OPTION_ALL)

const PurchaseOrderListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [supplier, setSupplier] = useQueryParam('supplier', SupplierParam)
  const [status, setStatus] = useQueryParam('status', StringParam)

  const payload: PayloadPurchaseOrdersFilter = {
    page,
    phrase,
    limit,
    sorts,
    supplier_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(supplier), null),
    status: FormUtil.withDefault((status as PurchaseOrderStatusEnum), null),
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PURCHASE_ORDER_FILTER, payload],
    queryFn: () => PurchaseOrdersAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deletePurchaseOrder = async (item: PurchaseOrderModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Pemesanan Produk',
        description: `Apakah anda yakin ingin menghapus pemesanan produk dengan faktur ${item.invoice_number}`,
      })
      if (!isConfirmed) return

      await PurchaseOrdersAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PURCHASE_ORDER_FILTER, payload] })
      snackbar.enqueueSnackbar('Pemesanan produk telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setSupplier(undefined)
    setStatus(undefined)
  }

  const handleApplyFilter = (values: PurchaseOrderFilterFormPopoverShape) => {
    setSupplier(values.supplier)
    setStatus(values.status)
  }

  return (
    <PageContainer metaTitle="Daftar Pemesanan Produk">
      <PageHeader title="Daftar Pemesanan Produk" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Pemesanan Produk" onChange={setPhrase} />
                <PurchaseOrderFilterFormPopover
                  initialValues={{ supplier, status: status as PurchaseOrderStatusEnum }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.PURCHASE_ORDER_CREATE}>
                <NavLink to="/purchase-orders/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Pemesanan Produk
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <PurchaseOrderTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.PURCHASE_ORDER_DETAIL}>
                      <Tooltip title="Detail Pemesanan Produk">
                        <NavLink to={`/purchase-orders/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PURCHASE_ORDER_DELETE]}>
                      <Tooltip title="Hapus Pemesanan Produk">
                        <IconButton onClick={() => deletePurchaseOrder(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[175, undefined, 250, 175, 125, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default PurchaseOrderListPage
