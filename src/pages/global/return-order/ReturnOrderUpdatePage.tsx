import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ReturnOrderFormCard from 'modules/return-order/components/ReturnOrderFormCard'
import ReturnOrderUtil from 'modules/return-order/utils'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'

type Props = {}

const ReturnOrderUpdatePage: FC<Props> = () => {
  const { returnOrderId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { returnOrderId }],
    queryFn: () => null,
    select: (response) => ReturnOrderUtil.randomRecord(),
  })

  const goBack = () => {
    navigate(-1)
  }

  // const handleSubmit = async (values: UnitFormCardShape) => {
  //   const response = await UnitsAPI.update(returnOrderId!, {
  //     name: FormUtil.withDefault(values.name, ''),
  //     description: FormUtil.withDefault(values.description, null),
  //   })

  //   if (response.data.data.unit) {
  //     return navigate(`/return-orders/${response.data.data.unit.id}`)
  //   }

  //   return goBack()
  // }

  return (
    <PageContainer metaTitle="Ubah Pengiriman Pesanan">
      <PageHeader
        title="Ubah Pengiriman Pesanan"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pengiriman Pesanan',
            href: '/return-orders',
          },
          {
            title: 'Detail',
            href: `/return-orders/${returnOrderId}`,
          },
          {
            title: 'Ubah',
          },
        ]}
      />

      {data && (
        <Box mt={2}>
          <ReturnOrderFormCard
            cardTitle="Ubah"
            initialValues={{
              delivery_order_id: null,
              reason: data.reason,
            }}
            disabledFields={{
              delivery_order_id: true,
            }}
            onSubmit={async () => {}}
            onCancel={goBack}
          />
        </Box>
      )}
    </PageContainer>
  )
}

export default ReturnOrderUpdatePage
