import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import ReturnOrderTable from 'modules/return-order/components/ReturnOrderTable'
import ReturnOrderUtil from 'modules/return-order/utils'
import { ReturnOrderModel } from 'models/returnOrder'

type Props = unknown

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)

const ReturnOrderListPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.UNIT_FILTER],
    queryFn: () => null,
    select: (response) => ({
      limit: 10,
      nodes: Array.from({ length: 5 }, () => ReturnOrderUtil.randomRecord()),
      page: 1,
      total: 10,
    }),
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteReturnOrder = async (item: ReturnOrderModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Satuan',
        description: `Apakah anda yakin ingin menghapus pengembalian pesanan "${item.delivery_order?.invoice_number}"?`,
      })
      if (!isConfirmed) return

      // await UnitsAPI.delete(item.id)
      // queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.UNIT_FILTER, payload] })
      snackbar.enqueueSnackbar('Pengembalian pesanan telah berhasil di hapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  if (!data) {
    return 'Loading ...'
  }

  return (
    <PageContainer metaTitle="Daftar Pengembalian Pesanan">
      <PageHeader
        title="Daftar Pengembalian Pesanan"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pengembalian Pesanan',
            href: '/return-orders',
          },
        ]}
      />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <SearchBar initialValue={phrase} placeholder="Cari Pesanan" onChange={setPhrase} />
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <NavLink to="/return-orders/create">
                <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                  Tambah Pengembalian Pesanan
                </Button>
              </NavLink>
            </Grid>
          </Grid>

          <Box mt={2}>
            <ReturnOrderTable
              data={data.nodes}
              sorts={sorts}
              pagination={{
                page,
                rowsPerPage: limit,
                total: data.total,
                onPaginationChange: handlePaginationChange,
              }}
              onSortChange={setSorts}
              action={(item) => (
                <Stack direction="row" spacing={1}>
                  <Tooltip title="Detail Pengembalian Pesanan">
                    <NavLink to={`/return-orders/${item.id}`}>
                      <IconButton>
                        <IconEye size="1.2rem" />
                      </IconButton>
                    </NavLink>
                  </Tooltip>

                  <Tooltip title="Hapus Satuan">
                    <IconButton onClick={() => deleteReturnOrder(item)}>
                      <IconTrash size="1.2rem" />
                    </IconButton>
                  </Tooltip>
                </Stack>
              )}
            />
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ReturnOrderListPage
