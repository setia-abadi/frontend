import { Box, CardContent, Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconCheck, IconEdit, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import Button from 'components/Button'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { ReturnOrderStatusEnum } from 'enums/returnOrderStatusEnum'
import { FileModel } from 'models/file'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import ReturnOrderAttachmentFormDialog from 'modules/return-order/components/ReturnOrderAttachmentFormDialog'
import ReturnOrderAttachmentTable from 'modules/return-order/components/ReturnOrderAttachmentTable'
import ReturnOrderDetailCard from 'modules/return-order/components/ReturnOrderDetailCard'
import ReturnOrderUtil from 'modules/return-order/utils'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'

type Props = {}

const ReturnOrderDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const { returnOrderId } = useParams()
  const [openAddAttachmentDialog, setOpenAttachmentDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(returnOrderId),
    queryKey: [QueryKeyEnum.UNIT_GET, { returnOrderId }],
    queryFn: () => null,
    select: (response) => ReturnOrderUtil.randomRecord(),
  })

  const deleteAttachment = async (item: FileModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Lampiran',
        description: `Apakah anda yakin ingin menghapus lampiran "${item.name}"?`,
      })
      if (!isConfirmed) return

      // await ProductsAPI.delete(item.id)
      // queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_FILTER, payload] })
      snackbar.enqueueSnackbar('Lampiran telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const receiveReturnOrder = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Status Pengembalian Pesanan',
        description: `Apakah anda yakin ingin mengubah status pengembalian pesanan menjadi diterima?`,
      })
      if (!isConfirmed) return

      // await ProductsAPI.delete(item.id)
      // queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_FILTER, payload] })
      snackbar.enqueueSnackbar('Lampiran telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  if (!data) {
    return null
  }

  const isEditable = data.status === ReturnOrderStatusEnum.PENDING

  return (
    <PageContainer metaTitle="Detail Pengembalian Pesanan">
      <PageHeader
        title="Detail Pengembalian Pesanan"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pemgembalian Pesanan',
            href: '/return-orders',
          },
          {
            title: 'Detail',
          },
        ]}
      />

      <Box mt={2}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <ReturnOrderDetailCard
              cardTitle="Detail"
              date={data.date}
              deliveryOrderId={data.delivery_order_id}
              invoiceNumber={data.delivery_order?.invoice_number ?? ''}
              reason={data.reason}
              status={data.status}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" gap={1}>
                  {isEditable && (
                    <Button
                      variant="contained"
                      color="success"
                      startIcon={<IconCheck size="1.2rem" />}
                      onClick={receiveReturnOrder}
                    >
                      Retur Order Diterima
                    </Button>
                  )}

                  {isEditable && (
                    <NavLink to={`/return-orders/${data.id}/update`}>
                      <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                        Ubah
                      </Button>
                    </NavLink>
                  )}
                </Stack>
              }
            />
          </Grid>

          <Grid item xs={12}>
            <Paper>
              <CardHeader
                title="Lampiran"
                action={
                  isEditable && (
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAttachmentDialog(true)}
                    >
                      Tambah Lampiran
                    </Button>
                  )
                }
              />
              <Divider />
              <CardContent>
                <ReturnOrderAttachmentTable
                  data={data.attachments ?? []}
                  action={
                    isEditable
                      ? (attachment) => (
                          <Tooltip title="Hapus Lampiran">
                            <IconButton onClick={() => deleteAttachment(attachment)}>
                              <IconTrash size="1.2rem" />
                            </IconButton>
                          </Tooltip>
                        )
                      : undefined
                  }
                />
              </CardContent>
            </Paper>
          </Grid>
        </Grid>
      </Box>

      <ReturnOrderAttachmentFormDialog
        dialogTitle="Tambah Lampiran"
        dialogSubTitle="Silahkan masukkan beberapa lampiran pengembalian pesanan, lalu tekan tombol kirim untuk menambahkan lampiran."
        onCancel={() => setOpenAttachmentDialog(false)}
        onClose={() => setOpenAttachmentDialog(false)}
        onSubmit={async () => {}}
        open={openAddAttachmentDialog}
      />
    </PageContainer>
  )
}

export default ReturnOrderDetailPage
