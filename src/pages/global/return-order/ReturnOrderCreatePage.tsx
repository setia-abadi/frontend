import { Box } from '@mui/material'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import ReturnOrderFormCard from 'modules/return-order/components/ReturnOrderFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'

type Props = {}

const ReturnOrderCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  // const handleSubmit = async (values: UnitFormCardShape) => {
  //   const response = await UnitsAPI.create({
  //     name: FormUtil.withDefault(values.name, ''),
  //     description: FormUtil.withDefault(values.description, null),
  //   })

  //   if (response.data.data.unit) {
  //     return navigate(`/units/${response.data.data.unit.id}`)
  //   }

  //   goBack()
  // }

  return (
    <PageContainer metaTitle="Tambah Pengembalian Pesanan">
      <PageHeader
        title="Tambah Pengembalian Pesanan"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pengembalian Pesanan',
            href: '/return-orders',
          },
          {
            title: 'Tambah',
          },
        ]}
      />

      <Box mt={2}>
        <ReturnOrderFormCard cardTitle="Tambah" onSubmit={async () => {}} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default ReturnOrderCreatePage
