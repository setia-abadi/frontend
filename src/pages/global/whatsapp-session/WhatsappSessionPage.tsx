import { Grid } from '@mui/material'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import WhatsappAPI from 'apis/whatsappAPI'
import PageContainer from 'components/PageContainer'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import WhatsappSessionActiveCard from 'modules/whatsapp-session/components/WhatsappSessionActiveCard'
import WhatsappSessionLoginQrCard from 'modules/whatsapp-session/components/WhatsappSessionLoginQrCard'
import { useSnackbar } from 'notistack'
import { FC } from 'react'

type Props = unknown

const WhatsappSessionPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()

  const { data, isLoading } = useQuery({
    queryKey: [QueryKeyEnum.WHATSAPP_SESSION_GET_CURRENT],
    queryFn: () => WhatsappAPI.isLoggedIn(),
    select: (response) => response.data.data.is_logged_in,
  })

  const handleEndSession = async () => {
    const { isConfirmed } = await confirmation.ask({
      title: 'Akhiri Sesi',
      description: 'Apakah Anda yakin ingin mengakhiri sesi whatsapp?',
    })
    if (!isConfirmed) return

    await WhatsappAPI.logout()
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.WHATSAPP_SESSION_GET_CURRENT] })
    snackbar.enqueueSnackbar('Sesi telah berhasil diakhiri', { variant: 'success' })
  }

  const handleLoggedIn = () => {
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.WHATSAPP_SESSION_GET_CURRENT] })
  }

  if (isLoading) {
    return 'Loading ...'
  }

  return (
    <PageContainer metaTitle="Sesi Whatsapp">
      <Grid container justifyContent="center" alignItems="center" sx={{ flex: 1 }}>
        <Grid item xs={12} sm={6} md={4} lg={5} xl={4}>
          {data ? (
            <WhatsappSessionActiveCard onEndSessionClick={handleEndSession} />
          ) : (
            <WhatsappSessionLoginQrCard onLoggedIn={handleLoggedIn} />
          )}
        </Grid>
      </Grid>
    </PageContainer>
  )
}

export default WhatsappSessionPage
