import { Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEdit, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import UsersAPI from 'apis/usersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { RoleModel } from 'models/role'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import RoleTable from 'modules/role/components/RoleTable'
import UserDetailCard from 'modules/user/components/UserDetailCard'
import UserPasswordFormDialog, { UserPasswordFormDialogShape } from 'modules/user/components/UserPasswordFormDialog'
import UserRoleFormDialog, { UserRoleFormDialogShape } from 'modules/user/components/UserRoleFormDialog'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import APIUtil from 'utils/APIUtil'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengguna',
    href: '/users',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const UserDetailPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()
  const { userId } = useParams()
  const { enqueueSnackbar } = useSnackbar()
  const [openAddRoleDialog, setOpenAddRoleDialog] = useState<boolean>(false)
  const [openChangePasswordDialog, setOpenChangePasswordDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(userId),
    queryKey: [QueryKeyEnum.USER_GET, { userId }],
    queryFn: () => UsersAPI.get(userId!),
    select: (response) => response.data.data.user,
  })

  const handleSubmitAddRoleForm = async (values: UserRoleFormDialogShape) => {
    await UsersAPI.addRole(userId!, {
      role_id: FormUtil.withDefault(values.role_id?.value?.toString(), ''),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.USER_GET, { userId }] })
    enqueueSnackbar('Peran telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddRoleDialog(false)
  }

  const handleChangePassword = async (values: UserPasswordFormDialogShape) => {
    await UsersAPI.updatePassword(userId!, {
      password: FormUtil.withDefault(values.password, ''),
    })
    enqueueSnackbar('Kata sandi telah berhasil diubah', { variant: 'success' })
    setOpenChangePasswordDialog(false)
  }

  const deleteUserRole = async (role: RoleModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Peran',
        description: `Apakah kamu yakin ingin menghapus peran "${role.name}"?`,
      })
      if (!isConfirmed) return

      const response = await UsersAPI.deleteRole(userId!, role.id)
      if (response.data.data.user) {
        queryClient.setQueryData([QueryKeyEnum.USER_GET, { userId }], () => response)
      }

      enqueueSnackbar('Peran telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Detail Pengguna">
      <PageHeader title="Detail Pengguna" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          {data ? (
            <UserDetailCard
              cardTitle="Profil"
              name={data.name}
              username={data.username}
              isActive={data.is_active}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" spacing={1}>
                  <PermissionControl match={[PermissionEnum.USER_UPDATE_PASSWORD]}>
                    <Button variant="contained" color="warning" onClick={() => setOpenChangePasswordDialog(true)}>
                      Ubah Password
                    </Button>
                  </PermissionControl>

                  <PermissionControl match={PagePermission.USER_UPDATE}>
                    <NavLink to={`/users/${data.id}/update`}>
                      <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                        Ubah
                      </Button>
                    </NavLink>
                  </PermissionControl>
                </Stack>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Profil" rows={3} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Peran"
              action={
                <PermissionControl match={[PermissionEnum.USER_ADD_ROLE]}>
                  {data ? (
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddRoleDialog(true)}
                    >
                      Tambah Peran
                    </Button>
                  ) : (
                    <Skeleton.Button />
                  )}
                </PermissionControl>
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <RoleTable
                  data={data.roles ?? []}
                  action={(role) => (
                    <PermissionControl match={[PermissionEnum.USER_DELETE_ROLE]}>
                      <Tooltip title="Hapus Peran">
                        <IconButton onClick={() => deleteUserRole(role)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  )}
                />
              ) : (
                <Skeleton.Table columns={[300, undefined, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <UserRoleFormDialog
        open={openAddRoleDialog}
        dialogTitle="Tambah Peran"
        dialogSubTitle="Untuk menambahkan peran, pilih peran dibawah lalu tekan tombol kirim."
        onCancel={() => setOpenAddRoleDialog(false)}
        onClose={() => setOpenAddRoleDialog(false)}
        onSubmit={handleSubmitAddRoleForm}
      />

      <UserPasswordFormDialog
        open={openChangePasswordDialog}
        dialogTitle="Ubah Kata Sandi"
        dialogSubTitle="Untuk mengubah kata sandi, silahkan isi kata sandi baru dan konfirmasi kata sandi lalu tekan simpan."
        onCancel={() => setOpenChangePasswordDialog(false)}
        onClose={() => setOpenChangePasswordDialog(false)}
        onSubmit={handleChangePassword}
      />
    </PageContainer>
  )
}

export default UserDetailPage
