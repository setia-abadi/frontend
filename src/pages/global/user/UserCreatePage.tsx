import { Box } from '@mui/material'
import UsersAPI from 'apis/usersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import UserFormCard, { UserFormCardShape } from 'modules/user/components/UserFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengguna',
    href: '/users',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const UserCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: UserFormCardShape) => {
    const response = await UsersAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      password: FormUtil.withDefault(values.password, ''),
      username: FormUtil.withDefault(values.username, ''),
      is_active: FormUtil.withDefault(values.is_active, false),
    })

    if (response.data.data.user) {
      return navigate(`/users/${response.data.data.user.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Pengguna">
      <PageHeader title="Tambah Pengguna" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <UserFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default UserCreatePage
