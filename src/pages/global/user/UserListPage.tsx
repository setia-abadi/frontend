import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus } from '@tabler/icons'
import { keepPreviousData, useQuery } from '@tanstack/react-query'
import UsersAPI, { PayloadUsersFilter } from 'apis/usersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import PermissionControl from 'modules/permission/components/PermissionControl'
import UserFilterFormPopover, {
  UserFilterFormPopoverShape,
} from 'modules/user/components/UserFilterFormPopover/UserFilterFormPopover'
import UsersTable from 'modules/user/components/UsersTable'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { BooleanParam, NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengguna',
    href: '/users',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableBooleanParam = withDefault(BooleanParam, null)
const NullableStringParam = withDefault(StringParam, null)

const UserListPage: FC<Props> = () => {
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [isActive, setIsActive] = useQueryParam('is-active', NullableBooleanParam)

  const payload: PayloadUsersFilter = {
    page,
    phrase,
    limit,
    sorts,
    is_active: isActive,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.USER_FILTER, payload],
    placeholderData: keepPreviousData,
    queryFn: () => UsersAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setIsActive(undefined)
  }

  const handleApplyFilter = (values: UserFilterFormPopoverShape) => {
    setIsActive(values.is_active)
  }

  return (
    <PageContainer metaTitle="Daftar Pengguna">
      <PageHeader title="Daftar Pengguna" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" gap={1}>
                <SearchBar placeholder="Cari Pengguna" initialValue={phrase} onChange={setPhrase} />
                <UserFilterFormPopover
                  initialValues={{ is_active: isActive }}
                  onReset={handleResetFilter}
                  onApply={handleApplyFilter}
                />
              </Stack>
            </Grid>
            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.USER_CREATE]}>
                <NavLink to="/users/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Pengguna
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {!data ? (
              <Skeleton.Table columns={[250, 200, 150, 100]} />
            ) : (
              <UsersTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={2}>
                    <PermissionControl match={PagePermission.USER_CREATE}>
                      <Tooltip title="Detail Pengguna">
                        <NavLink to={`/users/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default UserListPage
