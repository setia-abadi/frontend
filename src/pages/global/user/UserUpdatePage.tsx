import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import UsersAPI from 'apis/usersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import UpdateUserFromCard, { UpdateUserFormCardShape } from 'modules/user/components/UpdateUserFromCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengguna',
    href: '/users',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const UserUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const { userId } = useParams()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(userId),
    queryKey: [QueryKeyEnum.USER_GET, { userId }],
    queryFn: () => UsersAPI.get(userId!),
    select: (response) => response.data.data.user,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: UpdateUserFormCardShape) => {
    const response = await UsersAPI.update(userId!, {
      name: FormUtil.withDefault(values.name, ''),
      is_active: FormUtil.withDefault(values.is_active, false),
    })

    if (response.data.data.user) {
      return navigate(`/users/${response.data.data.user.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Pengguna">
      <PageHeader title="Ubah Pengguna" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <UpdateUserFromCard
            cardTitle="Ubah"
            initialValues={{ name: data.name, is_active: data.is_active }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default UserUpdatePage
