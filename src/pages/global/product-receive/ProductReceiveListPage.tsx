import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductReceivesAPI, { PayloadProductReceivesFilter } from 'apis/productReceivesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SelectOptionParam, SortsParam } from 'libs/useQueryParam'
import { ProductReceiveModel } from 'models/productReceive'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductReceiveFilterFormPopover, {
  ProductReceiveFilterFormPopoverShape,
} from 'modules/product-receive/components/ProductReceiveFilterFormPopover'
import ProductReceiveTable from 'modules/product-receive/components/ProductReceiveTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'
import { SELECT_OPTION_ALL } from 'constants/option'
import FormUtil from 'utils/formUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Penerimaan Produk',
    href: '/product-receives',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)
const SupplierParam = withDefault(SelectOptionParam, SELECT_OPTION_ALL)

const ProductReceiveListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [supplier, setSupplier] = useQueryParam('supplier', SupplierParam)
  const [status, setStatus] = useQueryParam('status', StringParam)

  const payload: PayloadProductReceivesFilter = {
    page,
    phrase,
    limit,
    sorts,
    supplier_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(supplier), null),
    status: FormUtil.withDefault(status as ProductReceiveStatusEnum, null),
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_FILTER, payload],
    queryFn: () => ProductReceivesAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteProductReceive = async (item: ProductReceiveModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Penerimaan Produk',
        description: `Apakah anda yakin ingin menghapus penerimaan produk dengan faktur ${item.invoice_number}`,
      })
      if (!isConfirmed) return

      await ProductReceivesAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_FILTER, payload] })
      snackbar.enqueueSnackbar('Penerimaan produk telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setSupplier(undefined)
    setStatus(undefined)
  }

  const handleApplyFilter = (values: ProductReceiveFilterFormPopoverShape) => {
    setSupplier(values.supplier)
    setStatus(values.status)
  }

  return (
    <PageContainer metaTitle="Daftar Penerimaan Produk">
      <PageHeader title="Daftar Penerimaan Produk" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Penerimaan Produk" onChange={setPhrase} />
                <ProductReceiveFilterFormPopover
                  initialValues={{ supplier, status: status as ProductReceiveStatusEnum }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <ProductReceiveTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.PRODUCT_RECEIVE_DETAIL}>
                      <Tooltip title="Detail Penerimaan Produk">
                        <NavLink to={`/product-receives/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PRODUCT_RECEIVE_DELETE]}>
                      <Tooltip title="Hapus Penerimaan Produk">
                        <IconButton onClick={() => deleteProductReceive(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[175, undefined, 250, 175, 125, 125]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ProductReceiveListPage
