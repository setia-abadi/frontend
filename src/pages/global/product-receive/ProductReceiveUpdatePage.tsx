import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import ProductReceivesAPI from 'apis/productReceivesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import dayjs from 'dayjs'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ProductReceiveFormCard, {
  ProductReceiveFormCardShape,
} from 'modules/product-receive/components/ProductReceiveFormCard'
import { FC } from 'react'
import { Navigate, useNavigate, useParams } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Penerimaan Produk',
    href: '/product-receives',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const ProductReceiveUpdatePage: FC<Props> = () => {
  const { productReceiveId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }],
    queryFn: () => ProductReceivesAPI.get(productReceiveId!),
    select: (response) => response.data.data.product_receive,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductReceiveFormCardShape) => {
    const response = await ProductReceivesAPI.update(productReceiveId!, {
      invoice_number: FormUtil.withDefault(values.invoice_number, ''),
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
    })

    if (response.data.data.product_receive) {
      return navigate(`/product-receives/${response.data.data.product_receive.id}`)
    }

    return goBack()
  }

  if (data && data.status !== ProductReceiveStatusEnum.PENDING) {
    return <Navigate to={`/product-receives/${productReceiveId}`} replace />
  }

  return (
    <PageContainer metaTitle="Ubah Penerimaan Produk">
      <PageHeader title="Ubah Penerimaan Produk" breadcrumbs={pageBreadcrumbs} />

      {data && (
        <Box mt={2}>
          <ProductReceiveFormCard
            cardTitle="Ubah"
            initialValues={{
              date: dayjs(data.date),
              invoice_number: data.invoice_number,
              supplier_id: data.supplier
                ? {
                    label: data.supplier.name,
                    value: data.supplier.id,
                  }
                : undefined,
            }}
            disabledFields={{
              supplier_id: true,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        </Box>
      )}
    </PageContainer>
  )
}

export default ProductReceiveUpdatePage
