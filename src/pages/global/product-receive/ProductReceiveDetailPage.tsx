import { Divider, Grid, IconButton, Menu, MenuItem, Stack, Tooltip } from '@mui/material'
import { IconCloudUpload, IconEdit, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductReceivesAPI from 'apis/productReceivesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { ProductReceiveImageModel } from 'models/productReceiveImage'
import { ProductReceiveItemModel } from 'models/productReceiveItem'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductReceiveDetailCard from 'modules/product-receive/components/ProductReceiveDetailCard'
import ProductReceiveImageFormDialog, {
  ProductReceiveImageFormDialogShape,
} from 'modules/product-receive/components/ProductReceiveImageFormDialog'
import ProductReceiveImageTable from 'modules/product-receive/components/ProductReceiveImageTable'
import ProductReceiveItemFormDialog, {
  ProductReceiveItemFormDialogShape,
} from 'modules/product-receive/components/ProductReceiveItemFormDialog'
import ProductReceiveItemTable from 'modules/product-receive/components/ProductReceiveItemTable'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { Navigate, useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import ProductReceiveReturnDetailCard from 'modules/product-receive/components/ProductReceiveReturnDetailCard'
import Skeleton from 'components/Skeleton'
import ProductReceiveQtyEligibleItemFormDialog from 'modules/product-receive/components/ProductReceiveQtyEligibleItemFormDialog'
import { ProductReceiveQtyEligibleItemFormDialogShape } from 'modules/product-receive/components/ProductReceiveQtyEligibleItemFormDialog/ProductReceiveQtyEligibleItemFormDialog'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Penerimaan Produk',
    href: '/product-receives',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const ProductReceiveDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { productReceiveId } = useParams()
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null)
  const [openAddItemDialog, setOpenAddItemDialog] = useState<boolean>(false)
  const [openAddImageDialog, setOpenAddImageDialog] = useState<boolean>(false)
  const [openEditItemQtyEligibleDialog, setOpenEditItemQtyEligibleDialog] = useState<boolean>(false)
  const [selectedItemForEdit, setSelectedItemForEdit] = useState<ProductReceiveItemModel>()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(productReceiveId),
    queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }],
    queryFn: () => ProductReceivesAPI.get(productReceiveId!),
    select: (response) => response.data.data.product_receive,
  })

  const complete = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin menyelesaikan pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await ProductReceivesAPI.completed(productReceiveId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di selesaikan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const cancel = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin membatalkan pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await ProductReceivesAPI.cancel(productReceiveId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di batalkan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteImage = async (image: ProductReceiveImageModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Gambar',
        description: `Apakah kamu yakin ingin menghapus gambar "${image.file!.name}"?`,
      })
      if (!isConfirmed) return

      await ProductReceivesAPI.deleteImage(productReceiveId!, image.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
      snackbar.enqueueSnackbar('Gambar telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleAddItem = async (values: ProductReceiveItemFormDialogShape) => {
    await ProductReceivesAPI.addItem(productReceiveId!, {
      price_per_unit: FormUtil.withDefault(values.price_per_unit, 0),
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.product_id), ''),
      unit_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.unit_id), ''),
      qty: FormUtil.withDefault(values.qty, 0),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
    snackbar.enqueueSnackbar('Barang telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddItemDialog(false)
  }

  const handleAddImage = async (values: ProductReceiveImageFormDialogShape) => {
    await ProductReceivesAPI.addImage(productReceiveId!, {
      file_path: FormUtil.withDefault(values.file_path, ''),
      description: FormUtil.withDefault(values.description, null),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
    snackbar.enqueueSnackbar('Gambar telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddImageDialog(false)
  }

  const handleSubmitItemQtyEligibleForm = async (values: ProductReceiveQtyEligibleItemFormDialogShape) => {
    await ProductReceivesAPI.updateItem(productReceiveId!, selectedItemForEdit!.id, {
      qty_eligible: FormUtil.withDefault(values.qty_eligible, 0),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RECEIVE_GET, { productReceiveId }] })
    snackbar.enqueueSnackbar('Kuantias barang yang diterima telah berhasil diubah', { variant: 'success' })
    setOpenEditItemQtyEligibleDialog(false)
  }

  if (!productReceiveId) {
    return <Navigate to="/error/404" />
  }

  const isPending = data?.status === ProductReceiveStatusEnum.PENDING
  const isReturned = data?.status === ProductReceiveStatusEnum.RETURNED

  return (
    <PageContainer metaTitle="Detail Penerimaan Produk">
      <PageHeader title="Detail Penerimaan Produk" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12} md={isReturned ? 6 : 12}>
          {data ? (
            <ProductReceiveDetailCard
              cardTitle="Detail"
              date={data.date}
              invoiceNumber={data.invoice_number}
              status={data.status}
              supplier={data.supplier!.name}
              totalPrice={data.total_price}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" alignItems="center" spacing={1}>
                  {isPending && (
                    <Button variant="outlined" onClick={(e) => setAnchorMenu(e.currentTarget)}>
                      Ubah Status
                    </Button>
                  )}

                  {isPending && (
                    <PermissionControl match={PagePermission.PRODUCT_RECEIVE_UPDATE}>
                      <NavLink to={`/product-receives/${data.id}/update`}>
                        <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                          Ubah
                        </Button>
                      </NavLink>
                    </PermissionControl>
                  )}

                  <Menu
                    anchorEl={anchorMenu}
                    open={TypeUtil.isDefined(anchorMenu)}
                    onClose={() => setAnchorMenu(null)}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    slotProps={{
                      paper: {
                        sx: (theme) => ({
                          marginTop: 1,
                          border: `1px solid ${theme.palette.grey[300]}`,
                          minWidth: 200,
                        }),
                      },
                    }}
                  >
                    <PermissionControl match={[PermissionEnum.PRODUCT_RECEIVE_MARK_COMPLETE]}>
                      <MenuItem onClick={complete}>Selesai</MenuItem>
                    </PermissionControl>
                    <PermissionControl match={[PermissionEnum.PRODUCT_RECEIVE_CANCEL]}>
                      <MenuItem onClick={cancel}>Batalkan</MenuItem>
                    </PermissionControl>
                  </Menu>
                </Stack>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={7} />
          )}
        </Grid>

        {isReturned && data.return && (
          <Grid item xs={12} md={6}>
            <ProductReceiveReturnDetailCard
              cardTitle="Detail Retur"
              createdAt={data.return.created_at}
              description={data.return.description}
              images={data.return.images!.map((image) => image.file!)}
              returnedBy={data.return.created_by?.name ?? ''}
              returnedById={data.return.created_by?.id ?? ''}
            />
          </Grid>
        )}

        <Grid item xs={12}>
          <Paper>
            <CardHeader title="Barang" />
            <Divider />
            <CardContent>
              {data ? (
                <ProductReceiveItemTable
                  data={data.items ?? []}
                  onEditQtyBtnClick={
                    isPending
                      ? (node) => {
                          setSelectedItemForEdit(node)
                          setOpenEditItemQtyEligibleDialog(true)
                        }
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[undefined, 175, 150, 150, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Gambar"
              action={
                <Button
                  variant="contained"
                  startIcon={<IconCloudUpload size="1.2rem" />}
                  onClick={() => setOpenAddImageDialog(true)}
                >
                  Unggah Gambar
                </Button>
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <ProductReceiveImageTable
                  data={data.images ?? []}
                  action={
                    isPending
                      ? (image) => (
                          <PermissionControl match={[PermissionEnum.PRODUCT_RECEIVE_DELETE_IMAGE]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteImage(image)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[350, undefined, 175, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <ProductReceiveItemFormDialog
        open={openAddItemDialog}
        dialogTitle="Tambah Barang"
        productReceiveId={productReceiveId!}
        onCancel={() => setOpenAddItemDialog(false)}
        onClose={() => setOpenAddItemDialog(false)}
        onSubmit={handleAddItem}
      />

      <ProductReceiveImageFormDialog
        open={openAddImageDialog}
        dialogTitle="Unggah Gambar"
        onCancel={() => setOpenAddImageDialog(false)}
        onClose={() => setOpenAddImageDialog(false)}
        onSubmit={handleAddImage}
      />

      {selectedItemForEdit && (
        <ProductReceiveQtyEligibleItemFormDialog
          open={openEditItemQtyEligibleDialog}
          dialogTitle="Ubah Kuantitas Terima"
          dialogSubTitle="Untuk mengubah kuantias yang di terima, silahkan masukkan kuantias yang akan diterima."
          maxQty={selectedItemForEdit.qty}
          initialValues={{
            qty_eligible: selectedItemForEdit.qty_eligible,
          }}
          onCancel={() => setOpenEditItemQtyEligibleDialog(false)}
          onClose={() => setOpenEditItemQtyEligibleDialog(false)}
          onClosed={() => setSelectedItemForEdit(undefined)}
          onSubmit={handleSubmitItemQtyEligibleForm}
        />
      )}
    </PageContainer>
  )
}

export default ProductReceiveDetailPage
