import { Box, Divider, Grid } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import ShopOrdersAPI from 'apis/shopOrdersAPI'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ShopOrderDetailCard from 'modules/shop-order/components/ShopOrderDetailCard'
import ShopOrderItemTable from 'modules/shop-order/components/ShopOrderItemTable'
import ShopOrderRecepientDetailCard from 'modules/shop-order/components/ShopOrderRecepientDetailCard'
import { FC } from 'react'
import { useParams } from 'react-router'

type Props = {}

const ShopOrderDetailPage: FC<Props> = () => {
  const { shopOrderId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SHOP_ORDER_GET, { shopOrderId }],
    queryFn: () => ShopOrdersAPI.get(shopOrderId!),
    select: (response) => response.data.data.shop_order,
  })

  if (!data) {
    return 'Loading...'
  }

  return (
    <PageContainer metaTitle="Detail Pesanan Toko">
      <PageHeader
        title="Detail Pesanan Toko"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pesanan Toko',
            href: '/shop-orders',
          },
          {
            title: 'Detail',
          },
        ]}
      />

      <Box mt={2}>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <ShopOrderDetailCard
              cardTitle="Detail Pesanan"
              platformIdentifier={data.platform_identifier}
              platformType={data.platform_type}
              shippingFee={data.shipping_fee}
              serviceFee={data.service_fee}
              trackingNumber={data.tracking_number}
              trackingStatus={data.tracking_status}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
            />
          </Grid>

          <Grid item xs={12} md={6}>
            <ShopOrderRecepientDetailCard
              cardTitle="Penerima"
              fullAddress={data.recipient_full_address}
              name={data.recipient_name}
              phoneNumber={data.recipient_phone_number}
            />
          </Grid>

          <Grid item xs={12}>
            <Paper>
              <CardHeader title="Barang" />
              <Divider />
              <CardContent>
                <ShopOrderItemTable
                  data={data.items ?? []}
                  subTotal={data.subtotal}
                  tax={data.tax}
                  total={data.total_amount}
                  shippingFee={data.shipping_fee}
                  serviceFee={data.service_fee}
                />
              </CardContent>
            </Paper>
          </Grid>
        </Grid>
      </Box>
    </PageContainer>
  )
}

export default ShopOrderDetailPage
