import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import ShopOrdersAPI, { PayloadShopOrderFilter } from 'apis/shopOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { ShopOrderPlatformTypeEnum } from 'enums/shopOrderPlatformTypeEnum'
import { ShopOrderTrackingStatusEnum } from 'enums/shopOrderTrackingStatusEnum'
import { SortsParam } from 'libs/useQueryParam'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ShopOrderFilterFormPopover, {
  ShopOrderFilterFormPopoverShape,
} from 'modules/shop-order/components/ShopOrderFilterFormPopover'
import ShopOrderTable from 'modules/shop-order/components/ShopOrderTable'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import FormUtil from 'utils/formUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pesanan Toko',
    href: '/shop-orders',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)

const ShopOrderListPage: FC<Props> = () => {
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [trackingStatus, setTrackingStatus] = useQueryParam('tracking-status', StringParam)
  const [platform, setPlatform] = useQueryParam('platform', StringParam)

  const payload: PayloadShopOrderFilter = {
    page,
    phrase,
    limit,
    sorts,
    platform_type: FormUtil.withDefault((platform as ShopOrderPlatformTypeEnum), null),
    tracking_status: FormUtil.withDefault((trackingStatus as ShopOrderTrackingStatusEnum), null),
    with_items: false,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SHOP_ORDER_FILTER, payload],
    queryFn: () => ShopOrdersAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setPlatform(null)
    setTrackingStatus(null)
  }

  const handleApplyFilter = (values: ShopOrderFilterFormPopoverShape) => {
    setPlatform(values.platform_type)
    setTrackingStatus(values.tracking_status)
  }

  if (!data) {
    return 'Loading ...'
  }

  return (
    <PageContainer metaTitle="Daftar Pesanan Toko">
      <PageHeader title="Daftar Pesanan Toko" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" gap={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Pesanan" onChange={setPhrase} />
                <ShopOrderFilterFormPopover
                  initialValues={{
                    tracking_status: trackingStatus as ShopOrderTrackingStatusEnum,
                    platform_type: platform as ShopOrderPlatformTypeEnum,
                  }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>
          </Grid>

          <Box mt={2}>
            <ShopOrderTable
              data={data.nodes}
              sorts={sorts}
              pagination={{
                page,
                rowsPerPage: limit,
                total: data.total,
                onPaginationChange: handlePaginationChange,
              }}
              onSortChange={setSorts}
              action={(item) => (
                <PermissionControl match={PagePermission.SHOP_ORDER_DETAIL}>
                  <Tooltip title="Detail Pesanan">
                    <NavLink to={`/shop-orders/${item.id}`}>
                      <IconButton>
                        <IconEye size="1.2rem" />
                      </IconButton>
                    </NavLink>
                  </Tooltip>
                </PermissionControl>
              )}
            />
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ShopOrderListPage
