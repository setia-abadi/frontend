import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import CustomerTypesAPI from 'apis/customerTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import CustomerTypeFormCard, { CustomerTypeFormCardShape } from 'modules/customer-type/components/CustomerTypeFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pelanggan',
    href: '/customer-types',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const CustomerTypeUpdatePage: FC<Props> = () => {
  const { customerTypeId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CUSTOMER_TYPE_GET, { customerTypeId }],
    queryFn: () => CustomerTypesAPI.get(customerTypeId!),
    select: (response) => response.data.data.customer_type,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: CustomerTypeFormCardShape) => {
    const response = await CustomerTypesAPI.update(customerTypeId!, {
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.customer_type) {
      return navigate(`/customer-types/${response.data.data.customer_type.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Jenis Pelanggan">
      <PageHeader title="Ubah Jenis Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
        <CustomerTypeFormCard
          cardTitle="Ubah"
          initialValues={{
            name: data.name,
            description: data.description,
          }}
          onSubmit={handleSubmit}
          onCancel={goBack}
        />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default CustomerTypeUpdatePage
