import { Box } from '@mui/material'
import CustomerTypesAPI from 'apis/customerTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import CustomerTypeFormCard, { CustomerTypeFormCardShape } from 'modules/customer-type/components/CustomerTypeFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pelanggan',
    href: '/customer-types',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const CustomerTypeCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: CustomerTypeFormCardShape) => {
    const response = await CustomerTypesAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.customer_type) {
      return navigate(`/customer-types/${response.data.data.customer_type.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Jenis Pelanggan">
      <PageHeader title="Tambah Jenis Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <CustomerTypeFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default CustomerTypeCreatePage
