import { CircularProgress, Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEdit, IconPlus, IconSpeakerphone, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import CustomerTypesAPI from 'apis/customerTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { CustomerTypeDiscountModel } from 'models/customerTypeDiscount'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import CustomerTypeDetailCard from 'modules/customer-type/components/CustomerTypeDetailCard'
import CustomerTypeDiscountFormDialog, {
  CustomerTypeDiscountFormDialogShape,
} from 'modules/customer-type/components/CustomerTypeDiscountFormDialog/CustomerTypeDiscountFormDialog'
import CustomerTypeDiscountTable from 'modules/customer-type/components/CustomerTypeDiscountTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useNavigate, useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import useWhatsAppSessionStatus from 'modules/whatsapp-session/hooks/useWhatsAppSessionStatus'
import WhatsappAPI from 'apis/whatsappAPI'
import Skeleton from 'components/Skeleton'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pelanggan',
    href: '/customer-types',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const CustomerTypeDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const navigate = useNavigate()
  const { customerTypeId } = useParams()
  const { isLoading: isLoadingWhatsappSession, isLoggedIn } = useWhatsAppSessionStatus()
  const [openAddDiscountDialog, setOpenAddDiscountDialog] = useState<boolean>(false)
  const [selectedDiscountForUpdate, setSelectedDiscountForUpdate] = useState<CustomerTypeDiscountModel>()
  const [openUpdateDiscountDialog, setOpenUpdateDiscountDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(customerTypeId),
    queryKey: [QueryKeyEnum.CUSTOMER_TYPE_GET, { customerTypeId }],
    queryFn: () => CustomerTypesAPI.get(customerTypeId!),
    select: (response) => response.data.data.customer_type,
  })

  const deleteDiscount = async (discount: CustomerTypeDiscountModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Diskon',
        description: `Apakah kamu yakin ingin menghapus diskon produk "${discount.product!.name}"?`,
      })
      if (!isConfirmed) return

      await CustomerTypesAPI.deleteDiscount(customerTypeId!, discount.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_TYPE_GET, { customerTypeId }] })
      snackbar.enqueueSnackbar('Diskon telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleAddDiscount = async (payload: CustomerTypeDiscountFormDialogShape) => {
    await CustomerTypesAPI.addDiscount(customerTypeId!, {
      discount_amount: FormUtil.withDefault(payload.discount_amount, null),
      discount_percentage: FormUtil.withDefault(payload.discount_percentage, null),
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(payload.product_id), ''),
      is_active: FormUtil.withDefault(payload.is_active, false),
    })
    snackbar.enqueueSnackbar('Diskon telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_TYPE_GET, { customerTypeId }] })
    setOpenAddDiscountDialog(false)
  }

  const handleUpdateDiscount = async (payload: CustomerTypeDiscountFormDialogShape) => {
    await CustomerTypesAPI.updateDiscount(customerTypeId!, selectedDiscountForUpdate!.id, {
      discount_amount: FormUtil.withDefault(payload.discount_amount, null),
      discount_percentage: FormUtil.withDefault(payload.discount_percentage, null),
      is_active: FormUtil.withDefault(payload.is_active, false),
    })
    snackbar.enqueueSnackbar('Diskon telah berhasil diubah', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_TYPE_GET, { customerTypeId }] })
    setOpenUpdateDiscountDialog(false)
  }

  const broadcastDiscount = async (discount: CustomerTypeDiscountModel) => {
    if (!isLoggedIn) {
      snackbar.enqueueSnackbar('Sesi WhatsApp belum terhubung, silakan login terlebih dahulu.', { variant: 'error' })
      return navigate('/whatsapp-session')
    }

    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Siarkan Diskon',
        description: `Apakah kamu yakin menyiarkan diskon produk "${discount.product!.name}" kepada kostumer?`,
      })
      if (!isConfirmed) return

      await WhatsappAPI.broadcastCustomerTypeDiscount({
        customer_type_discount_id: discount.id,
      })
      snackbar.enqueueSnackbar('Diskon produk telah berhasil disiarkan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Detail Jenis Pelanggan">
      <PageHeader title="Detail Jenis Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          {data ? (
            <CustomerTypeDetailCard
              cardTitle="Detail"
              name={data.name}
              description={data.description}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <PermissionControl match={PagePermission.CUSTOMER_TYPE_UPDATE}>
                  <NavLink to={`/customer-types/${data.id}/update`}>
                    <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                      Ubah
                    </Button>
                  </NavLink>
                </PermissionControl>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={4} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Diskon"
              action={
                <PermissionControl match={[PermissionEnum.CUSTOMER_TYPE_ADD_DISCOUNT]}>
                  <Button
                    variant="contained"
                    startIcon={<IconPlus size="1.2rem" />}
                    onClick={() => setOpenAddDiscountDialog(true)}
                  >
                    Tambah Diskon
                  </Button>
                </PermissionControl>
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <CustomerTypeDiscountTable
                  data={data.discounts ?? []}
                  action={(discount) => (
                    <Stack direction="row" spacing={1}>
                      <PermissionControl match={[PermissionEnum.WHATSAPP_CUSTOMER_TYPE_DISCOUNT_BROADCAST]}>
                        <Tooltip title="Siarkan Diskon">
                          <IconButton disabled={isLoadingWhatsappSession} onClick={() => broadcastDiscount(discount)}>
                            {isLoadingWhatsappSession ? (
                              <CircularProgress size="1rem" />
                            ) : (
                              <IconSpeakerphone size="1.2rem" />
                            )}
                          </IconButton>
                        </Tooltip>
                      </PermissionControl>

                      <PermissionControl match={[PermissionEnum.CUSTOMER_TYPE_UPDATE_DISCOUNT]}>
                        <Tooltip title="Ubah Diskon">
                          <IconButton
                            onClick={() => {
                              setSelectedDiscountForUpdate(discount)
                              setOpenUpdateDiscountDialog(true)
                            }}
                          >
                            <IconEdit size="1.2rem" />
                          </IconButton>
                        </Tooltip>
                      </PermissionControl>

                      <PermissionControl match={[PermissionEnum.CUSTOMER_TYPE_DELETE_DISCOUNT]}>
                        <Tooltip title="Hapus Diskon">
                          <IconButton onClick={() => deleteDiscount(discount)}>
                            <IconTrash size="1.2rem" />
                          </IconButton>
                        </Tooltip>
                      </PermissionControl>
                    </Stack>
                  )}
                />
              ) : (
                <Skeleton.Table columns={[undefined, 175, 150, 150]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <CustomerTypeDiscountFormDialog
        open={openAddDiscountDialog}
        dialogTitle="Tambah Diskon"
        dialogSubTitle="Untuk menambahkan diskon, silahkan masukkan produk yang ingin diberikan diskon dan jumlah diskon, lalu tekan tombol kirim."
        customerTypeId={customerTypeId!}
        onCancel={() => setOpenAddDiscountDialog(false)}
        onClose={() => setOpenAddDiscountDialog(false)}
        onSubmit={handleAddDiscount}
      />

      {selectedDiscountForUpdate && (
        <CustomerTypeDiscountFormDialog
          open={openUpdateDiscountDialog}
          dialogTitle="Ubah Diskon"
          dialogSubTitle="Untuk mengubah diskon, silahkan masuk jumlah diskon yang diinginkan, lalu tekan tombol kirim."
          customerTypeId={customerTypeId!}
          initialValues={{
            discount_amount: selectedDiscountForUpdate.discount_amount,
            discount_percentage: selectedDiscountForUpdate.discount_percentage,
            is_active: selectedDiscountForUpdate.is_active,
            product_id: selectedDiscountForUpdate.product
              ? {
                  label: selectedDiscountForUpdate.product.name,
                  value: selectedDiscountForUpdate.product.id,
                }
              : undefined,
          }}
          disabledFields={{
            product_id: true,
          }}
          onCancel={() => setOpenUpdateDiscountDialog(false)}
          onClose={() => setOpenUpdateDiscountDialog(false)}
          onClosed={() => setSelectedDiscountForUpdate(undefined)}
          onSubmit={handleUpdateDiscount}
        />
      )}
    </PageContainer>
  )
}

export default CustomerTypeDetailPage
