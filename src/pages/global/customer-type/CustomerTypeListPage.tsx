import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import CustomerTypesAPI, { PayloadCustomerTypeFilter } from 'apis/customerTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { CustomerTypeModel } from 'models/customerType'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import CustomerTypeTable from 'modules/customer-type/components/CustomerTypeTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pelanggan',
    href: '/customer-types',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)

const CustomerTypeListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const payload: PayloadCustomerTypeFilter = {
    page,
    phrase,
    limit,
    sorts,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CUSTOMER_TYPE_FILTER, payload],
    queryFn: () => CustomerTypesAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteCustomerType = async (item: CustomerTypeModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Jenis Pelanggan',
        description: 'Apakah kamu yakin ingin menghapus jenis pelanggan?',
      })
      if (!isConfirmed) return

      await CustomerTypesAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_TYPE_FILTER, payload] })
      snackbar.enqueueSnackbar('Jenis pelanggan telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Daftar Jenis Pelanggan">
      <PageHeader title="Daftar Jenis Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <SearchBar initialValue={phrase} placeholder="Cari Jenis Pelanggan" onChange={setPhrase} />
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.CUSTOMER_TYPE_CREATE}>
                <NavLink to="/customer-types/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Jenis Pelanggan
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <CustomerTypeTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.CUSTOMER_TYPE_DETAIL}>
                      <Tooltip title="Detail Jenis Pelanggan">
                        <NavLink to={`/customer-types/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.CUSTOMER_TYPE_DELETE]}>
                      <Tooltip title="Hapus Jenis Pelanggan">
                        <IconButton onClick={() => deleteCustomerType?.(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[300, undefined, 150]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default CustomerTypeListPage
