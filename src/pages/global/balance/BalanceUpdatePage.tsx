import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import BalancesAPI from 'apis/balancesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import BalanceFormCard, { BalanceFormCardShape } from 'modules/balance/components/BalanceFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

type Props = {}

const BalanceUpdatePage: FC<Props> = () => {
  const { balanceId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.BALANCE_GET, { balanceId }],
    queryFn: () => BalancesAPI.get(balanceId!),
    select: (response) => response.data.data.balance,
  })

  const pageBreadcrumbs: BreadcrumbItem[] = [
    {
      title: 'Beranda',
    },
    {
      title: 'Saldo',
      href: '/balances',
    },
    {
      title: 'Ubah',
    },
  ]

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: BalanceFormCardShape) => {
    const response = await BalancesAPI.update(balanceId!, {
      account_name: FormUtil.withDefault(values.account_name, ''),
      account_number: FormUtil.withDefault(values.account_number, ''),
      bank_name: FormUtil.withDefault(values.bank_name, ''),
      name: FormUtil.withDefault(values.name, ''),
    })

    if (response.data.data.balance) {
      return navigate(`/balances/${response.data.data.balance.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Saldo">
      <PageHeader title="Ubah Saldo" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <BalanceFormCard
            cardTitle="Ubah"
            initialValues={{
              account_name: data.account_name,
              account_number: data.account_number,
              bank_name: data.bank_name,
              name: data.name,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={3} />
        )}
      </Box>
    </PageContainer>
  )
}

export default BalanceUpdatePage
