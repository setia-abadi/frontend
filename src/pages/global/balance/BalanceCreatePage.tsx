import { Box } from '@mui/material'
import BalancesAPI from 'apis/balancesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import BalanceFormCard, { BalanceFormCardShape } from 'modules/balance/components/BalanceFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Saldo',
    href: '/balances',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const BalanceCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: BalanceFormCardShape) => {
    const response = await BalancesAPI.create({
      account_name: FormUtil.withDefault(values.account_name, ''),
      account_number: FormUtil.withDefault(values.account_number, ''),
      bank_name: FormUtil.withDefault(values.bank_name, ''),
      name: FormUtil.withDefault(values.name, ''),
    })

    if (response.data.data.balance) {
      return navigate(`/balances/${response.data.data.balance.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Saldo">
      <PageHeader
        title="Tambah Saldo"
        breadcrumbs={pageBreadcrumbs}
      />

      <Box mt={2}>
        <BalanceFormCard
          cardTitle="Tambah"
          onSubmit={handleSubmit}
          onCancel={goBack}
        />
      </Box>
    </PageContainer>
  )
}

export default BalanceCreatePage
