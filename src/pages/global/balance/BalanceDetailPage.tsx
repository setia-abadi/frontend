import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import BalancesAPI from 'apis/balancesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import BalanceDetailCard from 'modules/balance/components/BalanceDetailCard'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Saldo',
    href: '/balances',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const BalanceDetailPage: FC<Props> = () => {
  const { balanceId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.BALANCE_GET, { balanceId }],
    queryFn: () => BalancesAPI.get(balanceId!),
    select: (response) => response.data.data.balance,
  })

  return (
    <PageContainer metaTitle="Detail Saldo">
      <PageHeader title="Detail Saldo" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <BalanceDetailCard
            cardTitle="Detail"
            accountName={data.account_name}
            accountNumber={data.account_number}
            bankName={data.bank_name}
            name={data.name}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={PagePermission.BALANCE_UPDATE}>
                <NavLink to={`/balances/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={6} />
        )}
      </Box>
    </PageContainer>
  )
}

export default BalanceDetailPage
