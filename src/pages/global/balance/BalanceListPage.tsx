import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import BalancesAPI, { PayloadFilterBalance } from 'apis/balancesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { BalanceModel } from 'models/balance'
import BalanceTable from 'modules/balance/components/BalanceTable'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import APIUtil from 'utils/APIUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Saldo',
    href: '/balances',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)

const BalanceListPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const payload: PayloadFilterBalance = {
    page,
    phrase,
    limit,
    sorts,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.BALANCE_FILTER, payload],
    queryFn: () => BalancesAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteBalance = async (item: BalanceModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Saldo',
        description: `Apakah kamu yakin ingin menghapus saldo "${item.account_name}"?`,
      })
      if (!isConfirmed) return

      await BalancesAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.BALANCE_FILTER, payload] })
      snackbar.enqueueSnackbar('Saldo telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Daftar Saldo">
      <PageHeader title="Daftar Saldo" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <SearchBar initialValue={phrase} placeholder="Cari Saldo" onChange={setPhrase} />
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.USER_CREATE]}>
                <NavLink to="/balances/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Saldo
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (

            <BalanceTable
              data={data.nodes}
              sorts={sorts}
              pagination={{
                page,
                rowsPerPage: limit,
                total: data.total,
                onPaginationChange: handlePaginationChange,
              }}
              onSortChange={setSorts}
              action={(item) => (
                <Stack direction="row" spacing={1}>
                  <PermissionControl match={PagePermission.BALANCE_DETAIL}>
                    <Tooltip title="Detail Saldo">
                      <NavLink to={`/balances/${item.id}`}>
                        <IconButton>
                          <IconEye size="1.2rem" />
                        </IconButton>
                      </NavLink>
                    </Tooltip>
                  </PermissionControl>

                  <PermissionControl match={[PermissionEnum.BALANCE_DELETE]}>
                    <Tooltip title="Hapus Saldo">
                      <IconButton onClick={() => deleteBalance(item)}>
                        <IconTrash size="1.2rem" />
                      </IconButton>
                    </Tooltip>
                  </PermissionControl>
                </Stack>
              )}
            />
            ) : (
              <Skeleton.Table columns={[undefined, 250, 250, 250, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default BalanceListPage
