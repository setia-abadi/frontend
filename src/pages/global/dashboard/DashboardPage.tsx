import { Stack } from '@mui/material'
import Grid from '@mui/material/Grid'
import { useQuery } from '@tanstack/react-query'
import DashboardsAPI from 'apis/dashboardsAPI'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import Typography from 'components/Typography'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import SummarizeCustomerDebtTable from 'modules/debt/components/SummarizeCustomerDebtTable'
import SummarizeSupplierDebtTable from 'modules/debt/components/SummarizeSupplierDebtTable'
import TransactionSummarize from './TransactionSummarize'
import dayjs from 'dayjs'

const DashboardPage = () => {
  const { data: summarizeDebt } = useQuery({
    queryKey: [QueryKeyEnum.SUMMARIZE_DEBT_FETCH],
    queryFn: () => DashboardsAPI.summarizeDebt(),
    select: (response) => response.data.data,
  })

  const { data: summarizeTransactions } = useQuery({
    queryKey: [QueryKeyEnum.SUMMARIZE_TRANSACTION_FETCH],
    queryFn: () => DashboardsAPI.summarizeTransactions({
      start_date: dayjs().startOf('month').format('YYYY-MM-DD'),
      end_date: dayjs().endOf('month').format('YYYY-MM-DD'),
    }),
    select: (response) => response.data.data.transaction_summaries,
  })

  return (
    <PageContainer metaTitle="Dashboard" metaDescription="this is Dashboard">
      <Grid container spacing={3} mt={1}>
        <Grid item xs={12} md={6}>
          <Paper>
            <CardContent>
              <Stack spacing={1} mb={3}>
                <Typography variant="h5">Hutang Pelanggan</Typography>
              </Stack>
              {summarizeDebt ? (
                <SummarizeCustomerDebtTable data={summarizeDebt.customer_debt_summaries} />
              ) : (
                <Skeleton.Table columns={[undefined, 150]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12} md={6}>
          <Paper>
            <CardContent>
              <Stack spacing={1} mb={3}>
                <Typography variant="h5">Hutang Supplier</Typography>
              </Stack>
              {summarizeDebt ? (
                <SummarizeSupplierDebtTable data={summarizeDebt.supplier_debt_summaries} />
              ) : (
                <Skeleton.Table columns={[undefined, 150]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          {summarizeTransactions ? (
            <TransactionSummarize  transactionSummaries={summarizeTransactions} />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Ringkasan Penjualan" rows={5} />
          )}
        </Grid>
      </Grid>
    </PageContainer>
  )
}

export default DashboardPage
