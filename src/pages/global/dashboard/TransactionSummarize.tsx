import { useTheme } from '@mui/material/styles'
import { ApexOptions } from 'apexcharts'
import { TransactionSummaryModel } from 'models/transactionSummary'
import React from 'react'
import Chart from 'react-apexcharts'

import DashboardCard from './DashboardCard'

type Props = {
  transactionSummaries: TransactionSummaryModel[]
}

const TransactionSummarize: React.FC<Props> = ({ transactionSummaries }) => {
  const theme = useTheme()
  // const [month, setMonth] = React.useState<string>('1')

  // chart color
  const primary = theme.palette.primary.main
  const secondary = theme.palette.secondary.main

  // const handleChange = (e: SelectChangeEvent<string>) => {
  //   setMonth(e.target.value)
  // }

  const categories = transactionSummaries.map((transaction) => transaction.date)
  const totalGrossSales = transactionSummaries.map((transaction) => transaction.total_gross_sales)
  const totalNetSales = transactionSummaries.map((transaction) => transaction.total_net_sales)

  // chart
  const optionscolumnchart: ApexOptions = {
    chart: {
      type: 'bar',
      fontFamily: "'Plus Jakarta Sans', sans-serif;",
      foreColor: '#adb0bb',
      toolbar: {
        show: true,
      },
      height: 370,
    },
    colors: [primary, secondary],
    plotOptions: {
      bar: {
        horizontal: false,
        barHeight: '60%',
        columnWidth: '42%',
        borderRadius: 6,
        borderRadiusApplication: 'end',
        borderRadiusWhenStacked: 'all',
      },
    },

    stroke: {
      show: true,
      width: 5,
      lineCap: 'butt',
      colors: ['transparent'],
    },
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: false,
    },
    grid: {
      borderColor: 'rgba(0,0,0,0.1)',
      strokeDashArray: 3,
      xaxis: {
        lines: {
          show: false,
        },
      },
    },
    yaxis: {
      tickAmount: 4,
    },
    xaxis: {
      categories: categories,
      axisBorder: {
        show: false,
      },
    },
    tooltip: {
      theme: theme.palette.mode === 'dark' ? 'dark' : 'light',
      fillSeriesColor: false,
    },
  }
  const seriescolumnchart = [
    {
      name: 'Penjualan Bersih',
      data: totalNetSales,
    },
    {
      name: 'Penjualan Kotor',
      data: totalGrossSales,
    },
  ]

  return (
    <DashboardCard
      title="Ringkasan Transaksi"
      // action={
      //   <Select value={month} size="small" onChange={handleChange}>
      //     <MenuItem value={1}>March 2023</MenuItem>
      //     <MenuItem value={2}>April 2023</MenuItem>
      //     <MenuItem value={3}>May 2023</MenuItem>
      //   </Select>
      // }
    >
      <Chart options={optionscolumnchart} series={seriescolumnchart} type="bar" height="370px" />
    </DashboardCard>
  )
}

export default TransactionSummarize
