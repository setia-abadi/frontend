import { Divider, Grid } from '@mui/material'
import { IconPlus } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import CustomerDebtsAPI from 'apis/customerDebtsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import CustomerDebtDetailCard from 'modules/customer-debt/components/CustomerDebtDetailCard'
import CustomerDebtPaymentFormDialog, {
  CustomerDebtPaymentFormDialogShape,
} from 'modules/customer-debt/components/CustomerDebtPaymentFormDialog'
import CustomerDebtPaymentTable from 'modules/customer-debt/components/CustomerDebtPaymentTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useParams } from 'react-router'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Piutang',
    href: '/receivables',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const CustomerDebtDetailPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { customerDebtId } = useParams()
  const [openAddPaymentModal, setOpenAddPaymentModal] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(customerDebtId),
    queryKey: [QueryKeyEnum.CUSTOMER_DEBT_GET, { customerDebtId }],
    queryFn: () => CustomerDebtsAPI.get(customerDebtId!),
    select: (response) => response.data.data.customer_debt,
  })

  const hadleAddPaymentFormSubmit = async (values: CustomerDebtPaymentFormDialogShape) => {
    await CustomerDebtsAPI.payment(customerDebtId!, {
      amount: FormUtil.withDefault(values.amount, 0),
      description: FormUtil.withDefault(values.description, null),
      image_file_path: FormUtil.withDefault(values.image_file_path, ''),
    })
    snackbar.enqueueSnackbar('Pembayaran telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_DEBT_GET, { customerDebtId }] })
    setOpenAddPaymentModal(false)
  }

  return (
    <PageContainer metaTitle="Detail Piutang">
      <PageHeader title="Detail Piutang" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          {data ? (
            <CustomerDebtDetailCard
              cardTitle="Detail"
              customer={data.customer!.name}
              dueDate={data.due_date}
              remainingAmount={data.remaining_amount}
              source={data.debt_source}
              status={data.status}
              totalAmount={data.amount}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={7} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Riwayat Pembayaran"
              action={
                data ? (
                  data.status === CustomerDebtStatusEnum.UNPAID && (
                    <PermissionControl match={[PermissionEnum.CUSTOMER_DEBT_PAYMENT]}>
                      <Button
                        variant="contained"
                        startIcon={<IconPlus size="1.2rem" />}
                        onClick={() => setOpenAddPaymentModal(true)}
                      >
                        Buat Pembayaran
                      </Button>
                    </PermissionControl>
                  )
                ) : (
                  <Skeleton.Button />
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <CustomerDebtPaymentTable data={data.payments ?? []} />
              ) : (
                <Skeleton.Table columns={[300, 200, undefined, 175, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <CustomerDebtPaymentFormDialog
        open={openAddPaymentModal}
        dialogTitle="Buat Pembayaran"
        onSubmit={hadleAddPaymentFormSubmit}
        onClose={() => setOpenAddPaymentModal(false)}
        onCancel={() => setOpenAddPaymentModal(false)}
      />
    </PageContainer>
  )
}

export default CustomerDebtDetailPage
