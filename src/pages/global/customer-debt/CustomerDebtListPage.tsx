import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconDownload, IconEye, IconSpeakerphone } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import CustomerDebtsAPI, { PayloadCustomerDebtFilter } from 'apis/customerDebtsAPI'
import WhatsappAPI from 'apis/whatsappAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import CustomerDebtBroadcastWhatsappFormDialog, {
  CustomerDebtBroadcastWhatsappFormDialogShape,
} from 'modules/customer-debt/components/CustomerDebtBroadcastWhatsappFormDialog'
import CustomerDebtDownloadReportFormDialog, {
  CustomerDebtDownloadReportFormDialogShape,
} from 'modules/customer-debt/components/CustomerDebtDownloadReportFormDialog'
import CustomerDebtFilterFormPopover, {
  CustomerDebtFilterFormPopoverShape,
} from 'modules/customer-debt/components/CustomerDebtFilterFormPopover'
import CustomerDebtTable from 'modules/customer-debt/components/CustomerDebtTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import useWhatsAppSessionStatus from 'modules/whatsapp-session/hooks/useWhatsAppSessionStatus'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import FileUtil from 'utils/fileUtil'
import FormUtil from 'utils/formUtil'
import StringUtil from 'utils/stringUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Piutang',
    href: '/receivables',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringEnum = withDefault(StringParam, null)
const StatusParam = withDefault(StringParam, undefined)

const CustomerDebtListPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringEnum)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [status, setStatus] = useQueryParam('status', StatusParam)
  const { isLoading: isLoadingWhatsappSession, isLoggedIn } = useWhatsAppSessionStatus()
  const [openDownloadReportDialog, setOpenDownloadReportDialog] = useState<boolean>(false)
  const [openBroadcastWhatsappDialog, setOpenBroadcastWhatsappDialog] = useState<boolean>(false)

  const payload: PayloadCustomerDebtFilter = {
    page,
    phrase,
    limit,
    sorts,
    status: FormUtil.withDefault(status as CustomerDebtStatusEnum, null),
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CUSTOMER_DEBT_FILTER, payload],
    queryFn: () => CustomerDebtsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setStatus(undefined)
  }

  const handleApplyFilter = (values: CustomerDebtFilterFormPopoverShape) => {
    setStatus(values.status)
  }

  const handleDownloadReportBtnClick = () => {
    setOpenDownloadReportDialog(true)
  }

  const handleBroadcastWhatsappBtnClick = () => {
    if (isLoggedIn) {
      setOpenBroadcastWhatsappDialog(true)
    } else {
      snackbar.enqueueSnackbar('Sesi WhatsApp belum terhubung, silakan login terlebih dahulu.', { variant: 'error' })
    }
  }

  const handleSubmitDownloadReportForm = async (values: CustomerDebtDownloadReportFormDialogShape) => {
    const response = await CustomerDebtsAPI.downloadReport({
      start_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.start_date), ''),
      end_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.end_date), ''),
      customer_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_id), null),
    })

    const contentDisposition = response.headers['content-disposition']
    const filename = contentDisposition
      ? FileUtil.getFilenameFromContentDisposition(contentDisposition)
      : StringUtil.uniqueId()

    FileUtil.download(response.data, filename!)
    snackbar.enqueueSnackbar('Berhasil diunduh', { variant: 'success' })
  }

  const handleSubmitBroadcastWhatsappForm = async (values: CustomerDebtBroadcastWhatsappFormDialogShape) => {
    await WhatsappAPI.broadcastCustomerDebt({
      customer_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_id), ''),
    })
    snackbar.enqueueSnackbar('Berita hutang telah berhasil di siarkan ke pelanggan', { variant: 'success' })
  }

  return (
    <PageContainer metaTitle="Daftar Piutang">
      <PageHeader title="Daftar Piutang" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Piutang" onChange={setPhrase} />
                <CustomerDebtFilterFormPopover
                  initialValues={{ status: status as CustomerDebtStatusEnum }}
                  onReset={handleResetFilter}
                  onApply={handleApplyFilter}
                />
              </Stack>
            </Grid>
            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.CUSTOMER_DEBT_DOWNLOAD_REPORT]}>
                <Button
                  startIcon={<IconDownload size="1rem" />}
                  variant="contained"
                  onClick={handleDownloadReportBtnClick}
                >
                  Unduh Laporan
                </Button>
              </PermissionControl>
            </Grid>
            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.WHATSAPP_CUSTOMER_DEBT_BROADCAST]}>
                <Button
                  disabled={isLoadingWhatsappSession}
                  startIcon={<IconSpeakerphone size="1rem" />}
                  variant="contained"
                  onClick={handleBroadcastWhatsappBtnClick}
                >
                  Siarkan Piutang
                </Button>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <CustomerDebtTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.CUSTOMER_DEBT_DETAIL}>
                      <Tooltip title="Detail Piutang">
                        <NavLink to={`/receivables/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[undefined, 150, 150, 175, 175, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>

      <CustomerDebtDownloadReportFormDialog
        open={openDownloadReportDialog}
        dialogTitle="Unduh Laporan"
        dialogSubTitle="Unduh laporan piutang pelanggan berdasarkan tanggal dan pelanggan"
        onCancel={() => setOpenDownloadReportDialog(false)}
        onClose={() => setOpenDownloadReportDialog(false)}
        onSubmit={handleSubmitDownloadReportForm}
      />

      <CustomerDebtBroadcastWhatsappFormDialog
        open={openBroadcastWhatsappDialog}
        dialogTitle="Siarkan Piutang"
        dialogSubTitle="Siarkan semua berita hutang yang belum lunas kepada pelanggan yang dipilih."
        onCancel={() => setOpenBroadcastWhatsappDialog(false)}
        onClose={() => setOpenBroadcastWhatsappDialog(false)}
        onSubmit={handleSubmitBroadcastWhatsappForm}
      />
    </PageContainer>
  )
}

export default CustomerDebtListPage
