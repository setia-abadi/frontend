import { Divider, Grid } from '@mui/material'
import { IconPlus } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import DebtsAPI from 'apis/debtsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import { DebtStatusEnum } from 'enums/debtStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import DebtDetailCard from 'modules/debt/components/DebtDetailCard'
import DebtPaymentFormDialog, { DebtPaymentFormDialogShape } from 'modules/debt/components/DebtPaymentFormDialog'
import DebtPaymentTable from 'modules/debt/components/DebtPaymentTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useParams } from 'react-router'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Hutang',
    href: '/debts',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const DebtDetailPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { debtId } = useParams()
  const [openAddPaymentModal, setOpenAddPaymentModal] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(debtId),
    queryKey: [QueryKeyEnum.DEBT_GET, { debtId }],
    queryFn: () => DebtsAPI.get(debtId!),
    select: (response) => response.data.data.debt,
  })

  const hadleAddPaymentFormSubmit = async (values: DebtPaymentFormDialogShape) => {
    await DebtsAPI.payment(debtId!, {
      amount: FormUtil.withDefault(values.amount, 0),
      description: FormUtil.withDefault(values.description, null),
      image_file_path: FormUtil.withDefault(values.image_file_path, ''),
    })
    snackbar.enqueueSnackbar('Pembayaran telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DEBT_GET, { debtId }] })
    setOpenAddPaymentModal(false)
  }

  return (
    <PageContainer metaTitle="Detail Hutang">
      <PageHeader title="Detail Hutang" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12}>
          {data ? (
            <DebtDetailCard
              cardTitle="Detail"
              supplier={data.supplier!.name}
              dueDate={data.due_date}
              remainingAmount={data.remaining_amount}
              source={data.debt_source}
              status={data.status}
              totalAmount={data.amount}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={7} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Riwayat Pembayaran"
              action={
                data ? (
                  data.status === DebtStatusEnum.UNPAID && (
                    <PermissionControl match={[PermissionEnum.DEBT_PAYMENT]}>
                      <Button
                        variant="contained"
                        startIcon={<IconPlus size="1.2rem" />}
                        onClick={() => setOpenAddPaymentModal(true)}
                      >
                        Buat Pembayaran
                      </Button>
                    </PermissionControl>
                  )
                ) : (
                  <Skeleton.Button />
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <DebtPaymentTable data={data.payments ?? []} />
              ) : (
                <Skeleton.Table columns={[300, 200, undefined, 175, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <DebtPaymentFormDialog
        open={openAddPaymentModal}
        dialogTitle="Buat Pembayaran"
        onSubmit={hadleAddPaymentFormSubmit}
        onClose={() => setOpenAddPaymentModal(false)}
        onCancel={() => setOpenAddPaymentModal(false)}
      />
    </PageContainer>
  )
}

export default DebtDetailPage
