import { Box, Button, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconDownload, IconEye } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import DebtsAPI, { PayloadDebtFilter } from 'apis/debtsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import DebtDownloadReportFormDialog, {
  DebtDownloadReportFormDialogShape,
} from 'modules/debt/components/DebtDownloadReportFormDialog'
import DebtFilterFormPopover, { DebtFilterFormPopoverShape } from 'modules/debt/components/DebtFilterFormPopover'
import DebtTable from 'modules/debt/components/DebtTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import FileUtil from 'utils/fileUtil'
import FormUtil from 'utils/formUtil'
import StringUtil from 'utils/stringUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Hutang',
    href: '/debts',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringEnum = withDefault(StringParam, null)
const StatusParam = withDefault(StringParam, undefined)

const DebtListPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringEnum)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [status, setStatus] = useQueryParam('status', StatusParam)
  const [openDownloadReportDialog, setOpenDownloadReportDialog] = useState<boolean>(false)

  const payload: PayloadDebtFilter = {
    page,
    phrase,
    limit,
    sorts,
    status: FormUtil.withDefault(status as CustomerDebtStatusEnum, null),
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.DEBT_FILTER, payload],
    queryFn: () => DebtsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setStatus(undefined)
  }

  const handleApplyFilter = (values: DebtFilterFormPopoverShape) => {
    setStatus(values.status)
  }

  const handleDownloadReportBtnClick = () => {
    setOpenDownloadReportDialog(true)
  }

  const handleSubmitDownloadReportForm = async (values: DebtDownloadReportFormDialogShape) => {
    const response = await DebtsAPI.downloadReport({
      start_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.start_date), ''),
      end_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.end_date), ''),
    })

    const contentDisposition = response.headers['content-disposition']
    const filename = contentDisposition
      ? FileUtil.getFilenameFromContentDisposition(contentDisposition)
      : StringUtil.uniqueId()

    FileUtil.download(response.data, filename!)
    snackbar.enqueueSnackbar('Berhasil diunduh', { variant: 'success' })
  }

  return (
    <PageContainer metaTitle="Daftar Hutang">
      <PageHeader title="Daftar Hutang" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Hutang" onChange={setPhrase} />
                <DebtFilterFormPopover
                  initialValues={{ status: status as CustomerDebtStatusEnum }}
                  onReset={handleResetFilter}
                  onApply={handleApplyFilter}
                />
              </Stack>
            </Grid>
            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.DEBT_DOWNLOAD_REPORT]}>
                <Button
                  startIcon={<IconDownload size="1rem" />}
                  variant="contained"
                  onClick={handleDownloadReportBtnClick}
                >
                  Unduh Laporan
                </Button>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <DebtTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.DEBT_DETAIL}>
                      <Tooltip title="Detail Hutang">
                        <NavLink to={`/debts/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[undefined, 150, 150, 175, 200, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>

      <DebtDownloadReportFormDialog
        open={openDownloadReportDialog}
        dialogTitle="Unduh Laporan"
        dialogSubTitle="Unduh laporan piutang pelanggan berdasarkan tanggal dan pelanggan"
        onCancel={() => setOpenDownloadReportDialog(false)}
        onClose={() => setOpenDownloadReportDialog(false)}
        onSubmit={handleSubmitDownloadReportForm}
      />
    </PageContainer>
  )
}

export default DebtListPage
