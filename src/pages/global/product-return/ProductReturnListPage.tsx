import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductReturnsAPI, { PayloadProductReturnsFilter } from 'apis/productReturnsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SelectOptionParam, SortsParam } from 'libs/useQueryParam'
import { ProductReturnModel } from 'models/productReturn'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductReturnFilterFormPopover, {
  ProductReturnFilterFormPopoverShape,
} from 'modules/product-return/components/ProductReturnFilterFormPopover'
import ProductReturnTable from 'modules/product-return/components/ProductReturnTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'
import FormUtil from 'utils/formUtil'
import { SELECT_OPTION_ALL } from 'constants/option'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Retur Produk',
    href: '/product-returns',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)
const SupplierParam = withDefault(SelectOptionParam, SELECT_OPTION_ALL)

const ProductReturnListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [supplier, setSupplier] = useQueryParam('supplier', SupplierParam)
  const [status, setStatus] = useQueryParam('status', StringParam)

  const payload: PayloadProductReturnsFilter = {
    page,
    phrase,
    limit,
    sorts,
    supplier_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(supplier), null),
    status: FormUtil.withDefault((status as ProductReturnStatusEnum), null),
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_RETURN_FILTER, payload],
    queryFn: () => ProductReturnsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteProductReturn = async (item: ProductReturnModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Retur Produk',
        description: `Apakah anda yakin ingin menghapus retur produk dengan faktur ${item.invoice_number}`,
      })
      if (!isConfirmed) return

      await ProductReturnsAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_FILTER, payload] })
      snackbar.enqueueSnackbar('Retur produk telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setSupplier(undefined)
    setStatus(undefined)
  }

  const handleApplyFilter = (values: ProductReturnFilterFormPopoverShape) => {
    setSupplier(values.supplier)
    setStatus(values.status)
  }

  return (
    <PageContainer metaTitle="Daftar Retur Produk">
      <PageHeader title="Daftar Retur Produk" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Retur Produk" onChange={setPhrase} />
                <ProductReturnFilterFormPopover
                  initialValues={{ supplier, status: status as ProductReturnStatusEnum }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.PRODUCT_RETURN_CREATE}>
                <NavLink to="/product-returns/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Retur Produk
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <ProductReturnTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.PRODUCT_RETURN_DETAIL}>
                      <Tooltip title="Detail Retur Produk">
                        <NavLink to={`/product-returns/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PRODUCT_RETURN_DELETE]}>
                      <Tooltip title="Hapus Retur Produk">
                        <IconButton onClick={() => deleteProductReturn(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[175, undefined, 250, 125, 125]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ProductReturnListPage
