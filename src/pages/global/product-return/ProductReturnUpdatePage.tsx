import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import ProductReturnsAPI from 'apis/productReturnsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import dayjs from 'dayjs'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ProductReturnFormCard, {
  ProductReturnFormCardShape,
} from 'modules/product-return/components/ProductReturnFormCard'
import { FC } from 'react'
import { Navigate, useNavigate, useParams } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Retur Produk',
    href: '/product-returns',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const ProductReturnUpdatePage: FC<Props> = () => {
  const { productReturnId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }],
    queryFn: () => ProductReturnsAPI.get(productReturnId!),
    select: (response) => response.data.data.product_return,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductReturnFormCardShape) => {
    const response = await ProductReturnsAPI.update(productReturnId!, {
      invoice_number: FormUtil.withDefault(values.invoice_number, ''),
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
    })

    if (response.data.data.product_return) {
      return navigate(`/product-returns/${response.data.data.product_return.id}`)
    }

    return goBack()
  }

  if (data && data.status !== ProductReturnStatusEnum.PENDING) {
    return <Navigate to={`/product-returns/${productReturnId}`} replace />
  }

  return (
    <PageContainer metaTitle="Ubah Retur Produk">
      <PageHeader title="Ubah Retur Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <ProductReturnFormCard
            cardTitle="Ubah"
            initialValues={{
              date: dayjs(data.date),
              invoice_number: data.invoice_number,
              supplier_id: data.supplier
                ? {
                    label: data.supplier.name,
                    value: data.supplier.id,
                  }
                : undefined,
            }}
            disabledFields={{
              supplier_id: true,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default ProductReturnUpdatePage
