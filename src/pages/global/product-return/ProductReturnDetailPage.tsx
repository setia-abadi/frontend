import { Divider, Grid, IconButton, Menu, MenuItem, Stack, Tooltip } from '@mui/material'
import { IconCloudUpload, IconEdit, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductReturnsAPI from 'apis/productReturnsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { ProductReturnImageModel } from 'models/productReturnImage'
import { ProductReturnItemModel } from 'models/productReturnItem'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductReturnDetailCard from 'modules/product-return/components/ProductReturnDetailCard'
import ProductReturnImageFormDialog, {
  ProductReturnImageFormDialogShape,
} from 'modules/product-return/components/ProductReturnImageFormDialog'
import ProductReturnImageTable from 'modules/product-return/components/ProductReturnImageTable'
import ProductReturnItemFormDialog, {
  ProductReturnItemFormDialogShape,
} from 'modules/product-return/components/ProductReturnItemFormDialog'
import ProductReturnItemTable from 'modules/product-return/components/ProductReturnItemTable'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { Navigate, useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import Skeleton from 'components/Skeleton'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Retur Produk',
    href: '/product-returns',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const ProductReturnDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { productReturnId } = useParams()
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null)
  const [openAddItemDialog, setOpenAddItemDialog] = useState<boolean>(false)
  const [openAddImageDialog, setOpenAddImageDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(productReturnId),
    queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }],
    queryFn: () => ProductReturnsAPI.get(productReturnId!),
    select: (response) => response.data.data.product_return,
  })

  const complete = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status',
        description: `Apakah kamu yakin ingin menyelesaikan pengantaran pesanan?`,
      })
      if (!isConfirmed) return
      await ProductReturnsAPI.completed(productReturnId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }] })
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil di selesaikan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteItem = async (item: ProductReturnItemModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Barang',
        description: `Apakah kamu yakin ingin menghapus barang "${item.product_unit?.product.name}"?`,
      })
      if (!isConfirmed) return

      await ProductReturnsAPI.deleteItem(productReturnId!, item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }] })
      snackbar.enqueueSnackbar('Barang telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteImage = async (image: ProductReturnImageModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Gambar',
        description: `Apakah kamu yakin ingin menghapus gambar "${image.file!.name}"?`,
      })
      if (!isConfirmed) return

      await ProductReturnsAPI.deleteImage(productReturnId!, image.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }] })
      snackbar.enqueueSnackbar('Gambar telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleAddItem = async (values: ProductReturnItemFormDialogShape) => {
    await ProductReturnsAPI.addItem(productReturnId!, {
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.product_id), ''),
      qty: FormUtil.withDefault(values.qty, 0),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }] })
    snackbar.enqueueSnackbar('Barang telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddItemDialog(false)
  }

  const handleAddImage = async (values: ProductReturnImageFormDialogShape) => {
    await ProductReturnsAPI.addImage(productReturnId!, {
      file_path: FormUtil.withDefault(values.file_path, ''),
      description: FormUtil.withDefault(values.description, null),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_RETURN_GET, { productReturnId }] })
    snackbar.enqueueSnackbar('Gamber telah berhasil ditambahkan', { variant: 'success' })
    setOpenAddImageDialog(false)
  }

  if (!productReturnId) {
    return <Navigate to="/error/404" />
  }

  const isPending = data?.status === ProductReturnStatusEnum.PENDING

  return (
    <PageContainer metaTitle="Detail Retur Produk">
      <PageHeader title="Detail Retur Produk" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12} md={12}>
          {data ? (
            <ProductReturnDetailCard
              cardTitle="Detail"
              date={data.date}
              invoiceNumber={data.invoice_number}
              status={data.status}
              supplier={data.supplier!.name}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" alignItems="center" spacing={1}>
                  {isPending && (
                    <Button variant="outlined" onClick={(e) => setAnchorMenu(e.currentTarget)}>
                      Ubah Status
                    </Button>
                  )}

                  {isPending && (
                    <PermissionControl match={PagePermission.PRODUCT_RETURN_UPDATE}>
                      <NavLink to={`/product-returns/${data.id}/update`}>
                        <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                          Ubah
                        </Button>
                      </NavLink>
                    </PermissionControl>
                  )}

                  <Menu
                    anchorEl={anchorMenu}
                    open={TypeUtil.isDefined(anchorMenu)}
                    onClose={() => setAnchorMenu(null)}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    slotProps={{
                      paper: {
                        sx: (theme) => ({
                          marginTop: 1,
                          border: `1px solid ${theme.palette.grey[300]}`,
                          minWidth: 200,
                        }),
                      },
                    }}
                  >
                    <PermissionControl match={[PermissionEnum.PRODUCT_RETURN_MARK_COMPLETE]}>
                      <MenuItem onClick={complete}>Selesai</MenuItem>
                    </PermissionControl>
                  </Menu>
                </Stack>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={6} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Barang"
              action={
                isPending && (
                  <PermissionControl match={[PermissionEnum.PRODUCT_RETURN_ADD_ITEM]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddItemDialog(true)}
                    >
                      Tambah barang
                    </Button>
                  </PermissionControl>
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <ProductReturnItemTable
                  data={data.items ?? []}
                  action={
                    isPending
                      ? (item) => (
                          <PermissionControl match={[PermissionEnum.PRODUCT_RETURN_DELETE_ITEM]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteItem(item)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[undefined, 175, 150, 150, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Gambar"
              action={
                <Button
                  variant="contained"
                  startIcon={<IconCloudUpload size="1.2rem" />}
                  onClick={() => setOpenAddImageDialog(true)}
                >
                  Unggah Gambar
                </Button>
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <ProductReturnImageTable
                  data={data.images ?? []}
                  action={
                    isPending
                      ? (image) => (
                          <PermissionControl match={[PermissionEnum.PRODUCT_RETURN_DELETE_IMAGE]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteImage(image)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[300, undefined, 175, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <ProductReturnItemFormDialog
        open={openAddItemDialog}
        dialogTitle="Tambah Barang"
        productReturnId={productReturnId!}
        onCancel={() => setOpenAddItemDialog(false)}
        onClose={() => setOpenAddItemDialog(false)}
        onSubmit={handleAddItem}
      />

      <ProductReturnImageFormDialog
        open={openAddImageDialog}
        dialogTitle="Unggah Gambar"
        onCancel={() => setOpenAddImageDialog(false)}
        onClose={() => setOpenAddImageDialog(false)}
        onSubmit={handleAddImage}
      />
    </PageContainer>
  )
}

export default ProductReturnDetailPage
