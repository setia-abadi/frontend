import { Box } from '@mui/material'
import ProductReturnsAPI from 'apis/productReturnsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import ProductReturnFormCard, {
  ProductReturnFormCardShape,
} from 'modules/product-return/components/ProductReturnFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Retur Produk',
    href: '/product-returns',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const ProductReturnCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductReturnFormCardShape) => {
    const response = await ProductReturnsAPI.create({
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
      supplier_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.supplier_id), ''),
      invoice_number: FormUtil.withDefault(values.invoice_number, ''),
    })

    if (response.data.data.product_return) {
      return navigate(`/product-returns/${response.data.data.product_return.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Retur Produk">
      <PageHeader title="Tambah Retur Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <ProductReturnFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default ProductReturnCreatePage
