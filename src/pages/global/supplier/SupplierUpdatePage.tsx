import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import SuppliersAPI from 'apis/suppliersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import SupplierFormCard, { SupplierFormCardShape } from 'modules/suppllier/components/SupplierFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemasok',
    href: '/suppliers',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const SupplierUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const { supplierId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { supplierId }],
    queryFn: () => SuppliersAPI.get(supplierId!),
    select: (response) => response.data.data.supplier,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: SupplierFormCardShape) => {
    const response = await SuppliersAPI.update(supplierId!, {
      name: FormUtil.withDefault(values.name, ''),
      address: FormUtil.withDefault(values.address, ''),
      code: FormUtil.withDefault(values.code, ''),
      description: FormUtil.withDefault(values.description, null),
      email: FormUtil.withDefault(values.email, null),
      is_active: FormUtil.withDefault(values.is_active, false),
      phone: FormUtil.withDefault(values.phone, ''),
      supplier_type_id: FormUtil.withDefault(values.supplier_type_id?.value?.toString(), ''),
    })

    if (response.data.data.supplier) {
      return navigate(`/suppliers/${response.data.data.supplier.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Pemasok">
      <PageHeader title="Ubah Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <SupplierFormCard
            cardTitle="Ubah"
            initialValues={{
              name: data.name,
              description: data.description,
              address: data.address,
              code: data.code,
              email: data.email,
              is_active: data.is_active,
              phone: data.phone,
              supplier_type_id: data.supplier_type
                ? {
                    value: data.supplier_type.id,
                    label: data.supplier_type.name,
                  }
                : null,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={6} />
        )}
      </Box>
    </PageContainer>
  )
}

export default SupplierUpdatePage
