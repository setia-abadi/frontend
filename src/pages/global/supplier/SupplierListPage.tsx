import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { keepPreviousData, useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import SuppliersAPI, { PayloadFilterSupplier } from 'apis/suppliersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { SupplierModel } from 'models/supplier'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import SupplierFilterFormPopover, { SupplierFilterFormPopoverShape } from 'modules/suppllier/components/SupplierFilterFormPopover'
import SupplierTable from 'modules/suppllier/components/SuppliersTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { BooleanParam, NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemasok',
    href: '/suppliers',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)
const NullableBooleanParam = withDefault(BooleanParam, null)

const SupplierListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [isActive, setIsActive] = useQueryParam('is-active', NullableBooleanParam)

  const payload: PayloadFilterSupplier = {
    page,
    phrase,
    limit,
    sorts,
    is_active: isActive,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_FILTER, payload],
    queryFn: () => SuppliersAPI.filter(payload),
    select: (response) => response.data.data,
    placeholderData: keepPreviousData,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteSupplier = async (item: SupplierModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Pemasok',
        description: `Apakah anda yakin ingin menghapus "${item.name}"?`,
      })
      if (!isConfirmed) return

      await SuppliersAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.SUPPLIER_FILTER, payload] })
      snackbar.enqueueSnackbar('Pemasok telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setIsActive(undefined)
  }

  const handleApplyFilter = (values: SupplierFilterFormPopoverShape) => {
    setIsActive(values.is_active)
  }

  return (
    <PageContainer metaTitle="Daftar Pemasok">
      <PageHeader title="Daftar Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Pemasok" onChange={setPhrase} />
                <SupplierFilterFormPopover
                  initialValues={{
                    is_active: isActive,
                  }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.SUPPLIER_CREATE}>
                <NavLink to="/suppliers/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Pemasok
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
            <SupplierTable
              data={data.nodes}
              sorts={sorts}
              pagination={{
                page,
                rowsPerPage: limit,
                total: data.total,
                onPaginationChange: handlePaginationChange,
              }}
              onSortChange={setSorts}
              action={(item) => (
                <Stack direction="row" spacing={1}>
                  <PermissionControl match={PagePermission.SUPPLIER_DETAIL}>
                    <Tooltip title="Detail Pemasok">
                      <NavLink to={`/suppliers/${item.id}`}>
                        <IconButton>
                          <IconEye size="1.2rem" />
                        </IconButton>
                      </NavLink>
                    </Tooltip>
                  </PermissionControl>

                  <PermissionControl match={[PermissionEnum.SUPPLIER_DELETE]}>
                    <Tooltip title="Hapus Pemasok">
                      <IconButton onClick={() => deleteSupplier(item)}>
                        <IconTrash size="1.2rem" />
                      </IconButton>
                    </Tooltip>
                  </PermissionControl>
                </Stack>
              )}
            />
            ) : (
              <Skeleton.Table columns={[120, undefined, 200, 200, 150, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default SupplierListPage
