import { Box } from '@mui/material'
import SuppliersAPI from 'apis/suppliersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import SupplierFormCard, { SupplierFormCardShape } from 'modules/suppllier/components/SupplierFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemasok',
    href: '/suppliers',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const SupplierCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: SupplierFormCardShape) => {
    const response = await SuppliersAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      address: FormUtil.withDefault(values.address, ''),
      code: FormUtil.withDefault(values.code, ''),
      description: FormUtil.withDefault(values.description, null),
      email: FormUtil.withDefault(values.email, null),
      is_active: FormUtil.withDefault(values.is_active, false),
      phone: FormUtil.withDefault(values.phone, ''),
      supplier_type_id: FormUtil.withDefault(values.supplier_type_id?.value?.toString(), ''),
    })

    if (response.data.data.supplier) {
      return navigate(`/suppliers/${response.data.data.supplier.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Pemasok">
      <PageHeader
        title="Tambah Pemasok"
        breadcrumbs={pageBreadcrumbs}
      />

      <Box mt={2}>
        <SupplierFormCard
          cardTitle="Tambah"
          onSubmit={handleSubmit}
          onCancel={goBack}
        />
      </Box>
    </PageContainer>
  )
}

export default SupplierCreatePage
