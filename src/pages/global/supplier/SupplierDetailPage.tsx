import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import SuppliersAPI from 'apis/suppliersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PermissionControl from 'modules/permission/components/PermissionControl'
import SupplierDetailCard from 'modules/suppllier/components/SupplierDetailCard'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pemasok',
    href: '/suppliers',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const SupplierDetailPage: FC<Props> = () => {
  const { supplierId } = useParams()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(supplierId),
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { supplierId }],
    queryFn: () => SuppliersAPI.get(supplierId!),
    select: (response) => response.data.data.supplier,
  })

  return (
    <PageContainer metaTitle="Detail Pemasok">
      <PageHeader title="Detail Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <SupplierDetailCard
            cardTitle="Detail"
            address={data.address}
            code={data.code}
            email={data.email}
            phone={data.phone}
            description={data.description}
            isActive={data.is_active}
            name={data.name}
            supplierType={data.supplier_type!.name}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={[PermissionEnum.SUPPLIER_TYPE_UPDATE]}>
                <NavLink to={`/suppliers/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={10} />
        )}
      </Box>
    </PageContainer>
  )
}

export default SupplierDetailPage
