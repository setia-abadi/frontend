import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import ProductsAPI from 'apis/productsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import UpdateProductFormCard, { UpdateProductFormCardShape } from 'modules/product/components/UpdateProductFormCard'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Produk',
    href: '/products',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const ProductUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const snackbar = useSnackbar()
  const { productId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }],
    queryFn: () => ProductsAPI.get(productId!),
    select: (response) => response.data.data.product,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: UpdateProductFormCardShape) => {
    const response = await ProductsAPI.update(productId!, {
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
      image_file_path: FormUtil.withDefault(values.image_file_path, null),
      price: FormUtil.withDefault(values.price, null),
      is_active: data?.is_active ?? false,
    })

    if (response.data.data.product) {
      snackbar.enqueueSnackbar('Produk telah berhasil diubah', { variant: 'success' })
      return navigate(`/products/${response.data.data.product.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Produk">
      <PageHeader title="Ubah Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <UpdateProductFormCard
            cardTitle="Ubah"
            initialProductImageSrc={data.image_file.link}
            initialValues={{
              name: data.name,
              description: data.description,
              price: data.price,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={5} />
        )}
      </Box>
    </PageContainer>
  )
}

export default ProductUpdatePage
