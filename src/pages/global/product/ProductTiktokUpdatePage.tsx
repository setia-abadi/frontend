import { Box } from '@mui/material'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import TiktokProductsAPI, { PayloadTiktokProductCreate } from 'apis/tiktokProductsAPI'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { TiktokProductDimensionUnitEnum } from 'enums/tiktokProductDimensionUnitEnum'
import { TiktokProductPackageWeightEnum } from 'enums/tiktokProductPackageWeightEnum'
import { TiktokCategoryModel } from 'models/tiktokCategory'
import TiktokProductFormCard, {
  TiktokProductFormCardShape,
} from 'modules/tiktok-product/compoents/TiktokProductFormCard'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type Props = {}

const ProductTiktokUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { productId } = useParams()

  const queryKey = [QueryKeyEnum.TIKTOK_PRODUCT_GET, { productId }]

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(productId),
    queryKey: queryKey,
    queryFn: () => TiktokProductsAPI.get(productId!),
    select: (response) => response.data.data.tiktok_product,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: TiktokProductFormCardShape) => {
    const attributes: PayloadTiktokProductCreate['attributes'] | undefined = values.attributes
      ?.filter((attribute) => TypeUtil.isDefined(attribute))
      .map((attribute) => ({
        id: attribute.id,
        values: attribute.values.map((value) => ({
          id: value.value!.toString(),
          name: value.label,
        })),
      }))

    await TiktokProductsAPI.update(productId!, {
      title: FormUtil.withDefault(values.title, ''),
      brand_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.brand_id), null),
      attributes: FormUtil.withDefault(attributes, []),
      category_id: FormUtil.withDefault(values.category_id?.at(-1), ''),
      description: FormUtil.withDefault(values.description, ''),
      dimension_height: FormUtil.withDefault(values.dimension_height, null),
      dimension_length: FormUtil.withDefault(values.dimension_length, null),
      dimension_unit: TiktokProductDimensionUnitEnum.CENTIMETER,
      dimension_width: FormUtil.withDefault(values.dimension_width, null),
      images_uri: FormUtil.withDefault(
        values.image_uri?.map((image) => image.value ?? ''),
        []
      ),
      size_chart_uri: FormUtil.withDefault(values.size_chart_uri, null),
      weight: FormUtil.withDefault(values.weight, 0),
      weight_unit: TiktokProductPackageWeightEnum.KILOGRAM,
    })

    queryClient.invalidateQueries({ queryKey })
    snackbar.enqueueSnackbar('Produk Tiktok telah berhasil diubah', { variant: 'success' })

    return navigate(`/products/${productId}`)
  }

  const getCategoryValues = (categories: TiktokCategoryModel[]): string[] => {
    return categories.reduce<string[]>((prev, category) => {
      prev.push(category.id)
      if (category.children_categories) {
        prev.push(...getCategoryValues(category.children_categories))
      }
      return prev
    }, [])
  }

  if (!data) {
    return 'Loading...'
  }

  return (
    <PageContainer metaTitle="Buat Produk Tiktok">
      <PageHeader
        title="Buat Produk Tiktok"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Produk',
            href: '/products',
          },
          {
            title: 'Ubah',
          },
        ]}
      />

      <Box mt={2}>
        <TiktokProductFormCard 
          cardTitle="Ubah" 
          initialValues={{
            attributes: data.attributes.map((attribute) => ({
              id: attribute.id,
              values: attribute.values.map((value) => ({
                label: value.name,
                value: value.id,
              }))
            })),
            brand_id: {
              label: data.brand.name,
              value: data.brand.id,
            },
            category_id: getCategoryValues([data.category]),
            description: data.description,
            dimension_height: data.dimension_height,
            dimension_length: data.dimension_length,
            dimension_width: data.dimension_width,
            image_uri: data.images.map((image) => ({
              placeholder: image.thumb_url,
              value: image.uri
            })),
            title: data.title,
            weight: data.weight_value,
          }}
          onSubmit={handleSubmit} 
          onCancel={goBack} 
        />
      </Box>
    </PageContainer>
  )
}

export default ProductTiktokUpdatePage
