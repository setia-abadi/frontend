import { Box } from '@mui/material'
import ProductsAPI from 'apis/productsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import ProductFormCard, { ProductFormCardShape } from 'modules/product/components/ProductFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Produk',
    href: '/products',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const ProductCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductFormCardShape) => {
    const response = await ProductsAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      image_file_path: FormUtil.withDefault(values.image_file_path, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.product) {
      return navigate(`/products/${response.data.data.product.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Produk">
      <PageHeader title="Tambah Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <ProductFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default ProductCreatePage
