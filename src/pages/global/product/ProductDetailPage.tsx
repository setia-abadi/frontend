import { Box, Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconAdjustments, IconEdit, IconHistory, IconPlus, IconSpeakerphone, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductStocksAPI from 'apis/productStocksAPI'
import ProductUnitsAPI from 'apis/productUnitsAPI'
import ProductsAPI from 'apis/productsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { ProductUnitModel } from 'models/productUnit'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import BaseProductUnitFormDialog from 'modules/product/components/BaseProductUnitFormDialog'
import ProductDetailCard from 'modules/product/components/ProductDetailCard'
import ProductStockDetailCard from 'modules/product/components/ProductStockDetailCard'
import ProductStockFormDialog, { ProductStockFormDialogShape } from 'modules/product/components/ProductStockFormDialog'
import ProductUnitFormDialog, { ProductUnitFormDialogShape } from 'modules/product/components/ProductUnitFormDialog'
import ProductUnitTable from 'modules/product/components/ProductUnitTable'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useNavigate, useParams } from 'react-router'
import { Link, NavLink } from 'react-router-dom'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import ProductStockAdjustmentHistoryDialog from 'modules/product-stock/components/ProductStockAdjustmentHistoryDialog'
import BroadcastProductPriceChangeFormDialog, {
  BroadcastProductPriceChangeFormDialogShape,
} from 'modules/product/components/BroadcastProductPriceChangeFormDialog'
import WhatsappAPI from 'apis/whatsappAPI'
import useWhatsAppSessionStatus from 'modules/whatsapp-session/hooks/useWhatsAppSessionStatus'
import TiktokProductDetailCard from 'modules/tiktok-product/compoents/TiktokProductDetailCard/TiktokProductDetailCard'
import TiktokProductsAPI from 'apis/tiktokProductsAPI'
import Typography from 'components/Typography'
import Skeleton from 'components/Skeleton'
import ProductActivateCard from 'modules/product/components/ProductActivateCard'
import ProductDeactivateCard from 'modules/product/components/ProductDeactivateCard'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Produk',
    href: '/products',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const ProductDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const navigate = useNavigate()
  const { productId } = useParams()
  const { isLoading: isLoadingWhatsappSession, isLoggedIn } = useWhatsAppSessionStatus()
  const [openAddUnitDialog, setOpenAddUnitDialog] = useState<boolean>(false)
  const [openAdjustmentStockDialog, setOpenAdjustmentStockDialog] = useState<boolean>(false)
  const [openBroadcastPriceChangeDialog, setOpenBroadcastPriceChangeDialog] = useState<boolean>(false)
  const [openHistoryStockAdjstumentDialog, setOpenHistoryStockAdjstumentDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(productId),
    queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }],
    queryFn: () => ProductsAPI.get(productId!),
    select: (response) => response.data.data.product,
  })

  const { data: tiktokProduct, isLoading: isLoadingTiktokProduct } = useQuery({
    enabled: TypeUtil.isDefined(productId),
    queryKey: [QueryKeyEnum.TIKTOK_PRODUCT_GET, { productId }],
    queryFn: () => TiktokProductsAPI.get(productId!),
    select: (response) => response.data.data.tiktok_product,
  })

  const deleteProductUnit = async (productUnit: ProductUnitModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Satuan Produk',
        description: `Apakah kamu yakin ingin menghapus satuan "${productUnit.unit!.name}"?`,
      })
      if (!isConfirmed) return

      await ProductUnitsAPI.delete(productUnit.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }] })
      snackbar.enqueueSnackbar('Satuan produk telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleSubmitAddUnitForm = async (values: ProductUnitFormDialogShape) => {
    await ProductUnitsAPI.create({
      product_id: productId!,
      scale: FormUtil.withDefault(values.scale, 0),
      unit_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.unit_id), ''),
      to_unit_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.to_unit_id), null),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }] })
    snackbar.enqueueSnackbar('Satuan produk telah ditambahkan', { variant: 'success' })
    setOpenAddUnitDialog(false)
  }

  const handleSubmitAdjustmentStockForm = async (values: ProductStockFormDialogShape) => {
    await ProductStocksAPI.adjustment(data!.stock.id, {
      qty: FormUtil.withDefault(values.qty, 0),
      cost_price: FormUtil.withDefault(values.cost_price, null),
    })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }] })
    snackbar.enqueueSnackbar('Stok produk telah disesuaikan', { variant: 'success' })
    setOpenAdjustmentStockDialog(false)
  }

  const handleSubmitBroadcastPriceChangeForm = async (values: BroadcastProductPriceChangeFormDialogShape) => {
    await WhatsappAPI.broadcastProductPriceChange({
      product_id: productId!,
      customer_type_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_type_id), ''),
      old_price: FormUtil.withDefault(values.old_price, 0),
    })
    snackbar.enqueueSnackbar('Perubahan harga produk telah disiarkan', { variant: 'success' })
    setOpenBroadcastPriceChangeDialog(false)
  }

  const handleButtonBroadcastClick = () => {
    if (!isLoggedIn) {
      snackbar.enqueueSnackbar('Sesi WhatsApp belum terhubung, silakan login terlebih dahulu.', { variant: 'error' })
      return navigate('/whatsapp-session')
    }
    setOpenBroadcastPriceChangeDialog(true)
  }

  const activateProduct = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Aktifasi Produk',
        description: `Apakah kamu yakin ingin mengaktifasi produk?`,
      })
      if (!isConfirmed) return

      await ProductsAPI.update(productId!, {
        description: data!.description,
        name: data!.name,
        price: data!.price,
        is_active: true,
        image_file_path: null,
      })

      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }] })
      snackbar.enqueueSnackbar('Produk telah berhasil diaktifkan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deactivateProduct = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Nonaktifkan Produk',
        description: `Apakah kamu yakin ingin menonaktifkan produk?`,
      })
      if (!isConfirmed) return

      await ProductsAPI.update(productId!, {
        description: data!.description,
        name: data!.name,
        price: data!.price,
        is_active: false,
        image_file_path: null,
      })

      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_GET, { productId }] })
      snackbar.enqueueSnackbar('Produk telah berhasil dinonaktifkan', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Detail Produk">
      <PageHeader title="Detail Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            {data ? (
              <ProductDetailCard
                cardTitle="Detail"
                name={data.name}
                imageSrc={data.image_file.link}
                isActive={data.is_active}
                price={data.price}
                description={data.description}
                createdAt={data.created_at}
                updatedAt={data.updated_at}
                cardAction={
                  <Stack direction="row" gap={1}>
                    <PermissionControl match={[PermissionEnum.WHATSAPP_PRODUCT_PRICE_CHANGE_BROADCAST]}>
                      <Button
                        variant="outlined"
                        startIcon={<IconSpeakerphone size="1.2rem" />}
                        isLoading={isLoadingWhatsappSession}
                        onClick={handleButtonBroadcastClick}
                      >
                        Siarkan Perubahan Harga
                      </Button>
                    </PermissionControl>

                    <PermissionControl match={PagePermission.PRODUCT_UPDATE}>
                      <NavLink to={`/products/${data.id}/update`}>
                        <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                          Ubah
                        </Button>
                      </NavLink>
                    </PermissionControl>
                  </Stack>
                }
              />
            ) : (
              <Skeleton.DetailLineCard cardTitle="Detail" rows={6} />
            )}
          </Grid>

          <Grid item xs={12}>
            {data ? (
              <ProductStockDetailCard
                stock={data.stock.qty}
                updatedAt={data.stock.updated_at}
                cardTitle="Stok"
                cardAction={
                  <Stack direction="row" gap={1}>
                    <PermissionControl match={[PermissionEnum.PRODUCT_STOCK_ADJUSTMENT_FETCH]}>
                      <Button
                        variant="outlined"
                        color="info"
                        startIcon={<IconHistory size="1.2rem" />}
                        onClick={() => setOpenHistoryStockAdjstumentDialog(true)}
                      >
                        Riwayat
                      </Button>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PRODUCT_STOCK_ADJUSTMENT]}>
                      <Button
                        variant="contained"
                        startIcon={<IconAdjustments size="1.2rem" />}
                        onClick={() => setOpenAdjustmentStockDialog(true)}
                      >
                        Sesuaikan Stok
                      </Button>
                    </PermissionControl>
                  </Stack>
                }
              />
            ) : (
              <Skeleton.DetailLineCard cardTitle="Stok" rows={2} />
            )}
          </Grid>

          <Grid item xs={12}>
            <Paper>
              <CardHeader
                title="Satuan"
                action={
                  <PermissionControl match={[PermissionEnum.PRODUCT_UNIT_CREATE]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddUnitDialog(true)}
                    >
                      Tambah Satuan
                    </Button>
                  </PermissionControl>
                }
              />
              <Divider />
              <CardContent>
                {data ? (
                  <ProductUnitTable
                    data={data.product_units ?? []}
                    action={(unit) => (
                      <Stack direction="row" spacing={1}>
                        <PermissionControl match={[PermissionEnum.PRODUCT_UNIT_DELETE]}>
                          <Tooltip title="Hapus Satuan">
                            <IconButton onClick={() => deleteProductUnit(unit)}>
                              <IconTrash size="1.2rem" />
                            </IconButton>
                          </Tooltip>
                        </PermissionControl>
                      </Stack>
                    )}
                  />
                ) : (
                  <Skeleton.Table columns={[300, 150, 150]} />
                )}
              </CardContent>
            </Paper>
          </Grid>

          {data && (
            <Grid item xs={12}>
              {data.is_active ? (
                <ProductDeactivateCard onBtnClick={deactivateProduct} />
              ) : (
                <ProductActivateCard onBtnClick={activateProduct} />
              )}
            </Grid>
          )}

          <Grid item xs={12}>
            {isLoadingTiktokProduct ? (
              <Skeleton.DetailLineCard cardTitle="Produk Tiktok" rows={5} />
            ) : tiktokProduct ? (
              <TiktokProductDetailCard
                cardTitle="Produk Tiktok"
                brand={tiktokProduct?.brand?.name || '-'}
                dimensionHeight={tiktokProduct.dimension_height}
                dimensionLength={tiktokProduct.dimension_length}
                dimensionWidth={tiktokProduct.dimension_width}
                images={tiktokProduct.images.map((image) => image.url)}
                status={tiktokProduct.status}
                title={tiktokProduct.title}
                weight={tiktokProduct.weight_value}
                cardAction={
                  <PermissionControl match={[PermissionEnum.TIKTOK_PRODUCT_UPDATE]}>
                    <Link to={`/products/${productId}/tiktok/update`}>
                      <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                        Edit Produk TikTok
                      </Button>
                    </Link>
                  </PermissionControl>
                }
              />
            ) : (
              <Paper>
                <CardHeader title="Produk Tiktok" />
                <Divider />
                <CardContent>
                  <Box display="flex" justifyContent="center" alignItems="center" p={3}>
                    <Stack direction="column" gap={5} alignItems="center">
                      <Typography variant="body1" color="gray" textAlign="center" maxWidth={400}>
                        Produk belum terhubung dengan TikTok. Silahkan buat produk TikTok untuk menghubungkan produk
                        dengan TiTkok produk.
                      </Typography>

                      <PermissionControl match={[PermissionEnum.TIKTOK_PRODUCT_CREATE]}>
                        <Link to={`/products/${productId}/tiktok/create`}>
                          <Button variant="contained">Buat Produk TikTok</Button>
                        </Link>
                      </PermissionControl>
                    </Stack>
                  </Box>
                </CardContent>
              </Paper>
            )}
          </Grid>
        </Grid>
      </Box>

      {data && (
        <ProductStockFormDialog
          dialogTitle="Penyesuaian Stok"
          dialogSubTitle={`Untuk mengubah satuan silahkan masukkan kuantitas, dan masukkan harga dasar jika kuantitas lebih besar dari ${data.stock.qty}.`}
          open={openAdjustmentStockDialog}
          onCancel={() => setOpenAdjustmentStockDialog(false)}
          onClose={() => setOpenAdjustmentStockDialog(false)}
          onSubmit={handleSubmitAdjustmentStockForm}
        />
      )}

      {data && (
        <ProductStockAdjustmentHistoryDialog
          dialogTitle="Riwayat Penyesuaian Stok"
          open={openHistoryStockAdjstumentDialog}
          productStockId={data.stock.id}
          onClose={() => setOpenHistoryStockAdjstumentDialog(false)}
        />
      )}

      {data && (
        <BroadcastProductPriceChangeFormDialog
          open={openBroadcastPriceChangeDialog}
          dialogTitle="Siarkan Perubahan Harga Produk"
          dialogSubTitle="Masukkan harga lama dan jenis pelanggan untuk menyiarkan perubahan harga kepada pelanggan."
          productName={data.name}
          newPrice={data.price}
          onCancel={() => setOpenBroadcastPriceChangeDialog(false)}
          onClose={() => setOpenBroadcastPriceChangeDialog(false)}
          onSubmit={handleSubmitBroadcastPriceChangeForm}
        />
      )}

      {data?.product_units && data?.product_units.length > 0 ? (
        <ProductUnitFormDialog
          dialogTitle="Tambah Satuan"
          dialogSubTitle="Untuk menambahkan satuan, silahkan pilih unit dasar, nilai konversi, dan unit tujuan lalu tekan kirim."
          productId={productId!}
          open={openAddUnitDialog}
          onCancel={() => setOpenAddUnitDialog(false)}
          onClose={() => setOpenAddUnitDialog(false)}
          onSubmit={handleSubmitAddUnitForm}
        />
      ) : (
        <BaseProductUnitFormDialog
          dialogTitle="Tambah Satuan Dasar"
          dialogSubTitle="Untuk menambahkan satuan dasar, silahkan masukkan nilai unit, dan pilih unit dasar lalu tekan kirim."
          productId={productId!}
          open={openAddUnitDialog}
          onCancel={() => setOpenAddUnitDialog(false)}
          onClose={() => setOpenAddUnitDialog(false)}
          onSubmit={handleSubmitAddUnitForm}
        />
      )}
    </PageContainer>
  )
}

export default ProductDetailPage
