import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { keepPreviousData, useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import ProductsAPI, { PayloadProductsFilter } from 'apis/productsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { ProductModel } from 'models/product'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductFilterFormPopover, {
  ProductFilterFormPopoverShape,
} from 'modules/product/components/ProductFilterFormPopover'
import ProductTable from 'modules/product/components/ProductTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { BooleanParam, NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Produk',
    href: '/products',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)
const NullableBooleanParam = withDefault(BooleanParam, null)

const ProductListPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [isActive, setIsActive] = useQueryParam('is-active', NullableBooleanParam)
  const [isLoss, setIsLoss] = useQueryParam('is-loss', NullableBooleanParam)

  const payload: PayloadProductsFilter = {
    page,
    phrase,
    limit,
    sorts,
    is_active: isActive,
    is_loss: isLoss,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_FILTER, payload],
    queryFn: () => ProductsAPI.filter(payload),
    select: (response) => response.data.data,
    placeholderData: keepPreviousData,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteProduct = async (item: ProductModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Produk',
        description: `Apakah anda yakin ingin menghapus produk "${item.name}"?`,
      })
      if (!isConfirmed) return

      await ProductsAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_FILTER, payload] })
      snackbar.enqueueSnackbar('Produk telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setIsActive(undefined)
  }

  const handleApplyFilter = (values: ProductFilterFormPopoverShape) => {
    setIsActive(values.is_active)
    setIsLoss(values.is_loss)
  }

  return (
    <PageContainer metaTitle="Daftar Produk">
      <PageHeader title="Daftar Produk" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Produk" onChange={setPhrase} />
                <ProductFilterFormPopover
                  initialValues={{ is_active: isActive, is_loss: isLoss }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.PRODUCT_UPDATE}>
                <NavLink to="/products/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Produk
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <ProductTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.PRODUCT_DETAIL}>
                      <Tooltip title="Detail Produk">
                        <NavLink to={`/products/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PRODUCT_DELETE]}>
                      <Tooltip title="Hapus Produk">
                        <IconButton onClick={() => deleteProduct(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[300, 125, 100, 100, 150]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ProductListPage
