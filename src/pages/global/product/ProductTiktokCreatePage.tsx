import { Box } from '@mui/material'
import TiktokProductsAPI, { PayloadTiktokProductCreate } from 'apis/tiktokProductsAPI'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import { TiktokProductDimensionUnitEnum } from 'enums/tiktokProductDimensionUnitEnum'
import { TiktokProductPackageWeightEnum } from 'enums/tiktokProductPackageWeightEnum'
import TiktokProductFormCard, {
  TiktokProductFormCardShape,
} from 'modules/tiktok-product/compoents/TiktokProductFormCard'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'

type Props = {}

const ProductTiktokCreatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const snackbar = useSnackbar()
  const { productId } = useParams()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: TiktokProductFormCardShape) => {
    const attributes: PayloadTiktokProductCreate['attributes'] | undefined = values.attributes
      ?.filter((attribute) => TypeUtil.isDefined(attribute))
      .map((attribute) => ({
        id: attribute.id,
        values: attribute.values.map((value) => ({
          id: value.value!.toString(),
          name: value.label,
        })),
      }))

    await TiktokProductsAPI.create({
      title: FormUtil.withDefault(values.title, ''),
      brand_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.brand_id), null),
      attributes: FormUtil.withDefault(attributes, []),
      category_id: FormUtil.withDefault(values.category_id?.at(-1), ''),
      description: FormUtil.withDefault(values.description, ''),
      dimension_height: FormUtil.withDefault(values.dimension_height, null),
      dimension_length: FormUtil.withDefault(values.dimension_length, null),
      dimension_unit: TiktokProductDimensionUnitEnum.CENTIMETER,
      dimension_width: FormUtil.withDefault(values.dimension_width, null),
      images_uri: FormUtil.withDefault(
        values.image_uri?.map((image) => image.value ?? ''),
        []
      ),
      product_id: productId!,
      size_chart_uri: FormUtil.withDefault(values.size_chart_uri, null),
      weight: FormUtil.withDefault(values.weight, 0),
      weight_unit: TiktokProductPackageWeightEnum.KILOGRAM,
    })

    snackbar.enqueueSnackbar('Produk Tiktok telah berhasil dibuat', { variant: 'success' })

    return navigate(`/products/${productId}`)
  }

  return (
    <PageContainer metaTitle="Buat Produk Tiktok">
      <PageHeader
        title="Buat Produk Tiktok"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Produk',
            href: '/products',
          },
          {
            title: 'Tambah',
          },
        ]}
      />

      <Box mt={2}>
        <TiktokProductFormCard cardTitle="Buat" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default ProductTiktokCreatePage
