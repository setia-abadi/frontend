import { Grid } from '@mui/material'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import CashierSessionsAPI from 'apis/cashierSessionsAPI'
import PageContainer from 'components/PageContainer'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import CashierSessionEndFromCard from 'modules/cashier-session/components/CashierSessionEndFromCard'
import CashierSessionStartFromCard, {
  CashierSessionStartFormCardShape,
} from 'modules/cashier-session/components/CashierSessionStartFromCard'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import FormUtil from 'utils/formUtil'

type Props = unknown

const CashierSessionPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()

  const { data, isLoading } = useQuery({
    queryKey: [QueryKeyEnum.CASHIER_SESSION_GET_CURRENT],
    queryFn: () => CashierSessionsAPI.current(),
    select: (response) => response.data.data.cashier_session,
  })

  const handleSubmitStartSessionForm = async (values: CashierSessionStartFormCardShape) => {
    const { isConfirmed } = await confirmation.ask({
      title: 'Mulai Sesi',
      description: 'Apakah Anda yakin ingin memulai sesi hari ini?',
    })
    if (!isConfirmed) return

    await CashierSessionsAPI.start({
      starting_cash: FormUtil.withDefault(values.starting_cash, 0),
    })

    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CASHIER_SESSION_GET_CURRENT] })
    snackbar.enqueueSnackbar('Sesi telah berhasil dimulai', { variant: 'success' })
  }

  const handleSubmitEndSessionForm = async () => {
    const { isConfirmed } = await confirmation.ask({
      title: 'Akhiri Sesi',
      description: 'Apakah Anda yakin ingin mengakhiri sesi hari ini?',
    })
    if (!isConfirmed) return

    await CashierSessionsAPI.end({})

    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CASHIER_SESSION_GET_CURRENT] })
    snackbar.enqueueSnackbar('Sesi telah berhasil diakhiri', { variant: 'success' })
  }

  if (isLoading) {
    return 'Loading ...'
  }

  return (
    <PageContainer metaTitle="Sesi Kasir">
      <Grid container justifyContent="center" alignItems="center" sx={{ flex: 1 }}>
        <Grid item xs={12} sm={6} md={4} lg={5} xl={4}>
          {data ? (
            <CashierSessionEndFromCard startDateTime={data.started_at} onSubmit={handleSubmitEndSessionForm} />
          ) : (
            <CashierSessionStartFromCard onSubmit={handleSubmitStartSessionForm} />
          )}
        </Grid>
      </Grid>
    </PageContainer>
  )
}

export default CashierSessionPage
