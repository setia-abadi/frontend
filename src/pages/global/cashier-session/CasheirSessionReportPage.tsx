import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import CashierSessionsAPI, { PayloadCashierSessionFilter } from 'apis/cashierSessionsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { SELECT_OPTION_ALL } from 'constants/option'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { DayjsParam, SelectOptionParam, SortsParam } from 'libs/useQueryParam'
import CashierSessionFilterFormPopover, {
  CashierSessionFilterFormPopoverShape,
} from 'modules/cashier-session/components/CashierSessionFilterFormPopover'
import CashierSessionTable from 'modules/cashier-session/components/CashierSessionTable'
import DownloadCashierSessionReportButton from 'modules/cashier-session/components/DownloadCashierSessionReportButton'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { usePermission } from 'modules/permission/hooks/usePermission'
import { FC, useMemo } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import FormUtil from 'utils/formUtil'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Laporan Sesi Kasir',
    href: '/cashier-sessions/reports',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)
const NullableDateParam = withDefault(DayjsParam, null)
const UserParam = withDefault(SelectOptionParam, SELECT_OPTION_ALL)
const StatusParam = withDefault(StringParam, undefined)

const CashierSessionReportPage: FC<Props> = () => {
  const { hasAnyPermission } = usePermission()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [status, setStatus] = useQueryParam('status', StatusParam)
  const [startedDate, setStartedDate] = useQueryParam('started-date', NullableDateParam)
  const [endedDate, setEndedDate] = useQueryParam('ended-date', NullableDateParam)
  const [user, setUser] = useQueryParam('user', UserParam)

  const payload: PayloadCashierSessionFilter = {
    page,
    phrase,
    limit,
    sorts,
    ended_at: FormUtil.withDefault(FormUtil.getDayjsValue(endedDate, { withTime: true }), null),
    started_at: FormUtil.withDefault(FormUtil.getDayjsValue(startedDate, { withTime: true }), null),
    status: FormUtil.withDefault(status as CashierSessionStatusEnum, null),
    user_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(user), null),
  }

  const isCashier = useMemo(
    () => hasAnyPermission([PermissionEnum.CASHIER_SESSION_FETCH_FOR_CURRENT_USER]),
    [hasAnyPermission]
  )

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CASHIER_SESSION_FILTER, payload],
    queryFn: () => (isCashier ? CashierSessionsAPI.filterForCurrentUser(payload) : CashierSessionsAPI.filter(payload)),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setStatus(undefined)
    setEndedDate(undefined)
    setStartedDate(undefined)
    setUser(undefined)
  }

  const handleApplyFilter = (values: CashierSessionFilterFormPopoverShape) => {
    setStatus(values.status)
    setEndedDate(values.ended_date)
    setStartedDate(values.started_date)
    setUser(values.user_id)
  }

  return (
    <PageContainer metaTitle="Laporan Sesi Kasir">
      <PageHeader title="Laporan Sesi Kasir" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Sesi Kasir" onChange={setPhrase} />
                <CashierSessionFilterFormPopover
                  initialValues={{
                    status: status as CashierSessionStatusEnum,
                    ended_date: endedDate,
                    started_date: startedDate,
                    user_id: user,
                  }}
                  hiddenFields={{
                    user_id: isCashier,
                  }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <CashierSessionTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={[PermissionEnum.CASHIER_SESSION_DOWNLOAD_REPORT]}>
                      <Tooltip title="Detail Sesi Kasir">
                        <NavLink to={`/cashier-sessions/report/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.CASHIER_SESSION_DOWNLOAD_REPORT]}>
                      <Tooltip title="Unduh Laporan">
                        <DownloadCashierSessionReportButton
                          disabled={item.status === CashierSessionStatusEnum.ACTIVE}
                          cashierSessionId={item.id}
                        />
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[undefined, 175, 175, 300, 100, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default CashierSessionReportPage
