import { Box, Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import CashierSessionsAPI, { PayloadCashierSessionTransactionFilter } from 'apis/cashierSessionsAPI'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import Skeleton from 'components/Skeleton'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import CashierSessionDetailCard from 'modules/cashier-session/components/CashierSessionDetailCard'
import CashierSessionTransactionTable from 'modules/cashier-session/components/CashierSessionTransactionTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'

type Props = unknown

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)

const CashierSessionDetailPage: FC<Props> = () => {
  const { cashierSessionId } = useParams()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const payload: PayloadCashierSessionTransactionFilter = {
    page,
    phrase,
    limit,
    sorts,
    cashier_session_id: cashierSessionId!,
  }

  const { data: cashierSession } = useQuery({
    queryKey: [QueryKeyEnum.CASHIER_SESSION_GET, { cashierSessionId }],
    queryFn: () => CashierSessionsAPI.get(cashierSessionId!),
    select: (response) => response.data.data.cashier_session,
  })

  const { data: transaction } = useQuery({
    queryKey: [QueryKeyEnum.CASHIER_SESSION_TRANSACTION_FILTER, { cashierSessionId }],
    queryFn: () => CashierSessionsAPI.transactionFilter(cashierSessionId!, payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  return (
    <PageContainer metaTitle="Detail Sesi Kasir">
      <PageHeader
        title="Detail Sesi Kasir"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Laporan Sesi Kasir',
            href: '/cashier-sessions/report',
          },
          {
            title: 'Detail Sesi Kasir',
          },
        ]}
      />

      <Grid container gap={4}>
        <Grid item xs={12}>
          {cashierSession ? (
            <CashierSessionDetailCard
              cardTitle="Detail"
              endedAt={cashierSession.ended_at}
              endingCash={cashierSession.ending_cash}
              startedAt={cashierSession.started_at}
              startingCash={cashierSession.starting_cash}
              status={cashierSession.status}
              user={cashierSession.user?.name ?? null}
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={6} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader title="Transaksi" />
            <Divider />
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md>
                  <Stack direction="row" spacing={1}>
                    <SearchBar initialValue={phrase} placeholder="Cari Transaksi" onChange={setPhrase} />
                  </Stack>
                </Grid>
              </Grid>

              <Box mt={2}>
                {transaction ? (
                  <CashierSessionTransactionTable
                    data={transaction.nodes ?? []}
                    sorts={sorts}
                    pagination={{
                      page,
                      rowsPerPage: limit,
                      total: transaction.total,
                      onPaginationChange: handlePaginationChange,
                    }}
                    onSortChange={setSorts}
                    action={(item) => (
                      <Stack direction="row" spacing={1}>
                        <PermissionControl match={[PermissionEnum.TRANSACTION_GET]}>
                          <Tooltip title="Detail Transaksi">
                            <NavLink to={`/cashier-sessions/report/${item.cashier_session_id}/transactions/${item.id}`}>
                              <IconButton>
                                <IconEye size="1.2rem" />
                              </IconButton>
                            </NavLink>
                          </Tooltip>
                        </PermissionControl>
                      </Stack>
                    )}
                  />
                ) : (
                  <Skeleton.Table columns={[undefined, 150, 200, 200, 100]} />
                )}
              </Box>
            </CardContent>
          </Paper>
        </Grid>
      </Grid>
    </PageContainer>
  )
}

export default CashierSessionDetailPage
