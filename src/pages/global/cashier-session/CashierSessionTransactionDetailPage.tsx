import { Divider, Grid } from '@mui/material'
import { IconPrinter } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import TransactionsAPI from 'apis/transactionsAPI'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import Skeleton from 'components/Skeleton'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PermissionControl from 'modules/permission/components/PermissionControl'
import TransactionDetailCard from 'modules/transaction/components/TransactionDetailCard'
import TransactionItemTable from 'modules/transaction/components/TransactionItemTable'
import TransactionPaymentTable from 'modules/transaction/components/TransactionPaymentTable'
import TransactionPrintReceiptButton from 'modules/transaction/components/TransactionPrintReceiptButton'
import { FC } from 'react'
import { useParams } from 'react-router'

type Props = unknown

const CashierSessionTransactionDetailPage: FC<Props> = () => {
  const { cashierSessionId, transactionId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.TRANSACTION_GET, { transactionId }],
    queryFn: () => TransactionsAPI.get(transactionId!),
    select: (response) => response.data.data.transaction,
  })

  return (
    <PageContainer metaTitle="Detail Transaksi">
      <PageHeader
        title="Detail Transaksi"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Laporan Sesi Kasir',
            href: '/cashier-sessions/report',
          },
          {
            title: 'Detail Sesi Kasir',
            href: `/cashier-sessions/report/${cashierSessionId}`,
          },
          {
            title: 'Detail Transaksi',
          },
        ]}
      />

      <Grid container gap={4}>
        <Grid item xs={12}>
          {data ? (
            <TransactionDetailCard
              cardTitle="Detail"
              paymentAt={data.payment_at}
              status={data.status}
              total={data.total}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <PermissionControl match={[PermissionEnum.TRANSACTION_REPRINT]}>
                  <TransactionPrintReceiptButton
                    transactionId={transactionId!}
                    variant="contained"
                    startIcon={<IconPrinter size="1.2rem" />}
                  >
                    Cetak Kuitansi
                  </TransactionPrintReceiptButton>
                </PermissionControl>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={5} />
          )}
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader title="Barang" />
            <Divider />
            <CardContent>
              {data ? (
                <TransactionItemTable data={data.items ?? []} />
              ) : (
                <Skeleton.Table columns={[undefined, 150, 175, 175, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader title="Pembayaran" />
            <Divider />
            <CardContent>
              {data ? (
                <TransactionPaymentTable data={data.payments ?? []} />
              ) : (
                <Skeleton.Table columns={[200, 150, 200, 175, 100]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>
    </PageContainer>
  )
}

export default CashierSessionTransactionDetailPage
