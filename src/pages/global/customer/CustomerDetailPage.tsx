import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import CustomersAPI from 'apis/customersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import CustomerDetailCard from 'modules/customer/components/CustomerDetailCard'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pelanggan',
    href: '/customers',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const CustomerDetailPage: FC<Props> = () => {
  const { customerId } = useParams()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(customerId),
    queryKey: [QueryKeyEnum.CUSTOMER_GET, { customerId }],
    queryFn: () => CustomersAPI.get(customerId!),
    select: (response) => response.data.data.customer,
  })

  return (
    <PageContainer metaTitle="Detail Pelanggan">
      <PageHeader title="Detail Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <CustomerDetailCard
            cardTitle="Detail"
            address={data.address}
            email={data.email}
            phone={data.phone}
            isActive={data.is_active}
            name={data.name}
            latitude={data.latitude}
            longitude={data.longitude}
            customerType={data.customer_type?.name ?? ''}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={PagePermission.CUSTOMER_UPDATE}>
                <NavLink to={`/customers/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={9} />
        )}
      </Box>
    </PageContainer>
  )
}

export default CustomerDetailPage
