import { Box } from '@mui/material'
import CustomersAPI from 'apis/customersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import CustomerFormCard, { CustomerFormCardShape } from 'modules/customer/components/CustomerFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pelanggan',
    href: '/customers',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const CustomerCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: CustomerFormCardShape) => {
    const response = await CustomersAPI.create({
      customer_type_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_type_id), ''),
      name: FormUtil.withDefault(values.name, ''),
      address: FormUtil.withDefault(values.address, ''),
      email: FormUtil.withDefault(values.email, ''),
      phone: FormUtil.withDefault(values.phone, ''),
      is_active: FormUtil.withDefault(values.is_active, false),
      latitude: FormUtil.withDefault(values.latitude, 0),
      longitude: FormUtil.withDefault(values.longitude, 0),
    })

    if (response.data.data.customer) {
      return navigate(`/customers/${response.data.data.customer.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Pelanggan">
      <PageHeader
        title="Tambah Pelanggan"
        breadcrumbs={pageBreadcrumbs}
      />

      <Box mt={2}>
        <CustomerFormCard
          cardTitle="Tambah"
          onSubmit={handleSubmit}
          onCancel={goBack}
        />
      </Box>
    </PageContainer>
  )
}

export default CustomerCreatePage
