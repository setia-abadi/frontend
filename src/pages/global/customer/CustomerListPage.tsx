import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import CustomersAPI, { PayloadCustomersFilter } from 'apis/customersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { CustomerModel } from 'models/customer'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import CustomerFilterFormPopover, {
  CustomerFilterFormPopoverShape,
} from 'modules/customer/components/CustomerFilterFormPopover'
import CustomerTable from 'modules/customer/components/CustomerTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { BooleanParam, NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pelanggan',
    href: '/customers',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)
const IsActiveParam = withDefault(BooleanParam, null)

const CustomerListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const confirmation = useConfirmation()
  const { enqueueSnackbar } = useSnackbar()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [isActive, setIsActive] = useQueryParam('is-active', IsActiveParam)

  const payload: PayloadCustomersFilter = {
    page,
    phrase,
    limit,
    sorts,
    is_active: isActive,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CUSTOMER_FILTER, payload],
    queryFn: () => CustomersAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteCustomer = async (item: CustomerModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Pelanggan',
        description: `Apakah anda yakin ingin menghapus pelanggan "${item.name}"?`,
      })
      if (!isConfirmed) return

      await CustomersAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CUSTOMER_FILTER, payload] })
      enqueueSnackbar('Pelanggan telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setIsActive(undefined)
  }

  const handleApplyFilter = (values: CustomerFilterFormPopoverShape) => {
    setIsActive(values.is_active)
  }

  return (
    <PageContainer metaTitle="Daftar Pelanggan">
      <PageHeader title="Daftar Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" gap={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Pelanggan" onChange={setPhrase} />
                <CustomerFilterFormPopover
                  initialValues={{ is_active: isActive }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <PermissionControl match={PagePermission.CUSTOMER_CREATE}>
              <Grid item xs={12} md={'auto'}>
                <NavLink to="/customers/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Pelanggan
                  </Button>
                </NavLink>
              </Grid>
            </PermissionControl>
          </Grid>

          <Box mt={2}>
            {data ? (
              <CustomerTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.CUSTOMER_DETAIL}>
                      <Tooltip title="Detail Pelanggan">
                        <NavLink to={`/customers/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.CUSTOMER_DELETE]}>
                      <Tooltip title="Hapus Pelanggan">
                        <IconButton onClick={() => deleteCustomer(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[undefined, 250, 225, 150, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default CustomerListPage
