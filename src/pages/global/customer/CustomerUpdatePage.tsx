import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import CustomersAPI from 'apis/customersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import CustomerFormCard, { CustomerFormCardShape } from 'modules/customer/components/CustomerFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pelanggan',
    href: '/customers',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const CustomerUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const { customerId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.CUSTOMER_GET, { customerId }],
    queryFn: () => CustomersAPI.get(customerId!),
    select: (response) => response.data.data.customer,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: CustomerFormCardShape) => {
    const response = await CustomersAPI.update(customerId!, {
      customer_type_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_type_id), ''),
      name: FormUtil.withDefault(values.name, ''),
      address: FormUtil.withDefault(values.address, ''),
      email: FormUtil.withDefault(values.email, ''),
      is_active: FormUtil.withDefault(values.is_active, false),
      phone: FormUtil.withDefault(values.phone, ''),
      latitude: FormUtil.withDefault(values.latitude, 0),
      longitude: FormUtil.withDefault(values.longitude, 0),
    })

    if (response.data.data.customer) {
      return navigate(`/customers/${response.data.data.customer.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Pelanggan">
      <PageHeader title="Ubah Pelanggan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <CustomerFormCard
            cardTitle="Ubah"
            initialValues={{
              name: data.name,
              address: data.address,
              email: data.email,
              is_active: data.is_active,
              phone: data.phone,
              latitude: data.latitude,
              longitude: data.longitude,
              customer_type_id: data.customer_type
                ? {
                    label: data.customer_type.name,
                    value: data.customer_type.id,
                  }
                : null,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={6} />
        )}
      </Box>
    </PageContainer>
  )
}

export default CustomerUpdatePage
