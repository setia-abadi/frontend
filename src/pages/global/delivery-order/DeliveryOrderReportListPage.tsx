import { Box, Grid, Stack } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { FC } from 'react'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import DeliveryOrderReviewsAPI, { PayloadDeliveryOrderReviewFilter } from 'apis/deliveryOrderReviewsAPI'
import Skeleton from 'components/Skeleton'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'
import DeliveryOrderReportFilterPopover, {
  DeliveryOrderReportFilterFormPopoverShape,
} from 'modules/delivery-order/components/DeliveryOrderReportFilterPopover'
import DeliveryOrderReportTable from 'modules/delivery-order/components/DeliveryOrderReportTable'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengiriman Pesanan',
    href: '/delivery-orders',
  },
  {
    title: 'Laporan',
    href: '/reports',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)

const DeliveryOrderReportListPage: FC<Props> = () => {
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [type, setType] = useQueryParam('type', NullableStringParam)

  const payload: PayloadDeliveryOrderReviewFilter = {
    page,
    phrase,
    limit,
    sorts,
    type: type ? (type as DeliveryOrderReviewTypeEnum) : null,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_FILTER, payload],
    queryFn: () => DeliveryOrderReviewsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const handleResetFilter = () => {
    setType(undefined)
  }

  const handleApplyFilter = (values: DeliveryOrderReportFilterFormPopoverShape) => {
    setType(values.type)
  }

  return (
    <PageContainer metaTitle="Daftar Laporan Pengiriman Pesanan">
      <PageHeader title="Daftar Laporan Pengiriman Pesanan" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" gap={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Laporan" onChange={setPhrase} />
                <DeliveryOrderReportFilterPopover
                  initialValues={{
                    type: type as DeliveryOrderReviewTypeEnum,
                  }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <DeliveryOrderReportTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
              />
            ) : (
              <Skeleton.Table columns={[200, 175, undefined, 175]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default DeliveryOrderReportListPage
