import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconDownload, IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import DeliveryOrdersAPI, { PayloadDeliveryOrdersFilter } from 'apis/deliveryOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SelectOptionParam, SortsParam } from 'libs/useQueryParam'
import { DeliveryOrderModel } from 'models/deliveryOrder'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import DeliveryOrderFilterFormPopover, {
  DeliveryOrderFilterFormPopoverShape,
} from 'modules/delivery-order/components/DeliveryOrderFilterFormPopover'
import DeliveryOrderTable from 'modules/delivery-order/components/DeliveryOrderTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'
import { SELECT_OPTION_ALL } from 'constants/option'
import FormUtil from 'utils/formUtil'
import FileUtil from 'utils/fileUtil'
import StringUtil from 'utils/stringUtil'
import DeliveryOrderDownloadReportFormDialog, {
  DeliveryOrderDownloadReportFormDialogShape,
} from 'modules/delivery-order/components/DeliveryOrderDownloadReportFormDialog'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengiriman Pesanan',
    href: '/delivery-orders',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const NullableStringParam = withDefault(StringParam, null)
const CustomerParam = withDefault(SelectOptionParam, SELECT_OPTION_ALL)

const DeliveryOrderListPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', NullableStringParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [status, setStatus] = useQueryParam('status', StringParam)
  const [customer, setCustomer] = useQueryParam('customer', CustomerParam)
  const [openDownloadReportDialog, setOpenDownloadReportDialog] = useState<boolean>(false)

  const payload: PayloadDeliveryOrdersFilter = {
    page,
    phrase,
    limit,
    sorts,
    customer_id: customer && customer.value ? customer.value.toString() : null,
    status: status ? (status as DeliveryOrderStatusEnum) : null,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_FILTER, payload],
    queryFn: () => DeliveryOrdersAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteDeliveryOrder = async (item: DeliveryOrderModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Pengiriman Pesanan',
        description: `Apakah anda yakin ingin menghapus pengantaran pesanan untuk customer ${item.customer!.name}`,
      })
      if (!isConfirmed) return

      await DeliveryOrdersAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.UNIT_FILTER, payload] })
      snackbar.enqueueSnackbar('Pengataran pesanan telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setStatus(undefined)
    setCustomer(undefined)
  }

  const handleApplyFilter = (values: DeliveryOrderFilterFormPopoverShape) => {
    setStatus(values.status)
    setCustomer(values.customer)
  }

  const handleDownloadReportBtnClick = () => {
    setOpenDownloadReportDialog(true)
  }

  const handleSubmitDownloadReportForm = async (values: DeliveryOrderDownloadReportFormDialogShape) => {
    const response = await DeliveryOrdersAPI.downloadReport({
      start_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.start_date), ''),
      end_date: FormUtil.withDefault(FormUtil.getDayjsValue(values.end_date), ''),
    })

    const contentDisposition = response.headers['content-disposition']
    const filename = contentDisposition
      ? FileUtil.getFilenameFromContentDisposition(contentDisposition)
      : StringUtil.uniqueId()

    FileUtil.download(response.data, filename!)
    snackbar.enqueueSnackbar('Berhasil diunduh', { variant: 'success' })
  }

  return (
    <PageContainer metaTitle="Daftar Pengiriman Pesanan">
      <PageHeader title="Daftar Pengiriman Pesanan" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" spacing={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Pengataran Pesanan" onChange={setPhrase} />
                <DeliveryOrderFilterFormPopover
                  initialValues={{ customer, status: status as DeliveryOrderStatusEnum }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_DOWNLOAD_REPORT]}>
                <Button
                  startIcon={<IconDownload size="1rem" />}
                  variant="contained"
                  onClick={handleDownloadReportBtnClick}
                >
                  Unduh Laporan
                </Button>
              </PermissionControl>
            </Grid>
            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.DELIVERY_ORDER_CREATE}>
                <NavLink to="/delivery-orders/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Pengataran Pesanan
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <DeliveryOrderTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.DELIVERY_ORDER_DETAIL}>
                      <Tooltip title="Detail Pengiriman Pesanan">
                        <NavLink to={`/delivery-orders/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_DELETE]}>
                      <Tooltip title="Hapus Pengataran Pesanan">
                        <IconButton onClick={() => deleteDeliveryOrder(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[200, undefined, 200, 175, 175, 125]} />
            )}
          </Box>
        </CardContent>
      </Paper>

      <DeliveryOrderDownloadReportFormDialog
        open={openDownloadReportDialog}
        dialogTitle="Unduh Laporan"
        dialogSubTitle="Unduh laporan piutang pelanggan berdasarkan tanggal dan pelanggan"
        onCancel={() => setOpenDownloadReportDialog(false)}
        onClose={() => setOpenDownloadReportDialog(false)}
        onSubmit={handleSubmitDownloadReportForm}
      />
    </PageContainer>
  )
}

export default DeliveryOrderListPage
