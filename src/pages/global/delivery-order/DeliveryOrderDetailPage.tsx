import { Divider, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconCheck, IconEdit, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import CardHeader from 'components/CardHeader'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import { PagePermission } from 'constants/pagePermission'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { DeliveryOrderImageModel } from 'models/deliveryOrderImage'
import { DeliveryOrderItemModel } from 'models/deliveryOrderitem'
import { UserModel } from 'models/user'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import DeliveryOrderDetailCard from 'modules/delivery-order/components/DeliveryOrderDetailCard'
import DeliveryOrderDriverFormDialog, {
  DeliveryOrderDriverFormDialogShape,
} from 'modules/delivery-order/components/DeliveryOrderDriverFormDialog'
import DeliveryOrderImageFormDialog, {
  DeliveryOrderImageFormDialogShape,
} from 'modules/delivery-order/components/DeliveryOrderImageFormDialog'
import DeliveryOrderImageTable from 'modules/delivery-order/components/DeliveryOrderImageTable'
import DeliveryOrderItemFormDialog, {
  DeliveryOrderItemFormDialogShape,
} from 'modules/delivery-order/components/DeliveryOrderItemFormDialog'
import DeliveryOrderItemTable from 'modules/delivery-order/components/DeliveryOrderItemTable'
import PermissionControl from 'modules/permission/components/PermissionControl'
import UsersTable from 'modules/user/components/UsersTable'
import { useSnackbar } from 'notistack'
import { FC, useState } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import FormUtil from 'utils/formUtil'
import TypeUtil from 'utils/typeUtil'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import DeliveryOrderReturnFormDialog, {
  DeliveryOrderReturnFormDialogShape,
} from 'modules/delivery-order/components/DeliveryOrderReturnFormDialog'
import DeliveryOrderReturnDetailCard from 'modules/delivery-order/components/DeliveryOrderReturnDetailCard'
import Skeleton from 'components/Skeleton'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengiriman Pesanan',
    href: '/delivery-orders',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const DeliveryOrderDetailPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const { deliveryOrderId } = useParams()
  const [openAddDriverDialog, setOpenAddDriverDialog] = useState<boolean>(false)
  const [openAddItemDialog, setOpenAddItemDialog] = useState<boolean>(false)
  const [openAddImageDialog, setOpenAddImageDialog] = useState<boolean>(false)
  const [openReturnDialog, setOpenReturnDialog] = useState<boolean>(false)

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(deliveryOrderId),
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }],
    queryFn: () => DeliveryOrdersAPI.get(deliveryOrderId!),
    select: (response) => response.data.data.delivery_order,
  })

  const deleteDriver = async (driver: UserModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Driver',
        description: `Apakah kamu yakin ingin menghapus driver "${driver.name}"?`,
      })
      if (!isConfirmed) return

      await DeliveryOrdersAPI.deleteDriver(deliveryOrderId!, driver.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
      snackbar.enqueueSnackbar('Driver telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteItem = async (item: DeliveryOrderItemModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Barang',
        description: `Apakah kamu yakin ingin menghapus barang "${item.product_unit?.product.name}"?`,
      })
      if (!isConfirmed) return

      await DeliveryOrdersAPI.deleteItem(deliveryOrderId!, item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
      snackbar.enqueueSnackbar('Barang telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteImage = async (image: DeliveryOrderImageModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Gambar',
        description: `Apakah kamu yakin ingin menghapus gambar "${image.file?.name}"?`,
      })
      if (!isConfirmed) return

      await DeliveryOrdersAPI.deleteImage(deliveryOrderId!, image.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
      snackbar.enqueueSnackbar('Gambar telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const updateStatusOngoing = async () => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Ubah Status Pengiriman Pesanan',
        description: `Apakah kamu yakin pesanan sudah siap diantar oleh driver?`,
      })
      if (!isConfirmed) return

      await DeliveryOrdersAPI.setStatusOnGoing(deliveryOrderId!)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
      snackbar.enqueueSnackbar('Status pengantaran telah berhasil diperbarui', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleAddDriver = async (values: DeliveryOrderDriverFormDialogShape) => {
    await DeliveryOrdersAPI.addDriver(deliveryOrderId!, {
      driver_user_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.driver_user_id), ''),
    })
    snackbar.enqueueSnackbar('Driver telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
    setOpenAddDriverDialog(false)
  }

  const handleAddItem = async (values: DeliveryOrderItemFormDialogShape) => {
    await DeliveryOrdersAPI.addItem(deliveryOrderId!, {
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.product_id), ''),
      unit_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.unit_id), ''),
      qty: FormUtil.withDefault(values.qty, 0),
    })
    snackbar.enqueueSnackbar('Barang telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
    setOpenAddItemDialog(false)
  }

  const handleAddImage = async (values: DeliveryOrderImageFormDialogShape) => {
    await DeliveryOrdersAPI.addImage(deliveryOrderId!, {
      description: FormUtil.withDefault(values.description, null),
      file_path: FormUtil.withDefault(values.file_path, ''),
    })
    snackbar.enqueueSnackbar('Gambar telah berhasil ditambahkan', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
    setOpenAddImageDialog(false)
  }

  const handleSubmitReturnForm = async (values: DeliveryOrderReturnFormDialogShape) => {
    await DeliveryOrdersAPI.setReturned(deliveryOrderId!, {
      description: FormUtil.withDefault(values.description, ''),
      file_paths: FormUtil.withDefault(values.file_paths, []),
    })
    snackbar.enqueueSnackbar('Status pesanan telah diubah menjadi diretur', { variant: 'success' })
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }] })
    setOpenReturnDialog(false)
  }

  const isPending = data?.status === DeliveryOrderStatusEnum.PENDING
  const isReturned = data?.status === DeliveryOrderStatusEnum.RETURNED

  return (
    <PageContainer metaTitle="Detail Pengiriman Pesanan">
      <PageHeader title="Detail Pengiriman Pesanan" breadcrumbs={pageBreadcrumbs} />

      <Grid container spacing={4}>
        <Grid item xs={12} md={isReturned ? 6 : 12}>
          {data ? (
            <DeliveryOrderDetailCard
              cardTitle="Detail"
              customer={data.customer!.name}
              invoiceNumber={data.invoice_number}
              status={data.status}
              totalPrice={data.total_price}
              deliveryDate={data.date}
              createdAt={data.created_at}
              updatedAt={data.updated_at}
              cardAction={
                <Stack direction="row" gap={1}>
                  {isPending && (
                    <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_MARK_ONGOING]}>
                      <Button
                        variant="contained"
                        color="success"
                        startIcon={<IconCheck size="1.2rem" />}
                        onClick={updateStatusOngoing}
                      >
                        Siap Diantar
                      </Button>
                    </PermissionControl>
                  )}

                  {isPending && (
                    <PermissionControl match={PagePermission.UNIT_UPDATE}>
                      <NavLink to={`/delivery-orders/${data.id}/update`}>
                        <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                          Ubah
                        </Button>
                      </NavLink>
                    </PermissionControl>
                  )}
                </Stack>
              }
            />
          ) : (
            <Skeleton.DetailLineCard cardTitle="Detail" rows={7} />
          )}
        </Grid>

        {isReturned && data.return && (
          <Grid item xs={12} md={6}>
            <DeliveryOrderReturnDetailCard
              cardTitle="Detail Retur"
              createdAt={data.return.created_at}
              description={data.return.description}
              images={data.return.images!.map((image) => image.file!)}
              returnedBy={data.return.created_by?.name ?? ''}
              returnedById={data.return.created_by?.id ?? ''}
            />
          </Grid>
        )}

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Driver"
              action={
                isPending && (
                  <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_ADD_DRIVER]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddDriverDialog(true)}
                    >
                      Tambah Driver
                    </Button>
                  </PermissionControl>
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <UsersTable
                  data={data.drivers ?? []}
                  action={
                    isPending
                      ? (user) => (
                          <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_DELETE_DRIVER]}>
                            <Tooltip title="Hapus Driver">
                              <IconButton onClick={() => deleteDriver(user)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[250, undefined, 150, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Barang"
              action={
                isPending && (
                  <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_ADD_ITEM]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddItemDialog(true)}
                    >
                      Tambah Barang
                    </Button>
                  </PermissionControl>
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <DeliveryOrderItemTable
                  data={data.items ?? []}
                  action={
                    isPending
                      ? (item) => (
                          <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_DELETE_ITEM]}>
                            <Tooltip title="Hapus Barang">
                              <IconButton onClick={() => deleteItem(item)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[undefined, 150, 175, 175, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper>
            <CardHeader
              title="Gambar"
              action={
                isPending && (
                  <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_ADD_IMAGE]}>
                    <Button
                      variant="contained"
                      startIcon={<IconPlus size="1.2rem" />}
                      onClick={() => setOpenAddImageDialog(true)}
                    >
                      Tambah Gambar
                    </Button>
                  </PermissionControl>
                )
              }
            />
            <Divider />
            <CardContent>
              {data ? (
                <DeliveryOrderImageTable
                  data={data.images ?? []}
                  action={
                    isPending
                      ? (image) => (
                          <PermissionControl match={[PermissionEnum.DELIVERY_ORDER_DELETE_IMAGE]}>
                            <Tooltip title="Hapus Gambar">
                              <IconButton onClick={() => deleteImage(image)}>
                                <IconTrash size="1.2rem" />
                              </IconButton>
                            </Tooltip>
                          </PermissionControl>
                        )
                      : undefined
                  }
                />
              ) : (
                <Skeleton.Table columns={[350, undefined, 175, 125]} />
              )}
            </CardContent>
          </Paper>
        </Grid>
      </Grid>

      <DeliveryOrderDriverFormDialog
        open={openAddDriverDialog}
        dialogTitle="Tambah Driver"
        dialogSubTitle="Untuk menambahkan driver, silahkan pilih user lalu tekan tombol kirim."
        deliveryOrderId={deliveryOrderId!}
        onCancel={() => setOpenAddDriverDialog(false)}
        onClose={() => setOpenAddDriverDialog(false)}
        onSubmit={handleAddDriver}
      />

      <DeliveryOrderItemFormDialog
        open={openAddItemDialog}
        dialogTitle="Tambah Barang"
        onCancel={() => setOpenAddItemDialog(false)}
        onClose={() => setOpenAddItemDialog(false)}
        onSubmit={handleAddItem}
      />

      <DeliveryOrderImageFormDialog
        open={openAddImageDialog}
        dialogTitle="Tambah Gambar"
        dialogSubTitle="Untuk menambahkan barang, silahkan pilih gambar dan isi deskripsi (opsional) lalu tekan tombol kirim."
        onCancel={() => setOpenAddImageDialog(false)}
        onClose={() => setOpenAddImageDialog(false)}
        onSubmit={handleAddImage}
      />

      <DeliveryOrderReturnFormDialog
        open={openReturnDialog}
        dialogTitle="Retur Pesanan"
        dialogSubTitle="Untuk meretur pesanan, silahkan masukkan alasan dari perereturan pesanan dan masukkan bukti dari returan pesanan."
        onCancel={() => setOpenReturnDialog(false)}
        onClose={() => setOpenReturnDialog(false)}
        onSubmit={handleSubmitReturnForm}
      />
    </PageContainer>
  )
}

export default DeliveryOrderDetailPage
