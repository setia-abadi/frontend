import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import dayjs from 'dayjs'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import DeliveryOrderFormCard, {
  DeliveryOrderFormCardShape,
} from 'modules/delivery-order/components/DeliveryOrderFormCard'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

type Props = {}

const DeliveryOrderUpdatePage: FC<Props> = () => {
  const navigate = useNavigate()
  const snackbar = useSnackbar()
  const { deliveryOrderId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.DELIVERY_ORDER_GET, { deliveryOrderId }],
    queryFn: () => DeliveryOrdersAPI.get(deliveryOrderId!),
    select: (response) => response.data.data.delivery_order,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: DeliveryOrderFormCardShape) => {
    const response = await DeliveryOrdersAPI.update(deliveryOrderId!, {
      customer_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_id), ''),
      date: FormUtil.withDefault(FormUtil.getDayjsValue(values.date), ''),
    })

    if (response.data.data.delivery_order) {
      snackbar.enqueueSnackbar('Pengiriman pesanan telah berhasil diperbarui', { variant: 'success' })
      return navigate(`/delivery-orders/${response.data.data.delivery_order.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Pengiriman Pesanan">
      <PageHeader
        title="Ubah Pengiriman Pesanan"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Pengiriman Pesanan',
            href: '/delivery-orders',
          },
          {
            title: 'Detail',
            href: `/delivery-orders/${deliveryOrderId}`,
          },
          {
            title: 'Ubah',
          },
        ]}
      />

      <Box mt={2}>
        {data ? (
          <DeliveryOrderFormCard
            cardTitle="Ubah"
            initialValues={{
              customer_id: data.customer
                ? {
                    label: data.customer.name,
                    value: data.customer.id,
                  }
                : null,
              date: dayjs(data.date),
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default DeliveryOrderUpdatePage
