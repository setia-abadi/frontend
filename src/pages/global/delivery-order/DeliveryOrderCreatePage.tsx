import { Box } from '@mui/material'
import DeliveryOrdersAPI from 'apis/deliveryOrdersAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import DeliveryOrderFormCard, {
  DeliveryOrderFormCardShape,
} from 'modules/delivery-order/components/DeliveryOrderFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import DateUtil from 'utils/dateUtil'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Pengiriman Pesanan',
    href: '/delivery-orders',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const DeliveryOrderCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: DeliveryOrderFormCardShape) => {
    const response = await DeliveryOrdersAPI.create({
      customer_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.customer_id), ''),
      date: values.date ? DateUtil(values.date).toISOString({ withTime: false }) : '',
    })

    if (response.data.data.delivery_order) {
      return navigate(`/delivery-orders/${response.data.data.delivery_order.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Pengiriman Pesanan">
      <PageHeader title="Tambah Pengiriman Pesanan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <DeliveryOrderFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default DeliveryOrderCreatePage
