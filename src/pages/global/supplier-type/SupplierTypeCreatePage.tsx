import { Box } from '@mui/material'
import SupplierTypesAPI from 'apis/supplierTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import SupplierTypeFormCard, { SupplierTypeFormCardShape } from 'modules/supplier-type/components/SupplierTypeFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pemasok',
    href: '/supplier-types',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const SupplierTypeCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: SupplierTypeFormCardShape) => {
    const response = await SupplierTypesAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.supplier_type) {
      return navigate(`/supplier-types/${response.data.data.supplier_type.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Jenis Pemasok">
      <PageHeader title="Tambah Jenis Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <SupplierTypeFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default SupplierTypeCreatePage
