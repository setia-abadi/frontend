import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import SupplierTypesAPI from 'apis/supplierTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PermissionControl from 'modules/permission/components/PermissionControl'
import SupplierTypeDetailCard from 'modules/supplier-type/components/SupplierTypeDetailCard'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pemasok',
    href: '/supplier-types',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const SupplierTypeDetailPage: FC<Props> = () => {
  const { supplierTypeId } = useParams()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(supplierTypeId),
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { supplierTypeId }],
    queryFn: () => SupplierTypesAPI.get(supplierTypeId!),
    select: (response) => response.data.data.supplier_type,
  })

  return (
    <PageContainer metaTitle="Detail Jenis Pemasok">
      <PageHeader title="Detail Jenis Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <SupplierTypeDetailCard
            cardTitle="Detail"
            name={data.name}
            description={data.description}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={PagePermission.SUPPLIER_TYPE_UPDATE}>
                <NavLink to={`/supplier-types/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={4} />
        )}
      </Box>
    </PageContainer>
  )
}

export default SupplierTypeDetailPage
