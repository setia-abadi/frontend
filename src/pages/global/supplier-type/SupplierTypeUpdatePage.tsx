import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import SupplierTypesAPI from 'apis/supplierTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import SupplierTypeFormCard, { SupplierTypeFormCardShape } from 'modules/supplier-type/components/SupplierTypeFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pemasok',
    href: '/supplier-types',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const SupplierTypeUpdatePage: FC<Props> = () => {
  const { supplierTypeId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { supplierTypeId }],
    queryFn: () => SupplierTypesAPI.get(supplierTypeId!),
    select: (response) => response.data.data.supplier_type,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: SupplierTypeFormCardShape) => {
    const response = await SupplierTypesAPI.update(supplierTypeId!, {
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.supplier_type) {
      return navigate(`/supplier-types/${response.data.data.supplier_type.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Jenis Pemasok">
      <PageHeader title="Ubah Jenis Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <SupplierTypeFormCard
            cardTitle="Ubah"
            initialValues={{
              name: data.name,
              description: data.description,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default SupplierTypeUpdatePage
