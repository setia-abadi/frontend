import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import SupplierTypesAPI, { PayloadFilterSupplierType } from 'apis/supplierTypesAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { SupplierTypeModel } from 'models/supplierType'
import PermissionControl from 'modules/permission/components/PermissionControl'
import SupplierTypeTable from 'modules/supplier-type/components/SupplierTypeTable'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import { useSnackbar } from 'notistack'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Jenis Pemasok',
    href: '/supplier-types',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)

const SupplierTypeListPage: FC<Props> = () => {
  const confirmation = useConfirmation()
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const payload: PayloadFilterSupplierType = {
    page,
    phrase,
    limit,
    sorts,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_FILTER, payload],
    queryFn: () => SupplierTypesAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteSupplierType = async (item: SupplierTypeModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Jenis Pemasok',
        description: `Apakah kamu yakin ingin menghapus jenis pemasok "${item.name}"?`,
      })
      if (!isConfirmed) return
      await SupplierTypesAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.SUPPLIER_TYPE_FILTER, payload] })
      snackbar.enqueueSnackbar('Jenis pemasok telah berhasil dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Daftar Jenis Pemasok">
      <PageHeader title="Daftar Jenis Pemasok" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <SearchBar initialValue={phrase} placeholder="Cari Jenis Pemasok" onChange={setPhrase} />
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.SUPPLIER_TYPE_CREATE}>
                <NavLink to="/supplier-types/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Jenis Pemasok
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <SupplierTypeTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.SUPPLIER_TYPE_DETAIL}>
                      <Tooltip title="Detail Jenis Pemasok">
                        <NavLink to={`/supplier-types/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.SUPPLIER_TYPE_DELETE]}>
                      <Tooltip title="Hapus Jenis Pemasok">
                        <IconButton onClick={() => deleteSupplierType?.(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[300, undefined, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default SupplierTypeListPage
