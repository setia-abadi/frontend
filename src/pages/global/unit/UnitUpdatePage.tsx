import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import UnitsAPI from 'apis/unitsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import UnitFormCard, { UnitFormCardShape } from 'modules/unit/components/UnitFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Satuan',
    href: '/units',
  },
  {
    title: 'Ubah',
  },
]

type Props = {}

const UnitUpdatePage: FC<Props> = () => {
  const { unitId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.SUPPLIER_TYPE_GET, { unitId }],
    queryFn: () => UnitsAPI.get(unitId!),
    select: (response) => response.data.data.unit,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: UnitFormCardShape) => {
    const response = await UnitsAPI.update(unitId!, {
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.unit) {
      return navigate(`/units/${response.data.data.unit.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Satuan">
      <PageHeader title="Ubah Satuan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <UnitFormCard
            cardTitle="Ubah"
            initialValues={{
              name: data.name,
              description: data.description,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={2} />
        )}
      </Box>
    </PageContainer>
  )
}

export default UnitUpdatePage
