import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import UnitsAPI, { PayloadFilterUnits } from 'apis/unitsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import { SupplierTypeModel } from 'models/supplierType'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import UnitTable from 'modules/unit/components/UnitTable'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Satuan',
    href: '/units',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)

const UnitListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)

  const payload: PayloadFilterUnits = {
    page,
    phrase,
    limit,
    sorts,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.UNIT_FILTER, payload],
    queryFn: () => UnitsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteUnit = async (item: SupplierTypeModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Satuan',
        description: `Apakah anda yakin ingin menghapus satuan ${item.name}`,
      })
      if (!isConfirmed) return

      await UnitsAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.UNIT_FILTER, payload] })
      snackbar.enqueueSnackbar('Satuan telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  return (
    <PageContainer metaTitle="Daftar Satuan">
      <PageHeader title="Daftar Satuan" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <SearchBar initialValue={phrase} placeholder="Cari Satuan" onChange={setPhrase} />
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.UNIT_CREATE}>
                <NavLink to="/units/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Satuan
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <UnitTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.UNIT_DETAIL}>
                      <Tooltip title="Detail Satuan">
                        <NavLink to={`/units/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.UNIT_DELETE]}>
                      <Tooltip title="Hapus Satuan">
                        <IconButton onClick={() => deleteUnit(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[300, undefined, 150]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default UnitListPage
