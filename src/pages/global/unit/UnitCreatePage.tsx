import { Box } from '@mui/material'
import UnitsAPI from 'apis/unitsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import UnitFormCard, { UnitFormCardShape } from 'modules/unit/components/UnitFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Satuan',
    href: '/units',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const UnitCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: UnitFormCardShape) => {
    const response = await UnitsAPI.create({
      name: FormUtil.withDefault(values.name, ''),
      description: FormUtil.withDefault(values.description, null),
    })

    if (response.data.data.unit) {
      return navigate(`/units/${response.data.data.unit.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Satuan">
      <PageHeader title="Tambah Satuan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <UnitFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default UnitCreatePage
