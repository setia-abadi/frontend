import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import UnitsAPI from 'apis/unitsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PermissionControl from 'modules/permission/components/PermissionControl'
import SupplierTypeDetailCard from 'modules/supplier-type/components/SupplierTypeDetailCard'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'
import TypeUtil from 'utils/typeUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Satuan',
    href: '/units',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const UnitDetailPage: FC<Props> = () => {
  const { unitId } = useParams()

  const { data } = useQuery({
    enabled: TypeUtil.isDefined(unitId),
    queryKey: [QueryKeyEnum.UNIT_GET, { unitId }],
    queryFn: () => UnitsAPI.get(unitId!),
    select: (response) => response.data.data.unit,
  })

  return (
    <PageContainer metaTitle="Detail Satuan">
      <PageHeader title="Detail Satuan" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <SupplierTypeDetailCard
            cardTitle="Detail"
            name={data.name}
            description={data.description}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={PagePermission.UNIT_UPDATE}>
                <NavLink to={`/units/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={4} />
        )}
      </Box>
    </PageContainer>
  )
}

export default UnitDetailPage
