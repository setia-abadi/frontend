import { Box } from '@mui/material'
import ProductDiscountsAPI from 'apis/productDiscountsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import ProductDiscountFormCard, { ProductDiscountFormCardShape } from 'modules/product-discount/components/ProductDiscountFormCard'
import { FC } from 'react'
import { useNavigate } from 'react-router'
import FormUtil from 'utils/formUtil'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Diskon Produk',
    href: '/product-discounts',
  },
  {
    title: 'Tambah',
  },
]

type Props = {}

const ProductDiscountCreatePage: FC<Props> = () => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductDiscountFormCardShape) => {
    const response = await ProductDiscountsAPI.create({
      discount_amount: FormUtil.withDefault(values.discount_amount, null),
      discount_percentage: FormUtil.withDefault(values.discount_percentage, null),
      is_active: FormUtil.withDefault(values.is_active, false),
      minimum_qty: FormUtil.withDefault(values.minimum_qty, 0),
      product_id: FormUtil.withDefault(FormUtil.getSelectOptionValue(values.product_id), ''),
    })

    if (response.data.data.product_discount) {
      return navigate(`/product-discounts/${response.data.data.product_discount.id}`)
    }

    goBack()
  }

  return (
    <PageContainer metaTitle="Tambah Diskon Produk">
      <PageHeader title="Tambah Diskon Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        <ProductDiscountFormCard cardTitle="Tambah" onSubmit={handleSubmit} onCancel={goBack} />
      </Box>
    </PageContainer>
  )
}

export default ProductDiscountCreatePage
