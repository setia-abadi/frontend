import { Box } from '@mui/material'
import { useQuery } from '@tanstack/react-query'
import ProductDiscountsAPI from 'apis/productDiscountsAPI'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ProductDiscountFormCard, {
  ProductDiscountFormCardShape,
} from 'modules/product-discount/components/ProductDiscountFormCard'
import { FC } from 'react'
import { useNavigate, useParams } from 'react-router'
import FormUtil from 'utils/formUtil'

type Props = {}

const ProductDiscountUpdatePage: FC<Props> = () => {
  const { productDiscountId } = useParams()
  const navigate = useNavigate()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_DISCOUNT_GET, { productDiscountId }],
    queryFn: () => ProductDiscountsAPI.get(productDiscountId!),
    select: (response) => response.data.data.product_discount,
  })

  const goBack = () => {
    navigate(-1)
  }

  const handleSubmit = async (values: ProductDiscountFormCardShape) => {
    const response = await ProductDiscountsAPI.update(productDiscountId!, {
      discount_amount: FormUtil.withDefault(values.discount_amount, null),
      discount_percentage: FormUtil.withDefault(values.discount_percentage, null),
      is_active: FormUtil.withDefault(values.is_active, false),
      minimum_qty: FormUtil.withDefault(values.minimum_qty, 0),
    })

    if (response.data.data.product_discount) {
      return navigate(`/product-discounts/${response.data.data.product_discount.id}`)
    }

    return goBack()
  }

  return (
    <PageContainer metaTitle="Ubah Diskon Produk">
      <PageHeader
        title="Ubah Diskon Produk"
        breadcrumbs={[
          {
            title: 'Beranda',
          },
          {
            title: 'Diskon Produk',
            href: '/product-discounts',
          },
          {
            title: 'Detail',
            href: `/product-discounts/${productDiscountId}`,
          },
          {
            title: 'Ubah',
          },
        ]}
      />

      <Box mt={2}>
        {data ? (
          <ProductDiscountFormCard
            cardTitle="Ubah"
            disabledFields={{
              product_id: true,
            }}
            initialValues={{
              discount_amount: data.discount_amount,
              discount_percentage: data.discount_percentage,
              is_active: data.is_active,
              minimum_qty: data.minimum_qty,
              product_id: data.product
                ? {
                    label: data.product.name,
                    value: data.product.id,
                  }
                : null,
            }}
            onSubmit={handleSubmit}
            onCancel={goBack}
          />
        ) : (
          <Skeleton.FormCard cardTitle="Ubah" rows={4} />
        )}
      </Box>
    </PageContainer>
  )
}

export default ProductDiscountUpdatePage
