import { Box } from '@mui/material'
import { IconEdit } from '@tabler/icons'
import { useQuery } from '@tanstack/react-query'
import ProductDiscountsAPI from 'apis/productDiscountsAPI'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Skeleton from 'components/Skeleton'
import { PagePermission } from 'constants/pagePermission'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import PermissionControl from 'modules/permission/components/PermissionControl'
import ProductDiscountDetailCard from 'modules/product-discount/components/ProductDiscountDetailCard'
import { FC } from 'react'
import { useParams } from 'react-router'
import { NavLink } from 'react-router-dom'

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Diskon Produk',
    href: '/product-discounts',
  },
  {
    title: 'Detail',
  },
]

type Props = {}

const ProductDiscountDetailPage: FC<Props> = () => {
  const { productDiscountId } = useParams()

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_DISCOUNT_GET, { productDiscountId }],
    queryFn: () => ProductDiscountsAPI.get(productDiscountId!),
    select: (response) => response.data.data.product_discount,
  })

  return (
    <PageContainer metaTitle="Detail Diskon Produk">
      <PageHeader title="Detail Diskon Produk" breadcrumbs={pageBreadcrumbs} />

      <Box mt={2}>
        {data ? (
          <ProductDiscountDetailCard
            cardTitle="Detail"
            discountAmount={data.discount_amount}
            discountPercentage={data.discount_percentage}
            isActive={data.is_active}
            minimumQty={data.minimum_qty}
            productName={data.product?.name ?? ''}
            createdAt={data.created_at}
            updatedAt={data.updated_at}
            cardAction={
              <PermissionControl match={PagePermission.PRODUCT_DISCOUNT_UPDATE}>
                <NavLink to={`/product-discounts/${data.id}/update`}>
                  <Button variant="contained" startIcon={<IconEdit size="1.2rem" />}>
                    Ubah
                  </Button>
                </NavLink>
              </PermissionControl>
            }
          />
        ) : (
          <Skeleton.DetailLineCard cardTitle="Detail" rows={5} />
        )}
      </Box>
    </PageContainer>
  )
}

export default ProductDiscountDetailPage
