import { Box, Grid, IconButton, Stack, Tooltip } from '@mui/material'
import { IconEye, IconPlus, IconTrash } from '@tabler/icons'
import { keepPreviousData, useQuery, useQueryClient } from '@tanstack/react-query'
import APIUtil from 'utils/APIUtil'
import { BreadcrumbItem } from 'components/Breadcrumbs'
import Button from 'components/Button'
import CardContent from 'components/CardContent'
import PageContainer from 'components/PageContainer'
import PageHeader from 'components/PageHeader'
import Paper from 'components/Paper'
import SearchBar from 'components/SearchBar'
import { PagePermission } from 'constants/pagePermission'
import { LIMIT_PER_PAGE } from 'constants/pagination'
import { PermissionEnum } from 'enums/permissionEnum'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import { SortsParam } from 'libs/useQueryParam'
import useConfirmation from 'modules/confirmation/hooks/useConfirmation'
import PermissionControl from 'modules/permission/components/PermissionControl'
import { useSnackbar } from 'notistack'
import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { BooleanParam, NumberParam, StringParam, useQueryParam, withDefault } from 'use-query-params'
import ProductDiscountTable from 'modules/product-discount/components/ProductDiscountTable'
import ProductDiscountsAPI, { PayloadProductDiscountsFilter } from 'apis/productDiscountsAPI'
import { ProductDiscountModel } from 'models/productDiscount'
import ProductDiscountFilterFormPopover, {
  ProductDiscountFilterFormPopoverShape,
} from 'modules/product-discount/components/ProductDiscountFilterFormPopover'
import Skeleton from 'components/Skeleton'

type Props = unknown

const pageBreadcrumbs: BreadcrumbItem[] = [
  {
    title: 'Beranda',
  },
  {
    title: 'Diskon Produk',
    href: '/product-discounts',
  },
]

const PageParam = withDefault(NumberParam, 1)
const LimitParam = withDefault(NumberParam, LIMIT_PER_PAGE)
const PhraseParam = withDefault(StringParam, null)
const NullableBooleanParam = withDefault(BooleanParam, null)

const ProductDiscountListPage: FC<Props> = () => {
  const queryClient = useQueryClient()
  const snackbar = useSnackbar()
  const confirmation = useConfirmation()
  const [limit, setLimit] = useQueryParam('limit', LimitParam)
  const [page, setPage] = useQueryParam('page', PageParam)
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [sorts, setSorts] = useQueryParam('sorts', SortsParam)
  const [isActive, setIsActive] = useQueryParam('is-active', NullableBooleanParam)

  const payload: PayloadProductDiscountsFilter = {
    page,
    phrase,
    limit,
    sorts,
    is_active: isActive,
    product_id: null,
  }

  const { data } = useQuery({
    queryKey: [QueryKeyEnum.PRODUCT_DISCOUNT_FILTER, payload],
    placeholderData: keepPreviousData,
    queryFn: () => ProductDiscountsAPI.filter(payload),
    select: (response) => response.data.data,
  })

  const handlePaginationChange = (newPage: number, newLimit: number) => {
    setPage(newPage)
    setLimit(newLimit)
  }

  const deleteDiscount = async (item: ProductDiscountModel) => {
    try {
      const { isConfirmed } = await confirmation.ask({
        title: 'Hapus Diskon Produk',
        description: `Apakah anda yakin ingin menghapus diskon produk "${item.product?.name}"`,
      })
      if (!isConfirmed) return
      await ProductDiscountsAPI.delete(item.id)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.PRODUCT_DISCOUNT_FILTER, payload] })
      snackbar.enqueueSnackbar('Diskon produk telah dihapus', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const handleResetFilter = () => {
    setIsActive(undefined)
  }

  const handleApplyFilter = (values: ProductDiscountFilterFormPopoverShape) => {
    setIsActive(values.is_active)
  }

  return (
    <PageContainer metaTitle="Daftar Diskon Produk">
      <PageHeader title="Daftar Diskon Produk" breadcrumbs={pageBreadcrumbs} />

      <Paper>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md>
              <Stack direction="row" gap={1}>
                <SearchBar initialValue={phrase} placeholder="Cari Produk" onChange={setPhrase} />
                <ProductDiscountFilterFormPopover
                  initialValues={{ is_active: isActive }}
                  onApply={handleApplyFilter}
                  onReset={handleResetFilter}
                />
              </Stack>
            </Grid>

            <Grid item xs={12} md={'auto'}>
              <PermissionControl match={PagePermission.PRODUCT_DISCOUNT_CREATE}>
                <NavLink to="/product-discounts/create">
                  <Button startIcon={<IconPlus size="1rem" />} variant="contained">
                    Tambah Diskon
                  </Button>
                </NavLink>
              </PermissionControl>
            </Grid>
          </Grid>

          <Box mt={2}>
            {data ? (
              <ProductDiscountTable
                data={data.nodes}
                sorts={sorts}
                pagination={{
                  page,
                  rowsPerPage: limit,
                  total: data.total,
                  onPaginationChange: handlePaginationChange,
                }}
                onSortChange={setSorts}
                action={(item) => (
                  <Stack direction="row" spacing={1}>
                    <PermissionControl match={PagePermission.UNIT_DETAIL}>
                      <Tooltip title="Detail Diskon Produk">
                        <NavLink to={`/product-discounts/${item.id}`}>
                          <IconButton>
                            <IconEye size="1.2rem" />
                          </IconButton>
                        </NavLink>
                      </Tooltip>
                    </PermissionControl>

                    <PermissionControl match={[PermissionEnum.PRODUCT_DISCOUNT_DELETE]}>
                      <Tooltip title="Hapus Diskon Produk">
                        <IconButton onClick={() => deleteDiscount(item)}>
                          <IconTrash size="1.2rem" />
                        </IconButton>
                      </Tooltip>
                    </PermissionControl>
                  </Stack>
                )}
              />
            ) : (
              <Skeleton.Table columns={[250, 175, 200, 175, 100]} />
            )}
          </Box>
        </CardContent>
      </Paper>
    </PageContainer>
  )
}

export default ProductDiscountListPage
