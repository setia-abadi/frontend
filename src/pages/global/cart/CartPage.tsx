import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import { Theme, styled } from '@mui/material/styles'
import PageContainer from 'components/PageContainer'
import Paper from 'components/Paper'
import CartDrawer from 'modules/cart/components/CartDrawer'
import { FC, Fragment, useMemo, useState } from 'react'
import SearchBar from 'components/SearchBar'
import { useInfiniteQuery, useQuery, useQueryClient } from '@tanstack/react-query'
import { QueryKeyEnum } from 'enums/queryKeyEnum'
import ProductsAPI from 'apis/productsAPI'
import { StringParam, useQueryParam, withDefault } from 'use-query-params'
import ProductCard from 'modules/product/components/ProductCard'
import CartsAPI from 'apis/cartsAPI'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Button from 'components/Button'
import Scrollbar from 'components/Scrollbar'
import { ID } from 'models/base'
import { useSnackbar } from 'notistack'
import APIUtil from 'utils/APIUtil'
import { Fab, Stack, useMediaQuery } from '@mui/material'
import FormattedCurrency from 'components/FormattedCurrency'
import CartItem from 'modules/cart/components/CartItem'
import { IconShoppingBag } from '@tabler/icons'
import TransactionCheckoutFormDialog, {
  TransactionCheckoutFormDialogShape,
} from 'modules/transaction/components/TransactionCheckoutFormDialog'
import TransactionsAPI from 'apis/transactionsAPI'
import FormUtil from 'utils/formUtil'
import { TransactionPaymentTypeEnum } from 'enums/transactionPaymentTypeEnum'
import { PrinterDataModel } from 'models/printerData'
import PrintersAPI from 'apis/printersAPI'
import InfiniteScroll from 'react-infinite-scroll-component'

type Props = {}

const WrapperStyled = styled(Paper)(({ theme }) => ({
  marginTop: theme.spacing(1),
  flex: 1,
  display: 'flex',
}))

const CartFloatingButton = styled(Fab)(({ theme }) => ({
  position: 'fixed',
  bottom: theme.spacing(4),
  right: theme.spacing(4),
}))

const PhraseParam = withDefault(StringParam, null)

const CartPage: FC<Props> = () => {
  const snackbar = useSnackbar()
  const queryClient = useQueryClient()
  const lgUp = useMediaQuery<Theme>((theme) => theme.breakpoints.up('lg'))
  const [phrase, setPhrase] = useQueryParam('phrase', PhraseParam)
  const [openCartDrawer, setOpenCartDrawer] = useState<boolean>(false)
  const [openCheckoutDialog, setOpenCheckoutDialog] = useState<boolean>(false)

  const { data: cart } = useQuery({
    queryKey: [QueryKeyEnum.CART_GET_ACTIVE],
    queryFn: () => CartsAPI.getActive(),
    select: (response) => response.data.data.cart,
  })

  const {
    data: products,
    hasNextPage: hasMoreProducts,
    fetchNextPage: fetchNextProducts,
  } = useInfiniteQuery({
    queryKey: [QueryKeyEnum.PRODUCT_FILTER_CART, { phrase }],
    queryFn: ({ pageParam }) =>
      ProductsAPI.optionsForCartAddItemForm({
        phrase,
        limit: 6,
        page: pageParam,
      }),
    getNextPageParam: (response) => {
      const { limit, page, total } = response.data.data
      if (page * limit < total) {
        return page + 1
      }
    },
    initialPageParam: 1,
  })

  const productLength = products?.pages.reduce((prev, page) => prev + page.data.data.nodes.length, 0)

  const cartItemsQuantity = useMemo<Record<string, number>>(() => {
    if (cart?.items) {
      return cart.items.reduce((prev, next) => {
        prev[next.product_unit.product_id] = next.qty
        return prev
      }, Object())
    }
    return {}
  }, [cart])

  const addItem = async (productId: ID) => {
    try {
      await CartsAPI.addItem({ product_id: productId, qty: 1 })
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CART_GET_ACTIVE] })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const updateItem = async (cartItemId: ID, qty: number) => {
    try {
      await CartsAPI.updateItem(cartItemId, { qty })
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CART_GET_ACTIVE] })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const deleteItem = async (cartItemId: ID) => {
    try {
      await CartsAPI.deleteItem(cartItemId)
      queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CART_GET_ACTIVE] })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar(String(err), { variant: 'error' })
      }
    }
  }

  const printReceipt = async (printerData: PrinterDataModel) => {
    try {
      await PrintersAPI.print({ printer_data: printerData })
      snackbar.enqueueSnackbar('Kuitansi telah berhasil di cetak', { variant: 'success' })
    } catch (err) {
      if (APIUtil.isAPIError(err) && err.response?.data.message) {
        snackbar.enqueueSnackbar(err.response.data.message, { variant: 'error' })
      } else {
        snackbar.enqueueSnackbar('Terjadi kesalahan saat mencetak kuitansi', { variant: 'error' })
      }
    }
  }

  const handleSubmitCheckoutForm = async (values: TransactionCheckoutFormDialogShape) => {
    const response = await TransactionsAPI.checkout({
      cash_paid: FormUtil.withDefault(values.cash_paid, null),
      payment_type: FormUtil.withDefault(values.payment_type, TransactionPaymentTypeEnum.CASH),
      reference_number: FormUtil.withDefault(values.reference_number, null),
    })
    printReceipt(response.data.data.printer_data)
    setOpenCheckoutDialog(false)
    queryClient.invalidateQueries({ queryKey: [QueryKeyEnum.CART_GET_ACTIVE] })
    snackbar.enqueueSnackbar('Transaksi telah berhasil dilakukan', { variant: 'success' })
  }

  return (
    <PageContainer metaTitle="Keranjang">
      <WrapperStyled>
        <Box flexGrow={1} display="flex" flexDirection="column">
          <Box p={3} display="flex" justifyContent="space-between">
            <SearchBar placeholder="Cari Produk" initialValue={phrase} onChange={setPhrase} />
          </Box>

          <Box flexGrow={1} height={200} overflow="auto" id="product-container">
            <InfiniteScroll
              dataLength={productLength ?? 0}
              hasMore={hasMoreProducts}
              next={fetchNextProducts}
              loader={<Box textAlign="center">Memuat ...</Box>}
              scrollableTarget="product-container"
            >
              <Grid container pb={3} px={3} spacing={2}>
                {products?.pages.map((page) =>
                  page.data.data.nodes.map((product) => {
                    const qty = cartItemsQuantity[product.id] ?? 0
                    return (
                      <Grid item xs={6} sm={4} md={3} lg={4} key={product.id}>
                        <ProductCard
                          addedCartCount={qty}
                          imageSrc={product.image_file?.link}
                          name={product.name}
                          price={product.price}
                          stock={product.stock.qty}
                          onClick={() => addItem(product.id)}
                        />
                      </Grid>
                    )
                  })
                )}
              </Grid>
            </InfiniteScroll>
          </Box>
        </Box>

        <CartDrawer open={openCartDrawer} onClose={() => setOpenCartDrawer(false)}>
          <Box p={3} display="flex" flexDirection="column">
            <Typography variant="h5">Keranjang</Typography>
          </Box>
          <Divider />

          <Box flexGrow={1}>
            <Scrollbar>
              {cart?.items?.map((item) => (
                <Fragment key={item.id}>
                  <CartItem
                    imageSrc={item.product_unit.product.image_file.link}
                    name={item.product_unit.product.name}
                    price={item.product_unit.product.price * item.qty}
                    qty={item.qty}
                    stock={item.product_unit.product.stock.qty}
                    onDeleteBtnClick={() => deleteItem(item.id)}
                    onQtyChange={(qty) => updateItem(item.id, qty)}
                  />
                  <Divider />
                </Fragment>
              ))}
            </Scrollbar>
          </Box>
          <Divider />

          {cart && (
            <>
              <Box px={2} py={1.5}>
                <Stack gap={1}>
                  <Box display="flex" justifyContent="space-between">
                    <Typography variant="body1">Subtotal</Typography>
                    <Typography variant="body1">
                      <FormattedCurrency value={cart.subtotal} />
                    </Typography>
                  </Box>

                  {cart.total_discount > 0 && (
                    <Box display="flex" justifyContent="space-between">
                      <Typography variant="body1">Diskon</Typography>
                      <Typography variant="body1">
                        <FormattedCurrency value={cart.total_discount} />
                      </Typography>
                    </Box>
                  )}
                </Stack>
              </Box>
              <Divider />

              <Box p={2}>
                <Box display="flex" justifyContent="space-between">
                  <Typography variant="h6">Total Bayar</Typography>
                  <Typography variant="h6">
                    <FormattedCurrency value={cart.grand_total} />
                  </Typography>
                </Box>
              </Box>
              <Divider />

              <Box p={2}>
                <Button variant="contained" size="large" fullWidth onClick={() => setOpenCheckoutDialog(true)}>
                  Bayar
                </Button>
              </Box>
            </>
          )}
        </CartDrawer>
      </WrapperStyled>

      {!lgUp && (
        <CartFloatingButton color="primary" onClick={() => setOpenCartDrawer(true)}>
          <IconShoppingBag />
        </CartFloatingButton>
      )}

      {cart && (
        <TransactionCheckoutFormDialog
          open={openCheckoutDialog}
          totalAmount={cart.grand_total}
          dialogTitle="Pembayaran"
          dialogSubTitle="Untuk melakukan pembayaran, silahkan pilih jenis pembayaran lalu isi tunai yang di berikan pelanggan atau nomor referensi dari mesin EDC."
          onCancel={() => setOpenCheckoutDialog(false)}
          onClose={() => setOpenCheckoutDialog(false)}
          onSubmit={handleSubmitCheckoutForm}
        />
      )}
    </PageContainer>
  )
}

export default CartPage
