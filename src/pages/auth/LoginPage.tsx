import React from 'react'
import { Box, Typography } from '@mui/material'
import PageContainer from 'components/PageContainer'
import UsernameLoginForm, { UsernameLoginFormShape } from 'modules/auth/components/UsernameLoginForm/UsernameLoginForm'
import AuthAPI from 'apis/authAPI'
import { useAuth } from 'modules/auth/hooks/useAuth'

const LoginPage: React.FC = () => {
  const { setToken } = useAuth()

  const handleFormSubmit = async (values: UsernameLoginFormShape) => {
    const response = await AuthAPI.login({
      password: values.password ?? '',
      username: values.username ?? '',
    }).fn()

    if (response.data.data) {
      setToken(response.data.data.access_token)
    }
  }

  return (
    <PageContainer metaTitle="Login" metaDescription="this is Login page">
      <Box sx={{ padding: 4, maxWidth: 450, width: '100%' }}>
        <Typography variant="h2" sx={{ mb: 1, fontWeight: 700 }}>
          Selamat Datang
        </Typography>
        <Typography variant="subtitle1" color="textSecondary" mb={1}>
          Silahkan masuk untuk lanjut ke dashboard admin toko Setia Abadi.
        </Typography>

        <Box mt={3}>
          <UsernameLoginForm onSubmit={handleFormSubmit} />
        </Box>
      </Box>
    </PageContainer>
  )
}

export default LoginPage
