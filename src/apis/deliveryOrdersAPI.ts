import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { DeliveryOrderModel } from 'models/deliveryOrder'
import { DeliveryOrderStatusEnum } from 'enums/deliveryOrderStatusEnum'
import FormUtil from 'utils/formUtil'
import { AxiosRequestConfig } from 'axios'

type ResponseDeliveryOrder = Response<{ delivery_order: DeliveryOrderModel }>
type ResponseDeliveryOrdersWithFilter = Response<WithFilter<DeliveryOrderModel>>

type PayloadDeliveryOrdersFilter = RequestWithFilter<{
  status: DeliveryOrderStatusEnum | null
  customer_id: ID | null
}>

type PayloadDeliveryOrdersDownloadReport = {
  start_date: string
  end_date: string
}

type PayloadDeliveryOrderCreate = {
  customer_id: ID
  date: string
}

type PayloadDeliveryOrderUpdate = {
  customer_id: ID
  date: string
}

type PayloadDeliveryOrderUploadImage = {
  file: File
}

type PayloadDeliveryOrderAddDriver = {
  driver_user_id: ID
}

type PayloadDeliveryOrderAddImage = {
  file_path: string
  description: string | null
}

type PayloadDeliveryOrderReturned = {
  file_paths: string[]
  description: string
}

type PayloadDeliveryOrderAddItem = {
  product_id: ID
  qty: number
  unit_id: ID
}

const DELIVERY_ORDERS_URL = '/delivery-orders'

const DeliveryOrdersAPI = {
  filter: (payload: PayloadDeliveryOrdersFilter): Promise<APIResponse<ResponseDeliveryOrdersWithFilter>> => {
    return APIClient.post(`${DELIVERY_ORDERS_URL}/filter`, payload)
  },

  downloadReport: (payload: PayloadDeliveryOrdersDownloadReport): Promise<APIResponse<Blob>> => {
    return APIClient.get(`${DELIVERY_ORDERS_URL}/report`, {
      params: {
        start_date: payload.start_date,
        end_date: payload.end_date,
      },
      responseType: 'blob',
    })
  },

  get: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.get(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}`)
  },

  getForGuest: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.get(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/guest`)
  },

  create: (payload: PayloadDeliveryOrderCreate): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.post(DELIVERY_ORDERS_URL, payload)
  },

  update: (deliveryOrderId: ID, payload: PayloadDeliveryOrderUpdate): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.put(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}`, payload)
  },

  uploadImage: (
    payload: PayloadDeliveryOrderUploadImage,
    config?: AxiosRequestConfig
  ): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${DELIVERY_ORDERS_URL}/upload`, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  delete: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.delete(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}`)
  },

  setStatusCancel: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.patch(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/cancel`)
  },

  setStatusCompleted: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.patch(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/completed`)
  },

  addDriver: (
    deliveryOrderId: ID,
    payload: PayloadDeliveryOrderAddDriver
  ): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.post(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/drivers`, payload)
  },

  deleteDriver: (deliveryOrderId: ID, driverUserId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.delete(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/drivers/${driverUserId}`)
  },

  addImage: (
    deliveryOrderId: ID,
    payload: PayloadDeliveryOrderAddImage
  ): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.post(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/images`, payload)
  },

  deleteImage: (deliveryOrderId: ID, deliveryOrderImageId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.delete(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/images/${deliveryOrderImageId}`)
  },

  addItem: (deliveryOrderId: ID, payload: PayloadDeliveryOrderAddItem): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.post(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/items`, payload)
  },

  deleteItem: (deliveryOrderId: ID, deliveryOrderItemId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.delete(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/items/${deliveryOrderItemId}`)
  },

  setStatusOnGoing: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.patch(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/on-going`)
  },

  setReturned: (
    deliveryOrderId: ID,
    payload: PayloadDeliveryOrderReturned
  ): Promise<APIResponse<ResponseDeliveryOrder>> => {
    return APIClient.patch(`${DELIVERY_ORDERS_URL}/${deliveryOrderId}/returned`, payload)
  },
}

export type {
  PayloadDeliveryOrdersDownloadReport,
  PayloadDeliveryOrderAddDriver,
  PayloadDeliveryOrderAddImage,
  PayloadDeliveryOrderAddItem,
  PayloadDeliveryOrderUpdate,
  PayloadDeliveryOrderCreate,
  PayloadDeliveryOrdersFilter,
  PayloadDeliveryOrderReturned,
}

export default DeliveryOrdersAPI
