import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { PurchaseOrderModel } from 'models/purchaseOrder'
import FormUtil from 'utils/formUtil'
import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'

type ResponsePurchaseOrder = Response<{ purchase_order: PurchaseOrderModel }>
type ResponsePurchaseOrdersWithFilter = Response<WithFilter<PurchaseOrderModel>>

type PayloadPurchaseOrdersFilter = RequestWithFilter<{
  supplier_id: ID | null
  status: PurchaseOrderStatusEnum | null
}>

type PayloadPurchaseOrderCreate = {
  date: string
  supplier_id: ID
  invoice_number: string
}

type PayloadPurchaseOrderUpdate = {
  date: string
  invoice_number: string
}

type PayloadPurchaseOrderAddItem = {
  price_per_unit: number
  product_id: ID
  qty: number
  unit_id: ID
}

type PayloadPurchaseOrderAddImage = {
  file_path: string
  description: string | null
}

type PayloadPurchaseOrderUploadImage = {
  file: File
}

const PURCHASE_ORDERS_URL = '/purchase-orders'

const PurchaseOrdersAPI = {
  filter: (payload: PayloadPurchaseOrdersFilter): Promise<APIResponse<ResponsePurchaseOrdersWithFilter>> => {
    return APIClient.post(`${PURCHASE_ORDERS_URL}/filter`, payload)
  },

  create: (payload: PayloadPurchaseOrderCreate): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.post(PURCHASE_ORDERS_URL, payload)
  },

  update: (purchaseOrderId: ID, payload: PayloadPurchaseOrderUpdate): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.put(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}`, payload)
  },

  get: (purchaseOrderId: ID): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.get(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}`)
  },

  delete: (purchaseOrderId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}`)
  },

  uploadImage: (
    payload: PayloadPurchaseOrderUploadImage,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${PURCHASE_ORDERS_URL}/upload`, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  addItem: (purchaseOrderId: ID, payload: PayloadPurchaseOrderAddItem): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/items`, payload)
  },

  deleteItem: (purchaseOrderId: ID, purchaseOrderItemId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/items/${purchaseOrderItemId}`)
  },

  addImage: (purchaseOrderId: ID, payload: PayloadPurchaseOrderAddImage): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/images`, payload)
  },

  deleteImage: (purchaseOrderId: ID, purchaseOrderImageId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/images/${purchaseOrderImageId}`)
  },

  completed: (purchaseOrderId: ID): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.patch(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/completed`)
  },

  ongoing: (purchaseOrderId: ID): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.patch(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/ongoing`)
  },

  cancel: (purchaseOrderId: ID): Promise<APIResponse<ResponsePurchaseOrder>> => {
    return APIClient.patch(`${PURCHASE_ORDERS_URL}/${purchaseOrderId}/cancel`)
  },
}

export type {
  PayloadPurchaseOrderAddImage,
  PayloadPurchaseOrderCreate,
  PayloadPurchaseOrderUploadImage,
  PayloadPurchaseOrdersFilter,
  PayloadPurchaseOrderUpdate,
  PayloadPurchaseOrderAddItem,
}

export default PurchaseOrdersAPI
