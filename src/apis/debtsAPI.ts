import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import FormUtil from 'utils/formUtil'
import { DebtModel } from 'models/debt'

type ResponseDebt = Response<{ debt: DebtModel }>
type ResponseDebtsWithFilter = Response<WithFilter<DebtModel>>

type PayloadDebtFilter = RequestWithFilter<{
  status: CustomerDebtStatusEnum | null
}>

type PayloadDebtUpdate = {
  amount: number
  image_file_path: string
  description: string | null
}

type PayloadDebtUploadImage = {
  file: File
}

type PayloadDebtDownloadReport = {
  start_date: string
  end_date: string
}

const DEBTS_URL = '/debts'

const DebtsAPI = {
  filter: (payload: PayloadDebtFilter): Promise<APIResponse<ResponseDebtsWithFilter>> => {
    return APIClient.post(`${DEBTS_URL}/filter`, payload)
  },

  get: (debtId: ID): Promise<APIResponse<ResponseDebt>> => {
    return APIClient.get(`${DEBTS_URL}/${debtId}`)
  },

  payment: (debtId: ID, payload: PayloadDebtUpdate): Promise<APIResponse<ResponseDebt>> => {
    return APIClient.patch(`${DEBTS_URL}/${debtId}/payment`, payload)
  },

  uploadImage: (payload: PayloadDebtUploadImage): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${DEBTS_URL}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  downloadReport: (payload: PayloadDebtDownloadReport): Promise<APIResponse<Blob>> => {
    return APIClient.get(`${DEBTS_URL}/report`, {
      params: {
        start_date: payload.start_date,
        end_date: payload.end_date,
      },
      responseType: 'blob',
    })
  },
}

export type { PayloadDebtFilter, PayloadDebtUpdate, PayloadDebtUploadImage, PayloadDebtDownloadReport }

export default DebtsAPI
