import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { ShopOrderPlatformTypeEnum } from 'enums/shopOrderPlatformTypeEnum'
import { ShopOrderTrackingStatusEnum } from 'enums/shopOrderTrackingStatusEnum'
import { ShopOrderModel } from 'models/shopOrder'

type ResponseShopOrder = Response<{ shop_order: ShopOrderModel }>
type ResponseShopOrdersWithFilter = Response<WithFilter<ShopOrderModel>>

type PayloadShopOrderFilter = RequestWithFilter<{
  platform_type: ShopOrderPlatformTypeEnum | null
  tracking_status: ShopOrderTrackingStatusEnum | null
  with_items: boolean
}>

const SHOP_ORDERS_URL = '/shop-orders'

const ShopOrdersAPI = {
  filter: (payload: PayloadShopOrderFilter): Promise<APIResponse<ResponseShopOrdersWithFilter>> => {
    return APIClient.post(`${SHOP_ORDERS_URL}/filter`, payload)
  },

  get: (unitId: ID): Promise<APIResponse<ResponseShopOrder>> => {
    return APIClient.get(`${SHOP_ORDERS_URL}/${unitId}`)
  },

}

export type { PayloadShopOrderFilter }

export default ShopOrdersAPI
