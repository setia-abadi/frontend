import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { RoleModel } from 'models/role'

type ResponseRolesWithFilter = Response<WithFilter<RoleModel>>

type PayloadRoleOptionsForUserForm = RequestWithFilter<{}>

const ROLES_URL = '/roles'

const RolesAPI = {
  optionsForUserForm: (
    payload: PayloadRoleOptionsForUserForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseRolesWithFilter>> => {
    return APIClient.post(`${ROLES_URL}/options/user-form`, payload, config)
  },
}

export type { PayloadRoleOptionsForUserForm }

export default RolesAPI
