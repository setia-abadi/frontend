import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { SupplierModel } from 'models/supplier'

type ResponseSupplier = Response<{ supplier: SupplierModel }>
type ResponseSuppliersWithFilter = Response<WithFilter<SupplierModel>>

type PayloadFilterSupplier = RequestWithFilter<{
  is_active: boolean | null
}>

type PayloadSupplierOptionsForProductReceiveForm = RequestWithFilter<{}>

type PayloadSupplierOptionsForProductReceiveFilter = RequestWithFilter<{}>

type PayloadUpdateSupplier = {
  address: string
  code: string
  description: string | null
  email: string | null
  is_active: boolean
  name: string
  phone: string
  supplier_type_id: ID
}

type PayloadCreateSupplier = {
  address: string
  code: string
  description: string | null
  email: string | null
  is_active: boolean
  name: string
  phone: string
  supplier_type_id: ID
}

const SUPPLIERS_URL = '/suppliers'

const SuppliersAPI = {
  filter: (payload: PayloadFilterSupplier): Promise<APIResponse<ResponseSuppliersWithFilter>> => {
    return APIClient.post(`${SUPPLIERS_URL}/filter`, payload)
  },

  create: (payload: PayloadCreateSupplier): Promise<APIResponse<ResponseSupplier>> => {
    return APIClient.post(SUPPLIERS_URL, payload)
  },

  get: (supplierId: ID): Promise<APIResponse<ResponseSupplier>> => {
    return APIClient.get(`${SUPPLIERS_URL}/${supplierId}`)
  },

  update: (supplierId: ID, payload: PayloadUpdateSupplier): Promise<APIResponse<ResponseSupplier>> => {
    return APIClient.put(`${SUPPLIERS_URL}/${supplierId}`, payload)
  },

  delete: (supplierId: ID): Promise<APIResponse<ResponseSupplier>> => {
    return APIClient.delete(`${SUPPLIERS_URL}/${supplierId}`)
  },

  optionsForProductReceiveForm: (
    payload: PayloadSupplierOptionsForProductReceiveForm
  ): Promise<APIResponse<ResponseSuppliersWithFilter>> => {
    return APIClient.post(`${SUPPLIERS_URL}/options/product-receive-form`, payload)
  },

  optionsForProductReceiveFilter: (
    payload: PayloadSupplierOptionsForProductReceiveFilter
  ): Promise<APIResponse<ResponseSuppliersWithFilter>> => {
    return APIClient.post(`${SUPPLIERS_URL}/options/product-receive-filter`, payload)
  },
}

export type { PayloadCreateSupplier, PayloadFilterSupplier, PayloadUpdateSupplier }

export default SuppliersAPI
