import { Response } from "models/apiBase";
import APIClient, { APIResponse } from "./APIClient";
import { TransactionModel } from "models/transaction";
import { TransactionPaymentTypeEnum } from "enums/transactionPaymentTypeEnum";
import { ID } from "models/base";
import { PrinterDataModel } from "models/printerData";

type ResponseTransaction = Response<{ 
  printer_data: PrinterDataModel
  transaction: TransactionModel 
}>

type ResponsePrinterData = Response<{
  printer_data: PrinterDataModel
}>

type PayloadTransactionCheckout = {
  cash_paid: number | null
  payment_type: TransactionPaymentTypeEnum
  reference_number: string | null
}

const TRANSACTIONS_URL = '/transactions'

const TransactionsAPI = {
  checkout: (payload: PayloadTransactionCheckout): Promise<APIResponse<ResponseTransaction>> => {
    return APIClient.post(`${TRANSACTIONS_URL}/checkout`, payload)
  },

  get: (transactionId: ID): Promise<APIResponse<ResponseTransaction>> => {
    return APIClient.get(`${TRANSACTIONS_URL}/${transactionId}`)
  },

  reprint: (transactionId: ID): Promise<APIResponse<ResponsePrinterData>> => {
    return APIClient.get(`${TRANSACTIONS_URL}/${transactionId}/reprint`)
  }
}

export type { PayloadTransactionCheckout }

export default TransactionsAPI