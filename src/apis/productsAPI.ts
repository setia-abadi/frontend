import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { ProductModel } from 'models/product'
import FormUtil from 'utils/formUtil'

type ResponseProduct = Response<{ product: ProductModel }>
type ResponseProductWithFilter = Response<WithFilter<ProductModel>>

type PayloadProductsFilter = RequestWithFilter<{
  is_active: boolean | null
  is_loss: boolean | null
}>

type PayloadProductOptionsForCustomerTypeDiscountForm = RequestWithFilter<{
  customer_type_id: ID
}>

type PayloadProductOptionsForCartAddItemForm = RequestWithFilter<{}>

type PayloadProductOptionsForDeliveryOrderItemForm = RequestWithFilter<{}>

type PayloadProductOptionsForProductReceiveItemForm = RequestWithFilter<{
  product_receive_id: ID
}>

type PayloadProductOptionsForProductDiscountForm = RequestWithFilter<{}>

type PayloadProductCreate = {
  description: string | null
  image_file_path: string
  name: string
}

type PayloadProductUpdate = {
  description: string | null
  image_file_path: string | null
  name: string
  is_active: boolean | null
  price: number | null
}

type PayloadProductUpload = {
  file: File
}

const PRODUCTS_URL = '/products'

const ProductsAPI = {
  filter: (payload: PayloadProductsFilter): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/filter`, payload)
  },

  create: (payload: PayloadProductCreate): Promise<APIResponse<ResponseProduct>> => {
    return APIClient.post(PRODUCTS_URL, payload)
  },

  get: (productId: ID): Promise<APIResponse<ResponseProduct>> => {
    return APIClient.get(`${PRODUCTS_URL}/${productId}`)
  },

  update: (productId: ID, payload: PayloadProductUpdate): Promise<APIResponse<ResponseProduct>> => {
    return APIClient.put(`${PRODUCTS_URL}/${productId}`, payload)
  },

  delete: (productId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCTS_URL}/${productId}`)
  },

  upload: (payload: PayloadProductUpload): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${PRODUCTS_URL}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  optionsForCartAddItemForm: (
    payload: PayloadProductOptionsForCartAddItemForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/options/cart-add-item-form`, payload, config)
  },

  optionsForCustomerTypeDiscountForm: (
    payload: PayloadProductOptionsForCustomerTypeDiscountForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/options/customer-type-discount-form`, payload, config)
  },

  optionsForDeliveryOrderItemForm: (
    payload: PayloadProductOptionsForDeliveryOrderItemForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/options/delivery-order-item-form`, payload, config)
  },

  optionsForProductReceiveItemForm: (
    payload: PayloadProductOptionsForProductReceiveItemForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/options/product-receive-item-form`, payload, config)
  },

  optionsForProductDiscountForm: (
    payload: PayloadProductOptionsForProductDiscountForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseProductWithFilter>> => {
    return APIClient.post(`${PRODUCTS_URL}/options/product-discount-form`, payload, config)
  },
}

export type {
  PayloadProductCreate,
  PayloadProductOptionsForCustomerTypeDiscountForm,
  PayloadProductOptionsForDeliveryOrderItemForm,
  PayloadProductOptionsForProductReceiveItemForm,
  PayloadProductOptionsForProductDiscountForm,
  PayloadProductOptionsForCartAddItemForm,
  PayloadProductUpdate,
  PayloadProductsFilter,
  PayloadProductUpload,
}

export default ProductsAPI
