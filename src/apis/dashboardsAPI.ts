import APIClient, { APIResponse } from './APIClient'
import { Response } from 'models/apiBase'
import { CustomerDebtSummarizeModel } from 'models/customerDebtSummarize'
import { SupplierDebtSummarizeModel } from 'models/supplierDebtSummarize'
import { TransactionSummaryModel } from 'models/transactionSummary'

type ResponseCustomerDebtAndSupplierDebtSummaries = Response<{
  customer_debt_summaries: CustomerDebtSummarizeModel[],
  supplier_debt_summaries: SupplierDebtSummarizeModel[],
}>

type ResponseTransactionSummaries = Response<{
  transaction_summaries: TransactionSummaryModel[]
}>

type PayloadDashboardSummarizeTransactions = {
  start_date: string
  end_date: string
}

const DASHBOARDS_URL = '/dashboards'

const DashboardsAPI = {
  summarizeDebt(): Promise<APIResponse<ResponseCustomerDebtAndSupplierDebtSummaries>> {
    return APIClient.post(`${DASHBOARDS_URL}/summarize-debt`)
  },

  summarizeTransactions(payload: PayloadDashboardSummarizeTransactions): Promise<APIResponse<ResponseTransactionSummaries>> {
    return APIClient.post(`${DASHBOARDS_URL}/summarize-transaction`, payload)
  },
}

export type {
  PayloadDashboardSummarizeTransactions,
}

export default DashboardsAPI
