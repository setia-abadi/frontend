import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { CustomerDebtModel } from 'models/customerDebt'
import { CustomerDebtStatusEnum } from 'enums/customerDebtStatusEnum'
import FormUtil from 'utils/formUtil'

type ResponseCustomerDebt = Response<{ customer_debt: CustomerDebtModel }>
type ResponseCustomerDebtsWithFilter = Response<WithFilter<CustomerDebtModel>>

type PayloadCustomerDebtFilter = RequestWithFilter<{
  status: CustomerDebtStatusEnum | null
}>

type PayloadCustomerDebtUpdate = {
  amount: number
  image_file_path: string
  description: string | null
}

type PayloadCustomerDebtUploadImage = {
  file: File
}

type PayloadCustomerDebtDownloadReport = {
  start_date: string
  end_date: string
  customer_id: string | null
}

const CUSTOMER_DEBTS_URL = '/customer-debts'

const CustomerDebtsAPI = {
  filter: (payload: PayloadCustomerDebtFilter): Promise<APIResponse<ResponseCustomerDebtsWithFilter>> => {
    return APIClient.post(`${CUSTOMER_DEBTS_URL}/filter`, payload)
  },

  get: (customerDebtId: ID): Promise<APIResponse<ResponseCustomerDebt>> => {
    return APIClient.get(`${CUSTOMER_DEBTS_URL}/${customerDebtId}`)
  },

  payment: (customerDebtId: ID, payload: PayloadCustomerDebtUpdate): Promise<APIResponse<ResponseCustomerDebt>> => {
    return APIClient.patch(`${CUSTOMER_DEBTS_URL}/${customerDebtId}/payment`, payload)
  },

  uploadImage: (payload: PayloadCustomerDebtUploadImage): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${CUSTOMER_DEBTS_URL}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  downloadReport: (payload: PayloadCustomerDebtDownloadReport): Promise<APIResponse<Blob>> => {
    return APIClient.get(`${CUSTOMER_DEBTS_URL}/report`, {
      params: {
        start_date: payload.start_date,
        end_date: payload.end_date,
        customer_id: payload.customer_id,
      },
      responseType: 'blob',
    })
  },
}

export type {
  PayloadCustomerDebtFilter,
  PayloadCustomerDebtUpdate,
  PayloadCustomerDebtUploadImage,
  PayloadCustomerDebtDownloadReport,
}

export default CustomerDebtsAPI
