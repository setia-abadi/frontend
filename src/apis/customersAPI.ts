import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, WithFilter } from 'models/apiBase'
import { CustomerModel } from 'models/customer'

type ResponseCustomer = Response<{ customer: CustomerModel }>
type ResponseCustomersWithFilter = Response<WithFilter<CustomerModel>>

type PayloadCustomersFilter = RequestWithFilter<{
  is_active: boolean | null
}>

type PayloadCustomerOptionsForDeliveryOrderForm = RequestWithFilter<{}>

type PayloadCustomerOptionsForDeliveryOrderFilter = RequestWithFilter<{}>

type PayloadCustomerOptionsForWhatsappCustomerDebtBroadcastForm = RequestWithFilter<{}>

type PayloadCustomerOptionsForCustomerDebtReportForm = RequestWithFilter<{}>

type PayloadCustomerCreate = {
  address: string
  email: string
  is_active: boolean
  name: string
  phone: string
  latitude: number
  longitude: number
  customer_type_id: ID
}

type PayloadCustomerUpdate = {
  address: string
  email: string
  is_active: boolean
  name: string
  phone: string
  latitude: number
  longitude: number
  customer_type_id: ID
}

const CUSTOMERS_URL = '/customers'

const CustomersAPI = {
  filter: (payload: PayloadCustomersFilter): Promise<APIResponse<ResponseCustomersWithFilter>> => {
    return APIClient.post(`${CUSTOMERS_URL}/filter`, payload)
  },

  create: (payload: PayloadCustomerCreate): Promise<APIResponse<ResponseCustomer>> => {
    return APIClient.post(CUSTOMERS_URL, payload)
  },

  get: (customerId: ID): Promise<APIResponse<ResponseCustomer>> => {
    return APIClient.get(`${CUSTOMERS_URL}/${customerId}`)
  },

  update: (customerId: ID, payload: PayloadCustomerUpdate): Promise<APIResponse<ResponseCustomer>> => {
    return APIClient.put(`${CUSTOMERS_URL}/${customerId}`, payload)
  },

  delete: (customerId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${CUSTOMERS_URL}/${customerId}`)
  },

  optionsForDeliveryOrderForm: (
    payload: PayloadCustomerOptionsForDeliveryOrderForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomersWithFilter>> => {
    return APIClient.post(`${CUSTOMERS_URL}/options/delivery-order-form`, payload, config)
  },

  optionsForDeliveryOrderFilter: (
    payload: PayloadCustomerOptionsForDeliveryOrderFilter,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomersWithFilter>> => {
    return APIClient.post(`${CUSTOMERS_URL}/options/delivery-order-filter`, payload, config)
  },

  optionsForWhatsappCustomerDebtBroadcastForm: (
    payload: PayloadCustomerOptionsForWhatsappCustomerDebtBroadcastForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomersWithFilter>> => {
    return APIClient.post(`${CUSTOMERS_URL}/options/whatsapp-customer-debt-broadcast-form`, payload, config)
  },

  optionsForCustomerDebtReportForm: (
    payload: PayloadCustomerOptionsForCustomerDebtReportForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomersWithFilter>> => {
    return APIClient.post(`${CUSTOMERS_URL}/options/customer-debt-report-form`, payload, config)
  },
}

export type {
  PayloadCustomerCreate,
  PayloadCustomerUpdate,
  PayloadCustomersFilter,
  PayloadCustomerOptionsForDeliveryOrderForm,
  PayloadCustomerOptionsForDeliveryOrderFilter,
  PayloadCustomerOptionsForWhatsappCustomerDebtBroadcastForm,
  PayloadCustomerOptionsForCustomerDebtReportForm,
}

export default CustomersAPI
