import APIClient, { APIResponse } from './APIClient'
import { Response } from 'models/apiBase'
import { ID } from 'models/base'
import { CartModel } from 'models/cart'

type ResponseCart = Response<{ cart: CartModel }>
type ResponseCarts = Response<{ carts: CartModel[] }>

type PayloadCartAddItem = {
  product_id: ID
  qty: number
}

type PayloadCartUpdateItem = {
  qty: number
}

type PayloadCartSetInactive = {
  name: string
}

const CARTS_URL = '/carts'

const CartsAPI = {
  getActive: (): Promise<APIResponse<ResponseCart>> => {
    return APIClient.get(`${CARTS_URL}/active`)
  },

  getInactive: (): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.get(`${CARTS_URL}/in-active`)
  },

  addItem: (payload: PayloadCartAddItem): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.post(`${CARTS_URL}/items`, payload)
  },

  updateItem: (cartItemId: ID, payload: PayloadCartUpdateItem): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.patch(`${CARTS_URL}/items/${cartItemId}`, payload)
  },

  deleteItem: (cartItemId: ID): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.delete(`${CARTS_URL}/items/${cartItemId}`)
  },

  setInActive: (payload: PayloadCartSetInactive): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.patch(`${CARTS_URL}/set-in-active`, payload)
  },

  setActive: (cartId: ID): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.patch(`${CARTS_URL}/${cartId}/set-active`)
  },

  delete: (cartId: ID): Promise<APIResponse<ResponseCarts>> => {
    return APIClient.delete(`${CARTS_URL}/${cartId}`)
  },
}

export type { }

export default CartsAPI
