import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { Response, ResponseSuccess } from 'models/apiBase'
import { TiktokPlatformProductModel } from 'models/tiktokPlatformProduct'
import { TiktokBrandModel } from 'models/tiktokBrand'
import { TiktokCategoryModel } from 'models/tiktokCategory'
import { TiktokPlatfromAttributeModel } from 'models/tiktokPlatformAttribute'
import { TiktokCategoryRuleModel } from 'models/tiktokCategoryRule'
import FormUtil from 'utils/formUtil'
import { TiktokProductDimensionUnitEnum } from 'enums/tiktokProductDimensionUnitEnum'
import { TiktokProductPackageWeightEnum } from 'enums/tiktokProductPackageWeightEnum'

type ResponseTiktokProduct = Response<{ tiktok_product: TiktokPlatformProductModel }>

type ResponseTiktokBrands = Response<{
  brands: TiktokBrandModel[]
  next_page_token: string | null
  total_count: number
}>

type RepsonseTiktokCategory = Response<{
  category: TiktokCategoryModel
}>

type ResponseTiktokCategories = Response<{
  categories: TiktokCategoryModel[]
}>

type ResponseTikokUploadImage = Response<{
  uri: string
  url: string
}>

type ResponseTiktokAttributes = Response<{
  attributes: TiktokPlatfromAttributeModel[]
}>

type ResponseTiktokCategoryRule = Response<{
  category_rule: TiktokCategoryRuleModel
}>

type PayloadTiktokProductCreate = {
  attributes: { id: ID, values: { id: ID, name: string }[] }[]
  brand_id: string | null
  category_id: string
  description: string
  dimension_height: number | null
  dimension_length: number | null 
  dimension_unit: TiktokProductDimensionUnitEnum | null
  dimension_width: number | null
  images_uri: string[]
  product_id: ID
  size_chart_uri: string | null
  title: string
  weight: number
  weight_unit: TiktokProductPackageWeightEnum
} 

type PayloadTiktokProductUpdate = {
  attributes: { id: ID, values: { id: ID, name: string }[] }[]
  brand_id: string | null
  category_id: string
  description: string
  dimension_height: number | null
  dimension_length: number | null 
  dimension_unit: TiktokProductDimensionUnitEnum | null
  dimension_width: number | null
  images_uri: string[]
  size_chart_uri: string | null
  title: string
  weight: number
  weight_unit: TiktokProductPackageWeightEnum
}

type PayloadTiktokProductFetchBrands = {
  category_id: ID
  next_page_token: string | null
  phrase: string | null
}

type PayloadTiktokProductFetchAttributes = {
  category_id: ID
}

type PayloadTiktokProductFetchCategoryRule = {
  category_id: ID
}

type PayloadTiktokProductFetchRecommendedCategories = {
  description: string | null
  image_uri: string[] | null
  product_title: string
}

type PayloadTiktokProductUploadImage = {
  file: File
}

const TIKTOK_PRODUCTS_URL = '/tiktok-products'

const TiktokProductsAPI = {
  get: (tiktokProductId: ID): Promise<APIResponse<ResponseTiktokProduct>> => {
    return APIClient.get(`${TIKTOK_PRODUCTS_URL}/${tiktokProductId}`)
  },

  create: (payload: PayloadTiktokProductCreate): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}`, payload)
  },

  update: (tiktokProductId: ID, payload: PayloadTiktokProductUpdate): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.put(`${TIKTOK_PRODUCTS_URL}/${tiktokProductId}`, payload)
  },

  brands: (payload: PayloadTiktokProductFetchBrands): Promise<APIResponse<ResponseTiktokBrands>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/brands`, payload)
  },

  categories: (): Promise<APIResponse<ResponseTiktokCategories>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/categories`)
  },

  attributes: (categoryId: ID, payload: PayloadTiktokProductFetchAttributes): Promise<APIResponse<ResponseTiktokAttributes>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/categories/${categoryId}/attributes`, payload)
  },

  categoryRule: (categoryId: ID, payload: PayloadTiktokProductFetchCategoryRule): Promise<APIResponse<ResponseTiktokCategoryRule>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/categories/${categoryId}/rules`, payload)
  },

  recommendedCategory: (payload: PayloadTiktokProductFetchRecommendedCategories): Promise<APIResponse<RepsonseTiktokCategory>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/recommended-category`, payload)
  },

  uploadImage: (payload: PayloadTiktokProductUploadImage): Promise<APIResponse<ResponseTikokUploadImage>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/upload-image`, formData, { 
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },

  activate: (tiktokProductId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/${tiktokProductId}/activate`)
  },

  deactivate: (tiktokProductId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${TIKTOK_PRODUCTS_URL}/${tiktokProductId}/deactivate`)
  },
}

export type {
  PayloadTiktokProductCreate,
  PayloadTiktokProductFetchAttributes,
  PayloadTiktokProductFetchBrands,
  PayloadTiktokProductFetchCategoryRule,
  PayloadTiktokProductFetchRecommendedCategories,
  PayloadTiktokProductUpdate,
  PayloadTiktokProductUploadImage,
}

export default TiktokProductsAPI
