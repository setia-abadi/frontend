import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { ProductStockAdjustmentModel } from 'models/productStockAdjustment'

type ResponseProductStockAdjustmentsWithFilter = Response<WithFilter<ProductStockAdjustmentModel>>

type PayloadFilterProductStockAdjustment = Omit<RequestWithFilter<{
  product_stock_id: ID
  user_id: ID | null
}>, 'phrase'>

const PRDOUCT_STOCK_ADJUSTMENTS_URL = '/product-stock-adjustments'

const ProductStockAdjustmentsAPI = {
  filter: (
    payload: PayloadFilterProductStockAdjustment
  ): Promise<APIResponse<ResponseProductStockAdjustmentsWithFilter>> => {
    return APIClient.post(`${PRDOUCT_STOCK_ADJUSTMENTS_URL}/filter`, payload)
  },
}

export type { PayloadFilterProductStockAdjustment }

export default ProductStockAdjustmentsAPI
