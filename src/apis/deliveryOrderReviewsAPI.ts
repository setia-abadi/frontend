import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { DeliveryOrderReviewModel } from 'models/deliveryOrderReview'
import { DeliveryOrderReviewTypeEnum } from 'enums/deliveryOrderReviewTypeEnum'

type ResponseDeliveryOrderReview = Response<{ delivery_order_review: DeliveryOrderReviewModel }>
type ResponseDeliveryOrderReviewIsExist = Response<{ review_exist: boolean }>
type ResponseDeliveryOrderReviewsWithFilter = Response<WithFilter<DeliveryOrderReviewModel>>

type PayloadDeliveryOrderReviewFilter = RequestWithFilter<{
  type: DeliveryOrderReviewTypeEnum | null
}>

type PayloadDeliveryOrderReviewCreateForGuest = {
  delivery_order_id: ID
  description: string
  type: DeliveryOrderReviewTypeEnum
}

const DELIVERY_ORDER_REVIEWS_URL = '/delivery-order-reviews'

const DeliveryOrderReviewsAPI = {
  filter: (payload: PayloadDeliveryOrderReviewFilter): Promise<APIResponse<ResponseDeliveryOrderReviewsWithFilter>> => {
    return APIClient.post(`${DELIVERY_ORDER_REVIEWS_URL}/filter`, payload)
  },

  get: (deliveryOrderReviewId: ID): Promise<APIResponse<ResponseDeliveryOrderReview>> => {
    return APIClient.get(`${DELIVERY_ORDER_REVIEWS_URL}/${deliveryOrderReviewId}`)
  },

  getIsExist: (deliveryOrderId: ID): Promise<APIResponse<ResponseDeliveryOrderReviewIsExist>> => {
    return APIClient.post(`${DELIVERY_ORDER_REVIEWS_URL}/delivery-orders/${deliveryOrderId}`)
  },

  createForGuest: (
    payload: PayloadDeliveryOrderReviewCreateForGuest
  ): Promise<APIResponse<ResponseDeliveryOrderReview>> => {
    return APIClient.post(`${DELIVERY_ORDER_REVIEWS_URL}/guest`, payload)
  },
}

export type { PayloadDeliveryOrderReviewCreateForGuest, PayloadDeliveryOrderReviewFilter }

export default DeliveryOrderReviewsAPI
