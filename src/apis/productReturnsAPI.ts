import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { ProductReturnModel } from 'models/productReturn'
import FormUtil from 'utils/formUtil'
import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'

type ResponseProductReturn = Response<{ product_return: ProductReturnModel }>
type ResponseProductReturnsWithFilter = Response<WithFilter<ProductReturnModel>>

type PayloadProductReturnsFilter = RequestWithFilter<{
  supplier_id: ID | null
  status: ProductReturnStatusEnum | null
}>

type PayloadProductReturnCreate = {
  date: string
  supplier_id: ID
  invoice_number: string
}

type PayloadProductReturnUpdate = {
  date: string
  invoice_number: string
}

type PayloadProductReturnAddItem = {
  product_id: ID
  qty: number
}

type PayloadProductReturnAddImage = {
  file_path: string
  description: string | null
}

type PayloadProductReturnUploadImage = {
  file: File
}

type PayloadProductReturnReturned = {
  description: string
  file_paths: string[]
}

const PRODUCT_RETURNS_URL = '/product-returns'

const ProductReturnsAPI = {
  filter: (payload: PayloadProductReturnsFilter): Promise<APIResponse<ResponseProductReturnsWithFilter>> => {
    return APIClient.post(`${PRODUCT_RETURNS_URL}/filter`, payload)
  },

  create: (payload: PayloadProductReturnCreate): Promise<APIResponse<ResponseProductReturn>> => {
    return APIClient.post(PRODUCT_RETURNS_URL, payload)
  },

  update: (productReturnId: ID, payload: PayloadProductReturnUpdate): Promise<APIResponse<ResponseProductReturn>> => {
    return APIClient.put(`${PRODUCT_RETURNS_URL}/${productReturnId}`, payload)
  },

  get: (productReturnId: ID): Promise<APIResponse<ResponseProductReturn>> => {
    return APIClient.get(`${PRODUCT_RETURNS_URL}/${productReturnId}`)
  },

  delete: (productReturnId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RETURNS_URL}/${productReturnId}`)
  },

  uploadImage: (
    payload: PayloadProductReturnUploadImage,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${PRODUCT_RETURNS_URL}/upload`, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  addItem: (productReturnId: ID, payload: PayloadProductReturnAddItem): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PRODUCT_RETURNS_URL}/${productReturnId}/items`, payload)
  },

  deleteItem: (productReturnId: ID, productReturnItemId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RETURNS_URL}/${productReturnId}/items/${productReturnItemId}`)
  },

  addImage: (productReturnId: ID, payload: PayloadProductReturnAddImage): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PRODUCT_RETURNS_URL}/${productReturnId}/images`, payload)
  },

  deleteImage: (productReturnId: ID, productReturnImageId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RETURNS_URL}/${productReturnId}/images/${productReturnImageId}`)
  },

  completed: (productReturnId: ID): Promise<APIResponse<ResponseProductReturn>> => {
    return APIClient.patch(`${PRODUCT_RETURNS_URL}/${productReturnId}/completed`)
  },

  setStatusReturned: (
    productReturnId: ID,
    payload: PayloadProductReturnReturned
  ): Promise<APIResponse<ResponseProductReturn>> => {
    return APIClient.patch(`${PRODUCT_RETURNS_URL}/${productReturnId}/returned`, payload)
  },
}

export type {
  PayloadProductReturnAddImage,
  PayloadProductReturnCreate,
  PayloadProductReturnUploadImage,
  PayloadProductReturnsFilter,
  PayloadProductReturnUpdate,
  PayloadProductReturnAddItem,
  PayloadProductReturnReturned,
}

export default ProductReturnsAPI
