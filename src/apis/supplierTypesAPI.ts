import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { SupplierTypeModel } from 'models/supplierType'

type ResponseSupplierType = Response<{ supplier_type: SupplierTypeModel }>
type ResponseSupplierTypesWithFilter = Response<WithFilter<SupplierTypeModel>>

type PayloadFilterSupplierType = RequestWithFilter<{}>

type PayloadSupplierTypeOptionsForSupplierForm = RequestWithFilter<{}>

type PayloadUpdateSupplierType = {
  description: string | null
  name: string
}

type PayloadCreateSupplierType = {
  description: string | null
  name: string
}

const SUPPLIER_TYPES_URL = '/supplier-types'

const SupplierTypesAPI = {
  filter: (payload: PayloadFilterSupplierType): Promise<APIResponse<ResponseSupplierTypesWithFilter>> => {
    return APIClient.post(`${SUPPLIER_TYPES_URL}/filter`, payload)
  },

  create: (payload: PayloadCreateSupplierType): Promise<APIResponse<ResponseSupplierType>> => {
    return APIClient.post(SUPPLIER_TYPES_URL, payload)
  },

  get: (supplierTypeId: ID): Promise<APIResponse<ResponseSupplierType>> => {
    return APIClient.get(`${SUPPLIER_TYPES_URL}/${supplierTypeId}`)
  },

  update: (supplierTypeId: ID, payload: PayloadUpdateSupplierType): Promise<APIResponse<ResponseSupplierType>> => {
    return APIClient.put(`${SUPPLIER_TYPES_URL}/${supplierTypeId}`, payload)
  },

  delete: (supplierTypeId: ID): Promise<APIResponse<ResponseSupplierType>> => {
    return APIClient.delete(`${SUPPLIER_TYPES_URL}/${supplierTypeId}`)
  },

  optionsForSupplierForm: (
    payload: PayloadSupplierTypeOptionsForSupplierForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseSupplierTypesWithFilter>> => {
    return APIClient.post(`${SUPPLIER_TYPES_URL}/filter`, payload, config)
  },
}

export type { PayloadCreateSupplierType, PayloadFilterSupplierType, PayloadUpdateSupplierType }

export default SupplierTypesAPI
