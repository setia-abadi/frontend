import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { Response, ResponseSuccess } from 'models/apiBase'

type ResponseWhatsappIsLoggedIn = Response<{ is_logged_in: boolean }>

type PayloadWhatsappBroadcastCustomerTypeDiscount = {
  customer_type_discount_id: ID
}

type PayloadWhatsappBroadcastProductPriceChange = {
  customer_type_id: ID
  old_price: number
  product_id: ID
}

type PayloadWhatsappBroadcastCustomerDebt = {
  customer_id: ID
}

const WHATSAPP_URL = '/whatsapp'

const WhatsappAPI = {
  isLoggedIn: (): Promise<APIResponse<ResponseWhatsappIsLoggedIn>> => {
    return APIClient.get(`${WHATSAPP_URL}/is-logged-in`)
  },

  logout: (): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${WHATSAPP_URL}/logout`)
  },

  broadcastCustomerTypeDiscount: (
    payload: PayloadWhatsappBroadcastCustomerTypeDiscount
  ): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${WHATSAPP_URL}/broadcast/customer-type-discount`, payload)
  },

  broadcastProductPriceChange: (
    payload: PayloadWhatsappBroadcastProductPriceChange
  ): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${WHATSAPP_URL}/broadcast/product-price-change`, payload)
  },

  broadcastCustomerDebt: (
    payload: PayloadWhatsappBroadcastCustomerDebt
  ): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${WHATSAPP_URL}/broadcast/customer-debt`, payload)
  },
}

export type { PayloadWhatsappBroadcastCustomerTypeDiscount, PayloadWhatsappBroadcastProductPriceChange }

export default WhatsappAPI
