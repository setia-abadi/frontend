import { CashierSessionStatusEnum } from 'enums/cashierSessionStatusEnum'
import APIClient, { APIResponse } from './APIClient'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { CashierSessionModel } from 'models/cashierSession'
import { ID } from 'models/base'
import { TransactionModel } from 'models/transaction'

type ResponseCashierSession = Response<{ cashier_session: CashierSessionModel }>
type ResponseNullableCashierSession = Response<{ cashier_session: CashierSessionModel | null }>
type ResponseCashierSessionsWithFilter = Response<WithFilter<CashierSessionModel>>
type ResponseCashierSessionTransactionsWithFilter = Response<WithFilter<TransactionModel>>

type PayloadCashierSessionFilter = RequestWithFilter<{
  user_id: ID | null
  status: CashierSessionStatusEnum | null
  started_at: string | null
  ended_at: string | null
}>

type PayloadCashierSessionFilterForCurrentUser = RequestWithFilter<{
  status: CashierSessionStatusEnum | null
  started_at: string | null
  ended_at: string | null
}>

type PayloadCashierSessionTransactionFilter = RequestWithFilter<{
  cashier_session_id: ID
}>

type PayloadCashierSessionStart = {
  starting_cash: number
}

type PayloadCashierSessionEnd = {}

const CASHIER_SESSIONS_URL = '/cashier-sessions'

const CashierSessionsAPI = {
  filter: (payload: PayloadCashierSessionFilter): Promise<APIResponse<ResponseCashierSessionsWithFilter>> => {
    return APIClient.post(`${CASHIER_SESSIONS_URL}/filter`, payload)
  },

  filterForCurrentUser: (
    payload: PayloadCashierSessionFilterForCurrentUser
  ): Promise<APIResponse<ResponseCashierSessionsWithFilter>> => {
    return APIClient.post(`${CASHIER_SESSIONS_URL}/filter/current-user`, payload)
  },

  start: (payload: PayloadCashierSessionStart): Promise<APIResponse<ResponseCashierSession>> => {
    return APIClient.post(`${CASHIER_SESSIONS_URL}/start`, payload)
  },

  end: (payload: PayloadCashierSessionEnd): Promise<APIResponse<ResponseCashierSession>> => {
    return APIClient.post(`${CASHIER_SESSIONS_URL}/end`, payload)
  },

  get: (cashierSessionId: ID): Promise<APIResponse<ResponseCashierSession>> => {
    return APIClient.get(`${CASHIER_SESSIONS_URL}/${cashierSessionId}`)
  },

  transactionFilter: (
    cashierSessionId: ID,
    payload: PayloadCashierSessionTransactionFilter
  ): Promise<APIResponse<ResponseCashierSessionTransactionsWithFilter>> => {
    return APIClient.post(`${CASHIER_SESSIONS_URL}/${cashierSessionId}/transactions`, payload)
  },

  current: (): Promise<APIResponse<ResponseNullableCashierSession>> => {
    return APIClient.get(`${CASHIER_SESSIONS_URL}/current`)
  },

  downloadReport: (cashierSessionId: ID): Promise<APIResponse<Blob>> => {
    return APIClient.get(`${CASHIER_SESSIONS_URL}/${cashierSessionId}/report`, {
      responseType: 'blob',
    })
  },
}

export type {
  PayloadCashierSessionEnd,
  PayloadCashierSessionFilter,
  PayloadCashierSessionFilterForCurrentUser,
  PayloadCashierSessionStart,
  PayloadCashierSessionTransactionFilter,
}

export default CashierSessionsAPI
