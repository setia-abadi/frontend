import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { CustomerTypeModel } from 'models/customerType'

type ResponseCustomerType = Response<{ customer_type: CustomerTypeModel }>
type ResponseCustomerTypesWithFilter = Response<WithFilter<CustomerTypeModel>>

type PayloadCustomerTypeFilter = RequestWithFilter<{}>

type PayloadCustomerTypeOptionsForCustomerForm = RequestWithFilter<{}>

type PayloadCustomerTypeOptionsForWhatsappProductChangePriceBroadcastForm = RequestWithFilter<{}>

type PayloadCustomerCreate = {
  description: string | null
  name: string
}

type PayloadCustomerUpdate = {
  description: string | null
  name: string
}

type PayloadCustomerAddDiscount = {
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
  product_id: string
}

type PayloadCustomerUpdateDiscount = {
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
}

const CUSTOMER_TYPES_URL = '/customer-types'

const CustomerTypesAPI = {
  filter: (payload: PayloadCustomerTypeFilter): Promise<APIResponse<ResponseCustomerTypesWithFilter>> => {
    return APIClient.post(`${CUSTOMER_TYPES_URL}/filter`, payload)
  },

  create: (payload: PayloadCustomerCreate): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.post(CUSTOMER_TYPES_URL, payload)
  },

  get: (customerTypeId: ID): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.get(`${CUSTOMER_TYPES_URL}/${customerTypeId}`)
  },

  update: (customerTypeId: ID, payload: PayloadCustomerUpdate): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.put(`${CUSTOMER_TYPES_URL}/${customerTypeId}`, payload)
  },

  delete: (customerTypeId: ID): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.delete(`${CUSTOMER_TYPES_URL}/${customerTypeId}`)
  },

  optionsForCustomerForm: (
    payload: PayloadCustomerTypeOptionsForCustomerForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomerTypesWithFilter>> => {
    return APIClient.post(`${CUSTOMER_TYPES_URL}/options/customer-form`, payload, config)
  },

  optionsForWhatsappProductPriceChangeBroadcastForm: (
    payload: PayloadCustomerTypeOptionsForWhatsappProductChangePriceBroadcastForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseCustomerTypesWithFilter>> => {
    return APIClient.post(`${CUSTOMER_TYPES_URL}/options/whatsapp-product-price-change-broadcast-form`, payload, config)
  },

  addDiscount: (
    customerTypeId: ID,
    payload: PayloadCustomerAddDiscount
  ): Promise<APIResponse<ResponseCustomerTypesWithFilter>> => {
    return APIClient.post(`${CUSTOMER_TYPES_URL}/${customerTypeId}/discount`, payload)
  },

  updateDiscount: (
    customerTypeId: ID,
    discountId: ID,
    payload: PayloadCustomerUpdateDiscount
  ): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.put(`${CUSTOMER_TYPES_URL}/${customerTypeId}/discount/${discountId}`, payload)
  },

  deleteDiscount: (customerTypeId: ID, discountId: ID): Promise<APIResponse<ResponseCustomerType>> => {
    return APIClient.delete(`${CUSTOMER_TYPES_URL}/${customerTypeId}/discount/${discountId}`)
  },
}

export type {
  PayloadCustomerCreate,
  PayloadCustomerTypeFilter,
  PayloadCustomerTypeOptionsForCustomerForm,
  PayloadCustomerUpdate,
}

export default CustomerTypesAPI
