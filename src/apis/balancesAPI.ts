import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, WithFilter } from 'models/apiBase'
import { BalanceModel } from 'models/balance'

type ResponseBalance = Response<{ balance: BalanceModel }>
type ResponseBalancesWithFilter = Response<WithFilter<BalanceModel>>

type PayloadFilterBalance = RequestWithFilter<{}>

type PayloadUpdateBalance = {
  account_name: string
  account_number: string
  bank_name: string
  name: string
}

type PayloadCreateBalance = {
  account_name: string
  account_number: string
  bank_name: string
  name: string
}

const BALANCES_URL = '/balances'

const BalancesAPI = {
  filter: (payload: PayloadFilterBalance): Promise<APIResponse<ResponseBalancesWithFilter>> => {
    return APIClient.post(`${BALANCES_URL}/filter`, payload)
  },

  create: (payload: PayloadCreateBalance): Promise<APIResponse<ResponseBalance>> => {
    return APIClient.post(BALANCES_URL, payload)
  },

  get: (balanceId: ID): Promise<APIResponse<ResponseBalance>> => {
    return APIClient.get(`${BALANCES_URL}/${balanceId}`)
  },

  update: (userId: ID, payload: PayloadUpdateBalance): Promise<APIResponse<ResponseBalance>> => {
    return APIClient.put(`${BALANCES_URL}/${userId}`, payload)
  },

  delete: (balanceId: ID): Promise<APIResponse<ResponseBalance>> => {
    return APIClient.delete(`${BALANCES_URL}/${balanceId}`)
  },
}

export type { PayloadCreateBalance, PayloadFilterBalance, PayloadUpdateBalance }

export default BalancesAPI
