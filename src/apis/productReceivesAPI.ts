import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, ResponseUploadFile, WithFilter } from 'models/apiBase'
import { ProductReceiveModel } from 'models/productReceive'
import FormUtil from 'utils/formUtil'
import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'

type ResponseProductReceive = Response<{ product_receive: ProductReceiveModel }>
type ResponseProductReceivesWithFilter = Response<WithFilter<ProductReceiveModel>>

type PayloadProductReceivesFilter = RequestWithFilter<{
  supplier_id: ID | null
  status: ProductReceiveStatusEnum | null
}>

type PayloadProductReceiveCreate = {
  date: string
  supplier_id: ID
  invoice_number: string
}

type PayloadProductReceiveUpdate = {
  date: string
  invoice_number: string
}

type PayloadProductReceiveAddItem = {
  price_per_unit: number
  product_id: ID
  qty: number
  unit_id: ID
}

type PayloadProductReceiveAddImage = {
  file_path: string
  description: string | null
}

type PayloadProductReceiveUploadImage = {
  file: File
}

type PayloadProductReceiveReturned = {
  description: string
  file_paths: string[]
}

type PayloadProductReceiveUpdateItem = {
  qty_eligible: number
}

const PRODUCT_RECEIVES_URL = '/product-receives'

const ProductReceivesAPI = {
  filter: (payload: PayloadProductReceivesFilter): Promise<APIResponse<ResponseProductReceivesWithFilter>> => {
    return APIClient.post(`${PRODUCT_RECEIVES_URL}/filter`, payload)
  },

  create: (payload: PayloadProductReceiveCreate): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.post(PRODUCT_RECEIVES_URL, payload)
  },

  update: (
    productReceiveId: ID,
    payload: PayloadProductReceiveUpdate
  ): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.put(`${PRODUCT_RECEIVES_URL}/${productReceiveId}`, payload)
  },

  get: (productReceiveId: ID): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.get(`${PRODUCT_RECEIVES_URL}/${productReceiveId}`)
  },

  delete: (productReceiveId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RECEIVES_URL}/${productReceiveId}`)
  },

  uploadImage: (
    payload: PayloadProductReceiveUploadImage,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseUploadFile>> => {
    const formData = FormUtil.jsonToFormData(payload)
    return APIClient.post(`${PRODUCT_RECEIVES_URL}/upload`, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    })
  },

  addItem: (productReceiveId: ID, payload: PayloadProductReceiveAddItem): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/items`, payload)
  },

  deleteItem: (productReceiveId: ID, productReceiveItemId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/items/${productReceiveItemId}`)
  },

  addImage: (productReceiveId: ID, payload: PayloadProductReceiveAddImage): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.post(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/images`, payload)
  },

  deleteImage: (productReceiveId: ID, productReceiveImageId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/images/${productReceiveImageId}`)
  },

  completed: (productReceiveId: ID): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.patch(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/completed`)
  },

  cancel: (productReceiveId: ID): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.patch(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/cancel`)
  },

  setStatusReturned: (productReceiveId: ID, payload: PayloadProductReceiveReturned): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.patch(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/returned`, payload)
  },

  updateItem: (productReceiveId: ID, productReceiveItemId: ID, payload: PayloadProductReceiveUpdateItem): Promise<APIResponse<ResponseProductReceive>> => {
    return APIClient.patch(`${PRODUCT_RECEIVES_URL}/${productReceiveId}/items/${productReceiveItemId}`, payload)
  },
}

export type {
  PayloadProductReceiveAddImage,
  PayloadProductReceiveCreate,
  PayloadProductReceiveUploadImage,
  PayloadProductReceivesFilter,
  PayloadProductReceiveUpdate,
  PayloadProductReceiveAddItem,
  PayloadProductReceiveReturned,
}

export default ProductReceivesAPI
