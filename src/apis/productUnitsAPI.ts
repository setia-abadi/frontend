import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, WithFilter } from 'models/apiBase'
import { ProductUnitModel } from 'models/productUnit'

type ResponseProductUnitsWithFilter = Response<WithFilter<ProductUnitModel>>
type ResponseProductUnit = Response<{ product_unit: ProductUnitModel }>

type PayloadProductUnitOptionsForDeliveryOrderItemForm = RequestWithFilter<{
  product_id: ID
}>

type PayloadProductUnitOptionsProductReceiveItemForm = RequestWithFilter<{
  product_id: ID
}>

type PayloadProductUnitCreate = {
  product_id: ID
  scale: number
  to_unit_id: ID | null
  unit_id: ID
}

type PayloadProductUnitUpdate = {
  product_id: ID
  unit_id: ID
}

const PRODUCT_UNITS_URL = '/product-units'

const ProductUnitsAPI = {
  create: (payload: PayloadProductUnitCreate): Promise<APIResponse<ResponseProductUnit>> => {
    return APIClient.post(PRODUCT_UNITS_URL, payload)
  },

  delete: (productUnitId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_UNITS_URL}/${productUnitId}`)
  },

  getOptionsForDeliveryOrderItemForm: (
    payload: PayloadProductUnitOptionsForDeliveryOrderItemForm
  ): Promise<APIResponse<ResponseProductUnitsWithFilter>> => {
    return APIClient.post(`${PRODUCT_UNITS_URL}/options/delivery-order-form`, payload)
  },

  getOptionsForProductReceiveItemForm: (
    payload: PayloadProductUnitOptionsProductReceiveItemForm
  ): Promise<APIResponse<ResponseProductUnitsWithFilter>> => {
    return APIClient.post(`${PRODUCT_UNITS_URL}/options/product-receive-item-form`, payload)
  },
}

export type {
  PayloadProductUnitCreate,
  PayloadProductUnitUpdate,
  PayloadProductUnitOptionsForDeliveryOrderItemForm,
  PayloadProductUnitOptionsProductReceiveItemForm,
}

export default ProductUnitsAPI
