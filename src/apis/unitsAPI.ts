import APIClient, { APIRequestConfig, APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, WithFilter } from 'models/apiBase'
import { UnitModel } from 'models/unit'

type ResponseUnit = Response<{ unit: UnitModel }>
type ResponseUnitsWithFilter = Response<WithFilter<UnitModel>>

type PayloadFilterUnits = RequestWithFilter<{}>

type PayloadUnitOptionsForProductUnitForm = RequestWithFilter<{
  product_id: ID
}>

type PayloadUnitOptionsForProductUnitToUnitForm = RequestWithFilter<{
  product_id: ID
}>

type PayloadUpdateUnit = {
  description: string | null
  name: string
}

type PayloadCreateUnit = {
  description: string | null
  name: string
}

const UNITS_URL = '/units'

const UnitsAPI = {
  filter: (payload: PayloadFilterUnits): Promise<APIResponse<ResponseUnitsWithFilter>> => {
    return APIClient.post(`${UNITS_URL}/filter`, payload)
  },

  create: (payload: PayloadCreateUnit): Promise<APIResponse<ResponseUnit>> => {
    return APIClient.post(UNITS_URL, payload)
  },

  get: (unitId: ID): Promise<APIResponse<ResponseUnit>> => {
    return APIClient.get(`${UNITS_URL}/${unitId}`)
  },

  update: (unitId: ID, payload: PayloadUpdateUnit): Promise<APIResponse<ResponseUnit>> => {
    return APIClient.put(`${UNITS_URL}/${unitId}`, payload)
  },

  delete: (unitId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${UNITS_URL}/${unitId}`)
  },

  optionsForProductUnitForm: (
    payload: PayloadUnitOptionsForProductUnitForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseUnitsWithFilter>> => {
    return APIClient.post(`${UNITS_URL}/options/product-unit-form`, payload, config)
  },

  optionForProductUnitToUnitForm: (
    payload: PayloadUnitOptionsForProductUnitToUnitForm,
    config?: APIRequestConfig
  ): Promise<APIResponse<ResponseUnitsWithFilter>> => {
    return APIClient.post(`${UNITS_URL}/options/product-unit-to-unit-form`, payload, config)
  },
}

export type { PayloadCreateUnit, PayloadFilterUnits, PayloadUpdateUnit }

export default UnitsAPI
