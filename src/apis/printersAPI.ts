import axios, { AxiosResponse } from 'axios'
import { Response, ResponseSuccess } from 'models/apiBase'
import { PrinterDataModel } from 'models/printerData'

const APIClient = axios.create({
  baseURL: process.env.REACT_APP_PRINTER_API_URL,
})

type ResponsePrint = Response<ResponseSuccess>

type PayloadPrintersPrint = {
  printer_data: PrinterDataModel
}

const PrintersAPI = {
  print: async (payload: PayloadPrintersPrint): Promise<AxiosResponse<ResponsePrint>> => {
    return APIClient.post(`/printers`, payload)
  },
}

export default PrintersAPI