import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { Response } from 'models/apiBase'
import { ProductStockModel } from 'models/productStock'

type ResponseProductStock = Response<{ product_stock: ProductStockModel }>

type PayloadAdjustmentProductStock = {
  cost_price: number | null
  qty: number
}

const PRODUCT_STOCKS_URL = '/product-stocks'

const ProductStocksAPI = {
  adjustment: (productStockId: ID, payload: PayloadAdjustmentProductStock): Promise<APIResponse<ResponseProductStock>> => {
    return APIClient.patch(`${PRODUCT_STOCKS_URL}/${productStockId}/adjustment`, payload)
  },

  get: (productStockId: ID): Promise<APIResponse<ResponseProductStock>> => {
    return APIClient.get(`${PRODUCT_STOCKS_URL}/${productStockId}`)
  },
}

export type {
  PayloadAdjustmentProductStock
}

export default ProductStocksAPI
