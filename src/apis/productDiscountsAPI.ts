import APIClient, { APIResponse } from './APIClient'
import { ID } from 'models/base'
import { RequestWithFilter, Response, ResponseSuccess, WithFilter } from 'models/apiBase'
import { ProductDiscountModel } from 'models/productDiscount'

type ResponseProductDiscount = Response<{ product_discount: ProductDiscountModel }>
type ResponseProductDiscountsWithFilter = Response<WithFilter<ProductDiscountModel>>

type PayloadProductDiscountsFilter = RequestWithFilter<{
  is_active: boolean | null
  product_id: boolean | null
}>

type PayloadProductDiscountUpdate = {
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
  minimum_qty: number
}

type PayloadProductDiscountCreate = {
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
  minimum_qty: number
  product_id: ID
}

const PRODUCT_DISCOUNTS_URL = '/product-discounts'

const ProductDiscountsAPI = {
  filter: (payload: PayloadProductDiscountsFilter): Promise<APIResponse<ResponseProductDiscountsWithFilter>> => {
    return APIClient.post(`${PRODUCT_DISCOUNTS_URL}/filter`, payload)
  },

  create: (payload: PayloadProductDiscountCreate): Promise<APIResponse<ResponseProductDiscount>> => {
    return APIClient.post(PRODUCT_DISCOUNTS_URL, payload)
  },

  get: (productDiscountId: ID): Promise<APIResponse<ResponseProductDiscount>> => {
    return APIClient.get(`${PRODUCT_DISCOUNTS_URL}/${productDiscountId}`)
  },

  update: (productDiscountId: ID, payload: PayloadProductDiscountUpdate): Promise<APIResponse<ResponseProductDiscount>> => {
    return APIClient.put(`${PRODUCT_DISCOUNTS_URL}/${productDiscountId}`, payload)
  },

  delete: (productDiscountId: ID): Promise<APIResponse<ResponseSuccess>> => {
    return APIClient.delete(`${PRODUCT_DISCOUNTS_URL}/${productDiscountId}`)
  },
}

export type { PayloadProductDiscountCreate, PayloadProductDiscountUpdate, PayloadProductDiscountsFilter }

export default ProductDiscountsAPI
