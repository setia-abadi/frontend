import DialogActions from "@mui/material/DialogActions";
import { styled } from "@mui/material/styles";

const DialogActionsStyled = styled(DialogActions)(({ theme }) => ({
  padding: theme.spacing(3)
}))

export default DialogActionsStyled