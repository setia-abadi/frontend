export { default } from './Table'
export type { TableProps, ColumnProps } from './Table'