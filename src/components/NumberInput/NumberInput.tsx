import TextInput, { TextInputProps } from 'components/TextInput/TextInput'
import { ChangeEvent, FC } from 'react'
import TypeUtil from 'utils/typeUtil'

type NumberInputProps = Omit<TextInputProps, 'name' | 'value' | 'onChange'> & {
  value: number
  min?: number
  max?: number
  onChange: (value: number) => void
}

const NumberInput: FC<NumberInputProps> = ({ min, max, onChange, ...props }) => {
  const handleChange = (ev: ChangeEvent<HTMLInputElement>) => {
    let value = parseInt(ev.target.value)
    if (TypeUtil.isDefined(min) && value < min) {
      value = min
    }
    if (TypeUtil.isDefined(max) && value > max) {
      value = max
    }
    onChange(value)
  }

  return <TextInput {...props} onChange={handleChange} inputProps={{ type: 'number' }} />
}

export type { NumberInputProps }

export default NumberInput
