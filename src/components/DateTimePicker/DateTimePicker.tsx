import { DateTimePicker as MuiDateTimePicker, DateTimePickerProps } from '@mui/x-date-pickers'
import { IconCalendarEvent, TablerIconProps } from '@tabler/icons'
import { Dayjs } from 'dayjs'
import { FC } from 'react'

type Props = Omit<DateTimePickerProps<Dayjs>, 'renderInput'> & {
  fullWidth?: boolean
}

const DateIcon = (props: TablerIconProps) => {
  return <IconCalendarEvent {...props} size="1.2rem" />
}

const DateTimePicker: FC<Props> = ({ fullWidth, ...props }) => {
  return (
    <MuiDateTimePicker
      {...props}
      format="DD/MM/YYYY hh:mm A"
      slots={{
        openPickerIcon: DateIcon,
      }}
      slotProps={{
        textField: {
          fullWidth: fullWidth,
        },
      }}
    />
  )
}

export type { Props as DateTimePickerProps }

export default DateTimePicker
