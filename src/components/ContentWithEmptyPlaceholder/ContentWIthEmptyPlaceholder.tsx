import EmptyTextPlaceholder from 'components/EmptyTextPlaceholder'
import { FC, PropsWithChildren } from 'react'

type Props = {}

const ContentWithEmptyPlaceholder: FC<PropsWithChildren<Props>> = ({ children }) => {
  if (children) {
    return children
  }

  return <EmptyTextPlaceholder />
}

export default ContentWithEmptyPlaceholder
