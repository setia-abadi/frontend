import L from 'leaflet'
import { FC, PropsWithChildren, useEffect, useMemo, useRef } from 'react'

import 'leaflet/dist/leaflet.css'
import TypeUtil from 'utils/typeUtil'
import { Box, SxProps, Theme } from '@mui/material'

const defaultLatitude = -6.25617443912924
const defaultLongitude = 106.85387450345333

type Props = {
  width: string | number
  height: string | number
  latitude?: number | string
  longitude?: number | string
  sx?: SxProps<Theme>
  className?: string
  zoom?: number
  options?: L.MapOptions
  onLoad?: (map: L.Map) => void
}

const Map: FC<PropsWithChildren<Props>> = ({
  children,
  width,
  height,
  className,
  sx,
  latitude,
  longitude,
  options,
  zoom = 15,
  onLoad,
}) => {
  const mapRef = useRef<L.Map>()
  const containerRef = useRef<HTMLDivElement>(null)

  const parsedLatitude = useMemo<number>(() => {
    if (latitude) {
      if (TypeUtil.isString(latitude)) {
        const parsedValue = parseFloat(latitude)
        if (Number.isNaN(parsedValue)) {
          return 0
        }
        return parsedValue
      }
      return latitude
    }
    return defaultLatitude
  }, [latitude])

  const parsedLongitude = useMemo<number>(() => {
    if (longitude) {
      if (TypeUtil.isString(longitude)) {
        const parsedValue = parseFloat(longitude)
        if (Number.isNaN(parsedValue)) {
          return 0
        }
        return parsedValue
      }
      return longitude
    }
    return defaultLongitude
  }, [longitude])

  useEffect(() => {
    if (!containerRef.current) return

    const m = L.map(containerRef.current, options).setView([parsedLatitude, parsedLongitude], zoom)

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }).addTo(m)

    onLoad?.(m)
    mapRef.current = m

    return () => {
      m.remove()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (mapRef.current) {
      mapRef.current.setZoom(zoom)
      mapRef.current.panTo(L.latLng(parsedLatitude, parsedLongitude))
    }
  }, [zoom, parsedLatitude, parsedLongitude])

  return (
    <Box
      ref={containerRef}
      className={className}
      sx={{
        height,
        width,
        '.leaflet-pane': {
          zIndex: 1,
        },
        ...sx,
      }}
    >
      {children}
    </Box>
  )
}

export default Map
