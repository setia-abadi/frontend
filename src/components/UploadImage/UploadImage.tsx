import { CircularProgress, IconButton } from '@mui/material'
import Box from '@mui/material/Box'
import { styled } from '@mui/material/styles'
import { IconPhotoPlus, IconResize, IconTrash, TablerIcon } from '@tabler/icons'
import Image from 'components/Image'
import ImageCropDialog from 'components/ImageCropDialog'
import { ChangeEvent, useId, useState } from 'react'
import StringUtil from 'utils/stringUtil'

type Props<T> = {
  aspectRatio: number
  initialImageSrc?: string
  imageWidth?: number
  imageHeight?: number
  containerWidth?: string | number
  containerHeight?: string | number
  placeholderText?: string
  placeholderIcon?: TablerIcon
  onChange: (image: string) => void
  select: (data: T) => string
  uploadFn: (file: File) => Promise<T>
}

type ContainerProps = {
  aspectRatio: number
}

const ToolContainer = styled('div')(({ theme }) => ({
  position: 'absolute',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'flex-end',
  top: 0,
  left: 0,
  zIndex: 1,
  padding: theme.spacing(1),
  width: '100%',
  height: '100%',
  transition: 'opacity 300ms',
  backgroundColor: 'rgba(0,0,0,.4)',
  color: theme.palette.common.white,
  opacity: 0,
  '&:hover': {
    opacity: 1,
  },
}))

const ImageContainer = styled(Box)<ContainerProps>(({ theme, aspectRatio }) => ({
  aspectRatio,
  maxWidth: 150,
  position: 'relative',
  border: `1px solid ${theme.palette.grey[300]}`,
  borderRadius: theme.shape.borderRadius,
  overflow: 'hidden',
}))

const Container = styled('label')<ContainerProps>(({ theme, aspectRatio }) => ({
  aspectRatio,
  color: theme.palette.grey[400],
  backgroundColor: theme.palette.grey[100],
  border: `2px dashed ${theme.palette.grey[300]}`,
  borderRadius: theme.shape.borderRadius,
  maxWidth: 150,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  cursor: 'pointer',
}))

const IconUploadImage = styled(IconPhotoPlus)(({ theme }) => ({
  width: '40%',
  height: '40%',
  color: theme.palette.grey[400],
}))

const TextPlaceholder = styled('div')(({ theme }) => ({
  fontSize: theme.typography.pxToRem(12),
  color: theme.palette.grey[400],
  marginTop: theme.spacing(0.5),
}))

const CircularProgressContainer = styled(Box)(({ theme }) => ({
  width: '100%',
  height: '100%',
  position: 'absolute',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  top: 0,
  left: 0,
  zIndex: 1,
  backgroundColor: 'rgba(0,0,0,.4)',
  color: theme.palette.common.white
}))

const UploadImage = <T,>({
  aspectRatio,
  initialImageSrc,
  containerHeight,
  containerWidth,
  imageHeight,
  imageWidth,
  placeholderText = "Pilih Gambar",
  placeholderIcon = IconUploadImage,
  onChange,
  select,
  uploadFn,
}: Props<T>) => {
  const id = useId()
  const [loading, setLoading] = useState<boolean>(false)
  const [image, setImage] = useState<string | undefined>(initialImageSrc)
  const [originalImage, setOriginalImage] = useState<string | undefined>(initialImageSrc)
  const [openCropDialog, setOpenCropDialog] = useState<boolean>(false)
  const Icon = placeholderIcon

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const file = e.target.files.item(0)
      if (file) {
        const imageUrl = URL.createObjectURL(file)
        setOriginalImage(imageUrl)
        setOpenCropDialog(true)
      }
    }
  }

  const resetState = () => {
    setLoading(false)
    setOpenCropDialog(false)
    setOriginalImage(undefined)
    setImage(undefined)
  }

  const handleSaveCroppedImage = async (newImage: Blob | null) => {
    if (newImage) {
      try {
        const imageUrl = URL.createObjectURL(newImage)
        setLoading(true)
        setOpenCropDialog(false)
        setImage(imageUrl)
        const file = new File([newImage], `${StringUtil.uniqueId()}.png`)
        const response = await uploadFn(file)
        const selectedData = select(response)
        onChange(selectedData)
      } catch (err) {
        resetState()
      } finally {
        setLoading(false)
      }
    } else {
      resetState()
      setOpenCropDialog(false)
    }
  }

  return (
    <>
      {image ? (
        <ImageContainer aspectRatio={aspectRatio} sx={{ width: containerWidth, height: containerHeight }}>
          <Image src={image} aspectRatio={aspectRatio} width="100%" height="100%" />
          {loading ? (
            <CircularProgressContainer>
              <CircularProgress color="inherit" size="20%" />
            </CircularProgressContainer>
          ) : (
            <ToolContainer>
              <IconButton color="inherit" onClick={() => setOpenCropDialog(true)}>
                <IconResize size="1.3rem" />
              </IconButton>
              <IconButton color="inherit" onClick={resetState}>
                <IconTrash size="1.3rem" />
              </IconButton>
            </ToolContainer>
          )}
        </ImageContainer>
      ) : (
        <Container aspectRatio={aspectRatio} htmlFor={id} sx={{ width: containerWidth, height: containerHeight }}>
          <input type="file" id={id} hidden onChange={handleInputChange} />
          <Box textAlign="center" p={1}>
            <Icon />
            <TextPlaceholder>{placeholderText}</TextPlaceholder>
          </Box>
        </Container>
      )}

      {originalImage && (
        <ImageCropDialog
          open={openCropDialog}
          imageSrc={originalImage}
          aspectRatio={aspectRatio}
          imageHeight={imageHeight}
          imageWidth={imageWidth}
          onClose={() => setOpenCropDialog(false)}
          onSave={handleSaveCroppedImage}
        />
      )}
    </>
  )
}

export type { Props as UploadImageProps }

export default UploadImage
