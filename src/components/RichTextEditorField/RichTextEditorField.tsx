import RichTextEditor, { RichTextEditorProps } from 'components/RichTextEditor/RichTextEditor';
import { useField } from 'formik';
import { FC } from 'react'
import 'react-quill/dist/quill.snow.css';

type RichTextEditorFieldProps = Omit<RichTextEditorProps, 'value' | 'onChange'> & {
  name: string
  onChange?: RichTextEditorProps['onChange']
}

const RichTextEditorField: FC<RichTextEditorFieldProps> = ({ name, onChange }) => {
  const [field,,helpers] = useField(name)

  const handleChange = (value: string) => {
    helpers.setValue(value)
    if (onChange) {
      onChange(value)
    }
  }

  return (
    <RichTextEditor {...field} onChange={handleChange} />
  )
}


export type { RichTextEditorFieldProps }
export default RichTextEditorField
