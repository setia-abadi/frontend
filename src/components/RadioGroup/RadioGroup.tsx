import MuiRadioGroup, { RadioGroupProps } from '@mui/material/RadioGroup'
import FormControlLabel from 'components/FormControlLabel'
import Radio from 'components/Radio'
import { FC } from 'react'

type RadioOption = {
  label: string
  value: number | string | boolean | null
}

type Props = Omit<RadioGroupProps, 'onChange' | 'value'> & {
  options: RadioOption[]
  value: RadioOption['value']
  disabled?: boolean
  onChange?: (value: RadioOption['value']) => void
}

const RadioGroup: FC<Props> = ({ options, onChange, disabled, ...props }) => {
  return (
    <MuiRadioGroup {...props}>
      {options.map(({ label, value }) => (
        <FormControlLabel
          key={label}
          value={value}
          label={label}
          control={<Radio />}
          disabled={disabled}
          onChange={() => onChange?.(value)}
        />
      ))}
    </MuiRadioGroup>
  )
}

export type { RadioOption, Props as RadioGroupProps }
export default RadioGroup
