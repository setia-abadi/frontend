import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Dialog from '@mui/material/Dialog'
import Button from 'components/Button'
import DialogActions from 'components/DialogActions'
import DialogContent from 'components/DialogContent'
import DialogTitle from 'components/DialogTitle'
import Cropper, { ReactCropperElement } from 'react-cropper'
import { FC, useRef } from 'react'
import 'cropperjs/dist/cropper.css'

type Props = {
  open: boolean
  imageSrc: string
  aspectRatio?: number
  imageWidth?: number
  imageHeight?: number
  onSave: (image: Blob | null) => void
  onClose?: () => void
}

const CropperContainer = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.common.black,
  '& .cropper-modal': {
    opacity: 0.75,
  },
  '& .cropper-point': {
    backgroundColor: theme.palette.common.white,
  },
  '& .cropper-line': {
    backgroundColor: 'transparent',
  },
  '& .cropper-view-box': {
    outline: `1px dashed ${theme.palette.common.white}`,
  },
}))

const ImageCropDialog: FC<Props> = ({ open, imageSrc, aspectRatio, imageWidth, imageHeight, onSave, onClose }) => {
  const cropperRef = useRef<ReactCropperElement>(null)

  const handleSave = () => {
    if (cropperRef.current) {
      cropperRef.current.cropper
        .getCroppedCanvas({
          height: imageHeight,
          width: imageWidth,
        })
        .toBlob(onSave)
    }
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Editor Gambar</DialogTitle>
      <DialogContent sx={{ padding: 0 }}>
        <CropperContainer>
          <Cropper
            ref={cropperRef}
            style={{ maxHeight: 400, width: '100%' }}
            zoomTo={0.5}
            src={imageSrc}
            viewMode={1}
            aspectRatio={aspectRatio}
            minCropBoxHeight={10}
            minCropBoxWidth={10}
            background={false}
            responsive={true}
            autoCropArea={0.8}
            checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
            guides={false}
            center={false}
            highlight={false}
          />
        </CropperContainer>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" fullWidth onClick={handleSave}>
          Simpan
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ImageCropDialog
