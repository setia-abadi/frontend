import { styled } from '@mui/material/styles'
import MuiSelect, { SelectProps } from '@mui/material/Select'
import { FC } from 'react'
import MuiMenuItem, { MenuItemProps } from '@mui/material/MenuItem'
import { IconChevronDown, TablerIconProps } from '@tabler/icons'

type SelectOption = {
  value: MenuItemProps['value']
  label: string
}

type SelectValue = SelectOption['value'] | SelectOption['value'][]

type Props = Omit<SelectProps<SelectValue>, 'value'> & {
  options: SelectOption[]
  value: SelectValue | undefined
}

const StyledMenuItem = styled(MuiMenuItem)(() => ({}))

const ArrowIcon = (props: TablerIconProps) => {
  return <IconChevronDown size="1.2rem" {...props} style={{ top: 'unset', ...props.style }} />
}

const Select: FC<Props> = ({ options, ...props }) => {
  return (
    <MuiSelect<SelectValue> {...props} IconComponent={ArrowIcon}>
      {options.map((option, i) => (
        <StyledMenuItem key={i} value={option.value}>
          {option.label}
        </StyledMenuItem>
      ))}
    </MuiSelect>
  )
}

export type { SelectOption, Props as SelectProps, SelectValue }
export default Select
