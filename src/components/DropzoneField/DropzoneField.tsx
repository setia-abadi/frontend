import Dropzone, { DropzoneProps } from 'components/Dropzone'
import { useField } from 'formik'
import { useEffect, useState } from 'react'

type Props<T> = Omit<DropzoneProps<T>, 'onUploaded' | 'onRemove'> & {
  name: string
  select: (data: T) => string
}

const DropzoneField = <T,>({ name, multiple, select, ...props }: Props<T>) => {
  const [field,, helpers] = useField(name)
  const [value, setValue] = useState<string[]>([])

  const handleUploaded = (response: T) => {
    const path = select(response)
    if (multiple) {
      setValue((prev) => [...prev, path])
    } else {
      setValue([path])
    }
  }

  const handleRemove = (index: number) => {
    if (multiple && field.value) {
      const copyValue = [...field.value]
      copyValue.splice(index, 1)
      helpers.setValue(copyValue)
    } else {
      helpers.setValue([])
    }
  }

  useEffect(() => {
    helpers.setValue(multiple ? value : value[0])
  }, [value])

  return (
    <Dropzone {...props} multiple={multiple} onUploaded={handleUploaded} onRemove={handleRemove} />
  )
}

export default DropzoneField
