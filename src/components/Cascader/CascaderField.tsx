import { FC } from 'react'
import Cascader, { CascaderProps } from './Cascader'
import { useField } from 'formik'

type CascaderFieldProps = Omit<CascaderProps, 'value' | 'onChange'> & {
  name: string
  onChange?: CascaderProps['onChange']
}

const CascaderField: FC<CascaderFieldProps> = ({ name, onChange, ...props }) => {
  const [field, , helpers] = useField(name)

  const handleChange: CascaderProps['onChange'] = (value, selectedOptions) => {
    helpers.setValue(value)

    if (onChange) {
      onChange(value, selectedOptions)
    }
  }

  return <Cascader {...props} value={field.value} onChange={handleChange} />
}

export type { CascaderFieldProps }

export default CascaderField
