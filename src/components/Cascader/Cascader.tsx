import { FC, useMemo } from 'react'
import RcCascader from 'rc-cascader'
import GlobalStyles from '@mui/material/GlobalStyles'
import Box from '@mui/material/Box'
import { GetValueType } from 'rc-cascader/lib/Cascader'
import TextInput from 'components/TextInput'
import 'rc-cascader/assets/list.less'
import arrayTreeFilter from 'array-tree-filter'
import { IconChevronRight } from '@tabler/icons'

type CascaderOption = {
  label: string
  value: string
  disabled?: boolean
  disableCheckbox?: boolean
  children?: CascaderOption[]
}

type CascaderProps = {
  options: CascaderOption[]
  value: string[]
  disabled?: boolean
  fullWidth?: boolean
  onChange: (value: GetValueType<CascaderOption, 'value', false>, selectedOptions: CascaderOption[]) => void
}

const Cascader: FC<CascaderProps> = ({ options, value, disabled, fullWidth, onChange }) => {
  const label = useMemo(() => {
    if (value) {
      return arrayTreeFilter(options, (o, level) => o.value === value[level])
        .map((o) => o.label)
        .join(' > ')
    }
    return ''
  }, [value, options])

  return (
    <Box>
      <GlobalStyles
        styles={(theme) => ({
          '.rc-cascader': {
            '&-dropdown': {
              minHeight: 'auto',
              position: 'absolute',
              background: '#fff',
              boxShadow: theme.shadows[9],
              borderRadius: theme.shape.borderRadius,
              zIndex: 1,

              '&-hidden': {
                display: 'none',
              },
            },

            '&-menus': {
              display: 'flex',
              flexWrap: 'nowrap',
            },

            '&-menu': {
              flex: 'none',
              margin: 0,
              listStyle: 'none',
              height: 180,
              minWidth: 100,
              overflow: 'auto',
              padding: theme.spacing(1),
              borderLeft: `1px solid ${theme.palette.grey[300]}`,

              '&:first-child': {
                borderLeft: 0,
              },

              '&-item': {
                display: 'flex',
                alignItems: 'center',
                flexWrap: 'nowrap',
                cursor: 'pointer',
                padding: '7px 10px',
                borderRadius: theme.shape.borderRadius,

                '&:hover': {
                  background: theme.palette.grey[100],
                },

                '&-selected': {
                  background: theme.palette.primary.light,
                },

                '&-active': {
                  background: theme.palette.primary.light,
                },

                '&-disabled': {
                  opacity: 0.5,
                },

                '&-content': {
                  flexGrow: 1,
                },

                '&-expand-icon': {
                  marginLeft: 10,
                  display: 'flex',
                  color: theme.palette.grey[400]
                },
              },
            },

            '&-checkbox': {
              position: 'relative',
              display: 'block',
              flex: 'none',
              width: 20,
              height: 20,
              border: '1px solid blue',

              '&::after': {
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                content: '""',
              },

              '&-checked::after': {
                content: '"✔"',
              },

              '&-indeterminate::after': {
                content: '"➖"',
              },
            },
          },
        })}
      />
      <RcCascader value={value} options={options} disabled={disabled} onChange={onChange} expandIcon={<IconChevronRight size="1.2rem" />}>
        <TextInput value={label} fullWidth={fullWidth} />
      </RcCascader>
    </Box>
  )
}

export type { CascaderProps, CascaderOption }

export default Cascader
