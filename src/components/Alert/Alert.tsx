import MuiAlert from "@mui/material/Alert";
import { styled } from "@mui/material/styles";

const AlertStyled = styled(MuiAlert)(({ theme }) => ({
  ...theme.typography.body1
}))

export default AlertStyled