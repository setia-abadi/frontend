import TextInput, { TextInputProps } from 'components/TextInput/TextInput'
import { FC, useId } from 'react'
import { useField } from 'formik'

type NumberInputFieldProps = Omit<TextInputProps, 'name'> & {
  name: string
}

const NumberInputField: FC<NumberInputFieldProps> = ({ name, ...props }) => {
  const id = useId()
  const [field] = useField(name)

  return <TextInput key={`${id}-${name}`} {...props} {...field} inputProps={{ type: 'number' }} />
}

export type { NumberInputFieldProps }
export default NumberInputField
