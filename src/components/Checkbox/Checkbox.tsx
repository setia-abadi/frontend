import MuiCheckbox, { CheckboxProps } from '@mui/material/Checkbox'
import { styled } from '@mui/material/styles'
import { FC } from 'react'

type Props = CheckboxProps

const Checkbox: FC<Props> = (props) => {
  const Icon = styled('span')(({ theme }) => ({
    borderRadius: 3,
    width: 19,
    height: 19,
    boxShadow: `${theme.palette.grey[300]} 0px 0px 0px 1px inset`,
    backgroundColor: 'transparent',
  }))

  return <MuiCheckbox icon={<Icon />} {...props} />
}

export default Checkbox
