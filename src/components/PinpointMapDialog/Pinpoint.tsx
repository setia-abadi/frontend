import { styled, useTheme } from '@mui/material'
import clsx from 'clsx'

type Props = {
  elevated?: boolean
}

const PinpointStyled = styled('div')`
  z-index: 2;
  position: absolute;
  top: calc(50% - 42px);
  left: 50%;
  transform: translateX(-50%);

  &::before {
    content: '';
    position: absolute;
    width: 48px;
    height: 16px;
    background-image: url("data:image/svg+xml;utf8,<svg width='16' height='11' viewBox='0 0 16 11' xmlns='http://www.w3.org/2000/svg'><defs><filter x='-25%' y='-42.9%' width='150%' height='185.7%' filterUnits='objectBoundingBox' id='a'><feGaussianBlur stdDeviation='1' in='SourceGraphic'/></filter></defs><ellipse filter='url(%23a)' cx='6' cy='3.5' rx='6' ry='3.5' transform='translate(2, 2)' opacity='.2'/></svg>");
    background-position: center bottom;
    background-repeat: no-repeat;
    z-index: 99;
    top: 100%;
    transform: translate(0px, -100%);
    left: 0px;
  }

  .icon {
    position: relative;
    transition: transform 300ms cubic-bezier(0.63, 0.01, 0.29, 1) 0s;
    z-index: 100;
    transform: translate(0px, 0px);
  }

  &.elevated {
    .icon {
      transform: translate(0px, -12px);
    }
  }
`

const Pinpoint: React.FC<Props> = ({ elevated }) => {
  const theme = useTheme()

  return (
    <PinpointStyled className={clsx({ elevated })}>
      <svg className="icon" viewBox="0 0 24 24" width="48" height="48" fill={theme.palette.primary.main}>
        <path d="M12 2.24A7.67 7.67 0 0 0 4.25 10c0 7.36 7.08 11.48 7.38 11.66a.81.81 0 0 0 .74 0c.3-.18 7.38-4.3 7.38-11.66A7.669 7.669 0 0 0 12 2.24ZM12 13a3 3 0 1 1 0-6.001A3 3 0 0 1 12 13Z"></path>
      </svg>
    </PinpointStyled>
  )
}

export default Pinpoint
