import L from 'leaflet'
import Dialog from '@mui/material/Dialog'
import Map from 'components/Map'
import React from 'react'
import Button from 'components/Button'
import Box from '@mui/material/Box'
import Pinpoint from './Pinpoint'
import DialogTitle from 'components/DialogTitle'
import DialogContent from 'components/DialogContent'
import DialogActions from 'components/DialogActions'
import DialogContentText from '@mui/material/DialogContentText'

type Props = {
  open: boolean
  dialogTitle: React.ReactNode
  dialogSubTitle?: React.ReactNode
  latitude?: number
  longitude?: number
  onSelected: (latitude: number, longitude: number) => void
  onClose: () => void
}

const PinpointMapDialog: React.FC<Props> = ({
  open,
  dialogTitle,
  dialogSubTitle,
  latitude,
  longitude,
  onSelected,
  onClose,
}) => {
  const [map, setMap] = React.useState<L.Map>()
  const [isMoving, setIsMoving] = React.useState<boolean>(false)

  const handleMapLoad = (mapInstance: L.Map) => {
    setMap(mapInstance)
  }

  const handleSelectLocation = () => {
    if (map) {
      const { lat, lng } = map.getCenter()
      onSelected(lat, lng)
    }
  }

  React.useEffect(() => {
    const handleMoveStart = () => {
      setIsMoving(true)
    }

    const handleMoveEnd = () => {
      setIsMoving(false)
    }

    map?.addEventListener('movestart', handleMoveStart)
    map?.addEventListener('moveend', handleMoveEnd)

    return () => {
      map?.removeEventListener('movestart', handleMoveStart)
      map?.removeEventListener('moveend', handleMoveEnd)
    }
  }, [map])

  return (
    <Dialog fullWidth maxWidth={'md'} open={open} onClose={onClose}>
      <DialogTitle>{dialogTitle}</DialogTitle>

      <DialogContent sx={{ py: 0 }}>
        {dialogSubTitle && <DialogContentText>{dialogSubTitle}</DialogContentText>}

        <Box mt={2}>
          <Map height={200} latitude={latitude} longitude={longitude} width="100%" onLoad={handleMapLoad}>
            <Pinpoint elevated={isMoving} />
          </Map>
        </Box>
      </DialogContent>

      <DialogActions>
        <Button variant="outlined" onClick={onClose}>
          Tutup
        </Button>

        <Button variant="contained" disabled={isMoving} onClick={handleSelectLocation}>
          Pilih Lokasi
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default PinpointMapDialog
