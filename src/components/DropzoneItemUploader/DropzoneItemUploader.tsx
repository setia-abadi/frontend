import { APIRequestConfig } from 'apis/APIClient'
import { AxiosProgressEvent, isCancel } from 'axios'
import { ReactNode, useCallback, useEffect, useState } from 'react'
import APIUtil from 'utils/APIUtil'
import TypeUtil from 'utils/typeUtil'

type Props<T> = {
  file: File
  children: (file: File, isUploading: boolean, progressPercentage: number, error?: string) => ReactNode
  uploadFn: (file: File, config: APIRequestConfig) => Promise<T>
  onUploadSuccess: (response: T) => void
}

const DropzoneItemUploader = <T,>({ file, uploadFn, onUploadSuccess, children }: Props<T>) => {
  const [error, setError] = useState<string>()
  const [loading, setLoading] = useState<boolean>(false)
  const [percent, setPercent] = useState<number>(0)

  const handleUploadProgress = (progressEvent: AxiosProgressEvent) => {
    if (TypeUtil.isDefined(progressEvent.progress)) {
      const percentCompleted = progressEvent.progress * 100
      setPercent(percentCompleted)
      setLoading(percentCompleted < 100)
    }
  }

  const handleUpload = useCallback(
    async (signal: AbortSignal) => {
      try {
        setError(undefined)
        const response = await uploadFn(file, {
          signal,
          onUploadProgress: handleUploadProgress,
        })
        onUploadSuccess(response)
      } catch (err) {
        if (APIUtil.isAPIError(err) && isCancel(err)) {
          // Don't show error canceled
        } else if (APIUtil.isAPIError(err) && err.response?.data.message) {
          setError(err.response.data.message)
        } else {
          setError(String(err))
        }
      } finally {
        setLoading(false)
      }
    },
    [file]
  )

  useEffect(() => {
    const abortController = new AbortController()
    handleUpload(abortController.signal)
    return () => {
      abortController.abort()
    }
  }, [handleUpload])

  return <>{children(file, loading, percent, error)}</>
}

export default DropzoneItemUploader
