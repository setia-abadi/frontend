import { useTheme } from '@mui/material/styles'
import { IconSearch } from '@tabler/icons'
import TextInput from 'components/TextInput'
import useDebounce from 'hooks/useDebounce'

import { ChangeEvent, FC, useState } from 'react'
import TypeUtil from 'utils/typeUtil'

type Props = {
  initialValue?: string | null
  placeholder?: string
  onChange: (value: string | null) => void
}

const SearchBar: FC<Props> = ({ initialValue, placeholder, onChange }) => {
  const theme = useTheme()
  const [value, setValue] = useState<string | null>(initialValue ?? null)

  const debouncedChange = useDebounce((searchTerm: string) => {
    onChange(TypeUtil.isEmpty(searchTerm) ? null : searchTerm)
  }, 1000)

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    setValue(value)
    debouncedChange(value)
  }

  return (
    <TextInput
      size="small"
      value={value}
      onChange={handleChange}
      placeholder={placeholder ?? 'Cari dengan kata kunci'}
      InputProps={{
        startAdornment: <IconSearch color={theme.palette.grey[400]} />,
      }}
    />
  )
}

export default SearchBar
