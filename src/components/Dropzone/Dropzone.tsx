import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Stack from '@mui/material/Stack'
import { styled } from '@mui/material/styles'
import { Accept, FileRejection, useDropzone } from 'react-dropzone'
import Typography from 'components/Typography'
import { IconCloudUpload } from '@tabler/icons'
import { useSnackbar } from 'notistack'
import { APIRequestConfig } from 'apis/APIClient'
import DropzoneItem from 'components/DropzoneItem'
import DropzoneItemUploader from 'components/DropzoneItemUploader'
import { useState } from 'react'
import StringUtil from 'utils/stringUtil'

type Props<T> = {
  accept?: Accept
  multiple?: boolean
  disabled?: boolean
  uploadFn: (file: File, config: APIRequestConfig) => Promise<T>
  onUploaded: (responses: T) => void
  onRemove: (index: number) => void
}

const PaperStyled = styled(Paper)(({ theme }) => ({
  boxShadow: theme.shadows[0],
  border: `2px dashed ${theme.palette.grey[300]}`,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: theme.palette.grey[100],
  minHeight: 150,
  color: theme.palette.grey[400],
  transition: 'all 300ms',
  padding: theme.spacing(2),
  '&:hover': {
    cursor: 'pointer',
    backgroundColor: theme.palette.grey[200],
  },
}))

const Dropzone = <T,>({ accept, multiple, disabled, uploadFn, onUploaded, onRemove }: Props<T>) => {
  const snackbar = useSnackbar()
  const [unuploadFiles, setUnuploadFiles] = useState<Record<string, File>>({})
  const [uploadedFiles, setUploadedFiles] = useState<File[]>([])

  const handleDropAccepted = (files: File[]) => {
    setUnuploadFiles((prev) => {
      const newFiles = files.reduce((prev, file) => {
        prev[StringUtil.uniqueId()] = file
        return prev
      }, Object())
      return { ...prev, ...newFiles }
    })
  }

  const handleDropRejected = (fileRejections: FileRejection[]) => {
    fileRejections.forEach((file) => {
      snackbar.enqueueSnackbar(file.errors.map((error) => error.message).join(), { variant: 'error' })
    })
  }

  const handleError = (err: Error) => {
    snackbar.enqueueSnackbar(String(err))
  }

  const handleUploadSuccess = (key: string, file: File, response: T) => {
    removeUnuploadedFile(key)
    setUploadedFiles((prev) => [...prev, file])
    onUploaded(response)
  }

  const removeUnuploadedFile = (key: string) => {
    setUnuploadFiles((prev) => {
      const copyPrev = { ...prev }
      delete copyPrev[key]
      return copyPrev
    })
  }

  const removeUploadedFile = (index: number) => {
    setUploadedFiles((prev) => {
      const copy = [...prev]
      copy.splice(index, 1)
      return copy
    })
  }

  const handleDeleteBtnClick = (index: number) => {
    removeUploadedFile(index)
    onRemove(index)
  }

  const { getRootProps, getInputProps } = useDropzone({
    accept,
    multiple,
    disabled,
    onDropAccepted: handleDropAccepted,
    onDropRejected: handleDropRejected,
    onError: handleError,
  })

  const unuploadedFileKeys = Object.keys(unuploadFiles)

  return (
    <>
      <PaperStyled {...getRootProps()}>
        <input {...getInputProps()} />
        <Stack spacing={1} alignItems="center">
          <IconCloudUpload size="2.5rem" />
          <Typography variant="body1" textAlign="center">
            Tarik dan lepaskan beberapa file di sini, atau klik untuk memilih file
          </Typography>
        </Stack>
      </PaperStyled>

      {(unuploadedFileKeys.length > 0 || uploadedFiles.length > 0) && (
        <Box mt={2}>
          <Typography variant="body1" fontWeight={600}>
            Unggahan:
          </Typography>
          <Stack spacing={1} mt={2}>
            {unuploadedFileKeys.map((key) => (
              <DropzoneItemUploader
                key={key}
                file={unuploadFiles[key]}
                uploadFn={uploadFn}
                onUploadSuccess={(response) => handleUploadSuccess(key, unuploadFiles[key], response)}
              >
                {(file, loading, progress, error) => (
                  <DropzoneItem
                    fileName={file.name}
                    fileSizeByte={file.size}
                    isUploading={loading}
                    progressPercentage={progress}
                    error={error}
                    onCancelBtnClick={() => removeUnuploadedFile(key)}
                  />
                )}
              </DropzoneItemUploader>
            ))}
            {uploadedFiles.map((file, index) => (
              <DropzoneItem
                key={index}
                fileName={file.name}
                fileSizeByte={file.size}
                onDeleteBtnClick={() => handleDeleteBtnClick(index)}
              />
            ))}
          </Stack>
        </Box>
      )}
    </>
  )
}

export type { Props as DropzoneProps }

export default Dropzone
