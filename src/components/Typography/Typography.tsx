import Typography from '@mui/material/Typography'
import { styled } from '@mui/material/styles'

type Props = {
  maxLine?: number
}

const TypographyStyled = styled(Typography, { shouldForwardProp: (propName) => propName !== 'maxLine' })<Props>(
  ({ maxLine }) => ({
    ...(maxLine && {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      ...(maxLine > 1 && {
        display: '-webkit-box',
        WebkitLineClamp: maxLine,
        WebkitBoxOrient: 'vertical',
      })
    }),
  })
)

export default TypographyStyled
