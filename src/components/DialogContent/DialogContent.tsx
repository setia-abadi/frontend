import { DialogContent } from "@mui/material";
import { styled } from "@mui/material/styles";

const DialogContentStyled = styled(DialogContent)(({ theme }) => ({
  padding: `${theme.spacing(2.5)} ${theme.spacing(3)}`
}))

export default DialogContentStyled