import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import { FC } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'

type RichTextEditorProps = {
  value: string
  onChange: (value: string) => void
}

const Container = styled(Box)(({ theme }) => ({
  '.ql-toolbar.ql-snow': {
    ...theme.typography.body1,
    border: `1px solid ${theme.palette.grey[300]}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
  },

  '.ql-container.ql-snow': {
    ...theme.typography.body1,
    border: `1px solid ${theme.palette.grey[300]}`,
    borderBottomLeftRadius: theme.shape.borderRadius,
    borderBottomRightRadius: theme.shape.borderRadius,
  },

  '.ql-editor': {
    minHeight: 200,
  }
}))

const RichTextEditor: FC<RichTextEditorProps> = ({ value, onChange }) => {
  const modules: Record<string, any> = {
    toolbar: [
      [{ header: [false] }],
      ['bold', 'italic', 'underline',],
      [{ list: 'ordered' }, { list: 'bullet' }],
      ['clean'],
    ],
  }

  return (
    <Container>
      <ReactQuill theme="snow" value={value} modules={modules} onChange={onChange} />
    </Container>
  )
}

export type { RichTextEditorProps }

export default RichTextEditor
