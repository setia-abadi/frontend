import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import MuiFormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel'
import { FC, PropsWithChildren } from 'react'

type Props = FormControlLabelProps

const FormControlLabelStyled = styled(MuiFormControlLabel)(() => ({
}))

const FormControlLabel: FC<PropsWithChildren<Props>> = ({ children, ...props }) => {
  return (
    <FormControlLabelStyled {...props}>
      <Typography variant="subtitle1" fontWeight={600}>
        {children}
      </Typography>
    </FormControlLabelStyled>
  )
}

export default FormControlLabel
