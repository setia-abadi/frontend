import RadioGroup, { RadioGroupProps, RadioOption } from 'components/RadioGroup'
import { useField } from 'formik'
import { FC } from 'react'

type Props = Omit<RadioGroupProps, 'name' | 'value'> & {
  name: string
}

const RadioGroupField: FC<Props> = ({ name, onChange, ...props }) => {
  const [field, , helpers] = useField(name)

  const handleChange = (value: RadioOption['value']) => {
    helpers.setValue(value)
    onChange?.(value)
  }

  return <RadioGroup {...field} {...props} onChange={handleChange} />
}

export default RadioGroupField
