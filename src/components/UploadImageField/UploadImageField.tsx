import UploadImage, { UploadImageProps } from 'components/UploadImage/UploadImage'
import { useField } from 'formik'

type Props<T> = Omit<UploadImageProps<T>, 'onChange'> & {
  name: string
}

const UploadImageField = <T, >({ name, ...props }: Props<T>) => {
  const [,, helpers] = useField(name)

  const handleChange = (value: string) => {
    helpers.setValue(value)
  }

  return (
    <UploadImage {...props} onChange={handleChange} />
  )
}

export default UploadImageField
