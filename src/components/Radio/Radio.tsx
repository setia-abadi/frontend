import { styled } from '@mui/material/styles'
import MuiRadio, { RadioProps } from '@mui/material/Radio'
import { FC } from 'react'

type Props = RadioProps

const Icon = styled('span')(() => ({
  position: 'relative',
  borderRadius: '50%',
  width: 21,
  height: 21,
  boxShadow: 'rgb(223, 229, 239) 0px 0px 0px 1px inset',
  backgroundColor: 'transparent',
}))

const CheckedIcon = styled(Icon)(({ theme }) => ({
  boxShadow: 'none',
  backgroundColor:  theme.palette.primary.main,
  '&:before': {
    content: '""',
    display: 'block',
    width: 21,
    height: 21,
    backgroundImage: 'radial-gradient(rgb(255, 255, 255), rgb(255, 255, 255) 28%, transparent 32%)',
  },
}))

const Radio: FC<Props> = (props) => {
  return <MuiRadio icon={<Icon />} checkedIcon={<CheckedIcon />} {...props} />
}

export default Radio
