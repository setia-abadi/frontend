import Select, { SelectProps } from 'components/Select/Select'
import { useField } from 'formik'
import { FC } from 'react'

type Props = Omit<SelectProps, 'value'> & {
  name: string
}

const SelectField: FC<Props> = ({ name, onChange, ...props }) => {
  const [field, , helpers] = useField(name)

  const handleChange: SelectProps['onChange'] = (event, child) => {
    helpers.setValue(event.target.value)
    onChange?.(event, child)
  }

  return <Select {...props} {...field} onChange={handleChange} />
}

export type { Props as SelectFieldProps }

export default SelectField
