import { FC } from 'react'

type Props = {
  value: number
}
const formatter = new Intl.NumberFormat('id-ID', { maximumFractionDigits: 2 })

const FormattedNumber: FC<Props> = ({ value }) => {
  return <>{formatter.format(value)}</>
}

export default FormattedNumber
