import DateTimePicker, { DateTimePickerProps } from 'components/DateTimePicker/DateTimePicker'
import { Dayjs } from 'dayjs'
import { useField } from 'formik'
import { FC } from 'react'

type DateTimePickerFieldProps = Omit<DateTimePickerProps, 'value' | 'onChange'> & {
  name: string
  onChange?: (date: Dayjs | null) => void
}

const DateTimePickerField: FC<DateTimePickerFieldProps> = ({ name, onChange, ...props }) => {
  const [field, , helpers] = useField(name)

  const handleChange = (value: Dayjs | null) => {
    helpers.setValue(value)
    onChange?.(value)
  }

  return <DateTimePicker {...field} {...props} onChange={handleChange} />
}

export type { DateTimePickerFieldProps }
export default DateTimePickerField
