import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import Stack from '@mui/material/Stack'
import { styled } from '@mui/material/styles'
import { IconFile, IconTrash, IconX } from '@tabler/icons'
import Paper from 'components/Paper'
import Typography from 'components/Typography'
import { FC } from 'react'
import FileUtil from 'utils/fileUtil'
import TypeUtil from 'utils/typeUtil'

type Props = {
  fileName: string
  fileSizeByte: number
  progressPercentage?: number
  error?: string
  isUploading?: boolean
  isPending?: boolean
  onDeleteBtnClick?: () => void
  onCancelBtnClick?: () => void
}

const PaperStyled = styled(Paper)(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  border: `1px solid ${theme.palette.grey[300]}`,
  backgroundColor: 'transparent',
  padding: theme.spacing(1.2),
  overflow: 'hidden',
}))

const ItemIcon = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.grey[100],
  width: 50,
  height: 50,
}))

const Icon = styled(IconFile)(({ theme }) => ({
  color: theme.palette.grey[400],
  width: '50%',
}))

const FileDescription = styled(Box)(({ theme }) => ({
  fontSize: theme.typography.pxToRem(13),
  color: theme.palette.grey[400],
}))

const FileError = styled(Box)(({ theme }) => ({
  fontSize: theme.typography.pxToRem(13),
  color: theme.palette.error.main,
}))

const Progress = styled(Box)<{ percentage: number }>(({ theme, percentage }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  height: '100%',
  width: '100%',
  zIndex: 1,
  maxWidth: `${percentage}%`,
  backgroundColor: theme.palette.primary.light,
  transition: 'max-width 300ms',
}))

const DropzoneItem: FC<Props> = ({
  fileName,
  fileSizeByte,
  isUploading,
  progressPercentage,
  error,
  onCancelBtnClick,
  onDeleteBtnClick,
}) => {
  return (
    <PaperStyled elevation={0}>
      <Stack direction="row" alignItems="center" spacing={1.5} position="relative" zIndex={2}>
        <ItemIcon>
          <Icon />
        </ItemIcon>

        <Box flexGrow={1} width={10}>
          <Typography variant="body1" fontWeight={600} maxLine={1}>
            {fileName}
          </Typography>
          {error ? (
            <FileError>{error}</FileError>
          ) : (
            <FileDescription>{isUploading ? 'Mengunggah...' : FileUtil.relativeSize(fileSizeByte)}</FileDescription>
          )}
        </Box>

        <Box>
          {isUploading || error ? (
            <IconButton onClick={onCancelBtnClick}>
              <IconX size="1.2rem" />
            </IconButton>
          ) : (
            <IconButton onClick={onDeleteBtnClick}>
              <IconTrash size="1.2rem" />
            </IconButton>
          )}
        </Box>
      </Stack>

      {isUploading && TypeUtil.isDefined(progressPercentage) && <Progress percentage={progressPercentage} />}
    </PaperStyled>
  )
}

export default DropzoneItem
