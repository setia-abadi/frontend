import { styled } from '@mui/material/styles'
import CardHeader from '@mui/material/CardHeader'

const StyledCardHeader = styled(CardHeader)(({ theme }) => ({
  padding: `${theme.spacing(2)} ${theme.spacing(3)}`
}))

export default StyledCardHeader
