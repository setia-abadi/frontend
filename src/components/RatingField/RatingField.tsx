import Rating, { RatingProps } from '@mui/material/Rating'
import { useField } from 'formik'
import { FC } from 'react'

type Props = Omit<RatingProps, 'name'> & {
  name: string
}

const RatingField: FC<Props> = ({ name, onChange, ...props }) => {
  const [field, , helpers] = useField(name)

  const handleChange = (event: React.SyntheticEvent, value: number | null) => {
    helpers.setValue(value)
    if (onChange) {
      onChange(event, value)
    }
  }

  return <Rating {...field} {...props} onChange={handleChange} />
}

export default RatingField
