import IconButton from '@mui/material/IconButton'
import Stack from '@mui/material/Stack'
import { styled } from '@mui/material/styles'
import { IconMinus, IconPlus } from '@tabler/icons'
import TextInput from 'components/TextInput'
import { ChangeEvent, FC } from 'react'
import TypeUtil from 'utils/typeUtil'

type Props = {
  value: number
  min?: number
  max?: number
  onChange: (value: number) => void
}

const InputStyled = styled(TextInput)(({ theme }) => ({
  '& .MuiOutlinedInput-input': {
    padding: `${theme.spacing(0.7)} ${theme.spacing(1)}`,
    textAlign: 'center',
  }
}))

const IconButtonStyled = styled(IconButton)(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  color: theme.palette.primary.contrastText,
  '&:hover': {
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.primary.contrastText,
  }
}))

const InputDialer: FC<Props> = ({ value, min, max, onChange }) => {
  const isIncreaseable = !TypeUtil.isDefined(max) || value < max
  const isDecreaseable = !TypeUtil.isDefined(min) || value > min

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const nextValue = parseInt(e.target.value)
    onChange(nextValue)
  }

  const handleIncrease = () => {
    if (isIncreaseable) {
      onChange(value + 1)
    }
  }

  const handleDecrease = () => {
    if (isDecreaseable) {
      onChange(value - 1)
    }
  }

  return (
    <Stack direction="row" alignItems="center" spacing={0.5}>
      <IconButtonStyled size="small" disabled={!isDecreaseable} onClick={handleDecrease}>
        <IconMinus size="1rem" />
      </IconButtonStyled>
      <InputStyled
        size="small"
        value={value}
        inputProps={{
          pattern: '[0-9]{1,5}'
        }}
        onChange={handleInputChange}
        sx={{ width: 45 }}
      />
      <IconButtonStyled size="small" disabled={!isIncreaseable} onClick={handleIncrease}>
        <IconPlus size="1rem" />
      </IconButtonStyled>
    </Stack>
  )
}

export default InputDialer
