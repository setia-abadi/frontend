import Chip from 'components/Chip'
import { FC } from 'react'

type Props = {
  isActive: boolean
}

const StatusChip: FC<Props> = ({ isActive }) => {
  const label = isActive ? 'Aktif' : 'Tidak Aktif'

  return <Chip label={label} size="small" color={isActive ? 'success' : 'error'} />
}

export default StatusChip
