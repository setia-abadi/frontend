import { styled } from '@mui/material/styles'
import CardContent from '@mui/material/CardContent'

const StyledCardContent = styled(CardContent)(({ theme }) => ({
  padding: theme.spacing(3)
}))

export default StyledCardContent
