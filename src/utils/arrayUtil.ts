import NumberUtil from "./numberUtil"

const ArrayUtil = {
  pickRandom: <T>(records: T[]): T => {
    return records[NumberUtil.random(0, records.length)]
  }
}

export default ArrayUtil