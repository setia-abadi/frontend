const NumberUtil = {
  random: (min: number, max: number) => {
    const random = Math.floor(Math.random() * max)
    if (random < min) {
      return min
    }
    return random
  }
}

export default NumberUtil