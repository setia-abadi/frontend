const FileUtil = {
  getType: (filename: string): string | null => {
    const data = filename.split('.')
    if (data.length > 1) {
      return '.' + data[data.length - 1]
    }
    return null
  },

  relativeSize: (byte: number): string => {
    const i = byte === 0 ? 0 : Math.floor(Math.log(byte) / Math.log(1024))
    return (parseFloat((byte / Math.pow(1024, i)).toFixed(2)) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i])
  },

  isAllowed: (fileType: string, allowedFileType: Array<string>): boolean => {
    let result = false
    for (let i = 0; i < allowedFileType.length; i++) {
      if (allowedFileType[i] === fileType.toLowerCase()) {
        result = true
        break
      }
    }
    return result
  },

  getFilenameFromContentDisposition: (header: string): string | null => {
    const match = header.match(/filename=(?:["']?)([^"']+)["]?/)
    if (!match || match.length < 0) {
      return null
    }
    return match[1]
  },

  download: (blob: Blob, filename: string) => {
    const tempURL = URL.createObjectURL(blob)
    const anchorElement = document.createElement('a')

    anchorElement.setAttribute('href', tempURL)
    anchorElement.setAttribute('download', filename)
    anchorElement.click()
    anchorElement.remove()
    URL.revokeObjectURL(tempURL)
  }
}

export default FileUtil
