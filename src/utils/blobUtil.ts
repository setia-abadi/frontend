const BlobUtil = {
  toJson: async <T>(blob: Blob): Promise<T | null> => {
    if (blob.type === 'application/json') {
      const text = await blob.text()
      return JSON.parse(text)
    }
    return null
  }
}

export default BlobUtil