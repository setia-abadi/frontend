import {
  IconBoxSeam,
  IconBrandWhatsapp,
  IconCashBanknote,
  IconClipboardText,
  IconClockPlay,
  IconComponents,
  IconDiscount2,
  IconFileInvoice,
  IconLayoutDashboard,
  IconMessageReport,
  IconPaperBag,
  IconPoint,
  IconReportMoney,
  IconShoppingCart,
  IconStackPush,
  IconTruckDelivery,
  IconTruckReturn,
  IconUsers,
  TablerIcon,
} from '@tabler/icons'
import { PermissionEnum } from 'enums/permissionEnum'
import StringUtil from 'utils/stringUtil'

import { PagePermission } from './pagePermission'

type MenuItem = {
  type: 'item'
  id: string
  title: string
  icon?: TablerIcon
  href?: string
  disabled?: boolean
  childrens?: MenuItem[]
  permissions?: PermissionEnum[]
}

type MenuGroup = {
  type: 'group'
  navlabel?: boolean
  title: string
  childrens: MenuItem[]
}

const Menuitems: MenuGroup[] = [
  {
    type: 'group',
    navlabel: true,
    title: 'Home',
    childrens: [
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Dashboard',
        icon: IconLayoutDashboard,
        href: '/dashboard',
        permissions: PagePermission.DASHBOARD,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Sesi Whatsapp',
        icon: IconBrandWhatsapp,
        href: '/whatsapp-session',
        permissions: PagePermission.WHATSAPP_SESSION,
      },
    ],
  },
  {
    type: 'group',
    navlabel: true,
    title: 'Resource',
    childrens: [
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Pengguna',
        icon: IconUsers,
        href: '/users',
        permissions: PagePermission.USER_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Pelanggan',
        icon: IconUsers,
        childrens: [
          {
            type: 'item',
            id: StringUtil.uniqueId(),
            title: 'Jenis Pelanggan',
            icon: IconPoint,
            href: '/customer-types',
            permissions: PagePermission.CUSTOMER_TYPE_LIST,
          },
          {
            type: 'item',
            id: StringUtil.uniqueId(),
            title: 'Pelanggan',
            icon: IconPoint,
            href: '/customers',
            permissions: PagePermission.CUSTOMER_LIST,
          },
        ],
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Supplier',
        icon: IconUsers,
        childrens: [
          {
            type: 'item',
            id: StringUtil.uniqueId(),
            title: 'Jenis Supplier',
            icon: IconPoint,
            href: '/supplier-types',
            permissions: PagePermission.SUPPLIER_TYPE_LIST,
          },
          {
            type: 'item',
            id: StringUtil.uniqueId(),
            title: 'Supplier',
            icon: IconPoint,
            href: '/suppliers',
            permissions: PagePermission.SUPPLIER_LIST,
          },
        ],
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Piutang',
        icon: IconReportMoney,
        href: '/receivables',
        permissions: PagePermission.CUSTOMER_DEBT_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Hutang',
        icon: IconReportMoney,
        href: '/debts',
        permissions: PagePermission.DEBT_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Saldo',
        icon: IconCashBanknote,
        href: '/balances',
        permissions: PagePermission.BALANCE_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Satuan',
        icon: IconComponents,
        href: '/units',
        permissions: PagePermission.UNIT_LIST,
      },
    ],
  },
  {
    type: 'group',
    navlabel: true,
    title: 'Kasir',
    childrens: [
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Sesi Kasir',
        icon: IconClockPlay,
        href: '/cashier-sessions',
        permissions: PagePermission.CASHIER_SESSION_SESSION,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Laporan Sesi Kasir',
        icon: IconClipboardText,
        href: '/cashier-sessions/report',
        permissions: PagePermission.CASHIER_SESSION_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Keranjang',
        icon: IconShoppingCart,
        href: '/cart',
        permissions: PagePermission.CASHIER_SESSION_SESSION,
      },
    ],
  },
  {
    type: 'group',
    navlabel: true,
    title: 'Produk',
    childrens: [
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Produk',
        icon: IconBoxSeam,
        href: '/products',
        permissions: PagePermission.PRODUCT_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Diskon Produk',
        icon: IconDiscount2,
        href: '/product-discounts',
        permissions: PagePermission.PRODUCT_DISCOUNT_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Pemesanan Produk',
        icon: IconPaperBag,
        href: '/purchase-orders',
        permissions: PagePermission.PURCHASE_ORDER_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Penerimaan Produk',
        icon: IconStackPush,
        href: '/product-receives',
        permissions: PagePermission.PRODUCT_RECEIVE_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Retur Produk',
        icon: IconTruckReturn,
        href: '/product-returns',
        permissions: PagePermission.PRODUCT_RETURN_LIST,
      },
    ],
  },
  {
    type: 'group',
    title: 'Pesanan',
    childrens: [
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Pesanan Toko',
        icon: IconFileInvoice,
        href: '/shop-orders',
        permissions: PagePermission.SHOP_ORDER_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Pengiriman Pesanan',
        icon: IconTruckDelivery,
        href: '/delivery-orders',
        permissions: PagePermission.DELIVERY_ORDER_LIST,
      },
      {
        type: 'item',
        id: StringUtil.uniqueId(),
        title: 'Laporan Pengiriman',
        icon: IconMessageReport,
        href: '/delivery-orders/reports',
        permissions: PagePermission.DELIVERY_ORDER_REVIEW_LIST,
      },
    ],
  },
]

export type { MenuItem, MenuGroup }

export { Menuitems }
