import { SelectOption } from "components/Select"

const SELECT_OPTION_ALL: SelectOption = {
  label: '- Semua -',
  value: undefined,
}

export { SELECT_OPTION_ALL }