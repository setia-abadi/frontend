import { lazy } from 'react'
import { Navigate, Route, Routes } from 'react-router'
import FullLayout from 'layouts/full/FullLayout'
import DashboardPage from 'pages/global/dashboard/DashboardPage'
import SuspensedView from 'components/SuspensedView'
import DebtsRouter from './global/DebtsRouter'
import PurchaseOrdersRouter from './global/PurchaseOrdersRouter'
import ProductReturnsRouter from './global/ProductReturnsRouter'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import { PagePermission } from 'constants/pagePermission'

const BalancesRouter = lazy(() => import('./global/BalancesRouter'))
const SupplierTypesRouter = lazy(() => import('./global/SupplierTypesRouter'))
const UnitsRouter = lazy(() => import('./global/UnitsRouter'))
const UsersRouter = lazy(() => import('./global/UsersRouter'))
const SuppliersRouter = lazy(() => import('./global/SuppliersRouter'))
const CustomersRouter = lazy(() => import('./global/CustomersRouter'))
const CustomerTypeRouter = lazy(() => import('./global/CustomerTypesRouter'))
const ProductsRouter = lazy(() => import('./global/ProductsRouter'))
const CashierSessionsRouter = lazy(() => import('./global/CashierSessionsRouter'))
const CartRouter = lazy(() => import('./global/CartRouter'))
const CustomerDebtsRouter = lazy(() => import('./global/CustomerDebtsRouter'))
const DeliveryOrdersRouter = lazy(() => import('./global/DeliveryOrdersRouter'))
const ProductReceivesRouter = lazy(() => import('./global/ProductReceivesRouter'))
const ProductDiscountsRouter = lazy(() => import('./global/ProductDiscountsRouter'))
const ShopOrdersRouter = lazy(() => import('./global/ShopOrdersRouter'))
const ReturnOrdersRouter = lazy(() => import('./global/ReturnOrdersRouter'))
const WhatsappSessionRouter = lazy(() => import('./global/WhatsappSessionRouter'))

const GlobalRouter = () => {
  return (
    <Routes>
      <Route element={<FullLayout />}>
        <Route
          path="/dashboard"
          element={
            <RoutePermissionControl fallback={<></>} match={PagePermission.DASHBOARD}>
              <DashboardPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/users/*"
          element={
            <SuspensedView>
              <UsersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/balances/*"
          element={
            <SuspensedView>
              <BalancesRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/suppliers/*"
          element={
            <SuspensedView>
              <SuppliersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/supplier-types/*"
          element={
            <SuspensedView>
              <SupplierTypesRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/units/*"
          element={
            <SuspensedView>
              <UnitsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/customers/*"
          element={
            <SuspensedView>
              <CustomersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/customer-types/*"
          element={
            <SuspensedView>
              <CustomerTypeRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/products/*"
          element={
            <SuspensedView>
              <ProductsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/cashier-sessions/*"
          element={
            <SuspensedView>
              <CashierSessionsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/cart/*"
          element={
            <SuspensedView>
              <CartRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/receivables/*"
          element={
            <SuspensedView>
              <CustomerDebtsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/delivery-orders/*"
          element={
            <SuspensedView>
              <DeliveryOrdersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/product-receives/*"
          element={
            <SuspensedView>
              <ProductReceivesRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/product-discounts/*"
          element={
            <SuspensedView>
              <ProductDiscountsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/purchase-orders/*"
          element={
            <SuspensedView>
              <PurchaseOrdersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/product-returns/*"
          element={
            <SuspensedView>
              <ProductReturnsRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/shop-orders/*"
          element={
            <SuspensedView>
              <ShopOrdersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/return-orders/*"
          element={
            <SuspensedView>
              <ReturnOrdersRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/whatsapp-session/*"
          element={
            <SuspensedView>
              <WhatsappSessionRouter />
            </SuspensedView>
          }
        />

        <Route
          path="/debts/*"
          element={
            <SuspensedView>
              <DebtsRouter />
            </SuspensedView>
          }
        />

        <Route path="*" element={<Navigate to="/dashboard" />} />
      </Route>
    </Routes>
  )
}

export default GlobalRouter
