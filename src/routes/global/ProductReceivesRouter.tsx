import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ProductReceiveDetailPage from 'pages/global/product-receive/ProductReceiveDetailPage'
import ProductReceiveListPage from 'pages/global/product-receive/ProductReceiveListPage'
import ProductReceiveUpdatePage from 'pages/global/product-receive/ProductReceiveUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ProductReceivesRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RECEIVE_LIST}>
              <ProductReceiveListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productReceiveId"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RECEIVE_DETAIL}>
              <ProductReceiveDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productReceiveId/update"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RECEIVE_UPDATE}>
              <ProductReceiveUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/product-receives" />} />
      </Route>
    </Routes>
  )
}

export default ProductReceivesRouter
