import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import DebtDetailPage from 'pages/global/debt/DebtDetailPage'
import DebtListPage from 'pages/global/debt/DebtListPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const DebtsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.DEBT_LIST}>
              <DebtListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:debtId"
          element={
            <RoutePermissionControl match={PagePermission.DEBT_DETAIL}>
              <DebtDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/debts" />} />
      </Route>
    </Routes>
  )
}

export default DebtsRouter
