import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import WhatsappSessionPage from 'pages/global/whatsapp-session/WhatsappSessionPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const WhatsappSessionRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.WHATSAPP_SESSION}>
              <WhatsappSessionPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/whatsapp-session" />} />
      </Route>
    </Routes>
  )
}

export default WhatsappSessionRouter
