import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import SupplierCreatePage from 'pages/global/supplier/SupplierCreatePage'
import SupplierDetailPage from 'pages/global/supplier/SupplierDetailPage'
import SupplierListPage from 'pages/global/supplier/SupplierListPage'
import SupplierUpdatePage from 'pages/global/supplier/SupplierUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const SuppliersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_LIST}>
              <SupplierListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_CREATE}>
              <SupplierCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:supplierId"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_DETAIL}>
              <SupplierDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:supplierId/update"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_UPDATE}>
              <SupplierUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/suppliers" />} />
      </Route>
    </Routes>
  )
}

export default SuppliersRouter
