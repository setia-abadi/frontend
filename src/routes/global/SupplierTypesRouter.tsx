import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import SupplierTypeCreatePage from 'pages/global/supplier-type/SupplierTypeCreatePage'
import SupplierTypeListPage from 'pages/global/supplier-type/SupplierTypeListPage'
import SupplierTypeUpdatePage from 'pages/global/supplier-type/SupplierTypeUpdatePage'
import SupplierTypeDetailPage from 'pages/global/supplier-type/SuppllierTypeDetailPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const SupplierTypesRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_TYPE_LIST}>
              <SupplierTypeListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_TYPE_CREATE}>
              <SupplierTypeCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:supplierTypeId"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_TYPE_DETAIL}>
              <SupplierTypeDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:supplierTypeId/update"
          element={
            <RoutePermissionControl match={PagePermission.SUPPLIER_TYPE_UPDATE}>
              <SupplierTypeUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/supplier-types" />} />
      </Route>
    </Routes>
  )
}

export default SupplierTypesRouter
