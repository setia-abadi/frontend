import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import CustomerDebtDetailPage from 'pages/global/customer-debt/CustomerDebtDetailPage'
import CustomerDebtListPage from 'pages/global/customer-debt/CustomerDebtListPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const CustomerDebtsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_DEBT_LIST}>
              <CustomerDebtListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:customerDebtId"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_DEBT_DETAIL}>
              <CustomerDebtDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/receivables" />} />
      </Route>
    </Routes>
  )
}

export default CustomerDebtsRouter
