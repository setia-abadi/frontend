import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import CustomerCreatePage from 'pages/global/customer/CustomerCreatePage'
import CustomerListPage from 'pages/global/customer/CustomerListPage'
import CustomerDetailPage from 'pages/global/customer/CustomerDetailPage'
import CustomerUpdatePage from 'pages/global/customer/CustomerUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const CustomersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_LIST}>
              <CustomerListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_CREATE}>
              <CustomerCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:customerId"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_DETAIL}>
              <CustomerDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:customerId/update"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_UPDATE}>
              <CustomerUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/customers" />} />
      </Route>
    </Routes>
  )
}

export default CustomersRouter
