import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ReturnOrderCreatePage from 'pages/global/return-order/ReturnOrderCreatePage'
import ReturnOrderDetailPage from 'pages/global/return-order/ReturnOrderDetailPage'
import ReturnOrderListPage from 'pages/global/return-order/ReturnOrderListPage'
import ReturnOrderUpdatePage from 'pages/global/return-order/ReturnOrderUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ReturnOrdersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.UNIT_LIST}>
              <ReturnOrderListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_CREATE}>
              <ReturnOrderCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:returnOrderId"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_DETAIL}>
              <ReturnOrderDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:unitId/update"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_UPDATE}>
              <ReturnOrderUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/units" />} />
      </Route>
    </Routes>
  )
}

export default ReturnOrdersRouter
