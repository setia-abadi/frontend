import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import CashierSessionReportPage from 'pages/global/cashier-session/CasheirSessionReportPage'
import CashierSessionDetailPage from 'pages/global/cashier-session/CashierSessionDetailPage'
import CashierSessionPage from 'pages/global/cashier-session/CashierSessionPage'
import CashierSessionTransactionDetailPage from 'pages/global/cashier-session/CashierSessionTransactionDetailPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const CashierSessionsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.CASHIER_SESSION_SESSION}>
              <CashierSessionPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="report"
          element={
            <RoutePermissionControl matchAny={PagePermission.CASHIER_SESSION_LIST}>
              <CashierSessionReportPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="report/:cashierSessionId"
          element={
            <RoutePermissionControl match={PagePermission.CASHIER_SESSION_DETAIL}>
              <CashierSessionDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="report/:cashierSessionId/transactions/:transactionId"
          element={
            <RoutePermissionControl match={PagePermission.CASHIER_SESSION_TRANSACTION_DETAIL}>
              <CashierSessionTransactionDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/cashier-sessions" />} />
      </Route>
    </Routes>
  )
}

export default CashierSessionsRouter
