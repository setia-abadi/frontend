import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import DeliveryOrderCreatePage from 'pages/global/delivery-order/DeliveryOrderCreatePage'
import DeliveryOrderDetailPage from 'pages/global/delivery-order/DeliveryOrderDetailPage'
import DeliveryOrderListPage from 'pages/global/delivery-order/DeliveryOrderListPage'
import DeliveryOrderReportListPage from 'pages/global/delivery-order/DeliveryOrderReportListPage'
import DeliveryOrderUpdatePage from 'pages/global/delivery-order/DeliveryOrderUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const DeliveryOrdersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.DELIVERY_ORDER_LIST}>
              <DeliveryOrderListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.DELIVERY_ORDER_CREATE}>
              <DeliveryOrderCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/reports"
          element={
            <RoutePermissionControl match={PagePermission.DELIVERY_ORDER_REVIEW_LIST}>
              <DeliveryOrderReportListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:deliveryOrderId"
          element={
            <RoutePermissionControl match={PagePermission.DELIVERY_ORDER_DETAIL}>
              <DeliveryOrderDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:deliveryOrderId/update"
          element={
            <RoutePermissionControl match={PagePermission.DELIVERY_ORDER_UPDATE}>
              <DeliveryOrderUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/delivery-orders" />} />
      </Route>
    </Routes>
  )
}

export default DeliveryOrdersRouter
