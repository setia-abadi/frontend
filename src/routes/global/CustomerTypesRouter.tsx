import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import CustomerTypeCreatePage from 'pages/global/customer-type/CustomerTypeCreatePage'
import CustomerTypeDetailPage from 'pages/global/customer-type/CustomerTypeDetailPage'
import CustomerTypeListPage from 'pages/global/customer-type/CustomerTypeListPage'
import CustomerTypeUpdatePage from 'pages/global/customer-type/CustomerTypeUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const CustomerTypesRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_TYPE_LIST}>
              <CustomerTypeListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_TYPE_CREATE}>
              <CustomerTypeCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:customerTypeId"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_TYPE_DETAIL}>
              <CustomerTypeDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:customerTypeId/update"
          element={
            <RoutePermissionControl match={PagePermission.CUSTOMER_TYPE_UPDATE}>
              <CustomerTypeUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/customer-types" />} />
      </Route>
    </Routes>
  )
}

export default CustomerTypesRouter
