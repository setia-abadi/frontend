import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ShopOrderDetailPage from 'pages/global/shop-order/ShopOrderDetailPage'
import ShopOrderListPage from 'pages/global/shop-order/ShopOrderListPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ShopOrdersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.SHOP_ORDER_LIST}>
              <ShopOrderListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:shopOrderId"
          element={
            <RoutePermissionControl match={PagePermission.SHOP_ORDER_DETAIL}>
              <ShopOrderDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/shop-orders" />} />
      </Route>
    </Routes>
  )
}

export default ShopOrdersRouter
