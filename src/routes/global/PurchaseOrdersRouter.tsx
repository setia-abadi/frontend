import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import PurchaseOrderCreatePage from 'pages/global/purchase-order/PurchaseOrderCreatePage'
import PurchaseOrderDetailPage from 'pages/global/purchase-order/PurchaseOrderDetailPage'
import PurchaseOrderListPage from 'pages/global/purchase-order/PurchaseOrderListPage'
import PurchaseOrderUpdatePage from 'pages/global/purchase-order/PurchaseOrderUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const PurchaseOrdersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.PURCHASE_ORDER_LIST}>
              <PurchaseOrderListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.PURCHASE_ORDER_CREATE}>
              <PurchaseOrderCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:purchaseOrderId"
          element={
            <RoutePermissionControl match={PagePermission.PURCHASE_ORDER_DETAIL}>
              <PurchaseOrderDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:purchaseOrderId/update"
          element={
            <RoutePermissionControl match={PagePermission.PURCHASE_ORDER_UPDATE}>
              <PurchaseOrderUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/purchase-orders" />} />
      </Route>
    </Routes>
  )
}

export default PurchaseOrdersRouter
