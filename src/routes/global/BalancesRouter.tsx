import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import BalanceCreatePage from 'pages/global/balance/BalanceCreatePage'
import BalanceDetailPage from 'pages/global/balance/BalanceDetailPage'
import BalanceListPage from 'pages/global/balance/BalanceListPage'
import BalanceUpdatePage from 'pages/global/balance/BalanceUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const BalancesRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.BALANCE_LIST}>
              <BalanceListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.BALANCE_CREATE}>
              <BalanceCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:balanceId"
          element={
            <RoutePermissionControl match={PagePermission.BALANCE_DETAIL}>
              <BalanceDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:balanceId/update"
          element={
            <RoutePermissionControl match={PagePermission.BALANCE_UPDATE}>
              <BalanceUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/balances" />} />
      </Route>
    </Routes>
  )
}

export default BalancesRouter
