import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import CartPage from 'pages/global/cart/CartPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const CartRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.CART}>
              <CartPage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/units" />} />
      </Route>
    </Routes>
  )
}

export default CartRouter
