import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ProductReturnCreatePage from 'pages/global/product-return/ProductReturnCreatePage'
import ProductReturnDetailPage from 'pages/global/product-return/ProductReturnDetailPage'
import ProductReturnListPage from 'pages/global/product-return/ProductReturnListPage'
import ProductReturnUpdatePage from 'pages/global/product-return/ProductReturnUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ProductReturnsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RETURN_LIST}>
              <ProductReturnListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RETURN_CREATE}>
              <ProductReturnCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productReturnId"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RETURN_DETAIL}>
              <ProductReturnDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productReturnId/update"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_RETURN_UPDATE}>
              <ProductReturnUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/product-returns" />} />
      </Route>
    </Routes>
  )
}

export default ProductReturnsRouter
