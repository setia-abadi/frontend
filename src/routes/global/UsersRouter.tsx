import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import UserCreatePage from 'pages/global/user/UserCreatePage'
import UserDetailPage from 'pages/global/user/UserDetailPage'
import UserListPage from 'pages/global/user/UserListPage'
import UserUpdatePage from 'pages/global/user/UserUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const UsersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.USER_LIST}>
              <UserListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.USER_CREATE}>
              <UserCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:userId"
          element={
            <RoutePermissionControl match={PagePermission.USER_DETAIL}>
              <UserDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:userId/update"
          element={
            <RoutePermissionControl match={PagePermission.USER_UPDATE}>
              <UserUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/users" />} />
      </Route>
    </Routes>
  )
}

export default UsersRouter
