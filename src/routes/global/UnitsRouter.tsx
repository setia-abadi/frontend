import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import UnitCreatePage from 'pages/global/unit/UnitCreatePage'
import UnitDetailPage from 'pages/global/unit/UnitDetailPage'
import UnitListPage from 'pages/global/unit/UnitListPage'
import UnitUpdatePage from 'pages/global/unit/UnitUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const UnitsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.UNIT_LIST}>
              <UnitListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_CREATE}>
              <UnitCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:unitId"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_DETAIL}>
              <UnitDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:unitId/update"
          element={
            <RoutePermissionControl match={PagePermission.UNIT_UPDATE}>
              <UnitUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/units" />} />
      </Route>
    </Routes>
  )
}

export default UnitsRouter
