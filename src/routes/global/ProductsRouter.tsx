import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ProductCreatePage from 'pages/global/product/ProductCreatePage'
import ProductDetailPage from 'pages/global/product/ProductDetailPage'
import ProductListPage from 'pages/global/product/ProductListPage'
import ProductTiktokCreatePage from 'pages/global/product/ProductTiktokCreatePage'
import ProductTiktokUpdatePage from 'pages/global/product/ProductTiktokUpdatePage'
import ProductUpdatePage from 'pages/global/product/ProductUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ProductsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_LIST}>
              <ProductListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_CREATE}>
              <ProductCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productId"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_DETAIL}>
              <ProductDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productId/update"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_UPDATE}>
              <ProductUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productId/tiktok/create"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_TIKTOK_CREATE}>
              <ProductTiktokCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productId/tiktok/update"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_TIKTOK_UPDATE}>
              <ProductTiktokUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/products" />} />
      </Route>
    </Routes>
  )
}

export default ProductsRouter
