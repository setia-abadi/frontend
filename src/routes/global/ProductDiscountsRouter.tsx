import { PagePermission } from 'constants/pagePermission'
import RoutePermissionControl from 'modules/permission/components/RoutePermissionControl'
import ProductDiscountCreatePage from 'pages/global/product-discount/ProductDiscountCreatePage'
import ProductDiscountDetailPage from 'pages/global/product-discount/ProductDiscountDetailPage'
import ProductDiscountListPage from 'pages/global/product-discount/ProductDiscountListPage'
import ProductDiscountUpdatePage from 'pages/global/product-discount/ProductDiscountUpdatePage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const ProductDiscountsRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          index
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_DISCOUNT_LIST}>
              <ProductDiscountListPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/create"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_DISCOUNT_CREATE}>
              <ProductDiscountCreatePage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productDiscountId"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_DISCOUNT_DETAIL}>
              <ProductDiscountDetailPage />
            </RoutePermissionControl>
          }
        />

        <Route
          path="/:productDiscountId/update"
          element={
            <RoutePermissionControl match={PagePermission.PRODUCT_DISCOUNT_UPDATE}>
              <ProductDiscountUpdatePage />
            </RoutePermissionControl>
          }
        />

        <Route path="*" element={<Navigate to="/product-discounts" />} />
      </Route>
    </Routes>
  )
}

export default ProductDiscountsRouter
