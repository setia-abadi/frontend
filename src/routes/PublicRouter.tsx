import { Navigate, Route, Routes } from 'react-router'
import BlankLayout from 'layouts/blank/BlankLayout'
import DeliveryOrdersRouter from './public/DeliveryOrderRouter'

const PublicRouter = () => {
  return (
    <Routes>
      <Route element={<BlankLayout />}>
        <Route path="/delivery-orders/*" element={<DeliveryOrdersRouter />} />

        <Route path="*" element={<Navigate to="/error/404" />} />
      </Route>
    </Routes>
  )
}

export default PublicRouter
