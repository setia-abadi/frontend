import DeliveryOrderReportPage from 'pages/public/DeliveryOrderReportPage'
import DeliveryOrderTrackPage from 'pages/public/DeliveryOrderTrackPage'
import { Navigate, Outlet, Route, Routes } from 'react-router'

const DeliveryOrdersRouter = () => {
  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route path="/track/:deliveryOrderId" element={<DeliveryOrderTrackPage />} />

        <Route path="/report/:deliveryOrderId" element={<DeliveryOrderReportPage />} />

        <Route path="*" element={<Navigate to="/error/404" />} />
      </Route>
    </Routes>
  )
}

export default DeliveryOrdersRouter
