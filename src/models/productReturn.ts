import { ProductReturnStatusEnum } from 'enums/productReturnStatusEnum'
import { BaseModel, ID } from './base'
import { ProductReturnItemModel } from './productReturnItem'
import { SupplierModel } from './supplier'
import { ProductReturnImageModel } from './productReturnImage'

type ProductReturnModel = BaseModel<{
  date: string
  images: ProductReturnImageModel[] | null
  invoice_number: string
  items: ProductReturnItemModel[] | null
  status: ProductReturnStatusEnum
  supplier: SupplierModel | null
  supplier_id: ID
}>

export type { ProductReturnModel }
