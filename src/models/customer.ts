import { BaseModel, ID } from "./base"
import { CustomerTypeModel } from "./customerType"

type CustomerModel = BaseModel<{
  address: string
  email: string
  is_active: boolean
  name: string
  phone: string
  latitude: number
  longitude: number
  customer_type_id: ID
  customer_type: CustomerTypeModel | null
}>

export type { CustomerModel }