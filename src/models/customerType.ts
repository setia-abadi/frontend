import { BaseModel } from "./base";
import { CustomerTypeDiscountModel } from "./customerTypeDiscount";

type CustomerTypeModel = BaseModel<{
  name: string
  description: string | null
  discounts: CustomerTypeDiscountModel[] | null
}>

export type { CustomerTypeModel }