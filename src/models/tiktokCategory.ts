type TiktokCategoryModel = {
  children_categories: TiktokCategoryModel[] | null
  id: string
  is_leaf: boolean
  name: string
}

export type { TiktokCategoryModel }