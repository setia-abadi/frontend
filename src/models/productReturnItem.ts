import { BaseModel, ID } from './base'
import { ProductUnitModel } from './productUnit'
import { UserModel } from './user'

type ProductReturnItemModel = BaseModel<{
  created_by: UserModel
  price_per_unit: number
  product_return_id: string
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  qty: number
}>

export type { ProductReturnItemModel }
