import { BaseModel, ID } from './base'
import { FileModel } from './file'

type ProductReturnImageModel = BaseModel<{
  description: string | null
  file: FileModel | null
  file_id: ID
  product_return_id: ID
}>

export type { ProductReturnImageModel }
