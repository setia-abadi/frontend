import { SortDirectionEnum } from 'enums/sortDirectionEnum'

type Response<T> = {
  data: T
}

type ResponseSuccess = {
  message: string
}

type ResponseError = {
  message: string
  errors: ErrorDomain[]
}

type SortDomain = {
  direction: SortDirectionEnum
  field: string
}

type RequestWithFilter<T> = T & {
  limit: number
  page: number
  phrase: string | null
  sorts?: SortDomain[]
}

type WithFilter<Model> = {
  limit: number
  page: number
  nodes: Model[]
  total: number
}

type ErrorDomain = {
  code: number
  domain: string
  message: string
}

type ResponseUploadFile = Response<{
  path: string
}>

export type {
  Response,
  ResponseError,
  ResponseSuccess,
  RequestWithFilter,
  ErrorDomain,
  SortDomain,
  WithFilter,
  ResponseUploadFile,
}
