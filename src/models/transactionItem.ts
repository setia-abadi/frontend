import { BaseModel, ID } from "./base"
import { ProductUnitModel } from "./productUnit"

type TransactionItemModel = BaseModel<{
  discount_per_unit: number | null
  price_per_unit: number
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  qty: number
  transaction_id: ID
}>

export type { TransactionItemModel }