import { BaseModel } from "./base"

type BalanceModel = BaseModel<{
  account_name: string
  account_number: string
  bank_name: string
  name: string
}>

export type { BalanceModel }