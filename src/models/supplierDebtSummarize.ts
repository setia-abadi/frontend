import { ID } from './base'

type SupplierDebtSummarizeModel = {
  supplier_id: ID
  supplier_name: string
  total_debt: number
}

export type { SupplierDebtSummarizeModel }
