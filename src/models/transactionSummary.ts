import { TransactionModel } from "./transaction"

type TransactionSummaryModel = {
  date: string
  total_gross_sales: number
  total_net_sales: number
  transactions: TransactionModel[]
}

export type { TransactionSummaryModel }