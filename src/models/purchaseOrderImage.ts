import { BaseModel, ID } from './base'
import { FileModel } from './file'

type PurchaseOrderImageModel = BaseModel<{
  description: string | null
  file: FileModel | null
  file_id: ID
  purchase_order_id: ID
}>

export type { PurchaseOrderImageModel }
