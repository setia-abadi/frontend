import { BaseModel, ID } from './base'
import { ProductUnitModel } from './productUnit'

type PurchaseOrderItemModel = BaseModel<{
  price_per_unit: number
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  purchase_order_id: string
  qty: number
}>

export type { PurchaseOrderItemModel }
