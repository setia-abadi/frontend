type ID = string

type BaseModel<T> = T & {
  id: ID
  created_at: string
  updated_at: string
}

type FormShape<T> = {
  [key in keyof T]?: T[key] | null
}

type DisabledFields<T> = {
  [key in keyof T]-?: boolean
}

type HiddenFields<T> = {
  [key in keyof T]-?: boolean
}

export type { ID, BaseModel, FormShape, DisabledFields, HiddenFields }
