import { PurchaseOrderStatusEnum } from 'enums/purchaseOrderStatusEnum'
import { BaseModel, ID } from './base'
import { PurchaseOrderItemModel } from './purchaseOrderItem'
import { SupplierModel } from './supplier'
import { PurchaseOrderImageModel } from './purchaseOrderImage'

type PurchaseOrderModel = BaseModel<{
  date: string
  images: PurchaseOrderImageModel[] | null
  invoice_number: string
  items: PurchaseOrderItemModel[] | null
  status: PurchaseOrderStatusEnum
  supplier: SupplierModel | null
  supplier_id: ID
  total_estimated_price: number
}>

export type { PurchaseOrderModel }
