type TiktokBrandModel = {
  id: string
  name: string
}

export type { TiktokBrandModel }