import { BaseModel, ID } from "./base";
import { FileModel } from "./file";

type DeliveryOrderReturnImageModel = BaseModel<{
  delivery_order_return_id: ID
  file: FileModel | null
  file_id: ID
  id: ID
}>

export type { DeliveryOrderReturnImageModel }