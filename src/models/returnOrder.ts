import { ReturnOrderStatusEnum } from "enums/returnOrderStatusEnum";
import { BaseModel, ID } from "./base";
import { DeliveryOrderModel } from "./deliveryOrder";
import { FileModel } from "./file";

type ReturnOrderModel = BaseModel<{
  delivery_order_id: ID
  delivery_order: DeliveryOrderModel | null
  reason: string
  date: string | null
  status: ReturnOrderStatusEnum
  attachments: FileModel[] | null
}>

export type { ReturnOrderModel }