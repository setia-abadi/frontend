import { BaseModel } from './base'
import { FileModel } from './file'
import { ProductStockModel } from './productStock'
import { ProductUnitModel } from './productUnit'

type ProductModel = BaseModel<{
  description: string | null
  is_active: boolean
  image_file: FileModel
  name: string
  price: number
  product_units: ProductUnitModel[] | null
  stock: ProductStockModel

  is_loss: boolean | null
}>

export type { ProductModel }
