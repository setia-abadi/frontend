import { BaseModel, ID } from "./base";
import { ProductUnitModel } from "./productUnit";

type ShopOrderItemModel = BaseModel<{
  image_link: string
  original_price: number
  platform_product_id: ID
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  quantity: number
  sale_price: number
}>

export type { ShopOrderItemModel }