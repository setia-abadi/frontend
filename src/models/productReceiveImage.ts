import { BaseModel, ID } from "./base";
import { FileModel } from "./file";

type ProductReceiveImageModel = BaseModel<{
  description: string | null
  file: FileModel | null
  file_id: ID
  product_receive_id: ID
}>

export type { ProductReceiveImageModel }