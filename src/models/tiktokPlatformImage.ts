type TiktokPlatformImageModel = {
  height: number
  thumb_url: string
  uri: string
  url: string
  width: string
}

export type { TiktokPlatformImageModel }