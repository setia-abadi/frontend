import { BaseModel, ID } from "./base";
import { ProductReceiveReturnImageModel } from "./productReceiveReturnImage";
import { UserModel } from "./user";

type ProductReceiveReturnModel = BaseModel<{
  created_by: UserModel | null
  product_receive_id: ID
  description: string
  images: ProductReceiveReturnImageModel[] | null
}>

export type { ProductReceiveReturnModel }