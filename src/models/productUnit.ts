import { BaseModel, ID } from "./base";
import { ProductModel } from "./product";
import { UnitModel } from "./unit";

type ProductUnitModel = BaseModel<{
  to_unit: UnitModel | null
  to_unit_id: ID | null
  product: ProductModel
  product_id: ID
  scale: number
  scale_to_base: number
  unit: UnitModel | null
  unit_id: ID
}>

export type { ProductUnitModel }