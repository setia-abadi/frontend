import { BaseModel, ID } from "./base";
import { ProductUnitModel } from "./productUnit";
import { UserModel } from "./user";

type DeliveryOrderItemModel = BaseModel<{
  created_by: UserModel | null
  delivery_order_id: ID
  discount_per_unit: number
  price_per_unit: number
  price_total: number
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  qty: number
}>

export type { DeliveryOrderItemModel }