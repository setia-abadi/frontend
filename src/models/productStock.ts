import { BaseModel, ID } from './base'

type ProductStockModel = BaseModel<{
  product_id: ID
  qty: number
  base_cost_price: number
}>

export type { ProductStockModel }
