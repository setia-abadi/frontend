import { BaseModel, ID } from "./base";
import { DebtSourceEnum } from "enums/debtSourceEnum";
import { DebtStatusEnum } from "enums/debtStatusEnum";
import { DebtPaymentModel } from "./debtPayment";
import { SupplierModel } from "./supplier";

type DebtModel = BaseModel<{
  amount: number
  debt_source: DebtSourceEnum
  debt_source_id: ID
  supplier: SupplierModel | null
  due_date: string
  payments: DebtPaymentModel[] | null
  remaining_amount: number
  status: DebtStatusEnum
}>

export type { DebtModel }