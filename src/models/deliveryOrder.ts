import { DeliveryOrderStatusEnum } from "enums/deliveryOrderStatusEnum";
import { BaseModel, ID } from "./base";
import { CustomerModel } from "./customer";
import { UserModel } from "./user";
import { DeliveryOrderImageModel } from "./deliveryOrderImage";
import { DeliveryOrderItemModel } from "./deliveryOrderitem";
import { DeliveryOrderReturnModel } from "./deliveryOrderReturn";

type DeliveryOrderModel = BaseModel<{
  customer: CustomerModel | null
  customer_id: ID
  drivers: UserModel[] | null
  images: DeliveryOrderImageModel[] | null
  items: DeliveryOrderItemModel[] | null
  return: DeliveryOrderReturnModel | null
  date: string
  invoice_number: string
  status: DeliveryOrderStatusEnum
  supplier_id: ID
  total_price: number
  user_id: ID
}>

export type { DeliveryOrderModel }