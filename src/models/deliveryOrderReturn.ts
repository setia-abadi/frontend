import { BaseModel, ID } from "./base";
import { DeliveryOrderReturnImageModel } from "./deliveryOrderReturnImage";
import { UserModel } from "./user";

type DeliveryOrderReturnModel = BaseModel<{
  created_by: UserModel | null
  delivery_order_id: ID
  description: string
  images: DeliveryOrderReturnImageModel[] | null
}>

export type { DeliveryOrderReturnModel }