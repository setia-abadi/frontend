import { BaseModel, ID } from "./base"
import { CartItemModel } from "./cartItem"
import { CashierSessionModel } from "./cashierSession"

type CartModel = BaseModel<{
  cashier_session: CashierSessionModel | null
  cashier_session_id: ID
  is_active: boolean
  items: CartItemModel[] | null
  name: string | null
  grand_total: number
  subtotal: number
  total_discount: number
}>

export type { CartModel }