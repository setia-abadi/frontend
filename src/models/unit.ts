import { BaseModel } from "./base";

type UnitModel = BaseModel<{
  name: string
  description: string | null
}>

export type { UnitModel}