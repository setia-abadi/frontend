import { PermissionEnum } from 'enums/permissionEnum'
import { ID } from './base'
import { RoleModel } from './role'

type UserMeModel = {
  id: ID
  name: string
  permissions: PermissionEnum[]
  roles: RoleModel[]
}

export type { UserMeModel }
