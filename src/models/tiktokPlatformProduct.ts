import { TiktokProductDimensionUnitEnum } from "enums/tiktokProductDimensionUnitEnum";
import { BaseModel } from "./base";
import { TiktokBrandModel } from "./tiktokBrand";
import { TiktokCategoryModel } from "./tiktokCategory";
import { TiktokPlatfromAttributeModel } from "./tiktokPlatformAttribute";
import { TiktokPlatformImageModel } from "./tiktokPlatformImage";
import { TiktokProductPackageWeightEnum } from "enums/tiktokProductPackageWeightEnum";

type TiktokPlatformProductModel = BaseModel<{
  attributes: TiktokPlatfromAttributeModel[]
  brand: TiktokBrandModel
  category: TiktokCategoryModel
  description: string
  dimension_height: number
  dimension_length: number
  dimension_unit: TiktokProductDimensionUnitEnum
  dimension_width: number
  images: TiktokPlatformImageModel[]
  status: string
  title: string
  weight_unit: TiktokProductPackageWeightEnum
  weight_value: number
}>

export type { TiktokPlatformProductModel }