import { BaseModel, ID } from "./base"
import { ProductUnitModel } from "./productUnit"
import { UserModel } from "./user"

type ProductReceiveItemModel = BaseModel<{
  created_by: UserModel
  price_per_unit: number
  product_receive_id: string
  product_unit: ProductUnitModel | null
  product_unit_id: ID
  qty: number
  qty_eligible: number
}>

export type { ProductReceiveItemModel }
