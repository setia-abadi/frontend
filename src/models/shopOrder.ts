import { ShopOrderPlatformTypeEnum } from "enums/shopOrderPlatformTypeEnum";
import { BaseModel } from "./base";
import { ShopOrderItemModel } from "./shopOrderItem";
import { ShopOrderTrackingStatusEnum } from "enums/shopOrderTrackingStatusEnum";

type ShopOrderModel = BaseModel<{
  items: ShopOrderItemModel[] | null, 
  platform_identifier: string
  platform_type: ShopOrderPlatformTypeEnum
  recipient_full_address: string
  recipient_name: string
  recipient_phone_number: string
  shipping_fee: number
  service_fee: number
  subtotal: number
  tax: number
  total_amount: number
  total_original_product_price: number
  tracking_number: string
  tracking_status: ShopOrderTrackingStatusEnum
}>

export type { ShopOrderModel }
