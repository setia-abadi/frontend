import { CashierSessionStatusEnum } from "enums/cashierSessionStatusEnum";
import { BaseModel, ID } from "./base";
import { UserModel } from "./user";

type CashierSessionModel = BaseModel<{
  ended_at: string | null
  ending_cash: number | null
  started_at: string
  starting_cash: number
  status: CashierSessionStatusEnum
  user: UserModel | null
  user_id: ID
}>

export type { CashierSessionModel }