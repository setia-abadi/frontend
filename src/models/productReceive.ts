import { ProductReceiveStatusEnum } from 'enums/productReceiveStatusEnum'
import { BaseModel, ID } from './base'
import { ProductReceiveItemModel } from './productReceiveItem'
import { SupplierModel } from './supplier'
import { ProductReceiveImageModel } from './productReceiveImage'
import { ProductReceiveReturnModel } from './productReceiveReturn'

type ProductReceiveModel = BaseModel<{
  date: string
  images: ProductReceiveImageModel[] | null
  invoice_number: string
  items: ProductReceiveItemModel[] | null
  return: ProductReceiveReturnModel | null
  status: ProductReceiveStatusEnum
  supplier: SupplierModel | null
  supplier_id: ID
  total_price: number
}>

export type { ProductReceiveModel }
