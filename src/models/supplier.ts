import { BaseModel, ID } from "./base"
import { SupplierTypeModel } from "./supplierType"

type SupplierModel = BaseModel<{
  address: string
  code: string
  description: string | null
  email: string | null
  is_active: boolean
  name: string
  phone: string
  supplier_type: SupplierTypeModel | null
  supplier_type_id: ID
}>

export type { SupplierModel }