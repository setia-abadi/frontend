import { BaseModel } from "./base";

type SupplierTypeModel = BaseModel<{
  name: string
  description: string | null
}>

export type { SupplierTypeModel }