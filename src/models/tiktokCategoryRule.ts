type TiktokCategoryRuleModel = {
  package_dimension_is_required: boolean
  size_chart_is_required: boolean
  size_chart_is_supported: boolean
}

export type { TiktokCategoryRuleModel }