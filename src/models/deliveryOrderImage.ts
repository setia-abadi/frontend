import { BaseModel, ID } from './base'
import { FileModel } from './file'

type DeliveryOrderImageModel = BaseModel<{
  delivery_order_id: ID
  description: string
  file: FileModel | null
  file_id: ID
}>

export type { DeliveryOrderImageModel }
