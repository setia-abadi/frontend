import { BaseModel, ID } from "./base";
import { FileModel } from "./file";

type ProductReceiveReturnImageModel = BaseModel<{
  product_receive_return_id: ID
  file: FileModel | null
  file_id: ID
  id: ID
}>

export type { ProductReceiveReturnImageModel }