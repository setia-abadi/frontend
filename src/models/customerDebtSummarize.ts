import { ID } from './base'

type CustomerDebtSummarizeModel = {
  customer_id: ID
  customer_name: string
  total_debt: number
}

export type { CustomerDebtSummarizeModel }
