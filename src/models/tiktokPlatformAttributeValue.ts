type TiktokPlatfromAttributeValueModel = {
  id: string
  name: string
}

export type { TiktokPlatfromAttributeValueModel }