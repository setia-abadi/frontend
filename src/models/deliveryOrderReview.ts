import { DeliveryOrderReviewTypeEnum } from "enums/deliveryOrderReviewTypeEnum";
import { BaseModel, ID } from "./base";
import { DeliveryOrderModel } from "./deliveryOrder";

type DeliveryOrderReviewModel = BaseModel<{
  delivery_order: DeliveryOrderModel | null
  delivery_order_id: ID
  description: string
  type: DeliveryOrderReviewTypeEnum
}>

export type { DeliveryOrderReviewModel }