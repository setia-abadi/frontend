type AccessTokenModel = {
  access_token: string
  access_token_expired_at: string
  token_type: string
}

export type { AccessTokenModel }
