import { TiktokPlatfromAttributeValueModel } from "./tiktokPlatformAttributeValue"

type TiktokPlatfromAttributeModel = {
  id: string
  name: string
  is_customizable: boolean
  is_multiple_selection: boolean
  values: TiktokPlatfromAttributeValueModel[]
}

export type { TiktokPlatfromAttributeModel }