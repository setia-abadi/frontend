import { TransactionStatusEnum } from "enums/transactionStatusEnum";
import { BaseModel, ID } from "./base";
import { TransactionItemModel } from "./transactionItem";
import { TranscationPaymentModel } from "./transactionPayment";

type TransactionModel = BaseModel<{
  cashier_session_id: ID
  items: TransactionItemModel[] | null
  payment_at: string
  payments: TranscationPaymentModel[] | null
  status: TransactionStatusEnum
  total: number
}>

export type { TransactionModel }