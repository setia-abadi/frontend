import { BaseModel, ID } from "./base";
import { ProductModel } from "./product";

type CustomerTypeDiscountModel = BaseModel<{
  customer_type_id: ID
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
  product: ProductModel | null
  product_id: ID
}>

export type { CustomerTypeDiscountModel }