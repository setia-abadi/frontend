import { TransactionPaymentTypeEnum } from "enums/transactionPaymentTypeEnum";
import { BaseModel, ID } from "./base";

type TranscationPaymentModel = BaseModel<{
  payment_type: TransactionPaymentTypeEnum
  reference_number: string | number
  total: number
  transaction_id: ID
}>

export type { TranscationPaymentModel }