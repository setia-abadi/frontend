import { BaseModel, ID } from "./base";
import { FileModel } from "./file";

type DebtPaymentModel = BaseModel<{
  amount: number
  debt_id: ID
  description: string | null
  image_file: FileModel
  image_file_id: ID
  paid_at: string
  user_id: ID
}>

export type { DebtPaymentModel }