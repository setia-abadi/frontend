import { BaseModel } from "./base"
import { RoleModel } from "./role"

type UserModel = BaseModel<{
  is_active: boolean
  name: string
  password: string
  username: string
  roles: RoleModel[] | null
}>

export type { UserModel }
