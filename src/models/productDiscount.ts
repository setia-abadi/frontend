import { BaseModel, ID } from './base'
import { ProductModel } from './product'

type ProductDiscountModel = BaseModel<{
  discount_amount: number | null
  discount_percentage: number | null
  is_active: boolean
  minimum_qty: number
  product: ProductModel | null
  product_id: ID
}>

export type { ProductDiscountModel }
