import { RoleEnum } from 'enums/roleEnum'
import { BaseModel } from './base'

type RoleModel = BaseModel<{
  name: RoleEnum
  description: string
}>

export type { RoleModel }
