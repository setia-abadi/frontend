import { CustomerDebtSourceEnum } from "enums/customerDebtSourceEnum";
import { BaseModel, ID } from "./base";
import { CustomerDebtStatusEnum } from "enums/customerDebtStatusEnum";
import { CustomerModel } from "./customer";
import { CustomerPaymentModel } from "./customerPayment";

type CustomerDebtModel = BaseModel<{
  amount: number
  customer: CustomerModel | null
  customer_id: ID
  debt_source: CustomerDebtSourceEnum
  payments: CustomerPaymentModel[] | null
  due_date: string
  remaining_amount: number
  status: CustomerDebtStatusEnum
}>

export type { CustomerDebtModel }