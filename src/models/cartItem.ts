import { BaseModel, ID } from "./base";
import { ProductUnitModel } from "./productUnit";

type CartItemModel = BaseModel<{
  cart_id: ID
  product_unit: ProductUnitModel
  product_unit_id: ID
  qty: number
  subtotal: number
  total: number
  total_discount: number | null
}>

export type { CartItemModel }