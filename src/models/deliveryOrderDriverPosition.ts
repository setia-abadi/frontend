import { BaseModel, ID } from "./base"

type DeliveryOrderPositionModel = BaseModel<{
  bearing: number
  delivery_order_id: ID
  driver_user_id: ID
  latitude: number
  longitude: number
}>

export type { DeliveryOrderPositionModel }