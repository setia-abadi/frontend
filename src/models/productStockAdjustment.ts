import { BaseModel, ID } from "./base"
import { UserModel } from "./user"

type ProductStockAdjustmentModel = BaseModel<{
  previous_qty: number
  product_stock_id: ID
  updated_qty: number
  user_id: ID
  user: UserModel | null
}>

export type { ProductStockAdjustmentModel }