## How to setup and run

#### add env
```
copy value from .env.example and fill the required value
```

#### get library and run
```
$ npm install
$ npm start
```